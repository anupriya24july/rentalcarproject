<?php

use SilverStripe\CMS\Controllers\ContentController;
use SilverStripe\Control\Director;
use SilverStripe\Control\Controller;
use SilverStripe\Core\Environment;
use AceRentals\Pages\InformationPage;
use AceRentals\Pages\DrivingSafelyPage;
use AceRentals\Pages\MiscellaneousPage;
use TractorCow\Fluent\Model\Locale;

class PageController extends ContentController
{
    /**
     * An array of actions that can be accessed via a request. Each array element should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * @var array
     */
    private static $allowed_actions = array(
      'WebpackDevServer',
      'LoadAceBooking',
      'LoadCheckin',
      'getLocalizedLabels',
      'getLocale',
      'getAPI',
      'getCheckinAPI',
    );

    /**
     * @return bool
     */
    public function IsTablet()
    {
        $mobileDetect = new Detection\MobileDetect();
        if ($mobileDetect->isTablet()) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function IsMobile()
    {
        $mobileDetect = new Detection\MobileDetect();
        if ($mobileDetect->isMobile()) {
            return true;
        }
        return false;
    }

    public function getLocale(){
         return (Locale::getCurrentLocale()->URLSegment == Locale::getDefault(true)->getURLSegment() ? "" : Locale::getCurrentLocale()->URLSegment);
    }

    public function getAPI(){
      return '/acebook/client/dist/bundle_' . Environment::getEnv('API') . '.js';
    }

    public function getCheckinAPI(){
      return '/acebook/client/dist/checkin_' . Environment::getEnv('API') . '.js';
    }

    /**
     * Ping the webpack server
     * @return bool
     */
    public static function WebpackDevServer()
    {
        if (Director::isDev()) {
            $socket = @fsockopen('localhost', 3000, $errno, $errstr, 1);
            return !$socket ? false : true;
        }
    }

    public static function LoadAceBooking()
    {
        return true;
    }

    public static function LoadCheckin()
    {
        $urlSegment=Controller::curr()->getRequest()->getUrl();
        $segments=explode('/', $urlSegment);
        if(in_array("checkin", $segments)) {
            return true;
        }else{
            return false;
        }
    }

    public static function IsBooking()
    {
        $urlSegment=Controller::curr()->getRequest()->getUrl();
        $segments=explode('/', $urlSegment);
        if(in_array("bookings", $segments) && count($segments)==1) {
            return 1;
        }else if(in_array("bookings", $segments) && count($segments)>1) {
            return 2;
        }else{
            return false;
        }

    }

    /**
     * @return \SilverStripe\ORM\DataList
     */
    public function getInformationPages() {
        $list = \SilverStripe\ORM\ArrayList::create();

        $container = \AceRentals\Pages\InformationContainerPage::get()->first();
        $infoPages = InformationPage::get()->filter(['QuickLink' => false, 'VisibleInFooter' => true]);
        $redirectors = \SilverStripe\CMS\Model\RedirectorPage::get()->filter(['ParentID' => $container->ID]);


        $list->merge($infoPages);
        $list->merge($redirectors);
        $list = $list->sort('Sort', 'ASC');

        return $list;
    }

    /**
     * @return \SilverStripe\ORM\DataList
     */
    public function getQuickLinks() {
        return InformationPage::get()->filter(['QuickLink' => true, 'VisibleInFooter' => true]);
    }

    /**
     * @return \SilverStripe\ORM\DataObject
     */
    public function getDrivingSafelyPage() {
        return DrivingSafelyPage::get()->first();
    }

    /**
     * @return \SilverStripe\ORM\DataList
     */
    public function getLabels() {
        $currentLocale = Locale::getCurrentLocale()->Locale;
        return MiscellaneousPage::get()->first();
    }

    public function getLocalizedLabels() {
        $currentLocale = Locale::getCurrentLocale()->Locale;
        $locale = MiscellaneousPage::get()->first()->toMap();
        return json_encode($locale);
    }

    public function getLocaleSegment() {
        $urlSeg="";
        if(!Locale::getCurrentLocale()->IsGlobalDefault){
            $urlSeg = Locale::getCurrentLocale()->URLSegment.'/';
        }
        return $urlSeg;
    }


}
