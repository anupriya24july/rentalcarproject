<?php

/**
 * Class FilePublicExtension
 */
class FilePublicExtension extends \SilverStripe\ORM\DataExtension
{

    /**
     * @return bool
     */
    public function canPublish() {
        return true;
    }

    /**
     * @return bool
     */
    public function canUnpublish() {
        return true;
    }

    /**
     * @param $member
     * @return bool
     */
    public function canEdit($member) {
        return true;
    }

    /**
     * @param $member
     * @return bool
     */
    public function canCreate($member) {
        return true;
    }

    /**
     * @param $member
     * @return bool
     */
    public function canDelete($member) {
        return true;
    }

}