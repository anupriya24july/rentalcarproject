<?php
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;
/**
 * Class SiteConfigExtension
 */
class SiteConfigExtension extends \SilverStripe\ORM\DataExtension
{

    /**
     * @var array
     */
    private static $db = [
        'ShowTrustPilotTestimonials' => 'Boolean',
        'CountryVersion' => 'Enum("New Zealand,Australia", "New Zealand")',
        'NewZealandURL' => 'Varchar(255)',
        'AustraliaURL' => 'Varchar(255)',
        'BookingMetaTitle' => 'Varchar(255)',
        'BookingMetaDescription' => 'Varchar(361)',
        'BookingCustomMeta' => 'Varchar(255)',
        'ShowTranslation' => 'Boolean',
    ];
    
    public function updateCMSFields(FieldList $fields) {
        $fields->addFieldsToTab(
            'Root.Main',
            [
                CheckboxField::create('ShowTrustPilotTestimonials'),
                DropdownField::create('CountryVersion', null, $this->owner->dbObject('CountryVersion')->enumValues()),
                TextField::create('NewZealandURL', null),
                TextField::create('AustraliaURL', null),
                TextField::create('BookingMetaTitle'),
                TextField::create('BookingMetaDescription'),
                TextField::create('BookingCustomMeta'),
                CheckboxField::create('ShowTranslation'),
            ]
        );

        $fields->addFieldsToTab(
            'Root.BookingFunnel.SEO',
            [
                TextField::create('BookingMetaTitle')->setTitle('Meta Title'),
                TextareaField::create('BookingMetaDescription')->setTitle('Meta Description'),
                TextareaField::create('BookingCustomMeta')->setTitle('Custom Meta Tags'),
            ]
        ); 
        $fields->addFieldsToTab(
            'Root.BookingFunnel.Options',
            [
            ]
        );
    }

}