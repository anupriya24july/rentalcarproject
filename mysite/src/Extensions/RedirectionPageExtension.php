<?php

/**
 * Class RedirectionPageExtension
 */
class RedirectionPageExtension extends \SilverStripe\ORM\DataExtension
{

    /**
     *
     */
    public function onBeforeWrite()
    {
        $this->ShowInMenus = false;
    }

    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canCreate($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canEdit($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canDelete($member = null, $context = [])
    {
        return true;
    }

}