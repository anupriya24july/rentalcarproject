<?php

/**
 * Class GridFieldOrderableRowsExtension
 */
class GridFieldOrderableRowsExtension extends \SilverStripe\Core\Extension
{

    /**
     * @param \SilverStripe\ORM\SS_List $list
     */
    public function onAfterReorderItems($list)
    {
        error_log('onAfterReorderItems->2');
        foreach ($list as $item) {
//            if ($item instanceof \SilverStripe\Versioned\Versioned) {
            if ($item instanceof \SilverStripe\CMS\Model\SiteTree) {
                $item->publishSingle();
            }
        }
    }

}