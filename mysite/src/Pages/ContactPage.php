<?php

namespace AceRentals\Pages;

use ContactForm;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\File;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\Tab;
use SilverStripe\Forms\TabSet;
use SilverStripe\Security\Security;

/**
 * Class ContactPage
 */
class ContactPage extends Page
{

    /**
     * @var bool
     */
    private static $allowed_children = false;

    /**
     * @var array
     */
    private static $has_one = [
        'BackgroundImage' => Image::class,
    ];

    /**
     * @var array
     */
    private static $owns = [
        'BackgroundImage'
    ];

    /**
     *
     */
    public function onBeforeWrite()
    {
        parent::onBeforeWrite();

        $this->ShowInMenus = false;
    }

    /**
     * @return FieldList
     */
    public function getCMSFields()
    {
        // $fields = FieldList::create(
        //     TabSet::create('Root', Tab::create('Main'))
        // );

        $fields = parent::getCMSFields();
        $fields->addFieldsToTab(
            'Root.Main',
            [
                $backgroundImage = UploadField::create(
                    'BackgroundImage'
                )
            ]
        );

        $backgroundImage->setAllowedExtensions(File::get_category_extensions('image'));
        $backgroundImage->getValidator()->setAllowedMaxFileSize((1024 * 1024 * 5));

        return $fields;
    }

    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canEdit($member = null, $context = [])
    {
        if (parent::canEdit($member, $context)) {
            return true;
        }

        if (!$member) {
            $member = Security::getCurrentUser();
        }

        if ($member && $member->inGroup('content-authors')) {
            return true;
        }

        return false;
    }

}

/**
 * Class ContactPage_Controller
 */
class ContactPage_Controller extends PageController
{
    /**
     * @var array
     */
    private static $allowed_actions = [
        'ContactForm'
    ];

    /**
     * @return ContactForm
     */
    public function ContactForm()
    {
        return new ContactForm($this, 'ContactForm');
    }

    /**
     * @return bool
     */
    public function IsSuccess()
    {
        if (isset($_GET['cs'])) {
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getCountryAbbreviation()
    {
        $siteConfig = $this->SiteConfig();
        $country = $siteConfig->CountryVersion;

        switch ($country) {
            case 'Australia':
                return 'AU';
            default:
                return 'NZ';
        }
    }

    /**
     * @return \SilverStripe\ORM\DataList
     */
    public function getDepots()
    {
        $siteConfig = $this->SiteConfig();
        $country = $siteConfig->CountryVersion;

        return LocationPage::get()->filter('Country', $country);
    }

}
