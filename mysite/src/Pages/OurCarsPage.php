<?php

namespace AceRentals\Pages;

use SilverStripe\Forms\FieldList;
use SilverStripe\Security\Security;
use VehicleType;
use SilverStripe\Control\HTTPRequest;

/**
 * Class OurCarsPage
 */
class OurCarsPage extends Page
{

    /**
     * @var array
     */
    private static $allowed_children  = [
        'AceRentals\Pages\VehiclePage'
    ];

    public function getCMSFields()
    {
        // return FieldList::create();
        $fields = parent::getCMSFields();
        return $fields;
    }

    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canEdit($member = null, $context = [])
    {
        if (parent::canEdit($member, $context)) {
            return true;
        }

        if (!$member) {
            $member = Security::getCurrentUser();
        }

        if ($member && $member->inGroup('content-authors')) {
            return true;
        }

        return false;
    }

}

/**
 * Class OurCarsPageController
 */
class OurCarsPageController extends PageController
{

    /**
     * @var array
     */
    private static $allowed_actions = [
        'getVehicleTypes'
    ];

    /**
     * @return \SilverStripe\ORM\DataList
     */
    public function getVehicleTypes() {
        return VehicleType::get();
    }

    /**
     * @param HTTPRequest|null $request
     * @return \SilverStripe\ORM\DataList|string
     */
    public function getVehicles(HTTPRequest $request = null) {
        if(!$request){
            $request = $this->getRequest();
        }

        $list = VehiclePage::get();

        if($vehicleType = $request->requestVar('VehicleType')) {
            $list = $list->filter('VehicleTypeID', $vehicleType);
        }

        if($request->isAjax()) {
            return json_encode($list->toNestedArray());
        }

        return $list;
    }

}
