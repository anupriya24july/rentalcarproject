<?php

namespace AceRentals\Pages;

use SilverStripe\Assets\Image;

/**
 * Class PromotionPage
 */
class PromotionPage extends Page
{

    /**
     * @var array
     */
    private static $db = [
        'PromoCode' => 'Varchar(255)',
        'TravelDates' => 'Text',
        'DayHire' => 'Varchar(255)',
        'ShowOnRedirect' => 'Boolean',
        'Vehicles' => 'Varchar(255)',
        'Terms' => 'Text',
        'RedirectUrl' => 'Varchar(255)'
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'Image' => Image::class
    ];

    /**
     *
     */
    public function onBeforeWrite()
    {
        parent::onBeforeWrite();

        $parentPage = DealsPage::get()->first();

        if ($parentPage) {
            $this->ParentID = $parentPage->ID;
        }
    }

    /**
     * @return \SilverStripe\Forms\FieldList
     */
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->addFieldsToTab(
            'Root.Main',
            [
                $this->dbObject('PromoCode')->scaffoldFormField(),
                $this->dbObject('TravelDates')->scaffoldFormField(),
                $this->dbObject('DayHire')->scaffoldFormField(),
                $this->dbObject('Vehicles')->scaffoldFormField(),
                $this->dbObject('Terms')->scaffoldFormField(), 
                $this->dbObject('RedirectUrl')->scaffoldFormField(),
                $this->dbObject('ShowOnRedirect')->scaffoldFormField()->setTitle('Click to Hide on Current Website'),
            ]
        );

        $fields->addFieldsToTab(
            'Root.Images',
            [
                $this->dbObject('ImageID')->scaffoldFormField()
            ]
        );

        return $fields;
    }

    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canCreate($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canEdit($member = null, $context = [])
    {
        return true;
    }

}
