<?php

namespace AceRentals\Pages;

use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Assets\Image;
use SilverStripe\Assets\Upload_Validator;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

use VehicleType;

/**
 * Class VehiclePage
 */
class VehiclePage extends Page
{

    /**
     * @var array
     */
    private static $db = [
        'Age' => 'Varchar(255)',
        'Description' => 'Text',
        'CategoryTypeID' => 'Int',
        'VehicleClass' => 'Text',
        'Rate' => 'Varchar(1000)',
        'VehicleDescription' => 'HTMLText',
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'Image' => Image::class,
        'BackgroundImage' => Image::class,
        'CarImage' => Image::class,
        'VehicleType' => 'VehicleType'
    ];

    /**
     * @var array
     */
    private static $many_many = [
        'Specifications' => 'Specification',
        'CarImages' => Image::class,
        'Features' => 'Feature',
//        'ContentSections' => 'ContentSection'
    ];

    /**
     * @var array
     */
    private static $owns = [
        'Image',
        'BackgroundImage',
        'CarImage',
        'CarImages',
    ];

    /**
     *
     */
    public function onBeforeWrite()
    {
        parent::onBeforeWrite();

        $this->ShowInMenus = false;

        $parentPage = OurCarsPage::get()->first();

        if ($parentPage) {
            $this->ParentID = $parentPage->ID;
        }
    }

    /**
     * @return \SilverStripe\Forms\FieldList
     */
    public function getCMSFields() {
        $fields = parent::getCMSFields();

        $fields->addFieldsToTab(
            'Root.Main',
            [
                TextField::create('CategoryTypeID', 'CategoryTypeID'),
                TextField::create('VehicleClass', 'VehicleClass'),
                TextField::create('Age', 'Age'),
                DropdownField::create('VehicleTypeID', 'VehicleType', VehicleType::get()->map()),
                TextareaField::create('Description'),
            ],
            'Content'
        );

//        $fields->removeByName('Content');

        $fields->addFieldsToTab(
            'Root.Images',
            [
                $mainImage = UploadField::create('Image', 'Main Image'),
                $backgroundImage = UploadField::create('BackgroundImage', 'Header Background Image'),
                $carImage = UploadField::create('CarImage', 'Car Cutout Image'),
                $carImages = UploadField::create('CarImages', 'Car Images'),
            ]
        );

        $mbLimit = 10;
        $bytes = ($mbLimit * 1024 * 1024);

        $mainImage->getValidator()->setAllowedMaxFileSize($bytes);
        $backgroundImage->getValidator()->setAllowedMaxFileSize($bytes);
        $carImage->getValidator()->setAllowedMaxFileSize($bytes);
        $carImages->getValidator()->setAllowedMaxFileSize($bytes);

        $mainImage->setRightTitle("Your image must not be larger than $mbLimit MB");
        $backgroundImage->setRightTitle("Your image must not be larger than $mbLimit MB");
        $carImage->setRightTitle("Your image must not be larger than $mbLimit MB");
        $carImages->setRightTitle("Your images must not be larger than $mbLimit MB");

        $carImages->setIsMultiUpload(true);

        if ($this->exists()) {
            $config = GridFieldConfig_RecordEditor::create();

            /*$config->addComponent(new GridFieldSortableRows('Sort'));
            $config->removeComponentsByType('GridFieldAddExistingAutocompleter');

            $fields->addFieldToTab(
                'Root.ContentSections',
                GridField::create(
                    'ContentSections',
                    null,
                    $this->ContentSections(),
                    $config
                )
            );*/



            $fields->addFieldToTab(
                'Root.Features',
                GridField::create(
                    'Features',
                    '"Perfect For" features',
                    $this->Features(),
                    $config
                )
            );



            $fields->addFieldToTab(
                'Root.SpecificationsAndRates',
                GridField::create(
                    'Specifications',
                    'Specifications and Rate Details',
                    $this->Specifications(),
                    $config
                )
            );

            $fields->addFieldsToTab(
                'Root.UberDetails',
                [
                    TextField::create('Rate'),
                    HTMLEditorField::create('VehicleDescription'),
                ]
            );
        }

        return $fields;
    }

    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canCreate($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canEdit($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param null $member
     * @return bool
     */
    public function canPublish($member = null)
    {
        return true;
    }

    /**
     * @param null $member
     * @return bool
     */
    public function canUnpublish($member = null)
    {
        return true;
    }

}

/**
 * Class VehiclePageController
 * @package AceRentals\Pages
 */
class VehiclePageController extends PageController {

    /**
     * @return static
     */
    public function getRelatedVehicles() {
        return VehiclePage::get()->sort('RAND()')->limit(3);
    }

}
