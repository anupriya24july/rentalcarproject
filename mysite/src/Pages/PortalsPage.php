<?php

namespace AceRentals\Pages;

/**
 * Class PortalsPage
 * @package AceRentals\Pages
 */
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\Tab;
use SilverStripe\Forms\TabSet;
use SilverStripe\Security\Security;

/**
 * Class PortalsPage
 * @package AceRentals\Pages
 */
class PortalsPage extends Page
{

    /**
     * @var array
     */
    private static $allowed_children = [
        'AceRentals\Pages\PortalPage'
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        return $fields;

        // return FieldList::create(
        //     TabSet::create(
        //         'Root',
        //         Tab::create(
        //             'Main',
        //             $fields->dataFieldByName('Content')
        //         )
        //     )
        // );
    }

    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canEdit($member = null, $context = [])
    {
        if (parent::canEdit($member, $context)) {
            return true;
        }

        if (!$member) {
            $member = Security::getCurrentUser();
        }

        if ($member && $member->inGroup('content-authors')) {
            return true;
        }

        return false;
    }

}

/**
 * Class PortalPageController
 */
class PortalsPageController extends PageController
{

    /**
     * @return \SilverStripe\Control\HTTPResponse
     */
    public function init()
    {
        parent::init();

        if ($this->children()->count() == 1) {
            return $this->redirect(
                $this->children()->first()->Link(), 301
            );
        }
    }

    /**
     * @return \SilverStripe\ORM\DataList
     */
    public function getPortal() {
        return PortalPage::get();
    }

}
