<?php

namespace AceRentals\Pages;

use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Security\Security;
use SilverStripe\Forms\TextField;

/**
 * Class DrivingSafelyPage
 * @package AceRentals\Pages
 */
class DrivingSafelyPage extends Page
{

    /**
     * @var array
     */
    private static $db = [
        'DrivingInNZLabel' => 'Varchar(50)',
        'MustReadsLabel' => 'Varchar(50)',
        'DrivingInNZ' => 'HTMLText',
        'MustReads' => 'HTMLText'
    ];

    /**
     * @var array
     */
    private static $many_many = [
        'Videos' => 'Video'
    ];

    /**
     * @var array
     */
    private static $owns = [
        'Videos'
    ];

    /**
     *
     */
    public function onBeforeWrite()
    {
        parent::onBeforeWrite();

        $this->ShowInMenus = false;
    }

    /**
     * @return \SilverStripe\Forms\FieldList
     */
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->addFieldsToTab(
            'Root.Videos',
            [
                GridField::create(
                    'Videos',
                    'Videos',
                    $this->Videos(),
                    GridFieldConfig_RecordEditor::create()
                )
            ]
        );

        $fields->addFieldsToTab(
            'Root.DrivingInNZ',
            [
                TextField::create('DrivingInNZLabel'),
                HTMLEditorField::create('DrivingInNZ')
            ]
        );

        $fields->addFieldsToTab(
            'Root.MustReads',
            [
                TextField::create('MustReadsLabel'),
                HTMLEditorField::create('MustReads')
            ]
        );

        return $fields;
    }

    public function canEdit($member = null, $context = [])
    {
        if (parent::canEdit($member, $context)) {
            return true;
        }

        if (!$member) {
            $member = Security::getCurrentUser();
        }

        if ($member && $member->inGroup('content-authors')) {
            return true;
        }

        return false;
    }

}
