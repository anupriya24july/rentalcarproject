<?php

namespace AceRentals\Pages;

use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\File;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldAddNewButton;
use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
use SilverStripe\Forms\CheckboxField;
use GridFieldSortableRows;
use SilverStripe\Forms\GridField\GridFieldEditButton;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TabSet;
/**
 * Class MiscellaneousPage
 * @package AceRentals\Pages
 */
class MiscellaneousPage extends Page {

    /**
     * @var array
     */
    private static $db = [
        //Pages
        'BookingLabel' => 'Varchar(255)',
        'FeaturedCarsLabel' => 'Varchar(50)',
        'SeeAllCarsLabel' => 'Varchar(50)',
        'WhyFeaturesLabel' => 'Varchar(50)',
        'ReviewLabel' => 'Varchar(100)',
        'SeeAllReviewLabel' => 'Varchar(50)',
        'QuicklinksLabel' => 'Varchar(25)',
        'AboutUsLabel' => 'Varchar(25)',
        'OurLocationsLabel' => 'Varchar(50)',
        'RelocationsLabel' => 'Varchar(25)',
        'DealsLabel' => 'Varchar(25)',
        'ReviewsLabel' => 'Varchar(100)',
        'ContactUsLabel' => 'Varchar(25)',
        'TermsLabel' => 'Varchar(50)',
        'PrivacyPolicyLabel' => 'Varchar(25)',
        'EarlyCheckInLabel' => 'Varchar(25)',
        'DrivingSafeLabel' => 'Varchar(25)',
        'OurVehiclesLabel' => 'Varchar(25)',
        'FeaturedDealsLabel' => 'Varchar(100)',
        'SeeAllDealsLabel' => 'Varchar(50)',

        //BookingFunnel

        // Common
        'PageTitle' => 'Varchar(255)',
        'SelectVehicleLabel' => 'Varchar(25)',
        'AddExtrasLabel' => 'Varchar(25)',
        'PersonalDetailsLabel' => 'Varchar(100)',
        'PaymentLabel' => 'Varchar(25)',
        'CompleteLabel' => 'Varchar(25)',
        'ModifyTripDetailsLabel' => 'Varchar(255)',
        'PickupLabel' => 'Varchar(100)',
        'DropoffLabel' => 'Varchar(100)',
        'TotalPriceLabel' => 'Varchar(100)',
        'TotalPriceIncludesLabel' => 'Varchar(255)',
        'SelectLabel' => 'Varchar(25)',
        'YourQuoteLabel' => 'Varchar(25)',
        'AncillaryOptionsLabel' => 'Varchar(255)',
        'VehicleCostLabel' => 'Varchar(25)',
        'MandatoryFeesLabel' => 'Varchar(255)',
        'CoverageLabel' => 'Varchar(255)',
        'FreeLabel' => 'Varchar(25)',
        'DayLabel' => 'Varchar(25)',
        'ProcessingLabel' => 'Varchar(25)',
        'NextStepLabel' => 'Varchar(25)',


        // Itinerary
        'PickupLocationLabel' => 'Varchar(25)',
        'DropoffLocationLabel' => 'Varchar(25)',
        'PickupDateLabel' => 'Varchar(25)',
        'DropoffDateLabel' => 'Varchar(25)',
        'DriverAgeLabel' => 'Varchar(25)',
        'PromocodeLabel' => 'Varchar(25)',
        'DifferentDropoffLabel' => 'Varchar(255)',
        'LessOptionsLabel' => 'Varchar(25)',
        'MoreOptionsLabel' => 'Varchar(25)',
        'ItinerarySearchLabel' => 'Varchar(25)',

        // Vehicle Selection
        // 'PickupLocationLabel' => 'Varchar(255)',
        // 'DropoffLocationLabel' => 'Varchar(255)',
        // 'PickupDateLabel' => 'Varchar(255)',
        // 'DropoffDateLabel' => 'Varchar(255)',
        // 'DriverAgeLabel' => 'Varchar(255)',
        // 'PromocodeLabel' => 'Varchar(255)',
        // 'ItinerarySearchLabel' => 'Varchar(255)',

        //Extra
        'CoverageInstructons' => 'Varchar(255)',
        'OptionsInstructions' => 'Varchar(255)',
        'ExcessBlurb' => 'Text',
        'BondBlurb' => 'Text',
        'ExtrasCTA' => 'Varchar(255)',

        //Personal
        'RenterLabel' => 'Varchar(50)',
        'FirstNameLabel' => 'Varchar(50)',
        'LastNameLabel' => 'Varchar(50)',
        'ContactNumberLabel' => 'Varchar(50)',
        'EmailLabel' => 'Varchar(50)',
        'CountryLabel' => 'Varchar(50)',
        'AdditionalInfoLabel' => 'Varchar(50)',
        'AdditionalInfoDescription' => 'Varchar(200)',
        'NotesLabel' => 'Varchar(50)',
        'SignUpLabel' => 'Varchar(50)',
        'AgreeLabel' => 'Varchar(50)',
        'TermsLabel' => 'Varchar(50)',

        //Payment
        'PaymentAmountLabel' => 'Varchar(50)',
        'PayPercentLabel' => 'Varchar(50)',
        'PayFullLabel' => 'Varchar(50)',
        'CreditCardLabel' => 'Varchar(50)',
        'CardHolderLabel' => 'Varchar(50)',
        'CardNumberLabel' => 'Varchar(50)',
        'ExpiryDateLabel' => 'Varchar(50)',
        'CvcLabel' => 'Varchar(10)',
        'ContinueLabel' => 'Varchar(25)',

    ];

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->removeByName(['Content','Metadata','Navigationlabel','URLSegment','Pagename']);
        $fields->removeFieldFromTab('Root', ['MetaTags','Sitemap','Main']);

        $fields->addFieldToTab('Root', new TabSet('BookingFunnel'));
        $fields->addFieldsToTab(
            'Root.Pages',
            [
                TextField::create('BookingLabel'),
                TextField::create('FeaturedCarsLabel'),
                TextField::create('SeeAllCarsLabel'),
                TextField::create('WhyFeaturesLabel'),
                TextField::create('ReviewLabel'),
                TextField::create('SeeAllReviewLabel'),
                TextField::create('QuicklinksLabel'),
                TextField::create('AboutUsLabel'),
                TextField::create('OurLocationsLabel'),
                TextField::create('RelocationsLabel'),
                TextField::create('DealsLabel'),
                TextField::create('ReviewsLabel'),
                TextField::create('ContactUsLabel'),
                TextField::create('TermsLabel'),
                TextField::create('PrivacyPolicyLabel'),
                TextField::create('EarlyCheckInLabel'),
                TextField::create('DrivingSafeLabel'),
                TextField::create('OurVehiclesLabel'),
                TextField::create('FeaturedDealsLabel'),
                TextField::create('SeeAllDealsLabel'),
            ]
        );
        $fields->addFieldsToTab(
            'Root.BookingFunnel.Common',
            [
                TextField::create('SelectVehicleLabel'),
                TextField::create('AddExtrasLabel'),
                TextField::create('PersonalDetailsLabel'),
                TextField::create('PaymentLabel'),
                TextField::create('CompleteLabel'),
                TextField::create('ModifyTripDetailsLabel'),
                TextField::create('PickupLabel'),
                TextField::create('DropoffLabel'),
                TextField::create('TotalPriceLabel'),
                TextField::create('TotalPriceIncludesLabel'),
                TextField::create('SelectLabel'),
                TextField::create('YourQuoteLabel'),
                TextField::create('AncillaryOptionsLabel'),
                TextField::create('VehicleCostLabel'),
                TextField::create('MandatoryFeesLabel'),
                TextField::create('CoverageLabel'),
                TextField::create('FreeLabel'),
                TextField::create('DayLabel'),
                TextField::create('ProcessingLabel'),
                TextField::create('NextStepLabel'),
            ]
        );
        $fields->addFieldsToTab(
            'Root.BookingFunnel.Itinerary',
            [
                TextField::create('PickupLocationLabel'),
                TextField::create('DropoffLocationLabel'),
                TextField::create('PickupDateLabel'),
                TextField::create('DropoffDateLabel'),
                TextField::create('DriverAgeLabel'),
                TextField::create('PromocodeLabel'),
                TextField::create('ItinerarySearchLabel'),
                TextField::create('DifferentDropoffLabel'),
                TextField::create('LessOptionsLabel'),
                TextField::create('MoreOptionsLabel'),
            ]
        );
        $fields->addFieldsToTab(
            'Root.BookingFunnel.VehicleSelection',
            [
                // TextField::create('PickupLocationLabel'),
                // TextField::create('DropoffLocationLabel'),
                // TextField::create('PickupDateLabel'),
                // TextField::create('DropoffDateLabel'),
                // TextField::create('DriverAgeLabel'),
                // TextField::create('PromocodeLabel'),
                // TextField::create('ItinerarySearchLabel'),
            ]
        );
        $fields->addFieldsToTab(
            'Root.BookingFunnel.Extras',
            [
                TextField::create('CoverageInstructons'),
                TextField::create('OptionsInstructions'),
                TextareaField::create('ExcessBlurb'),
                TextareaField::create('BondBlurb'),
                TextField::create('ExtrasCTA'),
            ]
        );
        $fields->addFieldsToTab(
            'Root.BookingFunnel.Personal',
            [
                TextField::create('RenterLabel'),
                TextField::create('FirstNameLabel'),
                TextField::create('LastNameLabel'),
                TextField::create('ContactNumberLabel'),
                TextField::create('EmailLabel'),
                TextField::create('CountryLabel'),
                TextField::create('AdditionalInfoLabel'),
                TextField::create('AdditionalInfoDescription'),
                TextField::create('NotesLabel'),
                TextField::create('SignUpLabel'),
                TextField::create('AgreeLabel'),
                TextField::create('TermsLabel'),
            ]
        );
        
        $fields->addFieldsToTab(
            'Root.BookingFunnel.Payment',
            [
                TextField::create('PaymentAmountLabel'),
                TextField::create('PayPercentLabel'),
                TextField::create('PayFullLabel'),
                TextField::create('CreditCardLabel'),
                TextField::create('CardHolderLabel'),
                TextField::create('CardNumberLabel'),
                TextField::create('ExpiryDateLabel'),
                TextField::create('CvcLabel'),
                TextField::create('ContinueLabel'),
            ]
        );

        return $fields;
    }
    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canCreate($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canEdit($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param null $member
     * @return bool
     */
    public function canPublish($member = null)
    {
        return true;
    }

    /**
     * @param null $member
     * @return bool
     */
    public function canUnpublish($member = null)
    {
        return true;
    }

}

/**
 * Class MiscellaneousPageController
 * @package AceRentals\Pages
 */
class MiscellaneousPageController extends PageController {

    public function getMiscellaneous() {
        return MiscellaneousPage::get();
    }
}