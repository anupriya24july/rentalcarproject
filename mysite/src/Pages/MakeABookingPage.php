<?php

namespace AceRentals\Pages;

use VehicleType;

class MakeABookingPage extends Page
{


}

class MakeABookingPageController extends PageController
{
    /**
     * @return \SilverStripe\ORM\DataList
     */
    public function getVehicleTypes() {
        return VehicleType::get();
    }

    public function getVehicles()
    {
        $vehicles = VehiclePage::get();
        $data = [];

        // // TODO send out all necessary data
        // foreach($vehicles as $vehicle) {
        //     $data[] = [
        //         'Title' => $vehicle->Title,
        //         'Image' => $vehicle->ImageID ? $vehicle->Image()->URL : null
        //     ];
        // }
        //
        // return json_encode($data);

        return $vehicles;
    }
}
