<?php

namespace AceRentals\Pages;

use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\File;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldAddNewButton;
use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
use SilverStripe\Forms\CheckboxField;
use GridFieldSortableRows;
use SilverStripe\Forms\GridField\GridFieldEditButton;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
/**
 * Class HomePage
 * @package AceRentals\Pages
 */
class HomePage extends Page {

    /**
     * @var array
     */
    private static $db = [
        'ShowReviews' => 'Boolean',
        'ShowFeaturedCars' => 'Boolean',
        'ShowFeaturedDeals' => 'Boolean',
        'ShowFirstSlideLogo' => 'Boolean',
        'Description' => 'HTMLText'
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'FirstSlideImage' => Image::class
    ];

    /**
     * @var array
     */
    private static $many_many = [
        'FeaturedCars' => 'AceRentals\Pages\VehiclePage',
        'FeaturedDeals' => 'AceRentals\Pages\PromotionPage',
        'WhyFeatures' => 'Feature'
    ];

    /**
     * @var array
     */
    private static $many_many_extraFields = [
        'FeaturedCars' => [
            'Sort' => 'Int'
        ],
        'FeaturedDeals' => [
            'Sort' => 'Int'
        ],
        'WhyFeatures' => [
            'Sort' => 'Int'
        ],
    ];

    private static $owns = [
        'FirstSlideImage'
    ];

    /**
     *
     */
    public function onBeforeWrite()
    {
        parent::onBeforeWrite();

        $this->ShowInMenus = false;
    }

    /**
     * @return \SilverStripe\Forms\FieldList
     */
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        if ($this->exists()) {

            $fields->addFieldsToTab(
                'Root.Main',
                [
                    CheckboxField::create('ShowReviews', 'Show Reviews'),
                    CheckboxField::create('ShowFirstSlideLogo', 'Show First Slide Logo'),
                    HTMLEditorField::create('Description')
                ],
                'Content'
            );

            $slideField = UploadField::create(
                'FirstSlideImage',
                'First Slide Image'
            );

            $mbLimit = 15;
            $bytes = ($mbLimit * 1024 * 1024);

            $slideField->getValidator()->setAllowedMaxFileSize($bytes);
            $slideField->setAllowedExtensions(File::get_category_extensions('image'));
//            $slideField->setRightTitle("Your image must not be larger than $mbLimit MB");

            $fields->addFieldsToTab(
                'Root.Images',
                [
                    $slideField
                ]
            );

            $featuredCars = GridField::create(
                'FeaturedCars',
                'Featured Cars',
                $this->FeaturedCars()
            );

            $fields->addFieldsToTab(
                'Root.FeaturedCars',[
                    $featuredCars,
                    CheckboxField::create('ShowFeaturedCars'),
                ]
            );

            $featuredCarsConfig = GridFieldConfig_RelationEditor::create();
            $featuredCarsConfig->addComponent(new GridFieldOrderableRows('Sort'));

            $featuredCarsConfig->removeComponentsByType([
                GridFieldAddNewButton::class,
                GridFieldEditButton::class
            ]);

            if ($this->FeaturedCars()->count() >= 3) {
                $featuredCarsConfig->removeComponentsByType('GridFieldAddExistingAutocompleter');
            }

            $featuredCars->setConfig($featuredCarsConfig);

            $featuredDeals = GridField::create(
                'FeaturedDeals',
                'Featured Deals',
                $this->FeaturedDeals()
            );

            $fields->addFieldsToTab(
                'Root.FeaturedDeals',
                [
                    $featuredDeals,
                    CheckboxField::create('ShowFeaturedDeals'),
                ]
            );

            $featuredDealsConfig = GridFieldConfig_RelationEditor::create();
            $featuredDealsConfig->addComponent(new GridFieldOrderableRows('Sort'));

            $featuredDealsConfig->removeComponentsByType([
                GridFieldAddNewButton::class,
                GridFieldEditButton::class
            ]);

            if ($this->FeaturedDeals()->count() >= 3) {
                $featuredDealsConfig->removeComponentsByType('GridFieldAddExistingAutocompleter');
            }

            $featuredDeals->setConfig($featuredDealsConfig);

            $whyFeaturesConfig = GridFieldConfig_RelationEditor::create();

            $fields->addFieldToTab(
                'Root.WhyFeatures',
                $whyFeatures = GridField::create(
                    'WhyFeatures',
                    'Why Features',
                    $this->WhyFeatures()
                )
            );

            $whyFeatures->setConfig($whyFeaturesConfig);
        }

        $fields->removeByName('Content');

        return $fields;
    }

    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canEdit($member = null, $context = [])
    {
        return true;
    }

}

/**
 * Class HomePageController
 * @package AceRentals\Pages
 */
class HomePageController extends PageController {

    public function getExperiences() {
        return ExperiencePage::get();
    }

}