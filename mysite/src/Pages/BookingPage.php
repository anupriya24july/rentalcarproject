<?php

namespace AceRentals\Pages;

use Passbook\Pass\Field;
use Passbook\Pass\DateField;
use Passbook\Pass\Image;
use Passbook\PassFactory;
use Passbook\Pass\Barcode;
use Passbook\Pass\Structure;
use Passbook\Type\Generic;

class BookingPage extends Page {

}

class BookingPage_Controller extends PageController
{
    /**
     * @var array
     */
    private static $allowed_actions = [
        'GeneratePass',
        'DownloadPass',
    ];

    /**
     * Create a Pass
     *
     * @return string
     */
    public function GeneratePass()
    {
      $requestBody = json_decode($this->getRequest()->getBody());
      $this->getResponse()->addHeader("Content-Type", "application/json");

      if ($requestBody) {
        $passSerial = $requestBody->serial;

        $P12_FILE = BASE_PATH . '/certificates/certificate.p12';
        $P12_PASSWORD = '';
        $WWDR_FILE = BASE_PATH . '/certificates/wwdr.pem';

        $PASS_TYPE_IDENTIFIER = '';
        $TEAM_IDENTIFIER = '';

        $ORGANIZATION_NAME = 'Ace Rentals';
        $OUTPUT_PATH = BASE_PATH . '/passes/';
        $ICON_FILE = BASE_PATH . '/apple-touch-icon-152x152.png';

        // Create an event ticket
        $pass = new Generic($passSerial, "Ace Rentals Booking");
        $pass->setForegroundColor('rgb(255, 255, 255)');
        $pass->setBackgroundColor('rgb(3, 40, 92)');
        $pass->setLabelColor('rgb(254, 214, 0)');

        // Create pass structure
        $structure = new Structure();

        // Add header field
        $driver = new Field('driver', $requestBody->driver);
        $driver->setLabel('Driver');
        $structure->addHeaderField($driver);

        // Add primary field
        $vehicle = new Field('vehicle', $requestBody->vehicle);
        $vehicle->setLabel('Vehicle');
        $structure->addPrimaryField($vehicle);

        // Add secondary field
        $pickupLocation = new Field('pickup-location', $requestBody->pickupLocation);
        $pickupLocation->setLabel('Pick-up Location');
        $structure->addSecondaryField($pickupLocation);

        $returnLocation = new Field('return-location', $requestBody->returnLocation);
        $returnLocation->setLabel('Return Location');
        $returnLocation->setTextAlignment('PKTextAlignmentRight');
        $structure->addSecondaryField($returnLocation);

        // Add auxiliary field
        $pickupDate = new DateField('pickupDate', $requestBody->pickupDate);
        $pickupDate->setLabel('Pick-up Date');
        $pickupDate->setDateStyle('PKDateStyleMedium');
        $structure->addAuxiliaryField($pickupDate);

        $deposit = new Field('deposit', $requestBody->depositPaid);
        $deposit->setLabel('Deposit Paid');
        $deposit->setTextAlignment('PKTextAlignmentCenter');
        $structure->addAuxiliaryField($deposit);

        $balance = new Field('balance', $requestBody->balanceDue);
        $balance->setLabel('Balance Due');
        $balance->setTextAlignment('PKTextAlignmentCenter');
        $structure->addAuxiliaryField($balance);

        $returnDate = new DateField('returnDate', $requestBody->returnDate);
        $returnDate->setLabel('Return Date');
        $returnDate->setTextAlignment('PKTextAlignmentRight');
        $returnDate->setDateStyle('PKDateStyleMedium');
        $structure->addAuxiliaryField($returnDate);

        // Add icon image
        $icon = new Image($ICON_FILE, 'icon');
        $pass->addImage($icon);

        // Set pass structure
        $pass->setStructure($structure);

        // Add barcode
        $barcode = new Barcode(Barcode::TYPE_QR, 'barcodeMessage');
        $pass->setBarcode($barcode);

        // Create pass factory instance
        $factory = new PassFactory(
          $PASS_TYPE_IDENTIFIER,
          $TEAM_IDENTIFIER,
          $ORGANIZATION_NAME,
          $P12_FILE,
          $P12_PASSWORD,
          $WWDR_FILE
        );
        $factory->setOutputPath($OUTPUT_PATH);
        $factory->package($pass);

        return json_encode([
          'serial' => $passSerial,
          'path' => $factory->getOutputPath() . $passSerial . '.pkpass',
        ]);
      }

      $this->getResponse()->setStatusCode(400);

      return json_encode([
        'status' => 'ERROR',
        'message' => 'Bad request body.'
      ]);
    }

    public function DownloadPass() {
      $passSerial = $_GET['serial'];
      $pkpass_file = BASE_PATH . '/passes/' . $passSerial . '.pkpass';

    	header("Pragma: no-cache");
    	header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
    	header("Content-Type: application/vnd.apple.pkpass");
    	header('Content-Disposition: attachment; filename="' . $passSerial . '.pkpass"');

    	clearstatcache();
    	$filesize = filesize($pkpass_file);
      if ($filesize) {
        header("Content-Length: ". $filesize);
        header('Content-Transfer-Encoding: binary');
      }

      if (filemtime($pkpass_file)) {
        date_default_timezone_set("UTC");
        header('Last-Modified: ' . date("D, d M Y H:i:s", filemtime($pkpass_file)) . ' GMT');
      }

    	flush();
    	readfile($pkpass_file);
    }
}
