<?php

namespace AceRentals\Pages;

use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\RelationList;
use SilverStripe\Assets\Image;
use SilverStripe\Assets\File;

class Page extends \Page
{

    public function publishFile($fieldName)
    {
        $value = $this->{$fieldName}();

        if ($value) {
            if ($value instanceof RelationList) {
                foreach ($value as $item) {
                    $item->publishFile();
                }
            } elseif ($value instanceof DataObject) {
                $value->publishFile();
            }
        }
    }

    /**
     * @desc Publish our dependent objects (which is a stupid thing to have to do tbh).
     */
    public function onAfterWrite()
    {
        $hasOnes = $this->owner->stat('has_one');
        foreach ($hasOnes as $relation => $class) {
            if ($class == Image::class || $class == File::class) {
                $this->publishRelatedObject($this->owner->$relation());
            }
        }

        $hasMultiples = array_merge($this->owner->stat('has_many'), $this->owner->stat('many_many'));
        foreach ($hasMultiples as $relation => $class) {
            if ($class == Image::class || $class == File::class) {
                foreach ($this->$relation() as $object) {
                    $this->publishRelatedObject($object);
                }
            }
        }

        parent::onAfterWrite();
    }

    /**
     * @param $object
     */
    protected function publishRelatedObject($object)
    {
        if ($object && $object->owner->exists()) {
            $object->owner->publishSingle();
        }
    }

}
