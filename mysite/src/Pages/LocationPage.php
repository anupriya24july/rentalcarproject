<?php

namespace AceRentals\Pages;

use SilverStripe\Forms\TextField;
use GoogleMapField;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\TextareaField;

/**
 * Class LocationPage
 * @package AceRentals\Pages
 */
class LocationPage extends Page
{

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar(255)',
        'RcmLocationID' => 'Int',
        'PhoneNumber' => 'Varchar(255)',
        'FaxNumber' => 'Varchar(255)',
        'Address' => 'Varchar(255)',
        'InfoBoxDescription' => 'Text',
        'Email' => 'Varchar(255)',
        'OfficeHours' => 'Varchar(255)',
        'Country' => 'Enum("New Zealand,Australia", "New Zealand")',
        'Lat' => 'Varchar(255)',
        'Lng' => 'Varchar(255)',
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'BannerImage' => Image::class,
        'InfoBoxImage' => Image::class,

    ];

    /**
     * @var array
     */
    private static $owns = [
        'BannerImage',
        'InfoBoxImage',
    ];

    /**
     *
     */
    public function onBeforeWrite() {
        parent::onBeforeWrite();

        $parentPage = LocationsPage::get()->first();

        if($parentPage) {
            $this->ParentID = $parentPage->ID;
        }
    }

    /**
     * @return \SilverStripe\Forms\FieldList
     */
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName(['Lat', 'Lng']);

        $mapField = new GoogleMapField(
            $this,
            'Location',
            [
                'field_names' => [
                    'Latitude' => 'Lat',
                    'Longitude' => 'Lng',
                ],
                'api_key' => 'AIzaSyCs7Fe2Ys4NXg4W0hDIo1kohCi1S2kGSYM'  // TODO
            ]
        );

        $fields->addFieldsToTab(
            'Root.Main',
            [
                TextField::create('Title'),
                TextField::create('RcmLocationID'),
                TextField::create('PhoneNumber')->setTitle('Phone Number'),
                TextField::create('FaxNumber')->setTitle('Toll Free Number'),
                TextField::create('Address'),
                TextareaField::create('InfoBoxDescription'),
                $this->dbObject('InfoBoxImageID')->scaffoldFormField(),
                TextField::create('Email'),
                TextField::create('OfficeHours'),
                $this->dbObject('Country')->scaffoldFormField(),
                $this->dbObject('BannerImageID')->scaffoldFormField(),
                $mapField
            ],
            'Content'
        );
        return $fields;
    }

    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canCreate($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canEdit($member = null, $context = [])
    {
        return true;
    }

}
