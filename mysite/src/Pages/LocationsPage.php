<?php

namespace AceRentals\Pages;

/**
 * Class LocationsPage
 */
/**
 * Class LocationsPage
 * @package AceRentals\Pages
 */
class LocationsPage extends Page
{

    /**
     * @var array
     */
    private static $allowed_children  = [
        'AceRentals\Pages\LocationPage'
    ];

}

/**
 * Class LocationsPageController
 */
class LocationsPageController extends PageController
{

    /**
     * @return \SilverStripe\ORM\DataList
     */
    public function getLocations() {
        return LocationPage::get();
    }

    /**
     * @return string
     */
    public function getLocationsJSON() {
        return json_encode(
            array_map(
                function($item) {

                    $attributes = ['ID', 'Title', 'PhoneNumber', 'FaxNumber', 'OfficeHours', 'Country', 'Lat', 'Lng', 'URLSegment'];

                    $data = [];

                    foreach($attributes as $attribute) {
                        if(isset($item[$attribute])) {
                            $data[$attribute] = $item[$attribute];
                        } else {
                            $data[$attribute] = null;
                        }
                    }

                    return $data;
                },
                $this->getLocations()->toNestedArray()
            )
        );
    }

}
