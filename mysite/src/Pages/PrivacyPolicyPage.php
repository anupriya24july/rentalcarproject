<?php

namespace AceRentals\Pages;

use SilverStripe\Security\Security;

/**
 * Class PrivacyPolicy
 * @package AceRentals\Pages
 */
class PrivacyPolicy extends Page
{

    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canEdit($member = null, $context = [])
    {
        if (parent::canEdit($member, $context)) {
            return true;
        }

        if (!$member) {
            $member = Security::getCurrentUser();
        }

        if ($member && $member->inGroup('content-authors')) {
            return true;
        }

        return false;
    }

}