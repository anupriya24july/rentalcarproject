<?php

namespace AceRentals\Pages;


use SilverStripe\Forms\CheckboxField;
use SilverStripe\Security\Security;

/**
 * Class InformationPage
 * @package AceRentals\Pages
 */
class InformationPage extends Page
{

    /**
     * @var array
     */
    private static $db = [
        'QuickLink' => 'Boolean',
        'VisibleInFooter' => 'Boolean',
    ];

    /**
     * @return \SilverStripe\Forms\FieldList
     */
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->addFieldsToTab(
            'Root.Main',
            [
                $quickLink = CheckboxField::create('QuickLink', 'Quick Link Page'),
                CheckboxField::create('VisibleInFooter', 'Visible In Footer'),
            ],
            'Content'
        );

        $quickLink->setRightTitle('This will display items under quick links in the footer');

        return $fields;
    }

    // public function onBeforeDelete() 
    // {
    // }

    /**
     *
     */
    public function onBeforeWrite()
    {
        parent::onBeforeWrite();

        $this->ShowInMenus = false;
        $informationContainerPage = InformationContainerPage::get()->first();

        if ($informationContainerPage) {
            $this->ParentID = $informationContainerPage->ID;
        }
    }

    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canCreate($member = null, $context = [])
    {
        if (parent::canEdit($member, $context)) {
            return true;
        }

        if (!$member) {
            $member = Security::getCurrentUser();
        }

        if ($member && $member->inGroup('content-authors')) {
            return true;
        }

        return false;
    }

    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canEdit($member = null, $context = [])
    {
        if (parent::canEdit($member, $context)) {
            return true;
        }

        if (!$member) {
            $member = Security::getCurrentUser();
        }

        if ($member && $member->inGroup('content-authors')) {
            return true;
        }

        return false;
    }

}