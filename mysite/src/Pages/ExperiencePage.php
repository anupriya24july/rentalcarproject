<?php

namespace AceRentals\Pages;

use SilverStripe\Forms\FormField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
use SilverStripe\Forms\GridField\GridFieldAddNewButton;
use SilverStripe\Forms\GridField\GridFieldEditButton;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\DropdownField;

use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;
use SilverStripe\Assets\File;
use GridFieldSortableRows;
use SilverStripe\ORM\ArrayList;

/**
 * Class ExperiencePage
 */
class ExperiencePage extends Page
{

    /**
     * @var array
     */
    private static $db = [
        'ExperienceType' => "Enum(array('BeachHopping','MemoryMaking','SnowShredding','TownPainting','GridEscaping'), '')",
        'DealID' => 'Int',
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'Icon' => Image::class,
        'BackgroundImage' => Image::class,
        'RecommendedVehicle' => VehiclePage::class,
        'Deal' => PromotionPage::class,
        'VideoFile' => File::class,
    ];

    /**
     * @var array
     */
    private static $many_many = [
        'ContentSections' => 'ContentSection',
        'FeaturedCars' => 'AceRentals\Pages\VehiclePage'
    ];

    /**
     * @var array
     */
    private static $many_many_extraFields = [
        'ContentSections' => [
            'Sort' => 'Int'
        ],
        'FeaturedCars' => [
            'Sort' => 'Int'
        ],
    ];

    /**
     * @var array
     */
    private static $owns = [
        'Icon',
        'BackgroundImage',
        'VideoFile'
    ];

    /**
     *
     */
    public function onBeforeWrite()
    {
        parent::onBeforeWrite();

        $parentPage = RentTheExperiencePage::get()->first();

        if ($parentPage) {
            $this->ParentID = $parentPage->ID;
        }
    }

    /**
     * @return \SilverStripe\Forms\FieldList
     */
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $types = $this->dbObject('ExperienceType')->enumValues();
        $typeTitles = array_map(function ($item) {
            return FormField::name_to_label($item);
        }, $types);
        $types = array_combine($types, $typeTitles);

        $fields->addFieldsToTab(
            'Root.Main',
            [
                DropdownField::create(
                    'ExperienceType',
                    'Experience Type',
                    $types
                ),
                DropdownField::create(
                    'RecommendedVehicleID',
                    'Recommended Vehicle',
                    [0 => 'None'] + VehiclePage::get()->map()->toArray()
                ),
                DropdownField::create(
                    'DealID',
                    'Deal',
                    [0 => 'None'] + PromotionPage::get()->map()->toArray()
                )
            ],
            'Content'
        );

        $fields->removeByName('Content');

        if ($this->exists()) {
            $config = GridFieldConfig_RecordEditor::create();

//            $config->addComponent(new GridFieldSortableRows('Sort'));
            $config->removeComponentsByType('GridFieldAddExistingAutocompleter');

            $iconField = UploadField::create(
                'Icon',
                'Icon'
            );

            $fields->addFieldToTab(
                'Root.Images',
                $iconField
            );

            $fields->addFieldToTab(
                'Root.Images',
                $backgroundImage = UploadField::create(
                    'BackgroundImage',
                    'Background Image'
                )
            );

            $fields->addFieldToTab(
                'Root.Video',
                $videoFile = UploadField::create(
                    'VideoFile'
                )
            );

            $fields->addFieldToTab(
                'Root.ContentSections',
                GridField::create(
                    'ContentSections',
                    null,
                    $this->ContentSections(),
                    $config
                )
            );            

            $featuredCars = GridField::create(
                'FeaturedCars',
                'Featured Cars',
                $this->FeaturedCars()
            );

            $fields->addFieldToTab(
                'Root.FeaturedCars',
                $featuredCars
            );

            $featuredCarsConfig = GridFieldConfig_RelationEditor::create();
            $featuredCarsConfig->addComponent(new GridFieldOrderableRows('Sort'));

            $featuredCarsConfig->removeComponentsByType([
                GridFieldAddNewButton::class,
                GridFieldEditButton::class
            ]);

            if ($this->FeaturedCars()->count() >= 3) {
                $featuredCarsConfig->removeComponentsByType('GridFieldAddExistingAutocompleter');
            }

            $featuredCars->setConfig($featuredCarsConfig);

            $iconField->setAllowedExtensions(File::get_category_extensions('image'));
            $backgroundImage->setAllowedExtensions(File::get_category_extensions('image'));
            $videoFile->setAllowedExtensions(File::get_category_extensions('video'));

            $mbLimit = 30;
            $bytes = ($mbLimit * 1024 * 1024);

            $iconField->getValidator()->setAllowedMaxFileSize($bytes);
            $backgroundImage->getValidator()->setAllowedMaxFileSize($bytes);
            $videoFile->getValidator()->setAllowedMaxFileSize($bytes);

            $videoFile->setRightTitle("Your video must not be larger than $mbLimit MB");
        }

        return $fields;
    }

    /**
     * @return null|string
     */
    public function getSVGTitle()
    {
        $map = [
            'BeachHopping' => 'beach-hopping',
            'GridEscaping' => 'grid-escaping',
            'MemoryMaking' => 'memory-making',
            'SnowShredding' => 'snow-shredding',
            'TownPainting' => 'town-painting',
        ];

        return $this->ExperienceType ? "experiences/{$map[$this->ExperienceType]}" : null;
    }

    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canCreate($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canEdit($member = null, $context = [])
    {
        return true;
    }

    /**
     * @return static
     */
    public function getReversableContentSections()
    {
        $contentSections = $this->ContentSections();
        $carIndex = 2;
        $pos = 1;
        $newList = ArrayList::create();

        foreach ($contentSections as $contentSection) {
            if($contentSection->ShowInPage){
                if ($pos % 2 == 0) {
                    $contentSection->Reverse = true;
                }
                $newList->push($contentSection);
                $pos++;
            }
        }

        return $newList;
    }

}
