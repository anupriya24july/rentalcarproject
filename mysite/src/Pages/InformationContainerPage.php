<?php

namespace AceRentals\Pages;


use SilverStripe\CMS\Model\RedirectorPage;
use SilverStripe\Forms\FieldList;
use SilverStripe\Security\Security;

/**
 * Class InformationContainerPage
 * @package AceRentals\Pages
 */
class InformationContainerPage extends Page
{

    /**
     * @var array
     */
    private static $allowed_children = [
        InformationPage::class,
        RedirectorPage::class
    ];

    public function getCMSFields()
    {
        return FieldList::create();
    }

    /**
     *
     */
    public function onBeforeWrite()
    {
        parent::onBeforeWrite();

        $this->ShowInMenus = false;
        $this->URLSegment = 'information';
    }

    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canCreate($member = null, $context = [])
    {
        $existing = static::get();

        if ($existing->count()) {
            return false;
        }

        return parent::canCreate($member, $context);
    }

    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canEdit($member = null, $context = [])
    {
        if (parent::canEdit($member, $context)) {
            return true;
        }

        if (!$member) {
            $member = Security::getCurrentUser();
        }

        if ($member && $member->inGroup('content-authors')) {
            return true;
        }

        return false;
    }

}