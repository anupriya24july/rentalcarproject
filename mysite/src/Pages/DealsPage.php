<?php

namespace AceRentals\Pages;

use Relocation;
use RelocationGroup;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\Tab;
use SilverStripe\Forms\TabSet;
use SilverStripe\Security\Security;

use SilverStripe\Control\HTTPRequest;

/**
 * Class DealsPage
 * @package AceRentals\Pages
 */
class DealsPage extends Page
{

    private static $has_one = [
        'BannerImage' => Image::class
    ];

    /**
     * @var array
     */
    private static $allowed_children = [
         'AceRentals\Pages\PromotionPage',
     ];

    private static $owns = [
        'BannerImage'
    ];

    /**
     *
     */
//    public function onBeforeWrite()
//    {
//        parent::onBeforeWrite();
//
//        $this->ShowInMenus = false;
//    }

    public function getCMSFields()
    {
         $fields = parent::getCMSFields();
        $fields->addFieldsToTab(
            'Root.Main',
            [
                $mainImage = UploadField::create(
                    'BannerImage'
                ),
            ]
        );

        return $fields;
    }

    /**
     * @param null $member
     * @param array $context
     * @return bool
     */
    public function canEdit($member = null, $context = [])
    {
        if (parent::canEdit($member, $context)) {
            return true;
        }

        if (!$member) {
            $member = Security::getCurrentUser();
        }

        if ($member && $member->inGroup('content-authors')) {
            return true;
        }

        return false;
    }

}

/**
 * Class DealsPage_Controller
 * @package AceRentals\Pages
 */
class DealsPage_Controller extends PageController
{

    private static $allowed_actions = [
        'relocations',
        'promos',
    ];

    private static $url_handlers = [
        'relocations/$GroupID' => 'relocations',
    ];

    /**
     * @return \SilverStripe\ORM\DataList
     */
    public function getPromotions() {
        return PromotionPage::get();
    }

    /**
     * @return \SilverStripe\ORM\DataList
     */
    public function getRelocations() {
        return Relocation::get()->filter(['ShowInPage' =>  1 ]);
    }

    public function getRelocationGroups() {
        return RelocationGroup::get()->filter(['ShowInPage' =>  1 ]);
    }

    public function getImage($cid){
        return VehiclePage::get()->filter(['CategoryTypeID' =>  $cid ])->first()->Image()->URL;
    }

    public function getGroupID(){
        return $this->request->param('GroupID');
    }

    public function getGroupWebContent(){        
        return RelocationGroup::get()->filter(['ID' =>  $this->request->param('GroupID')])->first()->WebContent;
    }

}