<?php

use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Security\Security;

class Page extends SiteTree
{
    private static $db = [
    ];

    private static $has_one = [
    ];

    public function canCreate($member = null, $context = [])
    {
        if (!$member) {
            $member = Security::getCurrentUser();
        }

        if (!($member instanceof \SilverStripe\Security\Member)) {
            return false;
        }

        if ($member->inGroup('administrators')) {
            return true;
        }
        
        $extended = $this->extendedCan(__FUNCTION__, $member);

        if ($extended !== null) {
            return $extended;
        }

        return false;
    }

    public function canView($member = null, $context = [])
    {
        return true;

        /* if(!$member) {
             $member = Security::getCurrentUser();
         }

         if(!($member instanceof \SilverStripe\Security\Member)) {
             return false;
         }

         if($member->inGroup('administrators')) {
             return true;
         }

         return false;*/
    }

    public function canPublish($member = null)
    {
        return true;
    }

    public function canEdit($member = null, $context = [])
    {
        if (!$member) {
            $member = Security::getCurrentUser();
        }

        if (!($member instanceof \SilverStripe\Security\Member)) {
            return false;
        }

        if ($member->inGroup('administrators')) {
            return true;
        }

        $extended = $this->extendedCan(__FUNCTION__, $member);

        if ($extended !== null) {
            return $extended;
        }

        return false;
    }

    public function canDelete($member = null, $context = [])
    {
        if (!$member) {
            $member = Security::getCurrentUser();
        }

        if (!($member instanceof \SilverStripe\Security\Member)) {
            return false;
        }

        if ($member->inGroup('administrators')) {
            return true;
        }

        $extended = $this->extendedCan(__FUNCTION__, $member);

        if ($extended !== null) {
            return $extended;
        }

        return false;
    }


    /**
     * @return string
     */
//    public function getMetaDescription()
//    {
//        return $this->MetaDescription ?: $this->Content;
//    }


}
