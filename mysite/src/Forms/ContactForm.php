<?php

use SilverStripe\Forms\Form;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\DropdownField;
//use SilverStripe\Forms\HoneyPotField;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\EmailField;
use SilverStripe\Control\Session;
use SilverStripe\Core\Convert;

use Aws\Ses\SesClient;
use Aws\Ses\Exception\SesException;

/**
 * Class ContactForm
 */
class ContactForm extends Form
{

    protected $session;

    /**
     * @param Controller $controller
     * @param String $name
     */
    public function __construct($controller, $name)
    {
        $fields = FieldList::create(
            TextField::create("Name", "Name*")
                ->addExtraClass("l-form__input")
                ->addExtraClass("lg-validate")
                ->setAttribute('required', 'true')
                ->setAttribute('id', 'Name'),

            TextField::create("Phone", "Phone Number")
                ->addExtraClass("l-form__input")
                ->addExtraClass("lg-validate")
                ->setAttribute('id', 'Phone'),

            EmailField::create("Email", "Email Address*")
                ->addExtraClass("l-form__input")
                ->addExtraClass("lg-validate")
                ->setAttribute('required', 'true')
                ->setAttribute('id', 'Email'),

            DropdownField::create(
                "Type",
                "Enquiry Type",
                array_combine(
                    array_keys($this->getEmailRoutes()),
                    array_keys($this->getEmailRoutes())
                )
            )
                ->addExtraClass("l-form__input")
                ->addExtraClass("l-form__input--select")
                ->addExtraClass("lg-validate")
                ->setAttribute('id', 'Type')
                ->setAttribute('required', 'true')
                ->setEmptyString('Select Enquiry Type'),

            // please alter maxlength as per client requirements
            TextareaField::create("Message", "Message*")
                ->addExtraClass("l-form__input")
                ->addExtraClass("l-form__input--textarea")
                ->addExtraClass("lg-validate")
                ->setAttribute('placeholder', 'How can we help?')
                ->setAttribute('required', 'true')
                ->setAttribute('id', 'Message')

//            , HoneyPotField::create('Information', 'Information')
        );


        $actions = FieldList::create(
            FormAction::create("doSubmit", "Submit")
        );

        parent::__construct($controller, $name, $fields, $actions);

        // enable Akismet spam protection
        if (class_exists('AkismetSpamProtector')) {
            $this->enableSpamProtection(
                [
                    'mapping' => [
                        'Name' => 'authorName',
                        'Email' => 'authorMail',
                        'Message' => 'body'
                    ]
                ]
            );
        }

        $this->session = $this->getSession();

        if ($this->session->get("FormInfo.{$this->FormName()}.data")) {
            $this->loadDataFrom($this->session->get("FormInfo.{$this->FormName()}.data"));
        } else {
            $this->loadDataFrom($this->getRequest()->postVars());
        }

    }

    /**
     * @return bool
     */
    public function validate()
    {
        // clear out any previous messaging we have sent to the user
        $this->session->clear("FormInfo.{$this->FormName()}.formError.message");
        $this->session->clear("FormInfo.{$this->FormName()}.formError.type");

        $valid = parent::validate();
        if (!$valid) {
            // if the form isn't valid at this stage then return the errors which are coming first
            return $valid;
        }

        $data = $this->getData();
        $this->session->set("FormInfo.{$this->FormName()}.data", $data);

        $isSpam = false;
        $spamMessage = "Your contact form contains content that is deemed not suitable to send."
            . "Please review or contact us via email.";

        /**
         * Test 1:
         *
         * The following PHP code, when placed on your form processing page (the place where the form is submitted to),
         * will search all of the form elements for the most common header injections and other code that may trick your
         * mail processor into sending carbon copy or blind carbon copy messages to others. It also detects any content
         * that includes the string "[url" which is used by most forum software to specify links.
         * If any are found, it sets the $spam variable to true.
         */
        if (preg_match("/bcc:|cc:|multipart|\[url|Content-Type:/i", implode($data, ' '))) {
            $isSpam = true;
        }

        /**
         * Test 2:
         *
         * The following will set the $spam variable if more than $allowed instance(s) of "<a" or "http:" appear
         * anywhere within the form. You can change the $allowedURLs variable to zero or more if needed for any client
         * circumstance.
         */
        $patternAnchor = '/(<a)+/i';
        $patternHttp = '/(http:)/i';
        $patternHttps = '/(https:)/i';
        $patternWww = '/(www\.)/i';
        $allowedURLs = 1;
        preg_match_all($patternAnchor, $data['Message'], $anchorMatches);
        preg_match_all($patternHttp, $data['Message'], $httpMatches);
        preg_match_all($patternHttps, $data['Message'], $httpsMatches);
        preg_match_all($patternWww, $data['Message'], $wwwMatches);


        if (
            (
                !empty($anchorMatches[0]) ||
                !empty($httpMatches[0]) ||
                !empty($httpsMatches[0]) ||
                !empty($wwwMatches[0])
            )
            &&
            (
                count($anchorMatches[0]) > $allowedURLs ||
                count($httpMatches[0]) > $allowedURLs ||
                count($httpsMatches[0]) > $allowedURLs ||
                count($wwwMatches[0]) > $allowedURLs
            )
        ) {
            $spamMessage .= "\nPlease include no more than {$allowedURLs} url(s) in your message.";
            $isSpam = true;
        }

        /**
         * Test 3:
         *
         * Basic word filtering. Add to this as necessary - have fun!
         */
        $pattern = '/\b(?:anal|anus|arse|ass|ballsack|balls|bastard|bitch|biatch|bloody|blowjob|blow job|bollock|bollok|boner|boob|bugger|bum|butt|buttplug|clitoris|cock|coon|crap|cunt|damn|dick|dildo|dyke|fag|feck|fellate|fellatio|felching|fuck|f u c k|fudgepacker|fudge packer|flange|Goddamn|God damn|hell|homo|jerk|jizz|knobend|knob end|labia|lmao|lmfao|muff|nigger|nigga|omg|penis|piss|poop|prick|pube|pussy|scrotum|sex|shit|s hit|sh1t|slut|smegma|spunk|tit|tosser|turd|twat|vagina|wank|whore|wtf|ambien|anal|bulgary|cheapest pills|cialis|credit|discount|ejaculation|enlargement|levitra|money|mortgage|natural weight loss|omega|online pharmacy|penis|pharmaceuticals|porno|premature|prescription|refinance|rolex|sex|soma|valium|xanax)\b/i';
        preg_match_all($pattern, implode($data, ' '), $matches);

        if (!empty($matches[0])) {
            $isSpam = true;
        }

        // if content deemed to be unsuitable we let user know
        if ($isSpam) {
            $this->sessionMessage($spamMessage, 'bad');
            $this->validator->validationError('Spam', $spamMessage, 'bad');

            return false;
        }

        return true;
    }

    /**
     * @param $data
     * @param Form $form
     * @return SS_HTTPResponse
     */
    public function doSubmit($data, Form $form)
    {
        $siteConfig = \SilverStripe\SiteConfig\SiteConfig::current_site_config();

        $contactRequest = new ContactRequest();
        $form->saveInto($contactRequest);
        $contactRequest->write();
        $to = null;

        if (isset($data['Type'])) {
            $emailRoutes = $this->getEmailRoutes();
            $to = isset($emailRoutes[$data['Type']]) ? $emailRoutes[$data['Type']] : null;
        }

        if (!$to) {
            $to = $siteConfig->ContactEmail ? $siteConfig->ContactEmail : \Silverstripe\Core\Config\Config::inst()->get('Email',
                'admin_email');
        }

        $from = $data['Email'];
        $name = $data['Name'];
        $phone = $data['Phone'];
        $subject = isset($data['Subject']) && $data['Subject'] ? $data['Subject'] : 'Website Enquiry from ' . $data['Name'];

        $message = 'Name: ' . $name . '<br />';
        $message .= 'Phone: ' . $phone . '<br />';
        $message .= 'Email: ' . $from . '<br /><br />';
        $message .= '----------------------------------' . '<br /><br />';
        $message .= Convert::html2raw($data['Message']) . '<br /><br />';
        $message .= '----------------------------------' . '<br /><br />';
        $message .= '<small>Sent from IP: ' . $_SERVER['REMOTE_ADDR'] . '</small>';

        // $email = new \SilverStripe\Control\Email\Email();
        // $email->setTo($to);
        // $email->setFrom($to);
        // $email->setReplyTo($from);
        // $email->setSubject($subject);
        // $email->setBody($message);
        // $email->send();

         $client = SesClient::factory(array(
            'version'=> 'latest',     
            'region' => 'us-east-1',
            'credentials' => array(
                'key'    => 'AKIAIQLJLL2L73SGCSLA',
                'secret' => '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',
            )
        ));

        try {
             $result = $client->sendEmail([
            'Destination' => [
                'ToAddresses' => $to,
            ],
            'Message' => [
                'Body' => [
                    'Html' => [
                        'Charset' => 'UTF-8',
                        'Data' => $message,
                    ],
                ],
                'Subject' => [
                    'Charset' => 'UTF-8',
                    'Data' => $subject,
                ],
            ],
            'ReplyToAddresses' => [$from],
            'Source' => 'info@acerentals.co.nz',
        ]);
             $messageId = $result->get('MessageId');
             return $form->getController()->redirect($form->getController()->Link() . "?cs=1");

        } catch (SesException $error) {
             echo("The email was not sent. Error message: ".$error->getAwsErrorMessage()."\n");
        }

        // clear the data for the form after submission
        $this->session->clear("FormInfo.{$this->FormName()}.data");
        $this->session->clear("FormInfo.{$this->FormName()}.message");
        $this->session->clear("FormInfo.{$this->FormName()}.formError.message");
        $this->session->clear("FormInfo.{$this->FormName()}.formError.type");
        $this->session->clear("FormInfo.{$this->FormName()}.errors");
    }

    public function getEmailRoutes()
    {
        $siteConfig = \SilverStripe\SiteConfig\SiteConfig::current_site_config();

        switch ($siteConfig->CountryVersion) {
            case 'Australia':
                return [
                    'General Enquiry ' => ['info@acerentals.com.au'],
                    'Relocation Enquiry ' => ['relocations@acerentals.co.nz'],
                    'Add Additional Driver ' => ['support@acerentals.com.au'],
                    'Confirm a Booking ' => ['support@acerentals.com.au'],
                    'Change a Booking ' => ['support@acerentals.com.au'],
                    'Questions about Refunds ' => ['support@acerentals.com.au'],
                    'Insurance Questions ' => ['aceclaim@hertz.com'],
                    'Cancel a Booking ' => ['support@acerentals.com.au'],
                    'Make a Complaint ' => ['customercare@acerentals.co.nz'],
                ];
                break;
            default:
                return [
                    'General Enquiry ' => ['info@acerentals.co.nz','shynia.wiri@acerentals.co.nz','junjet@acerentals.co.nz'],
                    'Relocation Enquiry ' => ['relocations@acerentals.co.nz','shynia.wiri@acerentals.co.nz','junjet@acerentals.co.nz'],
                    'Add Additional Driver ' => ['support@acerentals.co.nz','shynia.wiri@acerentals.co.nz','junjet@acerentals.co.nz'],
                    'Confirm a Booking ' => ['support@acerentals.co.nz','shynia.wiri@acerentals.co.nz','junjet@acerentals.co.nz'],
                    'Change a Booking ' => ['support@acerentals.co.nz','shynia.wiri@acerentals.co.nz','junjet@acerentals.co.nz'],
                    'Questions about Refunds ' => ['support@acerentals.co.nz','shynia.wiri@acerentals.co.nz','junjet@acerentals.co.nz'],
                    'Insurance Questions ' => ['support@acerentals.co.nz','shynia.wiri@acerentals.co.nz','junjet@acerentals.co.nz'],
                    'Cancel a Booking ' => ['support@acerentals.co.nz','shynia.wiri@acerentals.co.nz','junjet@acerentals.co.nz'],
                    'Make a Complaint ' => ['customercare@acerentals.co.nz','shynia.wiri@acerentals.co.nz','junjet@acerentals.co.nz'],
                ];
                break;
        }
    }

}
