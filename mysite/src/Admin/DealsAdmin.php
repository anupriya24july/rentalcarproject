<?php
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\TextField;
use Guzzle\Http\Client;
/**
 * Class DealsAdmin
 */
class DealsAdmin extends \SilverStripe\Admin\ModelAdmin
{

    /**
     * @var array
     */
    private static $managed_models = [
        'Relocation', 'RelocationGroup'
    ];

    /**
     * @var string
     */
    private static $url_segment = 'relocations';

    /**
     * @var string
     */
    private static $menu_title = 'Relocations';

    public function getEditForm($id = null, $fields = null)
    {
        if(isset($_GET['action'])){
            $this->rcmApiImport();
        }
        $EditForm = parent::getEditForm($id, $fields);
        $EditForm->Fields()->add(LiteralField::create('Sync', '<a href="admin/relocations/Relocation?action=importRcm" class="action action-detail btn btn-primary new new-link"><span class="btn__title">Import Relocation Deals From RCM</span></a>'));
        return $EditForm;
    }

    public function rcmApiImport(){
        $client = new Client('https://api.acerentalcars.co.nz');
        $siteConfig = \SilverStripe\SiteConfig\SiteConfig::current_site_config(); 
        if($siteConfig->CountryVersion=='New Zealand'){
           $dbName='NZAce118';
        }else{            
           $dbName='AuAce';
        }
        $request= $client->get('/?mod=relocations&db='.$dbName.'&a=getRelocationSpecials');
        $importData=$request->send()->json();
        foreach ($importData as $row) {
            $isImported = Relocation::get()->filter(['RcmID' => $row['ID']]);
            if($isImported->exists()){            
                $player = Relocation::get()->byID($isImported[0]->ID);
            }else {                    
                $player = Relocation::create();
            }
            $player->EarliestPickupDate=$row['DateFrom'];
            $player->LatestDropOffDate=$row['DateTo'];
            $player->RcmID=$row['ID'];
            $player->DropoffID=(int)$row['DropoffLocationID'];
            $player->PickupID=(int)$row['PickupLocationID'];
            $player->DropoffLocation=$row['DropoffLocation'];
            $player->PickupLocation=$row['PickupLocation'];
            $player->DiscountType=$row['DiscountType'];
            $player->DiscountRate=$row['DiscountRate'];
            $player->Title=$row['DiscountName'];
            $player->CarSizeID=(int)$row['CarSizeID'];
            $player->CarSize=$row['VehicleCategory'];
            $player->BookingDateFrom=$row['BookingDateFrom'];
            $player->BookingDateTo=$row['BookingDateTo'];
            $player->MaxBookingDays=$row['MaxBookingDays'];
            $player->MinBookingDays=$row['MinBookingDays'];
            $id = $player->write();
        }
        $this->redirect('admin/relocations/Relocation');
    }

}