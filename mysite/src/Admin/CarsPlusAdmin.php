<?php
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\TextField;
use Guzzle\Http\Client;
/**
 * Class DealsAdmin
 */
class CarsPlusAdmin extends \SilverStripe\Admin\ModelAdmin
{

    /**
     * @var array
     */
    private static $managed_models = [
        'Location', 'DriverAge', 'Country', 'Option', 'BusinessSolution', 'TravelPartner'
    ];

    /**
     * @var string
     */
    private static $url_segment = 'carsplus';

    /**
     * @var string
     */
    private static $menu_title = 'Cars+ Settings';

}