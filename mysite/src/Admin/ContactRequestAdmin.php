<?php

/**
 * Class ContactRequestAdmin
 */
class ContactRequestAdmin extends \SilverStripe\Admin\ModelAdmin
{

    /**
     * @var array
     */
    private static $managed_models = [
        'ContactRequest',
    ];

    /**
     * @var string
     */
    private static $url_segment = 'contact-requests';

    /**
     * @var string
     */
    private static $menu_title = 'Contact Requests';

}