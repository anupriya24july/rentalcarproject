<?php

use SilverStripe\Admin\ModelAdmin;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class VehiclesAdmin extends CatalogPageAdmin
{

    /**
     * @var array
     */
    private static $managed_models = [
        'VehicleType'
    ];

    /**
     * @var string
     */
    private static $url_segment = 'vehicle-types';

    /**
     * @var string
     */
    private static $menu_title = 'Vehicles Types';

}