<?php

use SilverStripe\ORM\DataObject;
use AceRentals\Pages\VehiclePage;
use AceRentals\Pages\LocationPage;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\DropdownField;
/**
 * Class TravelPartner
 */
class TravelPartner extends DataObject
{

    /**
     * @var array
     */
    private static $db = [
        'CDP' => 'Varchar(50)',
        'Username' => 'Varchar(150)',
        'Password' => 'Varchar(150)',
    ];
    /**
     * @var array
     */
    private static $summary_fields = [
      'CDP','Username'
    ];
    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canView($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canEdit($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canCreate($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canDelete($member = null, $context = [])
    {
        return true;
    }
}
