<?php

use SilverStripe\Assets\Image;
use SilverStripe\Assets\File;
use SilverStripe\ORM\DataObject;

/**
 * Class Feature
 */
class Feature extends DataObject
{

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar(255)',
        'Description' => 'Text'
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'Image' => SilverStripe\Assets\Image::class
    ];

    /**
     * @var array
     */
    private static $owns = [
        'Image'
    ];

    /**
     * @desc Publish our dependent objects (which is a stupid thing to have to do tbh).
     */
    public function onAfterWrite()
    {
        $hasOnes = $this->owner->stat('has_one');
        foreach ($hasOnes as $relation => $class) {
            if ($class == Image::class || $class == File::class) {
                $this->publishRelatedObject($this->owner->$relation());
            }
        }

        $hasMultiples = array_merge($this->owner->stat('has_many'), $this->owner->stat('many_many'));
        foreach ($hasMultiples as $relation => $class) {
            if ($class == Image::class || $class == File::class) {
                foreach ($this->$relation() as $object) {
                    $this->publishRelatedObject($object);
                }
            }
        }

        parent::onAfterWrite();
    }

    /**
     * @param $object
     */
    protected function publishRelatedObject($object)
    {
        if ($object && $object->owner->exists()) {
            $object->owner->publishSingle();
        }
    }

    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canView($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canEdit($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canCreate($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canDelete($member = null, $context = [])
    {
        return true;
    }


}