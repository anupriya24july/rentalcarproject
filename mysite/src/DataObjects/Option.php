<?php

use SilverStripe\ORM\DataObject;
use AceRentals\Pages\VehiclePage;
use AceRentals\Pages\LocationPage;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\TextareaField;
/**
 * Class Option
 */
class Option extends DataObject
{

    /**
     * @var array
     */
    private static $db = [
        'Code' => 'Varchar(50)',
        'Description' => 'Varchar(250)',
        'Specifications' => 'Text',
        'Grouping' => 'Varchar(150)',
        'RateType' => 'Varchar(150)',
        'IncludedInTotal' => 'Boolean',
    ];

    /**
     * @var array
     */
    private static $summary_fields = [
      'Code' ,'Description', 'Grouping'
    ];
    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canView($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canEdit($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canCreate($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canDelete($member = null, $context = [])
    {
        return true;
    }
}
