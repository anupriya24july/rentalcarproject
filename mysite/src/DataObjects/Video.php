<?php

use SilverStripe\ORM\DataObject;

/**
 * Class Video
 */
class Video extends DataObject
{

    /**
     * @var array
     */
    private static $db = [
        'Language' => 'Varchar(255)',
        'VideoCode' => 'Varchar(255)',
    ];


    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canView($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canEdit($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canCreate($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canDelete($member = null, $context = [])
    {
        return true;
    }

}