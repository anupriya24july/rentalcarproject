<?php

use SilverStripe\ORM\DataObject;
use AceRentals\Pages\VehiclePage;
use AceRentals\Pages\LocationPage;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\DropdownField;
/**
 * Class Relocation
 */
class Relocation extends DataObject
{

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar(50)',
        'DiscountType' => 'Varchar(10)',
        'DiscountRate' => 'Varchar(10)',
        'LatestDropOffDate' => 'Date',
        'BookingDateFrom' => 'Date',
        'BookingDateTo' => 'Date',
        'CarSize' => 'Varchar(50)',
        'DropoffLocation' => 'Varchar(50)',
        'PickupLocation' => 'Varchar(50)',
        'MinBookingDays' => 'Varchar(10)',
        'MaxBookingDays' => 'Varchar(10)',
        'Available' => 'Int()',
        'FreeFuel' => 'Boolean',
        'DropoffID' => 'Int()',
        'PickupID' => 'Int()',
        'CarSizeID' => 'Int()',
        'RcmID' => 'Int()',
        'ShowInPage' => 'Boolean',
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'Group' => RelocationGroup::class
    ];

    /**
     * @var array
     */
    private static $summary_fields = [
      'Title'=> 'DiscountName',
      'PickupLocation'=> 'PickupLocation',
      'DropoffLocation'=> 'DropoffLocation',
      'CarSize'=> 'CarSize',
      'Available'=> 'Available',
      'showRelocationGroup' => 'Group'
    ];

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $currentUrlSegments=explode('/',SilverStripe\Control\Controller::curr()->getRequest()->getURL());
        if(!in_array("new", $currentUrlSegments) && $this->RcmID){
          if($this->DiscountType=='p'){
            $discountType='Percentage';
            $discount=$this->DiscountRate.' %';
          }else{
              $discountType='Per Day';
              $discount=$this->DiscountRate.' %';
          }
          $vehicleImage=VehiclePage::get()->filter(['CategoryTypeID' =>$this->CarSizeID])->first()->Image()->URL;
          $fields->insertBefore(
              new LiteralField(
                  "Summary",
                  "<table style='width:  50%;float: right;border-left: 1px' >
                  <tr><td style='padding: 5px;' colspan=2><center><h2>".$this->Title."</h2></center></td></tr><tr><td style='padding: 10px;' colspan=2><center>Vehicle Category<br><b>".$this->CarSize."</b><br><img height='200' width='350' src='".$vehicleImage."' alt='".$this->CarSize."'></center></td></tr>
                  <tr><td  style='padding: 10px;width:  50%;' ><center>Pickup Location<br><b>".$this->PickupLocation."</b></center></td><td  style='padding: 10px;width:  50%;' ><center>Dropoff Location<br><b>".$this->DropoffLocation."</b></center></td></tr>
                  <tr><td  style='padding: 10px;width:  50%;' ><center>Earliest Pickup date<br><b>".$this->EarliestPickupDate."</b></center></td><td  style='padding: 10px;width:  50%;' ><center>Latest DropOff Date<br><b>".$this->LatestDropOffDate."</b></center></td></tr>
                  <tr><td  style='padding: 10px;width:  50%;' ><center>Booking Date From<br><b>".$this->BookingDateFrom."</b></center></td><td  style='padding: 10px;width:  50%;' ><center>Booking Date To<br><b>".$this->BookingDateTo."</b></center></td></tr>
                  <tr><td  style='padding: 10px;width:  50%;' ><center>Discount Type<br><b>".$discountType."</b></center></td><td  style='width:  50%;' ><center>Discount<br><b>".$discount."</b></center></td></tr>
                  <tr><td  style='padding: 10px;width:  50%;' ><center>Max Booking Days<br><b>".$this->MaxBookingDays." Days</b></center></td><td  style='width:  50%;' ><center>Min Booking Days<br><b>".$this->MinBookingDays." Days</b></center></td></tr>
                  </table><br>"
              ),'Available');
          $fields->removeFieldFromTab('Root.Main',['CarSizeID' ,
              'EarliestPickupDate',
              'LatestDropOffDate',
              'MinBookingDays',
              'MaxBookingDays',
              'DiscountType',
              'DiscountRate',
              'Title',
              'BookingDateFrom',
              'BookingDateTo',
              'DropoffLocation',
              'PickupLocation',
              'CarSize',
              'DropoffID',
              'PickupID',
              'CarSizeID',
              'RcmID']);
      }else{
        $fields->addFieldsToTab('Root.Main', [
          new DropdownField(
              'DiscountType',
              'Discount Type',
              array( 'n' => 'Dollar (off per day rate)', 'p' => 'Percentage')),
          new DropdownField(
              'CarSizeID',
              'Vehicle Category',
              VehiclePage::get()->map('CategoryTypeID', 'Title')),
          new DropdownField(
              'DropoffID',
              'Dropoff Location:',
              LocationPage::get()->map('RcmLocationID', 'Title')),
          new DropdownField(
              'PickupID',
              'Pickup Location',
              LocationPage::get()->map('RcmLocationID', 'Title')),
        ]); 
        
      }
      $fields->removeFieldFromTab('Root.Main',[
              'DropoffLocation',
              'PickupLocation',
              'CarSize',
              'RcmID']);

      return $fields;
    }
     public function onBeforeWrite() {
        $this->CarSize=VehiclePage::get()->filter(['CategoryTypeID' =>$this->CarSizeID])->first()->Title;
        $this->DropoffLocation=LocationPage::get()->filter(['RcmLocationID' =>$this->DropoffID])->first()->Title;
        $this->PickupLocation=LocationPage::get()->filter(['RcmLocationID' =>$this->PickupID])->first()->Title;
        parent::onBeforeWrite();
    }
    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canView($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canEdit($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canCreate($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canDelete($member = null, $context = [])
    {
        return true;
    }
    
    public  function showRelocationGroup() {
        return $this->Group() ? $this->Group()->Title : '-';
    }
}
