<?php

use SilverStripe\Forms\DateField;
use SilverStripe\ORM\Filters\PartialMatchFilter;
use SilverStripe\ORM\Filters\LessThanOrEqualFilter;
use SilverStripe\ORM\Filters\GreaterThanFilter;
use SilverStripe\ORM\Search\SearchContext;
use SilverStripe\ORM\DataObject;

/**
 * Class ContactRequest
 */
class ContactRequest extends DataObject
{
    /**
     * @var array
     */
    private static $db = [
        'Name' => 'Varchar(255)',
        'Phone' => 'Varchar(16)',
        'Email' => 'Varchar(255)',
        'Type' => 'Varchar(255)',
        'Message' => 'Text',
        'Newsletter' => 'Boolean',
    ];

    /**
     * @var array
     */
    private static $summary_fields = [
        'Created',
        'Name',
        'Email'
    ];

    /**
     * @var string
     */
    private static $default_sort = 'Created DESC';

    /**
     * @return SearchContext
     */
    public function getDefaultSearchContext() {
        $fields = $this->scaffoldSearchFields([
            'restrictFields' => ['Title']
        ]);

        $fields->add(DateField::create('CreatedAfter', 'Created After')->setValue('showcalendar', true));
        $fields->add(DateField::create('CreatedBefore', 'Created Before')->setValue('showcalendar', true));

        $filters = [
            'Title' => new PartialMatchFilter('Title'),
            'CreatedAfter' => new GreaterThanFilter('Created'),
            'CreatedBefore' => new LessThanOrEqualFilter('Created'),
        ];

        return new SearchContext(
            static::class,
            $fields,
            $filters
        );
    }

    /**
     * @param Member $member
     * @return bool
     */
    public function canView($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param Member $member
     * @return bool
     */
    public function canEdit($member = null, $context = [])
    {
        return false;
    }

    /**
     * @param Member $member
     * @return bool
     */
    public function canCreate($member = null, $context = [])
    {
        return false;
    }

    /**
     * @param Member $member
     * @return bool
     */
    public function canDelete($member = null, $context = [])
    {
        return true;
    }

}