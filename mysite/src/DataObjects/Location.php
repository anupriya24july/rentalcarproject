<?php

use SilverStripe\ORM\DataObject;
use AceRentals\Pages\VehiclePage;
use AceRentals\Pages\LocationPage;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\DropdownField;
/**
 * Class Location
 */
class Location extends DataObject
{

    /**
     * @var array
     */
    private static $db = [
        'Code' => 'Varchar(50)',
        'Location' => 'Varchar(250)',
        'City' => 'Varchar(250)',
        'Hours' => 'Varchar(250)',
        'Latitude' => 'Varchar(250)',
        'Longitude' => 'Varchar(250)',
    ];

    /**
     * @var array
     */
    private static $summary_fields = [
      'Code' ,'Location', 'City'
    ];
    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canView($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canEdit($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canCreate($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canDelete($member = null, $context = [])
    {
        return true;
    }
}
