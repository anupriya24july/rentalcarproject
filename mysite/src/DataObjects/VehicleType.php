<?php

use SilverStripe\ORM\DataObject;

/**
 * Class VehicleType
 */
class VehicleType extends DataObject
{

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar(255)',
        'Sort' => 'Int',
    ];

    /**
     * @var string
     */
    private static $default_sort = 'Sort ASC';


    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canView($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canEdit($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canCreate($member = null, $context = [])
    {
        return true;
    }

    /**
     * @param Member $member
     * @param array $context
     * @return bool
     */
    public function canDelete($member = null, $context = [])
    {
        return true;
    }


}