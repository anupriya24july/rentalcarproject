<div class="c-vehicle-card<% if $Type %> c-vehicle-card__{$Type}<% end_if %> js-vehicle-card">
  <h3 class="c-vehicle-card__title notranslate">{$Title}</h3>
  <h4 class="c-vehicle-card__subtitle">{$Age}</h4>
  <div class="c-vehicle-card__image">
    <img src="{$Image.URL}" alt="{$Title}">
  </div>
  <% if $VehDesc %>
    <% if $VehicleDescription %>
      <ul class="c-vehicle-card__specifications">
          <h5 class="c-vehicle-card__specification c-vehicle-card__specification--fw js-vehicle-spec">
            {$VehicleDescription}
          </h5>
        <div class="c-vehicle-card__footer">
          <a href="{$Link}" class="c-vehicle-card__more btn btn--primary">
            Select
          </a>
        </div>
      </ul>
    <% end_if %>
  <% else %>
    <% if $Specifications %>
      <ul class="c-vehicle-card__specifications">
        <% loop $Specifications %>
          <h5 class="c-vehicle-card__specification<% if $FullWidth %> c-vehicle-card__specification--fw<% end_if %> js-vehicle-spec<% if $Pos > 4 %> js-vehicle-spec-more is-hidden<% end_if %>">
            <% if $Icon %><img class="c-vehicle-card__specification-icon" src="{$Icon.URL}" alt="{$Title}"><% end_if %>
            {$Title}
          </h5>
        <% end_loop %>
      </ul>
      <% if $Specifications.Count > 4 %>
        <button class="c-vehicle-card__specifications-more js-vehicle-specs-trigger">
          {$SVG('more-plus')}
          <span>View more features</span>
        </button>
      <% end_if %>
      <% if $showDescription %>
        <h5 class="c-vehicle-card__specification c-vehicle-card__specification--fw js-vehicle-spec">
          {$Content}
        </h5>
      <% end_if %>
    <% end_if %>

    <% if $CTA == 'Book' && $Price %>
      <div class="c-vehicle-card__footer c-vehicle-card__footer--book">
        <div class="c-vehicle-card__pricing">
          <div class="c-vehicle-card__price-label">
            From
          </div>
          <div class="c-vehicle-card__price">
            ${$Price} <span>NZD</span>
          </div>
        </div>
        <a target="<% if $URL %>_blank<% else %>_self<% end_if %>" href="<% if $URL %>$URL<% else %>#<% end_if %>" class="c-vehicle-card__action btn btn--primary <% if $URL %><% else %>js-booking-panel-toggle<% end_if %>">
          <% if $CTALabel %>$CTALabel<% else %>Select<% end_if %>
        </a>
      </div>
    <% else %>
      <div class="c-vehicle-card__footer">
        <a href="{$Link}" class="c-vehicle-card__more btn btn--primary">
          Select
        </a>
      </div>
    <% end_if %>
  <% end_if %>
</div>
