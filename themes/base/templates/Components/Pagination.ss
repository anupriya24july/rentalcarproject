<% if $DataList.MoreThanOnePage %>
    <% if $DataList.NotFirstPage %>
        <a href="$DataList.PrevLink" title="View the previous page">Prev</a>
    <% end_if %>
    <% loop $DataList.Pages(7) %>
        <% if $CurrentBool %>
            $PageNum
        <% else %>
            <% if $Link %>
                <a href="$Link" title="View page number $PageNum">$PageNum</a>
            <% end_if %>
        <% end_if %>
    <% end_loop %>
    <% if $DataList.NotLastPage %>
        <a href="$DataList.NextLink" title="View the next page">Next</a>
    <% end_if %>
<% end_if %>