<div class="c-relocation-card">
  <h2 class="c-relocation-card__title">
    {$Title}
  </h2>
  <img class="c-relocation-card__image" src="{$Me.Image.URL}"/>
  <div class="c-relocation-card__dates">
    {$Description}
  </div>
  <div class="c-relocation-card__footer">
    <a href="/{$segment}/{$action}/{$Me.ID}" id="{$Me.ID}" class="c-relocationgroup-card__action btn btn--secondary js-group-toggle">
      View Deals
    </a>
  </div>
</div>
