<div class="c-option-card<% if $Type %> c-option-card--{$Type}<% end_if %><% if $Selected %> is-selected<% end_if %>">
  <% if $Popular %>
    <div class="c-option-card__label">
      Most Popular
    </div>
  <% end_if %>

  <div class="c-option-card__main">
    <div class="c-option-card__title">{$Title}</div>
    <div class="c-option-card__price">{$Price}</div>
    <% if $Note %><div class="c-option-card__note">{$Note}</div><% end_if %>
  </div>

  <% if $Qty %>
    <div class="c-option-card__qty">
      <div class="l-form__field l-form__field--fw l-form__field--select">
        <div class="l-form__select">
          <select class="l-form__input l-form__input--select">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
          </select>
          {$SVG('defaults/select-arrow')}
        </div>
        <label class="l-form__label<% if $Label %> l-form__label--{$Label}<% end_if %>" for="{$Name}">{$Title}</label>
      </div>
    </div>
  <% end_if %>

  <% if $Features %>
    <ul class="c-option-card__features">
      <li class="c-option-card__feature">
        <div class="c-option-card__feature-title">Excess</div>
        <div class="c-option-card__feature-detail">
          $500
        </div>
      </li>
      <li class="c-option-card__feature">
        <div class="c-option-card__feature-title">Bond</div>
        <div class="c-option-card__feature-detail">
          $500
        </div>
      </li>
      <li class="c-option-card__feature is-included">
        <div class="c-option-card__feature-title">Windscreen Cover</div>
        <div class="c-option-card__feature-value">
          {$SVG('option-feature-check')}
        </div>
      </li>
      <li class="c-option-card__feature">
        <div class="c-option-card__feature-title">Windscreen Cover</div>
        <div class="c-option-card__feature-value">
          {$SVG('option-feature-cross')}
        </div>
      </li>
    </ul>
  <% end_if %>

  <% if $Details %>
    <button class="c-option-card__more js-option-card-details-trigger">
      {$SVG('more-plus')} See more details
    </button>
  <% end_if %>
</div>
