<div class="l-booking__step">
    <h2 class="l-booking__step-heading">
      1. Please choose an insurance option
      <a href="#" class="l-booking__video-link">
        {$SVG('video-link')}
        Insurance Explainer Video
      </a>
    </h2>

    <div class="l-booking__age l-form">
      <div class="l-form__field l-form__field--radio">
        <input class="l-form__input l-form__input--radio" type="radio" name="BookingAge" id="BookingAgeOver18">
        <label class="l-form__label" for="BookingAgeOver18">I am over 18</label>
      </div>
      <div class="l-form__field l-form__field--radio">
        <input class="l-form__input l-form__input--radio" type="radio" name="BookingAge" id="BookingAgeUnder18">
        <label class="l-form__label" for="BookingAgeUnder18">I am under 18</label>
      </div>
    </div>

    <div class="l-booking__options">
      <% include Booking/Option_Card Type="lg", Title="Ace Plus", Price="+ $24.00 /day", Features="true", Details="true", Popular="true", Selected="true" %>
      <% include Booking/Option_Card Type="lg", Title="Ace Minimum Cover", Price="FREE", Features="true", Details="true" %>
      <% include Booking/Option_Card Type="lg", Title="Ace Excess Reduction", Price="+ $19.00 /day", Features="true", Details="true" %>
    </div>
</div>
