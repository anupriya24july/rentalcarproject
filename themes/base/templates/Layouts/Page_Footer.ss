<footer class="l-footer">
    <div class="container">
        <nav class="l-footer__nav">
            <ul class="l-footer__nav-items">
                <li class="l-footer__nav-title">{$getLabels.QuicklinksLabel}</li>
                <li class="l-footer__nav-item">
                    <a href="/{$getLocaleSegment}our-cars" class="l-footer__nav-link">{$getLabels.OurVehiclesLabel}</a>
                </li>
                <% if $QuickLinks %>
                    <% loop $QuickLinks %>
                        <li class="l-footer__nav-item">
                            <a href="{$Link}" class="l-footer__nav-link">{$MenuTitle}</a>
                        </li>
                    <% end_loop %>
                <% end_if %>
                <li class="l-footer__nav-item">
                    <a href="/{$getLocaleSegment}deals/?type=relocation-deals" class="l-footer__nav-link">{$getLabels.RelocationsLabel}</a>
                </li>
                <li class="l-footer__nav-item">
                    <a href="/{$getLocaleSegment}deals" class="l-footer__nav-link">{$getLabels.DealsLabel}</a>
                </li>
            </ul>

            <ul class="l-footer__nav-items">
                <li class="l-footer__nav-title">{$getLabels.OurLocationsLabel}</li>
                <li class="l-footer__nav-item">
                    <a href="/{$getLocaleSegment}locations" class="l-footer__nav-link">New Zealand</a>
                </li>
                <li class="l-footer__nav-item">
                    <a href="/{$getLocaleSegment}locations" class="l-footer__nav-link">Australia</a>
                </li>
            </ul>

            <ul class="l-footer__nav-items">
                <li class="l-footer__nav-title">{$getLabels.AboutUsLabel}</li>
                <li class="l-footer__nav-item">
                    <a href="/{$getLocaleSegment}ace-rental-cars-customer-reviews"
                       rel="noopener noreferrer" class="l-footer__nav-link">{$getLabels.ReviewsLabel}</a>
                </li>
                <% if $DrivingSafelyPage %>
                    <% with $DrivingSafelyPage %>
                        <li class="l-footer__nav-item">
                            <a href="$Link" class="l-footer__nav-link">$MenuTitle</a>
                        </li>
                    <% end_with %>
                <% end_if %>
                <% if $InformationPages %>
                    <% loop $InformationPages %>
                        <li class="l-footer__nav-item">
                            <a href="{$Link}" class="l-footer__nav-link">{$MenuTitle}</a>
                        </li>
                    <% end_loop %>
                <% end_if %>
                <li class="l-footer__nav-item">
                    <a href="/{$getLocaleSegment}contact-us" class="l-footer__nav-link">{$getLabels.ContactUsLabel}</a>
                </li>
            </ul>

            <div class="l-footer__social">
                <ul class="l-footer__social-items">
                    <li class="l-footer__social-item">
                        <a href="https://www.facebook.com/AceRentalCars/" class="l-footer__social-link" target="_blank"
                           rel="noopener noreferrer">
                            {$SVG('social/facebook')}
                            <span class="sr-only">Facebook</span>
                        </a>
                    </li>
                    <li class="l-footer__social-item">
                        <a href="https://twitter.com/acerentalcars" class="l-footer__social-link" target="_blank"
                           rel="noopener noreferrer">
                            {$SVG('social/twitter')}
                            <span class="sr-only">Twitter</span>
                        </a>
                    </li>
                    <li class="l-footer__social-item">
                        <a href="<% if $SiteConfig.CountryVersion = 'New Zealand' %>https://plus.google.com/113066727800228195295<% else %>https://plus.google.com/+AcerentalcarsAustralia<% end_if %>" class="l-footer__social-link" target="_blank"
                           rel="noopener noreferrer">
                            {$SVG('social/google')}
                            <span class="sr-only">Google+</span>
                        </a>
                    </li>
                </ul>
                <div class="qualmark">
                    <img src="/themes/base/production/images/qualmark.png" alt="Endorsed Visitor Transport - Qualmark">
                </div>
            </div>
        </nav>
    </div>

    <div class="l-footer__fineprint">
        <div class="container">
      <span class="l-footer__copyright">
        &copy; {$Now.Year} {$SiteConfig.Title}
      </span>
            <nav class="l-footer__fineprint-nav">
                <ul class="l-footer__fineprint-nav-items">
                    <li class="l-footer__fineprint-nav-item">
                        <a href="/{$getLocaleSegment}terms" class="l-footer__fineprint-nav-link">{$getLabels.TermsLabel}</a>
                    </li>
                    <li class="l-footer__fineprint-nav-item">
                        <a href="/{$getLocaleSegment}privacy" class="l-footer__fineprint-nav-link">{$getLabels.PrivacyPolicyLabel}</a>
                    </li>
                </ul>
            </nav>
            <a href="https://www.littlegiant.co.nz/" class="l-footer__fineprint-lg" target="_blank"
               rel="noopener noreferrer">
                {$SVG('littlegiant-mammoth')}
                Solution by Little Giant
            </a>
        </div>
    </div>
</footer>
