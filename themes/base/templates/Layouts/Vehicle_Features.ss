<% if $Features %>
  <section class="l-vehicle-features">
    <h2 class="l-vehicle-features__heading">
      This vehicle is perfect for...
    </h2>
    <div class="container">
      <% loop $Features %>
        <div class="l-vehicle-features__feature">
          <% if $Image %>
            <div class="l-vehicle-features__feature-image">
              <img src="{$Image.URL}" alt="{$Title}" />
            </div>
          <% end_if %>
          <div class="l-vehicle-features__feature-content">
            <h3 class="l-vehicle-features__feature-title">{$Title}</h3>
            <div class="l-vehicle-features__feature-summary">{$Description}</div>
          </div>
        </div>
      <% end_loop %>
    </div>
  </section>
<% end_if %>
