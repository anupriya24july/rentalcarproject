<section class="l-split-section<% if $NarrowContent %> l-split-section--narrow-content<% end_if %><% if $Reverse && $Reverse != 0 && $Reverse != '0' %> l-split-section--reverse<% end_if %>">
    <div class="container">
        <div class="l-split-section__column l-split-section__column--content">
            <% if $Pos== 1 %>
            <h1 class="l-split-section__title">
                {$Title}
            </h1>
            <% else %>
            <h2 class="l-split-section__title_h2">
                {$Title}
            </h2>
            <% end_if %>
            <% if $Subtitle %>
                <h2 class="l-split-section__subtitle">{$Subtitle}</h2>
            <% end_if %>
            {$Content}

            <% if $Specifications %>
              <ul class="l-split-section__specifications js-vehicle-card">
                <% loop $Specifications %>
                  <li class="l-split-section__specification<% if $FullWidth %> c-vehicle-card__specification--fw<% end_if %> js-vehicle-spec<% if $Pos > 4 %> js-vehicle-spec-more is-hidden<% end_if %>">
                    <% if $Icon %><img class="l-split-section__specification-icon" src="{$Icon.URL}" alt="{$Title}"><% end_if %>
                    {$Title}
                  </li>
                <% end_loop %>
              </ul>
              <% if $Features.Count > 4 %>
                <button class="l-split-section__specifications-more js-vehicle-specs-trigger">
                  {$SVG('more-plus')}
                  <span>VEHICLE &amp; RATE DETAILS</span>
                </button>
              <% end_if %>
            <% end_if %>
        </div>
        <% with $Image %>
            <div class="l-split-section__column l-split-section__column--image">
                <img src="$URL" alt="$Up.Title"/>
            </div>
        <% end_with %>
    </div>
</section>
