<% if $FeaturedDeals %>
  <section class="l-popular-cars">
    <div class="container">
      <h1 class="l-popular-cars__heading">{$getLabels.FeaturedDealsLabel}</h1>
      <div class="l-deals__popular js-deals-carousel swiper-container">
          <div class="swiper-wrapper">
            <% loop $FeaturedDeals %>
              <div class="swiper-slide">
                <% include Components/Promo_Card Type="lg" %>
              </div>
            <% end_loop %>
          </div>
          <div>
          <% if $FeaturedDeals.Count > 3 %> 
            <div class="deal-swiper-button-next swiper-button-blue"></div>
            <div class="deal-swiper-button-prev swiper-button-blue-prev"></div>
          <% end_if %>  
      </div>
      <div class="l-popular-cars__more">
        <% include Components/Arrow_Link Text=$getLabels.SeeAllDealsLabel, Link="/deals" %>
      </div>
    </div>
  </section>
<% end_if %>
