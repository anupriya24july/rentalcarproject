<% if $WhyFeatures %>
  <section class="l-why-features">
    <div class="container">
      <h2 class="l-why-features__heading">
        {$getLabels.WhyFeaturesLabel}...
      </h2>
      <div class="l-why-features__features">
        <% loop $WhyFeatures.limit(4) %>
            <div class="l-why-features__feature">
              <img class="l-why-features__feature-image" src="{$Image.URL}" alt="{$Title} "/>
              <h2 class="l-why-features__feature-title">
                {$Title}
              </h2>
              <div class="l-why-features__feature-description">
                {$Description}
              </div>
            </div>
        <% end_loop %>
      </div>
    </div>
  </section>
<% end_if %>
