<% if $RelatedVehicles %>
  <section class="l-related-vehicles">
    <div class="container">
      <h2 class="l-related-vehicles__heading">
        You may also like these Rental Cars...
      </h2>

      <div class="l-related-vehicles__cards js-vehicles-carousel  swiper-container">
        <div class="swiper-wrapper">
          <% loop $RelatedVehicles.limit(3) %>
            <div class="swiper-slide">
              <% include Components/Vehicle_Card %>
            </div>
          <% end_loop %>
        </div>
      </div>
    </div>
  </section>
<% end_if %>
