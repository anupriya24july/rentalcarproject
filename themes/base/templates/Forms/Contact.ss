<% with $ContactForm %>
    <% if $Message %>
        <div class="alert<% if $MessageType='bad' %> alert--danger<% else_if $MessageType='good' %> alert--success<% end_if %>">
            {$Message}
        </div>
    <% end_if %>

    <form class="l-form" role="form" $FormAttributes novalidate name="ContactForm" id="ContactForm">
        <% include Forms/Field Name="Name", Modifier="fw" %>
        <% include Forms/Field Name="Email", Modifier="fw" %>
        <% include Forms/Field Name="Type", Type="select" %>
        <% include Forms/Field Name="Phone" %>
        <% include Forms/Field Name="Message", Modifier="fw", Type="textarea" %>

        {$Fields.dataFieldByName('Information').FieldHolder}
        {$Fields.dataFieldByName('SecurityID')}

        <div class="l-form__field l-form__field--fw">
            <button
              id="ContactSubmit"
              class="l-contact__submit btn btn--secondary"
              type="submit"
              data-flare='{
                "category": "Form",
                "action": "Enquiry",
                "label": "Enquiry Submitted"
              }'>
                Enquire Now
            </button>
        </div>
    </form>
<% end_with %>
