<div class="l-vehicle js-overlay-header">
  <section class="l-vehicle__hero"<% if $BackgroundImage %> style="background-image: url('{$BackgroundImage.URL}')"<% end_if %>>
    <div class="container">
      <% include Layouts/Vehicle_Details %>
      <div class="l-vehicle__image">
        <img src="{$CarImage.URL}" alt="{$Title}">
      </div>
    </div>
  </section>

  <div class="l-arrow-container">

    <% include Layouts/Vehicle_Media %>

    <% if $Content %>
      <br /><br /><br />
      <div class="l-page__container">
        <div class="l-page__content">
          {$Content}
        </div>
      </div>
    <% end_if %>

    <% include Layouts/Vehicle_Features %>
    <% include Layouts/Related_Vehicles %>
  </div>
</div>
