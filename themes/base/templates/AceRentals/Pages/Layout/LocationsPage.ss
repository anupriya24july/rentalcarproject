<div class="l-locations">
  <div class="l-locations__map js-locations-map"></div>
  <div class="l-locations__depots js-locations-depots" data-json="{$getLocationsJSON}" data-sitecountry="{$SiteConfig.CountryVersion}">
    <% if $SiteConfig.CountryVersion == 'Australia' %>
      <div class="l-locations__country l-locations__country--au js-locations-country" data-country="Australia" role="button" tabindex="0">
        Australia Depots
      </div>
      <div class="l-locations__depot-group js-depot-group" data-country="Australia">
        <% loop $Locations %>
          <% if $Country == 'Australia' %>
            <div class="l-locations__depot js-depot" data-depot="{$ID}">
              <div class="l-locations__depot-name notranslate">{$Title}</div>
              <div class="l-locations__depot-address notranslate">{$SVG('pin')} {$Address}</div>
              <% if $PhoneNumber || $FaxNumber %>
                <div class="l-locations__depot-numbers">
                  <% if $PhoneNumber %>
                    <div class="l-locations__depot-number">
                      <span class="l-locations__depot-label notranslate">TF</span>
                      <a href="tel:{$PhoneNumber}">{$FaxNumber}</a>
                    </div>
                  <% end_if %>
                  <% if $FaxNumber %>
                    <div class="l-locations__depot-number">
                      <span class="l-locations__depot-label notranslate">PH</span>
                      <a href="tel:{$FaxNumber}">{$PhoneNumber}</a>
                    </div>
                  <% end_if %>
                </div>
              <% end_if %>
              <a class="l-locations__depot-details" href="{$Link}">View Details</a>
            </div>
          <% end_if %>
        <% end_loop %>
      </div>

      <div class="l-locations__country l-locations__country--nz js-locations-country" data-country="New Zealand" role="button" tabindex="0">
        New Zealand Depots
      </div>
      <div class="l-locations__depot-group js-depot-group" data-country="New Zealand">
        <% loop $Locations %>
          <% if $Country == 'New Zealand' %>
            <div class="l-locations__depot js-depot" data-depot="{$ID}">
              <div class="l-locations__depot-name notranslate">{$Title}</div>
              <div class="l-locations__depot-address notranslate">{$SVG('pin')} {$Address}</div>
              <% if $PhoneNumber || $FaxNumber %>
                <div class="l-locations__depot-numbers">
                  <% if $PhoneNumber %>
                    <div class="l-locations__depot-number">
                      <span class="l-locations__depot-label notranslate">TF</span>
                      <a href="tel:{$PhoneNumber}">{$FaxNumber}</a>
                    </div>
                  <% end_if %>
                  <% if $FaxNumber %>
                    <div class="l-locations__depot-number">
                      <span class="l-locations__depot-label notranslate">PH</span>
                      <a href="tel:{$FaxNumber}">{$PhoneNumber}</a>
                    </div>
                  <% end_if %>
                </div>
              <% end_if %>
              <a class="l-locations__depot-details" href="{$Link}">View Details</a>
            </div>
          <% end_if %>
        <% end_loop %>
      </div>
    <% else_if $SiteConfig.CountryVersion == 'New Zealand' %>
      <div class="l-locations__country l-locations__country--nz js-locations-country" data-country="New Zealand" role="button" tabindex="0">
        New Zealand Depots
      </div>
      <div class="l-locations__depot-group js-depot-group" data-country="New Zealand">
        <% loop $Locations %>
          <% if $Country == 'New Zealand' %>
            <div class="l-locations__depot js-depot" data-depot="{$ID}">
              <div class="l-locations__depot-name notranslate">{$Title}</div>
              <div class="l-locations__depot-address notranslate">{$SVG('pin')} {$Address}</div>
              <% if $PhoneNumber || $FaxNumber %>
                <div class="l-locations__depot-numbers">
                  <% if $PhoneNumber %>
                    <div class="l-locations__depot-number">
                      <span class="l-locations__depot-label notranslate">TF</span>
                      <a href="tel:{$PhoneNumber}">{$FaxNumber}</a>
                    </div>
                  <% end_if %>
                  <% if $FaxNumber %>
                    <div class="l-locations__depot-number">
                      <span class="l-locations__depot-label notranslate">PH</span>
                      <a href="tel:{$FaxNumber}">{$PhoneNumber}</a>
                    </div>
                  <% end_if %>
                </div>
              <% end_if %>
              <a class="l-locations__depot-details" href="{$Link}">View Details</a>
            </div>
          <% end_if %>
        <% end_loop %>
      </div>

      <div class="l-locations__country l-locations__country--au js-locations-country" data-country="Australia" role="button" tabindex="0">
        Australia Depots
      </div>
      <div class="l-locations__depot-group js-depot-group" data-country="Australia">
        <% loop $Locations %>
          <% if $Country == 'Australia' %>
            <div class="l-locations__depot js-depot" data-depot="{$ID}">
              <div class="l-locations__depot-name notranslate">{$Title}</div>
              <div class="l-locations__depot-address notranslate">{$SVG('pin')} {$Address}</div>
              <% if $PhoneNumber || $FaxNumber %>
                <div class="l-locations__depot-numbers">
                  <% if $PhoneNumber %>
                    <div class="l-locations__depot-number">
                      <span class="l-locations__depot-label notranslate">TF</span>
                      <a href="tel:{$PhoneNumber}">{$FaxNumber}</a>
                    </div>
                  <% end_if %>
                  <% if $FaxNumber %>
                    <div class="l-locations__depot-number">
                      <span class="l-locations__depot-label notranslate">PH</span>
                      <a href="tel:{$FaxNumber}">{$PhoneNumber}</a>
                    </div>
                  <% end_if %>
                </div>
              <% end_if %>
              <a class="l-locations__depot-details" href="{$Link}">View Details</a>
            </div>
          <% end_if %>
        <% end_loop %>
      </div>
    <% end_if %>
  </div>
</div>
