<nav class="l-navigation">
  <ul class="l-navigation__items">
      <% loop $Menu(1) %>
        <li class="l-navigation__item<% if $ClassName == 'AceRentals\Pages\ContactPage' %> l-navigation__item--contact<% else_if $ClassName == 'AceRentals\Pages\DealsPage' %> l-navigation__item--deals<% end_if %>">
          <a href="{$Link}" class="js-nav-link l-navigation__link l-navigation__link--<% if $LinkingMode='current' %>current<% else %>{$LinkingMode}<% end_if %>">
            {$MenuTitle.XML}
          </a>
        </li>
      <% end_loop %>
      <li class="l-navigation__item">
          <a href="<% if $SiteConfig.CountryVersion = 'New Zealand' %>https://blog.acerentalcars.co.nz<% else %>https://blog.acerentalcars.com.au<% end_if %>" target="_blank" class="js-nav-link l-navigation__link l-navigation__link">
            BLOG
          </a>
       </li>
  </ul>
</nav>
