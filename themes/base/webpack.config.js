const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const CompressionPlugin = require("compression-webpack-plugin");

const PRODUCTION = process.env.NODE_ENV === 'production';
const PORT = process.env.PORT || 3000;
const EXCLUDE_JS = [
  /node_modules\/(?!(dom7|swiper)\/).*/,
];

/**
 *
 * @type {*[]}
 */
const plugins = [
  new webpack.optimize.ModuleConcatenationPlugin(),
  new StyleLintPlugin({
    configFile: './.stylelintrc',
    context: './source/sass/**/',
    files: '*.s?(a|c)ss',
    syntax: 'scss',
    emitErrors: true,
    failOnError: false,
  }),
];

/**
 * Plugins that will only be included in development build
 * @type {Array}
 */
const devPlugins = [
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NamedModulesPlugin(),
];

/**
 * Plugins that will only be included in production build
 * @type {*[]}
 */
const productionPlugins = [
  new ExtractTextPlugin({
    filename: 'css/[name].css',
  }),
  new webpack.optimize.UglifyJsPlugin({ minimize: true }),
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify('production'),
    },
  }),
  new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
  new CompressionPlugin({
    asset: "[path].gz[query]",
    algorithm: "gzip",
    test: /\.js$|\.css$|\.html$/,
    threshold: 10240,
    minRatio: 1
  })
];

/**
 * Dev Sass loaders
 * @type {[*]}
 */
const sassDev = [
  'style-loader',
  {
    loader: 'css-loader',
    options: {
      root: '/themes/base/',
      import: true,
      importLoaders: 1,
    },
  },
  'postcss-loader',
  {
    loader: 'sass-loader',
  },
];

/**
 * Because ExtractTextPlugin does not work with hot module replacement, only use it for production
 * @type {[*]}
 */
const sassProd = ExtractTextPlugin.extract({
  // style loader first
  fallback: 'style-loader',
  use: [{
    loader: 'css-loader',
    options: {
      minimize: true,
      importLoaders: 1,
      url: false,
    },
  }, 'postcss-loader', 'sass-loader'],
});

const sassConfig = PRODUCTION ? sassProd : sassDev;

/**
 * Loaders
 * @type {[*]}
 */
const loaders = [
  {
    test: /.js$/,
    use: 'eslint-loader',
    exclude: EXCLUDE_JS,
    enforce: 'pre', // this is a pre-loader
  },
  {
    test: /\.js$/,
    use: 'babel-loader',
    exclude: EXCLUDE_JS,
    include: __dirname,
  },
  {
    test: /\.scss$/,
    use: sassConfig,
  },
  {
    test: /\.(png|jpg|gif)$/,
    use: [
      {
        loader: 'file-loader',
      },
    ],
  },
  {
    test: /\.svg$/,
    use: [
      {
        loader: `file-loader?name=[path][name].[ext]&context=source/&publicPath=themes/base/${PRODUCTION ? 'production' : 'source'}/`,
      },
    ],
  },
];

module.exports = {
  entry: {
    editor: './source/editor.js',
    main: (PRODUCTION) ? './source/production.js' : './source/index.js',
  },
  output: {
    filename: 'js/[name].js',
    path: path.join(__dirname, 'production'),
    publicPath: `http://localhost:${PORT}/`,
  },
  module: {
    rules: loaders,
  },
  devServer: {
    publicPath: '/',
    compress: true,
    port: PORT,
    hot: true,
    hotOnly: false,
    historyApiFallback: false,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    stats: {
      colors: true,
      chunks: false,
      hash: false,
      version: false,
      assets: false,
    },
  },
  plugins: PRODUCTION ? plugins.concat(productionPlugins) : plugins.concat(devPlugins),
};
