// load production deps
import './index';

// requires and returns all modules that match
// reading - https://webpack.github.io/docs/context.html#context-module-api
function requireAll(requireContext) {
  return requireContext.keys().map(requireContext);
}

// Require all images, fonts and svg's
requireAll(require.context('./svg', true, /.*/));
