/*!
*                                        .sdNMMMNmy
*                                       :NMMMMMMMMMM
*                            .+hmNNdysohMMMMMMMMMMMMN-
*                         .+dMMMMMMMMMMMMMMMMMMMMMMMMN+
*               `-:/+osydNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMh.    `
*         `:oydNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm-   o/
*      `+dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm`  yd         `
*     +NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM/  dN`       -y
*   `yMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMNmMMMM+ +Ms        sN
*   yMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMmo:mMNMmmMMMydm+        .MM
*  :MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM:  .ooMMhNMN:.          yMd
*  yMdNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM:     +MMddd          .yMM:
*  ms+MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN+      -dmMNh+::---:/ohMMm:
*  M/`dMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMN.       `NNdmdmNMMMMNmdho:
*  Ns .mMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMm-         oMMMo   ``
*  -+  yMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM/           `mMMd
*     .NMMmNMMMMMMMMMMMMMMMMMNMMMMMMMMMMMy            .mMMo
*     hMMMdhMMMMMMMNMMMMMMMmhyyhMMMMMMMMMMs            .dMMh-
*    sMMMMsNMMMMMMd+syyso/ymMMMm+yMMMMMMMMMh`            /dMMms+/+o`
*   /MMMMMoMMMMMMMo       dMMMMM+ .yMMMMMMMMh              .+ydmNNh
*   mMMMMM+MMMMMMMs       oMMMMMs   :NMMMMMMM`
*   NMMMMM:NMMMMMMs       .NMMMMN`   -NMMMMMm
*   NMMMMMsoMMMMMM:       `NMMMMMd`   sMMMMMs
*   :osyso.`NMMMMMo       `+yhhys:    :MMMMMm-
*          .NMMMMMMd-                 /MMMMMMM:
*          `ohmNMMNh.                 `+yhdmds.
*
*  Solution by Little Giant
*
*/

import objectFitImages from 'object-fit-images';

// Components
import MobNav from './components/mobile-nav';
import Notifications from './components/notifications';
import Share from './components/share';
// import Forms from './components/forms';
import Modals from './components/modals';
import Hero from './components/hero';
// import Scroll from './components/scroll';
import Header from './components/header';
import BookingForm from './components/booking-form';
// import CreditCard from './components/credit-card';
import ToolTips from './components/tooltips';
import Experiences from './components/experiences';
import Deals from './components/deals';
import Video from './components/video';
import Vehicle from './components/vehicle';
import Locations from './components/locations';
import Gallery from './components/gallery';
import BookingPanel from './components/booking-panel';
// import PassKit from './components/passkit';
import Popup from './components/popup';

document.addEventListener('DOMContentLoaded', () => {
  document.components = [
    new MobNav(),
    new Notifications(),
    new Share(),
    // new Forms(),
    new Modals(),
    // new Scroll(),
    new Header(),
    new Hero(),
    new BookingForm(),
    // new CreditCard(),
    new ToolTips(),
    new Experiences(),
    new Deals(),
    new Video(),
    new Vehicle(),
    new Locations(),
    new Gallery(),
    new BookingPanel(),
    // new PassKit(),
    new Popup(),
  ];

  objectFitImages();
});

