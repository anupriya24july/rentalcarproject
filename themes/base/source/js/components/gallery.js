import baguetteBox from 'baguettebox.js';

export default class Gallery {
  constructor() {
    this.vehicleGallery();
  }

  vehicleGallery() {
    this.fish = 'fsdf';
    baguetteBox.run('.js-vehicle-gallery', {
      overlayBackgroundColor: 'rgba(255, 255, 255, 0.81)',
      noScrollbars: true,
    });
  }
}
