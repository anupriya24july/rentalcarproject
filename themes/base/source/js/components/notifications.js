export default class Notifications {
  constructor() {
    Notifications.closeTriggers();
    Notifications.timeout();
  }

  static closeNotification(el) {
    el.classList.add('c-notification--hidden');
  }

  static closeTriggers() {
    const closeNotificationTriggers = Array.from(document.querySelectorAll('.js-notification-close'));

    if (closeNotificationTriggers.length) {
      closeNotificationTriggers.forEach((trigger) => {
        trigger.addEventListener('click', Notifications.closeNotification(trigger));
      });
    }
  }

  static timeout() {
    const timeoutNotifications = Array.from(document.querySelectorAll('.c-notification--timeout'));

    if (timeoutNotifications.length) {
      timeoutNotifications.forEach((notification) => {
        window.setTimeout(() => {
          Notifications.closeNotification(notification, true);
        }, 5000);
      });
    }
  }
}
