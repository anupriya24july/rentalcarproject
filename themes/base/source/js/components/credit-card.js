import Cleave from 'cleave.js';

export default class Scroll {
  constructor() {
    this.formatFields();
  }

  formatFields() {
    if (document.querySelector('.js-cc-num')) {
      this.number = new Cleave('.js-cc-num', {
        creditCard: true,
        delimiter: ' - ',
        onCreditCardTypeChanged: (type) => {
          const visa = document.querySelector('.js-cc-visa');
          const mastercard = document.querySelector('.js-cc-mastercard');
          const unionpay = document.querySelector('.js-cc-unionpay');

          visa.classList.remove('is-active');
          mastercard.classList.remove('is-active');
          unionpay.classList.remove('is-active');

          if (type === 'visa') {
            visa.classList.add('is-active');
          } else if (type === 'mastercard') {
            mastercard.classList.add('is-active');
          } else {
            visa.classList.add('is-active');
            mastercard.classList.add('is-active');
            unionpay.classList.add('is-active');
          }
        },
      });
    }

    if (document.querySelector('.js-cc-exp')) {
      this.expiry = new Cleave('.js-cc-exp', {
        date: true,
        datePattern: ['m', 'y'],
        delimiter: ' / ',
      });
    }
  }
}
