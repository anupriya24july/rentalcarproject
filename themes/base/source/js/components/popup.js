export default class Popup {
  constructor() {
    const pathArray = window.location.pathname.split('/');
    if (pathArray[1] !== 'bookings') {
      window.addEventListener('scroll', () => {
        if (document.querySelector('.leadinModal-preview')) {
          document.querySelector('.leadin-preview-wrapper').addEventListener('click', () => {
            document.querySelector('.leadinModal-form').style.display = 'none';
            const homepage = '/deals/relocations';
            window.location.replace(homepage);
          });
        }
      });
    }
  }
}
