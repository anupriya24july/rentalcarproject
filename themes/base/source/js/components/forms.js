// ========================================================================
// Forms
// ========================================================================

import Pikaday from 'pikaday';
import FormValidation from './validation';

export default class Forms {
  constructor() {
    this.validation = new FormValidation();

    this.smallLabel();
    this.autoTab();
    // this.dateFields();
  }

  dateFields() {
    this.dateContainers = document.querySelectorAll('.js-date-container');

    if (this.dateContainers) {
      this.dateContainers.forEach((container) => {
        this.dateInput = container.querySelector('.js-date');
        this.dateConfig = new Pikaday({
          container,
          field: this.dateInput,
          minDate: new Date(),
          format: 'DD, MMM, YYYY',
          position: 'bottom right',
          i18n: {
            previousMonth: 'Previous Month',
            nextMonth: 'Next Month',
            months: [
              'January',
              'February',
              'March',
              'April',
              'May',
              'June',
              'July',
              'August',
              'September',
              'October',
              'November',
              'December',
            ],
            weekdays: [
              'Sunday',
              'Monday',
              'Tuesday',
              'Wednesday',
              'Thursday',
              'Friday',
              'Saturday',
            ],
            weekdaysShort: [
              'Su',
              'Mo',
              'Tu',
              'We',
              'Th',
              'Fr',
              'Sa',
            ],
          },
        });
      });
    }
  }

  autoTab() {
    this.autoTabInputs = Array.from(document.querySelectorAll('.js-auto-tab'));
    this.autoTabInputs.forEach((input, i) => {
      if (input.pattern === '\\d*') {
        input.addEventListener('keypress', (e) => {
          let key = e.keyCode || e.which;
          key = String.fromCharCode(key);
          const regex = /[0-9]|\./;
          if (!regex.test(key)) {
            e.returnValue = false;
            e.preventDefault();
          }
        });
      }

      input.addEventListener('keyup', () => {
        if (input.value.length === input.maxLength && this.autoTabInputs[i + 1]) {
          // this.autoTabInputs[i + 1].focus();
          this.autoTabInputs[i + 1].select();
        }
      });
    });
  }

  smallLabel() {
    const inlineLabel = document.querySelectorAll('.l-form__label--inline');

    if (inlineLabel.length) {
      [].forEach.call(inlineLabel, (el) => {
        const label = el;
        const input = el.parentNode.querySelector('.l-form__input');

        if (!label) {
          return;
        }

        const labelW = label.offsetWidth;

        if (input !== null) {
          input.style.paddingRight = `${labelW + 25}px`;

          if (input.value !== '' && !input.classList.contains('form-error')) {
            label.classList.add('label-show');
          } else {
            label.classList.remove('label-show');
          }

          input.addEventListener('keyup', this.showLabel);
          input.addEventListener('click', this.showLabel);
        }
      });
    }
  }

  static showLabel(el) {
    const input = el.target;
    const label = input.parentNode.querySelector('label');

    if (!label) {
      return;
    }

    const labelW = label.offsetWidth;
    input.style.paddingRight = `${labelW + 25}px`;

    if (input.value !== '' && !input.classList.contains('form-error')) {
      label.classList.add('label-show');
    } else {
      label.classList.remove('label-show');
    }
  }
}
