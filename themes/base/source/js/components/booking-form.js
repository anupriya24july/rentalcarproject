import Downup from 'downup';

export default class BookingForm {
  constructor() {
    this.form = document.querySelector('.js-booking-form');

    this.state = {
      showDropoff: false,
      showAdvanced: false,
      showMobItinerary: true,
    };

    if (this.form) {
      // this.advancedToggle();
      // this.dropoffToggle();
    }

    this.toggleMobItinerary();
  }

  toggleMobItinerary() {
    this.progress = document.querySelector('.l-booking__progress');
    this.summaryBar = document.querySelector('.l-booking-summary-bar');

    if (this.progress && this.summaryBar) {
      this.downup = new Downup();

      window.addEventListener('scroll', () => {
        this.downup.update(window.scrollY);

        this.downup.on(window.scrollY, (offset) => {
          this.progress.classList[offset > 0 ? 'add' : 'remove']('hide-itinerary');
          this.summaryBar.classList[offset > 0 ? 'add' : 'remove']('hide-itinerary');
          this.summaryBar.classList[offset > 0 ? 'remove' : 'add']('hide-totals');
        });
      });
    }
  }

  dropoffToggle() {
    this.dropoffToggle = document.querySelector('.js-booking__dropoff-location-toggle');
    this.field = document.querySelector('.js-booking__dropoff-location');

    // Hide the dropoff field dependant on initial states
    if (!this.state.showDropoff && !this.state.showAdvanced) {
      this.field.classList.add('is-hidden');
    }

    // Check the dropoff toggle dependant on initial state
    this.dropoffToggle.checked = this.state.showDropoff;

    this.dropoffToggle.addEventListener('change', () => {
      // Update showDropoff state
      this.state.showDropoff = !this.state.showDropoff;

      // Hide/show the dropoff field if advanced fields not shown
      if (!this.state.showAdvanced) {
        if (this.dropoffToggle.checked) {
          this.field.classList.remove('is-hidden');
        } else {
          this.field.classList.add('is-hidden');
        }
      }
    });
  }

  advancedToggle() {
    this.advancedToggle = document.querySelector('.js-booking__advanced-toggle');

    // Hide/show advanced fields dependant on initial state
    this.toggleAdvancedFields();

    this.advancedToggle.addEventListener('click', () => {
      // Update showAdvanced state
      this.state.showAdvanced = !this.state.showAdvanced;

      // Hide/show advanced fields dependant on updated state
      this.toggleAdvancedFields();
    });
  }

  toggleAdvancedFields() {
    this.advancedFields = Array.from(document.querySelectorAll('.js-booking__advanced-field'));

    this.advancedFields.forEach((field) => {
      this.isDropoffField = field.classList.contains('js-booking__dropoff-location');

      // Hide/show advanced fields dependant of dropoff field and state
      if (!this.isDropoffField || (this.isDropoffField && !this.state.showDropoff)) {
        if (this.state.showAdvanced) {
          field.classList.remove('is-hidden');
          this.advancedToggle.innerText = '- Less Options';
        } else {
          field.classList.add('is-hidden');
          this.advancedToggle.innerText = '+ More Options';
        }
      }
    });
  }
}
