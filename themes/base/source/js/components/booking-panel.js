export default class BookingPanel {
  constructor() {
    this.togglePanel();
  }

  togglePanel() {
    this.panel = document.querySelector('.js-booking-panel');
    this.toggles = Array.from(document.querySelectorAll('.js-booking-panel-toggle'));
    this.toggles.forEach((toggle) => {
      toggle.addEventListener('click', (e) => {
        window.scrollTo(0, 0);
        e.preventDefault();
        e.stopPropagation();
        if ((document.getElementById('acebook-inline-widget') === null && !(window.location.pathname === '/bookings' || window.location.pathname === '/bookings/')) || (e.target.id === 'homepage-cta' || e.target.classList.contains('l-booking-panel__close'))) {
          if (this.panel.classList.contains('is-open')) {
            this.panel.classList.remove('is-open');
          } else {
            this.panel.classList.add('is-open');
          }
        } else {
          const widgetStyle = window.getComputedStyle(document.getElementById('acebook-inline-widget').parentElement);
          if (widgetStyle.display === 'none') {
            const homepage = '/bookings';
            window.location.replace(homepage);
          } else {
            const widget = document.getElementById('acebook-inline-widget');
            document.documentElement.scrollTop = widget.parentElement.offsetHeight;
          }
        }
      });
    });
  }
}
