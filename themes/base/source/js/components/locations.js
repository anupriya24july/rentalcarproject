/*eslint-disable */

export default class Locations {
  constructor() {
    this.markers = [];
    this.depotCards = Array.from(document.querySelectorAll('.js-depot'));

    if (this.depotCards.length) {
      this.map();
      this.countryScroll();
    }
  }

  map() {
    const locationsMap = document.querySelector('.js-locations-map');;

    google.maps.controlStyle = 'azteca';

    const mapOptions = {
      center: new google.maps.LatLng(-41.899628, 172.594421),
      zoom: 5,
      zoomControl: false,
      draggable: true,
      mapTypeControl: false,
      streetViewControl: false,
    };
    let map = new google.maps.Map(locationsMap, mapOptions);
    let location = document.querySelector('.js-locations-depots').dataset.sitecountry;
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': location }, function(results, status){
      if (status == google.maps.GeocoderStatus.OK) {
          map.setCenter(results[0].geometry.location);
      } else {
          alert("Could not find location: " + location);
      }
    });
    this.setMarkers(map);
  }

  setMarkers(map) {
    this.depots = JSON.parse(document.querySelector('.js-locations-depots').dataset.json);

    const image = {
      url: '/themes/base/production/images/map_marker.png',
      size: new google.maps.Size(81,98),
      scaledSize: new google.maps.Size(40,49),
      anchor: new google.maps.Point(20,49),
    };

    const imageHover = {
      url: '/themes/base/production/images/map_marker_hover.png',
      size: new google.maps.Size(81,98),
      scaledSize: new google.maps.Size(40,49),
      anchor: new google.maps.Point(20,49),
    };

    this.depotCards.forEach((card) => {
      card.addEventListener('mouseover', () => {
        this.depotCards.forEach((sibling) => {
          card.classList.remove('is-active');
        });
        card.classList.add('is-active');
        this.markers.forEach((marker) => {
          if (marker.markerID == card.dataset.depot) {
            marker.setIcon(imageHover);
          }
        });
      });

      card.addEventListener('mouseout', () => {
        card.classList.remove('is-active');
        this.markers.forEach((marker) => {
          if (marker.markerID == card.dataset.depot) {
            marker.setIcon(image);
          }
        });
      });
    });

    this.depots.forEach((depot) => {
      const markerLat = parseFloat(depot.Lat);
      const markerLng = parseFloat(depot.Lng);
      const position = new google.maps.LatLng(markerLat, markerLng);

      const marker = new google.maps.Marker({
        position,
        icon: image,
        map,
        markerID: depot.ID,
      });

      google.maps.event.addListener(marker, 'mouseover', () => {
        marker.setIcon(imageHover);
      });

      google.maps.event.addListener(marker, 'mouseout', () => {
        marker.setIcon(image);
      });

      google.maps.event.addListener(marker, 'mousedown', () => {
        this.depotCards.forEach((card) => {
          if (card.dataset.depot == depot.ID) {
            document.querySelector('.js-locations-depots').scrollTop = card.offsetTop - 50;
            card.classList.add('is-active');
          } else {
            card.classList.remove('is-active');
          }
        });
      });

      this.markers.push(marker);
    });
  }

  countryScroll() {
    this.container = document.querySelector('.js-locations-depots');
    this.countries = Array.from(document.querySelectorAll('.js-locations-country'));
    this.groups = Array.from(document.querySelectorAll('.js-depot-group'));

    this.countries.forEach((country) => {
      country.addEventListener('click', () => {
        country.scrollIntoView();
        this.groups.forEach((group) => {
          if (country.dataset.country === group.dataset.country) {
            this.container.scrollTop = group.offsetTop - 40;
          }
        });
      });
    });
  }
}

/* eslint-enable */
