import tippy from 'tippy.js';

export default class ToolTips {
  constructor() {
    this.tooltip = tippy('.js-tooltip', {
      arrow: true,
      trigger: 'mouseenter',
      interactive: true,
      theme: 'light',
    });
  }
}
