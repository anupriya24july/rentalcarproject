export default class Deals {
  constructor() {
    // this.toggles = Array.from(document.querySelectorAll('.js-deals-toggle'));
    // this.cards = Array.from(document.querySelectorAll('.js-deals-cards'));

    this.state = {
      type: 'promotions',
    };

    // this.toggleType();

    // if (Deals.getType() === 'relocation-deals') {
    //   this.setType('relocation-deals');
    // } else {
    //   this.setType('promotions');
    // }
  }

  static getType() {
    const url = window.location.href;
    const name = 'type';
    const regex = new RegExp(`[?&]${name}(=([^&#]*)|&|#|$)`);
    const results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

  setType(type) {
    this.toggles.forEach((toggle) => {
      if (toggle.dataset.type === type) {
        toggle.classList.add('is-active');
      } else {
        toggle.classList.remove('is-active');
      }
    });

    this.cards.forEach((cards) => {
      if (cards.dataset.type === type) {
        cards.classList.add('is-visible');
      } else {
        cards.classList.remove('is-visible');
      }
    });
  }

  toggleType() {
    this.toggles.forEach((toggle) => {
      toggle.addEventListener('click', () => {
        this.state.type = toggle.dataset.type;
        this.setType(toggle.dataset.type);
        window.history.pushState(this.state, 'Type', `/deals/?type=${toggle.dataset.type}`);
      });
    });
  }
}
