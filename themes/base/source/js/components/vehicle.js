import Swiper from 'swiper';

export default class Vehicle {
  constructor() {
    this.moreSpecs();
    this.carousel();
  }

  carousel() {
    // Initialise the hero carousel
    this.configvehicle = {
      slidesPerView: 3,
      initialSlide: 1,
      speed: 400,
      simulateTouch: false,
      centeredSlides: true,
      keyboardControl: true,
      loop: true,
      breakpoints: {
        790: {
          slidesPerView: 'auto',
        },
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    };
    this.configdeal = {
      slidesPerView: 3,
      initialSlide: 1,
      speed: 400,
      simulateTouch: false,
      centeredSlides: true,
      keyboardControl: true,
      loop: true,
      breakpoints: {
        790: {
          slidesPerView: 'auto',
        },
      },
      navigation: {
        nextEl: '.deal-swiper-button-next',
        prevEl: '.deal-swiper-button-prev',
      },
    };

    this.swiper = new Swiper(document.querySelector('.js-vehicles-carousel'), this.configvehicle);
    this.swiper = new Swiper(document.querySelector('.js-recommended-vehicles-portal-carousel'), this.configvehicle);
    this.swiper = new Swiper(document.querySelector('.js-recommended-vehicles-carousel'), this.configvehicle);
    this.swiper = new Swiper(document.querySelector('.js-deals-carousel'), this.configdeal);

    // this.swiper.on('resize', () => {
    //   this.swiper.destroy(true, true);
    //   console.log(this.swiper, 'destroyed');
    //   setTimeout(() => {
    //     console.log('new swiper initialised');
    //     this.newSwiper = new Swiper(document.querySelector('.js-vehicles-carousel'), this.config);
    //     this.newSwiper.init();
    //   }, 100);
    // });
  }

  moreSpecs() {
    this.vehicles = Array.from(document.querySelectorAll('.js-vehicle-card'));

    this.vehicles.forEach((vehicle) => {
      const toggle = vehicle.querySelector('.js-vehicle-specs-trigger');
      const specs = Array.from(vehicle.querySelectorAll('.js-vehicle-spec-more'));

      if (toggle) {
        toggle.addEventListener('click', () => {
          if (toggle.classList.contains('is-active')) {
            toggle.classList.remove('is-active');
            // toggle.querySelector('span').innerText = 'More Specifications';
            specs.forEach((spec) => {
              spec.classList.add('is-hidden');
            });
          } else {
            toggle.classList.add('is-active');
            // toggle.querySelector('span').innerText = 'Fewer Specifications';
            specs.forEach((spec) => {
              spec.classList.remove('is-hidden');
            });
          }
        });
      }
    });
  }
}
