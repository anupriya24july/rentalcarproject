import Swiper from 'swiper';

// require('aframe');

/* eslint-disable */
// AFRAME.registerComponent('play-on-window-click', {
//   init: function() {
//     this.onClick = this.onClick.bind(this);
//   },

//   play: function () {
//     window.addEventListener('click', this.onClick);
//     window.addEventListener('touchStart', this.onClick);
//   },

//   pause: function () {
//     window.removeEventListener('click', this.onClick);
//     window.removeEventListener('touchStart', this.onClick);
//   },

//   onClick: function(evt) {
//     debugger;
//     const video = this.el.components.material.material.map.image;
//     if (!video) { return; }
//     if (document.querySelector('.js-360-video').getAttribute('src')) video.play();
//   }
// });
/* eslint-enable */

export default class Hero {
  constructor() {
    this.isCarousel = document.querySelector('.js-hero-carousel');

    if (this.isCarousel) {
      this.carousel(document.querySelector('.js-hero-carousel'));
    } else {
      this.standard();
    }

    Hero.detectIE();
    Hero.customEvent.prototype = window.Event.prototype;
    window.CustomEvent = Hero.customEvent;
  }

  static detectIE() {
    const ua = window.navigator.userAgent;

    const msie = ua.indexOf('MSIE ');
    if (msie > 0) {
      // IE 10 or older => return version number
      const version = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
      document.body.classList.add('is-ie');
      document.body.classList.add(`is-ie--${version}`);
      return version;
    }

    const trident = ua.indexOf('Trident/');
    if (trident > 0) {
      // IE 11 => return version number
      const rv = ua.indexOf('rv:');
      const version = parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
      document.body.classList.add('is-ie');
      document.body.classList.add(`is-ie--${version}`);
      return version;
    }

    const edge = ua.indexOf('Edge/');
    if (edge > 0) {
      // Edge (IE 12+) => return version number
      const version = parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
      document.body.classList.add('is-edge');
      document.body.classList.add(`is-edge--${version}`);
      return version;
    }

    return false;
  }

  static customEvent(event, params) {
    /* eslint-disable */
    params = params || { bubbles: false, cancelable: false, detail: undefined };
    var evt = document.createEvent( 'CustomEvent' );
    evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
    return evt;
    /* eslint-enable */
  }

  standard() {
    this.video = document.querySelector('.js-hero-video');
    this.container = document.querySelector('.l-hero__experience');
    this.instruction = document.querySelector('.js-hero-360-instruction');
    const actions = document.querySelector('.js-hero-actions');
    const background = document.querySelector('.l-hero__experience-background');

    if (this.container) {
      this.animateExperience(this.container, true);

      if (this.container.dataset.video) {
        this.video.addEventListener('mousedown', () => {
          Hero.handleVideoSwitch(this.container, this.instruction, this.video, background, actions);
        });

        this.video.addEventListener('touchstart', () => {
          this.hero.classList.add('has-video--playing');
          Hero.handleVideoSwitch(this.container, this.instruction, this.video, background, actions);
        });
      }
    }
  }

  carousel(carousel) {
    // Initialise the hero carousel
    this.swiper = new Swiper(carousel, {
      slidesPerView: 1,
      speed: 400,
      loop: true,
      lazy: true,
      simulateTouch: false,
      keyboardControl: true,
      effect: 'fade',
      slideClass: 'l-hero__slide',
      navigation: {
        nextEl: '.l-hero__switch--next',
        prevEl: '.l-hero__switch--prev',
      },
      on: {
        slideChange: () => {
          this.handleActiveSlide(carousel.swiper);
        },

        transitionEnd: () => {
          this.setSwitchLabels();
        },

        init: () => {
          this.setSwitchLabels();
        },
      },
    });
  }

  setSwitchLabels() {
    this.prevSlide = document.querySelector('.swiper-slide-prev');
    this.nextSlide = document.querySelector('.swiper-slide-next');

    this.prevLabel = document.querySelector('.js-hero-prev');
    this.nextLabel = document.querySelector('.js-hero-next');

    if (this.prevLabel) this.prevLabel.innerText = this.prevSlide.dataset.label;
    if (this.nextLabel) this.nextLabel.innerText = this.nextSlide.dataset.label;
  }

  handleActiveSlide(carousel) {
    for (let i = 0; i < carousel.slides.length; i += 1) {
      this.slide = carousel.slides[i];

      if (carousel.activeIndex === i) {
        // Trigger experience animation if active slide
        this.animateExperience(this.slide, true);
        this.slide.addEventListener('click', this.switchVideo(this.slide));
      } else {
        // Reset experience animation if not active slide
        this.animateExperience(this.slide, false);
      }
    }
  }

  switchVideo(container) {
    this.video = document.querySelector('.js-hero-video');
    this.instruction = document.querySelector('.js-hero-360-instruction');

    const actions = container.querySelector('.js-hero-actions');
    const background = container.querySelector('.l-hero__experience-background');

    if (container.dataset.video && this.video && Hero.detectIE() !== 11) {
      container.classList.add('has-video');
      this.instruction.classList.add('is-visible');
      background.classList.remove('is-hidden');
      actions.classList.remove('is-visible');

      this.video.addEventListener('mousedown', () => {
        Hero.handleVideoSwitch(container, this.instruction, this.video, background, actions);
      });

      this.video.addEventListener('touchstart', () => {
        container.classList.add('has-video--playing');
        Hero.handleVideoSwitch(container, this.instruction, this.video, background, actions);
      });
    } else {
      this.instruction.classList.remove('is-visible');
    }
  }

  static handleVideoSwitch(container, instruction, video, background, actions) {
    this.video = video;
    instruction.classList.remove('is-visible');
    this.video.classList.add('is-visible');
    background.classList.add('is-hidden');
    actions.classList.add('is-visible');

    if (!video.querySelector('.js-360-video').getAttribute('src')) {
      this.video.querySelector('.js-360-video').src = '';
      this.video.querySelector('.js-360-video').src = container.dataset.video;
    }
  }

  animateExperience(container, active) {
    this.brand = container.querySelector('.experience-brand');
    this.actions = container.querySelector('.js-hero-actions');

    container.classList.remove('has-video--playing');
    document.querySelector('.js-hero-video').classList.remove('is-visible');

    if (this.brand) {
      if (active) {
        this.brand.classList.add('is-active');
        this.actions.classList.add('is-visible');
      } else {
        this.brand.classList.remove('is-active');
        this.actions.classList.remove('is-visible');
      }
    }
  }
}
