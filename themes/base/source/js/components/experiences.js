export default class Experiences {
  constructor() {
    this.animateExperience();
    this.scrollToTerms();
  }

  animateExperience() {
    this.columns = Array.from(document.querySelectorAll('.js-experience-column'));

    this.columns.forEach((column) => {
      this.brand = column.querySelector('.experience-brand');
      this.brand.classList.add('is-active');
    });
  }

  scrollToTerms() {
    this.terms = document.querySelector('.l-experience__deal-terms');
    if (this.terms) {
      this.terms.addEventListener('click', (e) => {
        e.preventDefault();
        window.scrollTo(300, document.body.offsetHeight - (document.querySelector('.l-footer').offsetHeight + 800));
      });
    }
  }
}
