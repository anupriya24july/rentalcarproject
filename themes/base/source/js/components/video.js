export default class Video {
  constructor() {
    this.setVideo();
  }

  setVideo() {
    this.select = document.querySelector('.js-video-language');

    if (this.select) {
      const videos = Array.from(document.querySelectorAll('.js-video'));
      this.select.addEventListener('change', () => {
        videos.forEach((video) => {
          if (this.select.value === video.dataset.video) {
            video.classList.add('is-visible');
          } else {
            video.classList.remove('is-visible');
          }
        });
      });
    }
  }
}
