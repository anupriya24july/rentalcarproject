export default class Share {
  constructor() {
    const shareTriggers = Array.from(document.querySelectorAll('.js-share'));
    shareTriggers.forEach((trigger) => {
      const url = trigger.getAttribute('href');
      trigger.addEventListener('click', (e) => {
        e.preventDefault();
        window.open(url, '', 'menubar=no,toolbar=no,resizable=yes,height=600,width=600');
        return false;
      });
    });
  }
}
