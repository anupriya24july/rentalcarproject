// ========================================================================
// Validation
// ========================================================================

import Validation from 'lg-validation';

export default class FormValidation {
  constructor() {
    this.formValidator();
  }

  formValidator() {
    this.validation = new Validation({
      forms: {
        ContactForm: {
          Name: 'required',
          Email: 'email',
          Type: 'required',
          Message: 'minLength:20|maxLength:3000',
        },
      },
      messages: {
        ContactForm: {
          Name: {
            required: 'Please enter your name',
          },
          Email: {
            email: 'Please enter a valid email address',
          },
          Type: {
            required: 'Please let us know how we can help',
          },
          Message: {
            minLength: 'Your message must be a minimum of 20 characters',
            maxLength: 'Your message must be a maximum of 3000 characters',
          },
        },
      },
      submitIDs: {
        ContactForm: 'ContactSubmit',
      },
      inlineLabel: false,
      /* Scroll to Error */
      // onError: () => {
      //   setTimeout(() => {
      //     this.errors = Array.from(document.querySelectorAll('.error-container--filled'));
      //     if (this.errors) {
      //       this.container = document.querySelector('.l-contact__container');
      //       this.form = document.getElementById('ContactForm');
      //       this.scrollPos = (this.container.offsetTop + this.errors[0].parentNode.offsetTop) - 15;
      //
      //       window.scroll(0, this.scrollPos);
      //     }
      //   }, 50);
      // },
    });

    this.validation.bindEvents();
  }
}
