import request from 'superagent';

export default class PassKit {
  constructor() {
    this.download = document.querySelector('.js-download-pass');
    if (this.download) this.generatePass();
  }

  generatePass() {
    request
      .post('/booking/GeneratePass')
      .send({
        serial: new Date(), // Pass Serial Number must be unique
        driver: 'Aaron Rose',
        vehicle: 'Mazda Demio',
        pickupLocation: 'Auckland Airport',
        pickupDate: '2017-11-23T11:58:41+12:00',
        returnLocation: 'Wellington Airport',
        returnDate: '2017-12-23T11:58:41+12:00',
        depositPaid: '100.00 NZD',
        balanceDue: '400.00 NZD',
      })
      .set('accept', 'json')
      .end((err, res) => {
        if (err) {
          this.download.style.display = 'none';
        } else if (!window.ApplePaySession) {
          this.download.href = `/passes/${res.body.serial}.pkpass`;
        } else {
          this.download.href = `/booking/DownloadPass?serial=${res.body.serial}`;
        }
      });
  }
}
