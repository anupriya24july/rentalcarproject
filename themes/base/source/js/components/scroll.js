import ScrollTo from 'lg-scroll-to';

export default class Scroll {
  constructor() {
    const scrollTo = new ScrollTo({
      scrollDuration: 500,
      scrollOffset: window.innerWidth >= 1170 ? -130 : -80,
      linkClass: 'js-scroll',
    });
    scrollTo.bindEvents();
  }
}
