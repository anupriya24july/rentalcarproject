import Stickyfill from 'stickyfilljs';

export default class Header {
  constructor() {
    this.state = {
      fixed: false,
    };

    if (document.querySelector('.js-overlay-header')) {
      this.toggleFixed();

      window.addEventListener('scroll', () => {
        this.toggleFixed();
      });
    } else {
      Header.toggleFixedClasses();
    }

    Stickyfill.add(document.querySelectorAll('.js-header'));
  }

  toggleFixed() {
    if (window.pageYOffset > 0 && !this.state.fixed) {
      this.state.fixed = true;
      Header.toggleFixedClasses();
    } else if (window.pageYOffset === 0 && this.state.fixed) {
      this.state.fixed = false;
      Header.toggleFixedClasses();
    }
  }

  static toggleFixedClasses() {
    const header = document.querySelector('.js-header');
    const logo = document.querySelector('.js-logo');
    const navLinks = Array.from(document.querySelectorAll('.js-nav-link'));

    header.classList.toggle('is-fixed');
    logo.classList.toggle('is-fixed');
    navLinks.forEach((link) => {
      link.classList.toggle('is-fixed');
    });
  }
}
