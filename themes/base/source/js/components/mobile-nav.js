export default class MobileNav {
  constructor() {
    this.toggleMenu();
    MobileNav.subMenus();
  }

  toggleMenu() {
    const hamburger = document.getElementById('MobMenuToggle');
    const menu = document.getElementById('MobMenu');
    hamburger.addEventListener('click', () => {
      if (!hamburger.classList.contains('hamburger--active')) {
        this.scrollPos = window.pageYOffset;
        window.scroll(0, 0);
        document.body.classList.add('is-locked');

        // Remove closed state classes
        menu.classList.remove('mob-nav--closed');

        // Add open state classes
        hamburger.classList.add('hamburger--active');
        menu.classList.add('mob-nav--open');
      } else {
        document.body.scrollTop = this.scrollPos;
        document.body.classList.remove('is-locked');
        window.scroll(0, this.scrollPos);

        // Add closed state classes
        menu.classList.add('mob-nav--closed');

        // Remove open state classes
        menu.classList.remove('mob-nav--open');
        hamburger.classList.remove('hamburger--active');
      }
    });
  }

  static subMenus() {
    const parentTriggers = Array.from(document.querySelectorAll('.mob-nav__trigger--parent'));

    parentTriggers.forEach((trigger) => {
      trigger.addEventListener('click', () => {
        const triggerMenu = trigger.dataset.menu;

        const childrenMenus = Array.from(document.querySelectorAll('.mob-nav__menu--children'));
        childrenMenus.forEach((child) => {
          const childMenu = child.dataset.menu;

          if (triggerMenu === childMenu) {
            const parent = `.mob-nav__menu[data-menu=${child.dataset.parent}]`;
            const parentMenu = document.querySelector(parent);

            parentMenu.classList.add('mob-nav__menu--closed');
            parentMenu.classList.remove('mob-nav__menu--open');
            child.classList.add('mob-nav__menu--open');
          }
        });
      });
    });

    const backTriggers = Array.from(document.querySelectorAll('.mob-nav__trigger--back'));
    backTriggers.forEach((trigger) => {
      trigger.addEventListener('click', () => {
        const triggerMenu = trigger.dataset.menu;

        const childrenMenus = Array.from(document.querySelectorAll('.mob-nav__menu--children'));
        childrenMenus.forEach((child) => {
          const childMenu = child.dataset.menu;

          if (triggerMenu === childMenu) {
            const parent = `.mob-nav__menu[data-menu=${child.dataset.parent}]`;
            const parentMenu = document.querySelector(parent);

            parentMenu.classList.remove('mob-nav__menu--closed');
            parentMenu.classList.add('mob-nav__menu--open');
            child.classList.remove('mob-nav__menu--open');
          }
        });
      });
    });
  }
}
