export default class Modals {
  constructor() {
    this.closeModal();
    this.confirmRedirect();
    this.iframeModal();
  }

  openModal(heading, msg, mode) {
    this.modal = document.querySelector('.js-modal');
    document.querySelector('.modal-message-title').innerHTML = heading;
    document.querySelector('.modal-message-content').innerHTML = msg;
    this.modal.classList.remove('is-hidden');
    if (mode === 1) {
      document.querySelector('.modal-footer').classList.remove('is-hidden');
    }
  }

  closeModal() {
    this.trigger = document.querySelectorAll('.js-modal-close');
    this.trigger.forEach((trigger) => {
      trigger.addEventListener('click', () => {
        document.querySelector('.js-modal').classList.add('is-hidden');
        document.querySelector('.modal-message-content').innerHTML = '';
        document.querySelector('.modal-message-content').classList.remove('modal-iframe-wrapper');
      });
    });
  }

  continueRedirect(redirectUrl) {
    this.confirm = document.querySelector('.js-modal-confirm');
    this.confirm.addEventListener('click', () => {
      window.location = redirectUrl;
    });
  }

  confirmRedirect() {
    this.dealAction = document.querySelectorAll('.redirect-url');
    this.dealAction.forEach((action) => {
      action.addEventListener('click', () => {
        const msg = `This offer is only available on the Ace Rental Cars${action.dataset.country}website, Select OK to be taken to our${action.dataset.country}website to book the offer.`;
        const heading = 'Just to let you know...';
        this.openModal(heading, msg, 1);
        this.closeModal();
        this.continueRedirect(action.dataset.url);
      });
    });
  }

  iframeModal() {
    this.modal = document.querySelector('.js-modal');
    this.trigger = document.querySelector('.l-booking__insurance-video');
    if (this.trigger) {
      document.querySelector('.modal-message-title').innerHTML = 'Our Cover Options';
      this.trigger.addEventListener('click', () => {
        this.modal.classList.remove('is-hidden');
        document.querySelector('.modal-message-content').classList.add('modal-iframe-wrapper');
        document.querySelector('.modal-message-content').innerHTML = '<iframe class="modal-iframe" src="https://www.youtube.com/embed/HB1XlOBIT6c?autoplay=1" frameborder="0" allowfullscreen></iframe>';
      });
    }
  }
}

