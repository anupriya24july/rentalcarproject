// Add more polyfills as needed: https://github.com/zloirock/core-js
import 'core-js/es6/array';
import 'core-js/es6/object';
import 'custom-event-polyfill';
import 'classlist.js';

// Sass
import './sass/style.scss';

// JS
import './js/init';
