<?php

namespace AceRentals\API\Controllers;

ini_set('display_errors', 0);

use SilverStripe\Control\Controller;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Control\HTTPResponse;
use SilverStripe\Control\HTTPResponse_Exception;

class APIController extends Controller
{

    private static $allowed_actions = [
        'InitBooking',
        'getRates',
        'getEstimate',
        'getOptions',
        'createNewReservation',
        'retrieveReservation',
        'retrieveEstimate',
        'updateReservation'
    ];

    // private static $url_handlers = [
    //     'api/ResRates/$Data' => 'ResRates',
    // ];

    public function InitBooking() {
      $data = [
        "@attributes" => ["success" => 'true'],
        "Locations" => [
           [ "id" => "AAD60", "location" => "Adelaide", "city" => "Keswick", "hours" => [/*Sun-Sat */"08:00-17:00", "08:00-17:00", "08:00-17:00","08:00-17:00","08:00-17:00","08:00-17:00","08:00-17:00"]]
          ,[ "id" => "ABR60", "location" => "Brisbane", "city" => "Hendra", "hours" => [/*Sun-Sat */"08:00-17:00", "08:00-17:00", "08:00-17:00","08:00-17:00","08:00-17:00","08:00-17:00","08:00-17:00"]]
          ,[ "id" => "ACI60", "location" => "Cairns", "city" => "Cairns", "hours" => [/*Sun-Sat */"08:00-17:00", "08:00-17:00", "08:00-17:00","08:00-17:00","08:00-17:00","08:00-17:00","08:00-17:00"]]
          ,[ "id" => "ADA60", "location" => "Darwin", "city" => "Darwin", "hours" => [/*Sun-Sat */"08:00-12:00", "08:00-12:00", "08:00-17:00","08:00-17:00","08:00-17:00","08:00-17:00","08:00-17:00"]]
          ,[ "id" => "AGC60", "location" => "Gold Coast", "city" => "Currumbin Waters", "hours" => [/*Sun-Sat */"08:00-17:00", "08:00-17:00", "08:00-17:00","08:00-17:00","08:00-17:00","08:00-17:00","08:00-17:00"]]
          ,[ "id" => "AME60", "location" => "Melbourne", "city" => "Tullamarine", "hours" => [/*Sun-Sat */"08:00-17:00", "08:00-17:00", "08:00-17:00","08:00-17:00","08:00-17:00","08:00-17:00","08:00-17:00"]]
          ,[ "id" => "APR60", "location" => "Perth", "city" => "Belmont", "hours" => [/*Sun-Sat */"08:00-18:00", "08:00-18:00", "08:00-18:00","08:00-18:00","08:00-18:00","08:00-18:00","08:00-18:00"]]
          ,[ "id" => "ASY60", "location" => "Sydney", "city" => "Mascot", "hours" => [/*Sun-Sat */"05:30-23:00", "05:30-23:00", "05:30-23:00","05:30-23:00","05:30-23:00","05:30-23:00","05:30-23:00"]]
        ],
        "DriverAges" => [
           [ "id" => "16", "driverage" => "16 and under"]
          ,[ "id" => "17", "driverage" => "17"]
          ,[ "id" => "18", "driverage" => "18"]
          ,[ "id" => "19", "driverage" => "19"]
          ,[ "id" => "20", "driverage" => "20"]
          ,[ "id" => "21", "driverage" => "21 and above"]
        ],
        "Countries" => [
           ["c" => "AU", "r" => "WA", "s" => "WA", "desc" => "WESTERN AUSTRALIA"]
          ,["c" => "AU", "r" => "VI", "s" => "VIC", "desc" => "VICTORIA"]
          ,["c" => "AU", "r" => "TS", "s" => "TAS", "desc" => "TASMANIA"]
          ,["c" => "AU", "r" => "SA", "s" => "SA", "desc" => "SOUTH AUSTRALIA"]
          ,["c" => "AU", "r" => "QL", "s" => "QLD", "desc" => "QUEENSLAND"]
          ,["c" => "AU", "r" => "NT", "s" => "NT", "desc" => "NORTHERN TERRITORY"]
          ,["c" => "AU", "r" => "NS", "s" => "NSW", "desc" => "NEW SOUTH WALES"]
          ,["c" => "AU", "r" => "AC", "s" => "ACT", "desc" => "AUSTRALIAN CAPITAL TERRITORY"]
          ,["c" => "-", "r" => "CH", "s" => "-", "desc" => "SWITZERLAND"]
          ,["c" => "-", "r" => "SE", "s" => "-", "desc" => "SWEDEN"]
          ,["c" => "-", "r" => "SZ", "s" => "-", "desc" => "SWAZILAND"]
          ,["c" => "-", "r" => "SJ", "s" => "-", "desc" => "SVALBARD AND JAN MEYEN ISLANDS"]
          ,["c" => "-", "r" => "SR", "s" => "-", "desc" => "SURINAME"]
          ,["c" => "-", "r" => "SD", "s" => "-", "desc" => "SUDAN"]
          ,["c" => "-", "r" => "PM", "s" => "-", "desc" => "ST. PIERRE AND MIQUELON"]
          ,["c" => "-", "r" => "SH", "s" => "-", "desc" => "ST. HELENA"]
          ,["c" => "-", "r" => "LK", "s" => "-", "desc" => "SRI LANKA"]
          ,["c" => "-", "r" => "ES", "s" => "-", "desc" => "SPAIN"]
          ,["c" => "-", "r" => "GS", "s" => "-", "desc" => "STH GEORGIA & STH SANDWICH ISL"]
          ,["c" => "-", "r" => "ZA", "s" => "-", "desc" => "SOUTH AFRICA"]
          ,["c" => "-", "r" => "SO", "s" => "-", "desc" => "SOMALIA"]
          ,["c" => "-", "r" => "SB", "s" => "-", "desc" => "SOLOMON ISLANDS"]
          ,["c" => "-", "r" => "SI", "s" => "-", "desc" => "SLOVENIA"]
          ,["c" => "-", "r" => "SK", "s" => "-", "desc" => "SLOVAKIA (SLOVAK REPUBLIC)"]
          ,["c" => "-", "r" => "SG", "s" => "-", "desc" => "SINGAPORE"]
          ,["c" => "-", "r" => "SL", "s" => "-", "desc" => "SIERRA LEONE"]
          ,["c" => "-", "r" => "SC", "s" => "-", "desc" => "SEYCHELLES"]
          ,["c" => "-", "r" => "SN", "s" => "-", "desc" => "SENEGAL"]
          ,["c" => "-", "r" => "SU", "s" => "-", "desc" => "SAUDI ARABIA"]
          ,["c" => "-", "r" => "ST", "s" => "-", "desc" => "SAO TOME AND PRINCIPE"]
          ,["c" => "-", "r" => "SM", "s" => "-", "desc" => "SAN MARINO"]
          ,["c" => "-", "r" => "WS", "s" => "-", "desc" => "SAMOA"]
          ,["c" => "-", "r" => "LC", "s" => "-", "desc" => "SAINT LUCIA"]
          ,["c" => "-", "r" => "KN", "s" => "-", "desc" => "SAINT KITTS AND NEVIS"]
          ,["c" => "-", "r" => "RW", "s" => "-", "desc" => "RWANDA"]
          ,["c" => "-", "r" => "RU", "s" => "-", "desc" => "RUSSIAN FEDERATION"]
          ,["c" => "-", "r" => "RO", "s" => "-", "desc" => "ROMANIA"]
          ,["c" => "-", "r" => "RE", "s" => "-", "desc" => "REUNION"]
          ,["c" => "-", "r" => "QA", "s" => "-", "desc" => "QATAR"]
          ,["c" => "-", "r" => "PR", "s" => "-", "desc" => "PUERTO RICO"]
          ,["c" => "-", "r" => "PT", "s" => "-", "desc" => "PORTUGAL"]
          ,["c" => "-", "r" => "PL", "s" => "-", "desc" => "POLAND"]
          ,["c" => "-", "r" => "PN", "s" => "-", "desc" => "PITCAIRN"]
          ,["c" => "-", "r" => "PH", "s" => "-", "desc" => "PHILIPPINES"]
          ,["c" => "-", "r" => "PE", "s" => "-", "desc" => "PERU"]
          ,["c" => "-", "r" => "PY", "s" => "-", "desc" => "PARAGUAY"]
          ,["c" => "-", "r" => "PG", "s" => "-", "desc" => "PAPUA NEW GUINEA"]
          ,["c" => "-", "r" => "PA", "s" => "-", "desc" => "PANAMA"]
          ,["c" => "-", "r" => "PS", "s" => "-", "desc" => "PALESTINIAN TERRITORY OCCUPIED"]
          ,["c" => "-", "r" => "PW", "s" => "-", "desc" => "PALAU"]
          ,["c" => "-", "r" => "PK", "s" => "-", "desc" => "PAKISTAN"]
          ,["c" => "-", "r" => "OM", "s" => "-", "desc" => "OMAN"]
          ,["c" => "-", "r" => "NO", "s" => "-", "desc" => "NORWAY"]
          ,["c" => "-", "r" => "MP", "s" => "-", "desc" => "NORTHERN MARIANA ISLANDS"]
          ,["c" => "-", "r" => "NF", "s" => "-", "desc" => "NORFOLK ISLAND"]
          ,["c" => "-", "r" => "NU", "s" => "-", "desc" => "NIUE"]
          ,["c" => "-", "r" => "NG", "s" => "-", "desc" => "NIGERIA"]
          ,["c" => "-", "r" => "NE", "s" => "-", "desc" => "NIGER"]
          ,["c" => "-", "r" => "NI", "s" => "-", "desc" => "NICARAGUA"]
          ,["c" => "-", "r" => "NZ", "s" => "-", "desc" => "NEW ZEALAND"]
          ,["c" => "-", "r" => "NC", "s" => "-", "desc" => "NEW CALEDONIA"]
          ,["c" => "-", "r" => "AN", "s" => "-", "desc" => "NETHERLANDS ANTILLES"]
          ,["c" => "-", "r" => "NL", "s" => "-", "desc" => "NETHERLANDS"]
          ,["c" => "-", "r" => "NP", "s" => "-", "desc" => "NEPAL"]
          ,["c" => "-", "r" => "NR", "s" => "-", "desc" => "NAURU"]
          ,["c" => "-", "r" => "NA", "s" => "-", "desc" => "NAMIBIA"]
          ,["c" => "-", "r" => "MM", "s" => "-", "desc" => "MYANMAR"]
          ,["c" => "-", "r" => "MZ", "s" => "-", "desc" => "MOZAMBIQUE"]
          ,["c" => "-", "r" => "MA", "s" => "-", "desc" => "MOROCCO"]
          ,["c" => "-", "r" => "MS", "s" => "-", "desc" => "MONTSERRAT"]
          ,["c" => "-", "r" => "MN", "s" => "-", "desc" => "MONGOLIA"]
          ,["c" => "-", "r" => "MC", "s" => "-", "desc" => "MONACO"]
          ,["c" => "-", "r" => "MD", "s" => "-", "desc" => "MOLDOVA REPUBLIC OF"]
          ,["c" => "-", "r" => "FM", "s" => "-", "desc" => "MICRONESIA FEDERATED STATES OF"]
          ,["c" => "-", "r" => "MX", "s" => "-", "desc" => "MEXICO"]
          ,["c" => "-", "r" => "YT", "s" => "-", "desc" => "MAYOTTE"]
          ,["c" => "-", "r" => "MU", "s" => "-", "desc" => "MAURITIUS"]
          ,["c" => "-", "r" => "MR", "s" => "-", "desc" => "MAURITANIA"]
          ,["c" => "-", "r" => "MQ", "s" => "-", "desc" => "MARTINIQUE"]
          ,["c" => "-", "r" => "MT", "s" => "-", "desc" => "MALTA"]
          ,["c" => "-", "r" => "ML", "s" => "-", "desc" => "MALI"]
          ,["c" => "-", "r" => "MV", "s" => "-", "desc" => "MALDIVES"]
          ,["c" => "-", "r" => "MY", "s" => "-", "desc" => "MALAYSIA"]
          ,["c" => "-", "r" => "MW", "s" => "-", "desc" => "MALAWI"]
          ,["c" => "-", "r" => "MG", "s" => "-", "desc" => "MADAGASCAR"]
          ,["c" => "-", "r" => "MK", "s" => "-", "desc" => "MACEDONIA"]
          ,["c" => "-", "r" => "MO", "s" => "-", "desc" => "MACAU"]
          ,["c" => "-", "r" => "LU", "s" => "-", "desc" => "LUXEMBOURG"]
          ,["c" => "-", "r" => "LT", "s" => "-", "desc" => "LITHUANIA"]
          ,["c" => "-", "r" => "LI", "s" => "-", "desc" => "LEICHTENSTEIN"]
          ,["c" => "-", "r" => "LY", "s" => "-", "desc" => "LIBYAN ARAB JAMAHIRIYA"]
          ,["c" => "-", "r" => "LR", "s" => "-", "desc" => "LIBERIA"]
          ,["c" => "-", "r" => "LS", "s" => "-", "desc" => "LESOTHO"]
          ,["c" => "-", "r" => "LB", "s" => "-", "desc" => "LEBANON"]
          ,["c" => "-", "r" => "LV", "s" => "-", "desc" => "LATVIA"]
          ,["c" => "-", "r" => "LA", "s" => "-", "desc" => "LAO PEOPLES DEMOCTRATIC REPUBL"]
          ,["c" => "-", "r" => "KG", "s" => "-", "desc" => "KYRGYZSTAN"]
          ,["c" => "-", "r" => "KW", "s" => "-", "desc" => "KUWAIT"]
          ,["c" => "-", "r" => "KR", "s" => "-", "desc" => "KOREA, REPUBLIC OF"]
          ,["c" => "-", "r" => "KP", "s" => "-", "desc" => "KOREA, DEMOCRATIC REPUBLIC OF"]
          ,["c" => "-", "r" => "KI", "s" => "-", "desc" => "KIRIBATI"]
          ,["c" => "-", "r" => "KE", "s" => "-", "desc" => "KENYA"]
          ,["c" => "-", "r" => "KZ", "s" => "-", "desc" => "KAZAKHSTAN"]
          ,["c" => "-", "r" => "JO", "s" => "-", "desc" => "JORDAN"]
          ,["c" => "-", "r" => "JP", "s" => "-", "desc" => "JAPAN"]
          ,["c" => "-", "r" => "JM", "s" => "-", "desc" => "JAMAICA"]
          ,["c" => "-", "r" => "IT", "s" => "-", "desc" => "ITALY"]
          ,["c" => "-", "r" => "IL", "s" => "-", "desc" => "ISRAEL"]
          ,["c" => "-", "r" => "IE", "s" => "-", "desc" => "IRELAND"]
          ,["c" => "-", "r" => "IQ", "s" => "-", "desc" => "IRAQ"]
          ,["c" => "-", "r" => "IR", "s" => "-", "desc" => "IRAN (ISLAMIC REPUBLIC OF)"]
          ,["c" => "-", "r" => "ID", "s" => "-", "desc" => "INDONESIA"]
          ,["c" => "-", "r" => "IN", "s" => "-", "desc" => "INDIA"]
          ,["c" => "-", "r" => "IS", "s" => "-", "desc" => "ICELAND"]
          ,["c" => "-", "r" => "HU", "s" => "-", "desc" => "HUNGARY"]
          ,["c" => "-", "r" => "HK", "s" => "-", "desc" => "HONG KONG"]
          ,["c" => "-", "r" => "HN", "s" => "-", "desc" => "HONDURAS"]
          ,["c" => "-", "r" => "HM", "s" => "-", "desc" => "HEARD AND MC DONALD ISLANDS"]
          ,["c" => "-", "r" => "HT", "s" => "-", "desc" => "HAITI"]
          ,["c" => "-", "r" => "GY", "s" => "-", "desc" => "GUYANA"]
          ,["c" => "-", "r" => "GW", "s" => "-", "desc" => "GUINEA-BISSAU"]
          ,["c" => "-", "r" => "GN", "s" => "-", "desc" => "GUINEA"]
          ,["c" => "-", "r" => "GT", "s" => "-", "desc" => "GUATEMALA"]
          ,["c" => "-", "r" => "GU", "s" => "-", "desc" => "GUAM"]
          ,["c" => "-", "r" => "GP", "s" => "-", "desc" => "GUADELOUPE"]
          ,["c" => "-", "r" => "GD", "s" => "-", "desc" => "GRENADA"]
          ,["c" => "-", "r" => "GR", "s" => "-", "desc" => "GREECE"]
          ,["c" => "-", "r" => "GI", "s" => "-", "desc" => "GIBRALTAR"]
          ,["c" => "-", "r" => "GH", "s" => "-", "desc" => "GHANA"]
          ,["c" => "-", "r" => "DE", "s" => "-", "desc" => "GERMANY"]
          ,["c" => "-", "r" => "GM", "s" => "-", "desc" => "GAMBIA"]
          ,["c" => "-", "r" => "GA", "s" => "-", "desc" => "GABON"]
          ,["c" => "-", "r" => "TF", "s" => "-", "desc" => "FRENCH SOUTHERN TERRITORIES"]
          ,["c" => "-", "r" => "PF", "s" => "-", "desc" => "FRENCH POLYNESIA"]
          ,["c" => "-", "r" => "GF", "s" => "-", "desc" => "FRENCH GUIANA"]
          ,["c" => "-", "r" => "FX", "s" => "-", "desc" => "FRANCE, METROPOLITAN"]
          ,["c" => "-", "r" => "FJ", "s" => "-", "desc" => "FIJI"]
          ,["c" => "-", "r" => "FK", "s" => "-", "desc" => "FALKLAND ISLANDS (MALVINAS)"]
          ,["c" => "-", "r" => "ET", "s" => "-", "desc" => "ETHIOPIA"]
          ,["c" => "-", "r" => "FO", "s" => "-", "desc" => "FAROE ISLANDS"]
          ,["c" => "-", "r" => "EE", "s" => "-", "desc" => "ESTONIA"]
          ,["c" => "-", "r" => "ER", "s" => "-", "desc" => "ERITREA"]
          ,["c" => "-", "r" => "GQ", "s" => "-", "desc" => "EQUATORIAL GUINEA"]
          ,["c" => "-", "r" => "SV", "s" => "-", "desc" => "EL SALVADOR"]
          ,["c" => "-", "r" => "EG", "s" => "-", "desc" => "EGYPT"]
          ,["c" => "-", "r" => "EC", "s" => "-", "desc" => "ECUADOR"]
          ,["c" => "-", "r" => "TL", "s" => "-", "desc" => "EAST TIMOR"]
          ,["c" => "-", "r" => "DO", "s" => "-", "desc" => "DOMINICAN REPUBLIC"]
          ,["c" => "-", "r" => "DM", "s" => "-", "desc" => "DOMINICA"]
          ,["c" => "-", "r" => "DJ", "s" => "-", "desc" => "DJIBOUTI"]
          ,["c" => "-", "r" => "DK", "s" => "-", "desc" => "DENMARK"]
          ,["c" => "-", "r" => "CY", "s" => "-", "desc" => "CYPRUS"]
          ,["c" => "-", "r" => "CU", "s" => "-", "desc" => "CUBA"]
          ,["c" => "-", "r" => "HR", "s" => "-", "desc" => "CROATIA (HRVATSKA)"]
          ,["c" => "-", "r" => "CK", "s" => "-", "desc" => "COOK ISLANDS"]
          ,["c" => "-", "r" => "CG", "s" => "-", "desc" => "CONGO PEOPLES REPUBLIC OF"]
          ,["c" => "-", "r" => "KM", "s" => "-", "desc" => "COMOROS"]
          ,["c" => "-", "r" => "CO", "s" => "-", "desc" => "COLOMBIA"]
          ,["c" => "-", "r" => "CC", "s" => "-", "desc" => "COCOS (KEELING) ISLANDS"]
          ,["c" => "-", "r" => "CN", "s" => "-", "desc" => "CHINA"]
          ,["c" => "-", "r" => "CX", "s" => "-", "desc" => "CHRISTMAS ISLAND"]
          ,["c" => "-", "r" => "CL", "s" => "-", "desc" => "CHILE"]
          ,["c" => "-", "r" => "TD", "s" => "-", "desc" => "CHAD"]
          ,["c" => "-", "r" => "CF", "s" => "-", "desc" => "CENTRAL AFRICAN REPUBLIC"]
          ,["c" => "-", "r" => "KY", "s" => "-", "desc" => "CAYMAN ISLANDS"]
          ,["c" => "-", "r" => "CV", "s" => "-", "desc" => "CAPE VERDE"]
          ,["c" => "-", "r" => "CA", "s" => "-", "desc" => "CANADA"]
          ,["c" => "-", "r" => "CM", "s" => "-", "desc" => "CAMEROON"]
          ,["c" => "-", "r" => "KH", "s" => "-", "desc" => "CAMBODIA"]
          ,["c" => "-", "r" => "BI", "s" => "-", "desc" => "BURUNDI"]
          ,["c" => "-", "r" => "BF", "s" => "-", "desc" => "BURKINA FASO"]
          ,["c" => "-", "r" => "BN", "s" => "-", "desc" => "BRUNEI DARUSSALAM"]
          ,["c" => "-", "r" => "IO", "s" => "-", "desc" => "BRITISH INDIAN OCEAN TERRITORY"]
          ,["c" => "-", "r" => "BR", "s" => "-", "desc" => "BRAZIL"]
          ,["c" => "-", "r" => "BV", "s" => "-", "desc" => "BOUVET ISLAND"]
          ,["c" => "-", "r" => "BW", "s" => "-", "desc" => "BOTSWANA"]
          ,["c" => "-", "r" => "BA", "s" => "-", "desc" => "BOSNIA AND HERZEGOWINA"]
          ,["c" => "-", "r" => "BO", "s" => "-", "desc" => "BOLIVIA"]
          ,["c" => "-", "r" => "BT", "s" => "-", "desc" => "BHUTAN"]
          ,["c" => "-", "r" => "BM", "s" => "-", "desc" => "BERMUDA"]
          ,["c" => "-", "r" => "BJ", "s" => "-", "desc" => "BENIN"]
          ,["c" => "-", "r" => "BZ", "s" => "-", "desc" => "BELIZE"]
          ,["c" => "-", "r" => "BE", "s" => "-", "desc" => "BELGIUM"]
          ,["c" => "-", "r" => "BY", "s" => "-", "desc" => "BELARUS"]
          ,["c" => "-", "r" => "BB", "s" => "-", "desc" => "BARBADOS"]
          ,["c" => "-", "r" => "BD", "s" => "-", "desc" => "BANGLADESH"]
          ,["c" => "-", "r" => "BS", "s" => "-", "desc" => "BAHAMAS"]
          ,["c" => "-", "r" => "AZ", "s" => "-", "desc" => "AZERBAIJAN"]
          ,["c" => "-", "r" => "AU", "s" => "-", "desc" => "AUSTRALIA"]
          ,["c" => "-", "r" => "AW", "s" => "-", "desc" => "ARUBA"]
          ,["c" => "-", "r" => "AM", "s" => "-", "desc" => "ARMENIA"]
          ,["c" => "-", "r" => "AQ", "s" => "-", "desc" => "ANTARCTICA"]
          ,["c" => "-", "r" => "AI", "s" => "-", "desc" => "ANGUILLA"]
          ,["c" => "-", "r" => "AO", "s" => "-", "desc" => "ANGOLA"]
          ,["c" => "-", "r" => "CD", "s" => "-", "desc" => "CONGO DEMOCRATIC REPUBLIC OF"]
          ,["c" => "-", "r" => "BH", "s" => "-", "desc" => "BAHRAIN"]
          ,["c" => "-", "r" => "AD", "s" => "-", "desc" => "ANDORRA"]
          ,["c" => "-", "r" => "AS", "s" => "-", "desc" => "AMERICAN SAMOA"]
          ,["c" => "-", "r" => "DZ", "s" => "-", "desc" => "ALGERIA"]
          ,["c" => "-", "r" => "AL", "s" => "-", "desc" => "ALBANIA"]
          ,["c" => "-", "r" => "AF", "s" => "-", "desc" => "AFGHANISTAN"]
          ,["c" => "-", "r" => "SY", "s" => "-", "desc" => "SYRIAN ARAB REPUBLIC"]
          ,["c" => "-", "r" => "FR", "s" => "-", "desc" => "FRANCE"]
          ,["c" => "-", "r" => "CZ", "s" => "-", "desc" => "CZECH REPUBLIC"]
          ,["c" => "-", "r" => "AR", "s" => "-", "desc" => "ARGENTINA"]
          ,["c" => "-", "r" => "GL", "s" => "-", "desc" => "GREENLAND"]
          ,["c" => "-", "r" => "BG", "s" => "-", "desc" => "BULGARIA"]
          ,["c" => "-", "r" => "CR", "s" => "-", "desc" => "COSTA RICA"]
          ,["c" => "-", "r" => "AG", "s" => "-", "desc" => "ANTINGUA AND BARBUDA"]
          ,["c" => "-", "r" => "FI", "s" => "-", "desc" => "FINLAND"]
          ,["c" => "-", "r" => "CI", "s" => "-", "desc" => "COTE D'IVOIRE"]
          ,["c" => "-", "r" => "AT", "s" => "-", "desc" => "AUSTRIA"]
          ,["c" => "-", "r" => "TW", "s" => "-", "desc" => "TAIWAN"]
          ,["c" => "-", "r" => "TJ", "s" => "-", "desc" => "TAJIKISTAN"]
          ,["c" => "-", "r" => "TZ", "s" => "-", "desc" => "TANZANIA, UNITED REPUBLIC OF"]
          ,["c" => "-", "r" => "TH", "s" => "-", "desc" => "THAILAND"]
          ,["c" => "-", "r" => "TG", "s" => "-", "desc" => "TOGO"]
          ,["c" => "-", "r" => "TK", "s" => "-", "desc" => "TOKELAU"]
          ,["c" => "-", "r" => "TO", "s" => "-", "desc" => "TONGA"]
          ,["c" => "-", "r" => "TT", "s" => "-", "desc" => "TRINIDAD AND TOBAGO"]
          ,["c" => "-", "r" => "TN", "s" => "-", "desc" => "TUNISIA"]
          ,["c" => "-", "r" => "TR", "s" => "-", "desc" => "TURKEY"]
          ,["c" => "-", "r" => "TM", "s" => "-", "desc" => "TURKMENISTAN"]
          ,["c" => "-", "r" => "TC", "s" => "-", "desc" => "TURKS AND CAICOS ISLANDS"]
          ,["c" => "-", "r" => "TV", "s" => "-", "desc" => "TUVALU"]
          ,["c" => "-", "r" => "UG", "s" => "-", "desc" => "UGANDA"]
          ,["c" => "-", "r" => "UA", "s" => "-", "desc" => "UKRAINE"]
          ,["c" => "-", "r" => "AE", "s" => "-", "desc" => "UNITED ARAB EMIRATES"]
          ,["c" => "-", "r" => "GB", "s" => "-", "desc" => "UNITED KINGDOM"]
          ,["c" => "-", "r" => "US", "s" => "-", "desc" => "UNITED STATES"]
          ,["c" => "-", "r" => "UM", "s" => "-", "desc" => "US MINOR OUTLYING ISLANDS"]
          ,["c" => "-", "r" => "UY", "s" => "-", "desc" => "URUGUAY"]
          ,["c" => "-", "r" => "VU", "s" => "-", "desc" => "VANUATU"]
          ,["c" => "-", "r" => "VA", "s" => "-", "desc" => "VATICAN CITY STATE (HOLY SEE)"]
          ,["c" => "-", "r" => "VE", "s" => "-", "desc" => "VENEZUELA"]
          ,["c" => "-", "r" => "VN", "s" => "-", "desc" => "VIET NAM"]
          ,["c" => "-", "r" => "VG", "s" => "-", "desc" => "VIRGIN ISLANDS (BRITISH)"]
          ,["c" => "-", "r" => "WF", "s" => "-", "desc" => "WALLIS AND FUTUNA ISLANDS"]
          ,["c" => "-", "r" => "EH", "s" => "-", "desc" => "WESTERN SAHARA"]
          ,["c" => "-", "r" => "YE", "s" => "-", "desc" => "YEMEN"]
          ,["c" => "-", "r" => "YU", "s" => "-", "desc" => "YUGOSLAVIA"]
          ,["c" => "-", "r" => "ZM", "s" => "-", "desc" => "ZAMBIA"]
          ,["c" => "-", "r" => "ZW", "s" => "-", "desc" => "ZIMBABWE"]
          ,["c" => "US", "r" => "NV", "s" => "-", "desc" => "NEVADA"]
          ,["c" => "-", "r" => "GE", "s" => "-", "desc" => "GEORGIA"]
        ]
      ];

      $response = new HTTPResponse(json_encode($data));
      $response->addHeader('Content-Type', 'application/json');

      return $response;
    }

    public function getRates() {
      parse_str(base64_decode($this->getRequest()->param('Data')),$data);
      return $this->requestResRates($data);
    }

    public function getEstimate() {
      parse_str(base64_decode($this->getRequest()->param('Data')),$data);
      return $this->requestResEstimate($data);
    }

    public function getOptions() {
      parse_str(base64_decode($this->getRequest()->param('Data')),$data);
      return $this->requestResOptions($data);
    }

    public function createNewReservation() {
      parse_str(base64_decode($this->getRequest()->param('Data')),$data);
      return $this->requestNewReservation($data);
    }

    public function retrieveReservation() {
      parse_str(base64_decode($this->getRequest()->param('Data')),$data);
      return $this->requestReservationInfo($data);
    }

    public function retrieveEstimate() {
      parse_str(base64_decode($this->getRequest()->param('Data')),$data);
      return $this->requestEstimate($data);
    }

    public function updateReservation() {
      parse_str(base64_decode($this->getRequest()->param('Data')),$data);
      return $this->requestUpdateReservation($data);
    }

    function requestResRates($data){
      $data['rn'] = $this->guid();
      $request = "<?xml version='1.0' encoding='UTF-8'?>
      <Request xmlns='http://www.thermeon.com/webXG/xml/webxml/' referenceNumber='${data['rn']}' version='2.3100'>
          <ResRates>
              <Pickup locationCode='${data['pl']}' dateTime='${data['pd']}'/>
              <Return locationCode='${data['rl']}' dateTime='${data['rd']}'/>
              <CorpRateID>${data['cid']}</CorpRateID>
              <EstimateType>${data['et']}</EstimateType>
              <Renter>
                <BirthDate>${data['dob']}</BirthDate>
              </Renter>
          </ResRates>
      </Request>
      ";

      $response = new HTTPResponse($this->sendRequest($request));
      $response->addHeader('Content-Type', 'application/json');

      return $response;
    }

    function requestResEstimate($data){
      $data['rn'] = $this->guid();

      $options = '';
      if($data['opt'] != ''){
        $opt = explode('|',$data['opt']);
        foreach ($opt as $o) {
          $parts = explode(':',$o);
          $options .= "<Option>
            <Code>${parts[0]}</Code>
            <Qty>${parts[1]}</Qty>
          </Option>
          ";
        }
      }

      $request = "<?xml version='1.0' encoding='UTF-8'?>
      <Request xmlns='http://www.thermeon.com/webXG/xml/webxml/' referenceNumber='${data['rn']}' version='2.3100'>
        <ResEstimate>
          <Pickup locationCode='${data['pl']}' dateTime='${data['pd']}'/>
          <Return locationCode='${data['rl']}' dateTime='${data['rd']}'/>
          <Vehicle classCode='${data['car']}' />
          <Renter>
          <BirthDate>${data['dob']}</BirthDate>
        </Renter>
        <QuotedRate rateID='${data['rid']}' classCode='${data['car']}' corporateRateID='${data['cid']}' />
        $options
        </ResEstimate>
      </Request>
      ";

      $response = new HTTPResponse($this->sendRequest($request));
      $response->addHeader('Content-Type', 'application/json');

      return $response;
    }

    function requestResOptions($data){
      $data['rn'] = $this->guid();
      $request = "<?xml version='1.0' encoding='UTF-8'?>
      <Request xmlns='http://www.thermeon.com/webXG/xml/webxml/' referenceNumber='${data['rn']}' version='2.3100'>
        <ResOptions>
          <Pickup locationCode='${data['pl']}' dateTime='${data['pd']}'/>
          <Return locationCode='${data['rl']}' dateTime='${data['rd']}'/>
          <Vehicle classCode='${data['car']}' />
          <Renter>
          <BirthDate>${data['dob']}</BirthDate>
        </Renter>
        <QuotedRate rateID='${data['rid']}' classCode='${data['car']}' corporateRateID='${data['cid']}' />
        </ResOptions>
      </Request>
      ";

      $response = new HTTPResponse($this->sendRequest($request));
      $response->addHeader('Content-Type', 'application/json');

      return $response;
    }

    function requestNewReservation($data){
      $data['rn'] = $this->guid();

      $options = '';
      if($data['opt'] != ''){
        $opt = explode('|',$data['opt']);
        foreach ($opt as $o) {
          $parts = explode(':',$o);
          $options .= "<Option>
            <Code>${parts[0]}</Code>
            <Qty>${parts[1]}</Qty>
          </Option>
          ";
        }
      }

      $request = "<?xml version='1.0' encoding='UTF-8'?>
      <Request xmlns='http://www.thermeon.com/webXG/xml/webxml/' referenceNumber='${data['rn']}' version='2.3100'>
        <NewReservationRequest confirmAvailability='true' status='${data['st']}'>
          <Pickup locationCode='${data['pl']}' dateTime='${data['pd']}'/>
          <Return locationCode='${data['rl']}' dateTime='${data['rd']}'/>
          <Source confirmationNumber='${data['cn']}' countryCode='${data['cnt']}'/>
          <Vehicle classCode='${data['car']}' />
          <Renter>
            <RenterName firstName='${data['fnm']}' lastName='${data['lnm']}'/>
            <Address>
              <CountryCode>${data['cnt']}</CountryCode>
              <Email>${data['eml']}</Email>
              <CellTelephoneNumber>${data['mob']}</CellTelephoneNumber>
            </Address>
            <BirthDate>${data['dob']}</BirthDate>
          </Renter>
          <QuotedRate rateID='${data['rid']}' classCode='${data['car']}' corporateRateID='${data['cid']}' />
          <ReservationNotes>
            <Note>${data['rmk']}</Note>
          </ReservationNotes>
          $options
        </NewReservationRequest>
      </Request>
      ";

      $response = new HTTPResponse($this->sendRequest($request));
      $response->addHeader('Content-Type', 'application/json');

      return $response;
    }

    function requestUpdateReservation($data){
      $data['rn'] = $this->guid();

      $options = '';
      if($data['opt'] != ''){
        $opt = explode('|',$data['opt']);
        foreach ($opt as $o) {
          $parts = explode(':',$o);
          $options .= "<Option>
            <Code>${parts[0]}</Code>
            <Qty>${parts[1]}</Qty>
          </Option>
          ";
        }
      }

      $now = date("YmdH:i");
      $expparts = explode(' / ',$data['exp']);
      $exp = '20'. $expparts[1] . '-' . $expparts[0];
      $truncatednum = str_replace('00000000','..',$data['num']);

      if($data['payment'] != false){
        $payment = "<Deposit>
          <CreditCard cardType='WEBEFT' cardNumber='${data['num']}' expiresYearMonth='$exp' swiped='false'/>
          <Auth amount='${data['amount']}' currencyCode='${data['currency']}' use_dcc='false'>
            <CreditCard cardType='WEBEFT' cardNumber='${data['num']}' expiresYearMonth='$exp' swiped='true'/>
          </Auth>
        </Deposit>";
        $paymentnotes = "<Note>[PYMT]|$now|${data['amount']}${data['currency']}|$truncatednum|$exp</Note>";
      }else{
         $payment = "" ;
         $paymentnotes = "";
      }

      $request = "<?xml version='1.0' encoding='UTF-8'?>
      <Request xmlns='http://www.thermeon.com/webXG/xml/webxml/' referenceNumber='${data['rn']}' version='2.3100'>
        <UpdateReservationRequest confirmAvailability='true' status='${data['st']}' reservationNumber='${data['resno']}'>
          <Pickup locationCode='${data['pl']}' dateTime='${data['pd']}'/>
          <Return locationCode='${data['rl']}' dateTime='${data['rd']}'/>
          <Source confirmationNumber='${data['cn']}' countryCode='${data['cnt']}'/>
          <Vehicle classCode='${data['car']}' />
          <Renter>
            <RenterName firstName='${data['fnm']}' lastName='${data['lnm']}'/>
            <Address>
              <CountryCode>${data['cnt']}</CountryCode>
              <Email>${data['eml']}</Email>
              <CellTelephoneNumber>${data['mob']}</CellTelephoneNumber>
            </Address>
          </Renter>
          <QuotedRate rateID='${data['rid']}' classCode='${data['car']}' corporateRateID='${data['cid']}' />
          $payment
          <ReservationMainNote>Main Notes</ReservationMainNote>
          <ReservationNotes>
            $paymentnotes
          </ReservationNotes>
          $options
        </UpdateReservationRequest>
      </Request>";

      // echo $request;
      $response = new HTTPResponse($this->sendRequest($request));
      $response->addHeader('Content-Type', 'application/json');

      return $response;
    }

    function requestReservationInfo($data){
      $data['rn'] = $this->guid();

      $request = "<?xml version='1.0' encoding='UTF-8'?>
      <Request xmlns='http://www.thermeon.com/webXG/xml/webxml/' referenceNumber='${data['rn']}' version='2.3100'>
        <RetrieveReservationRequest reservationNumber='${data['resno']}' />
      </Request>
      ";

      $response = new HTTPResponse($this->sendRequest($request));
      $response->addHeader('Content-Type', 'application/json');
      return $response;
    }


    function requestEstimate($data){
      $data['rn'] = $this->guid();

      $request = "<?xml version='1.0' encoding='UTF-8'?>
      <Request xmlns='http://www.thermeon.com/webXG/xml/webxml/' referenceNumber='${data['rn']}' version='2.3100'>
        <ResEstimate reservationNumber='${data['resno']}' />
      </Request>
      ";

      $response = new HTTPResponse($this->sendRequest($request));
      $response->addHeader('Content-Type', 'application/json');
      return $response;
    }

    function sendRequest($req){
        $username ='hnzjjt';
        $password = 'WELCOME';
        $endpoint = "https://webxml.au.thermeon.io/webXG/hau001-webres-test/xml/webxml/";
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_CUSTOMREQUEST   => "POST",
          CURLOPT_URL             => $endpoint,
          CURLOPT_POSTFIELDS      => $req,
          CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
          CURLOPT_SSL_VERIFYHOST  => 0,
          CURLOPT_SSL_VERIFYPEER  => 0,
          CURLOPT_RETURNTRANSFER  => true,
          CURLOPT_ENCODING        => "UTF-8",
          CURLOPT_MAXREDIRS       => 10,
          CURLOPT_TIMEOUT         => 30,
          CURLOPT_HTTPHEADER      => array(
            "authorization: Basic " . base64_encode($username . ':' . $password),
            "cache-control: no-cache",
            "content-type: text/xml"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          return '{"Response":"cURL Error #: '. $err . '"}';
        } else {
          $xml = simplexml_load_string($response, "SimpleXMLElement", LIBXML_NOCDATA);
          return json_encode($xml);
        }
    }

    function guid(){
      if (function_exists('com_create_guid')){
        return com_create_guid();
      }else{
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45); // "-"
        $uuid =  substr($charid, 0, 8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid,12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid,20,12);
        return $uuid;
      }
    }
}
