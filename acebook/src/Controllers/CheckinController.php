<?php

namespace AceRentals\Checkin\Controllers;
use AceRentals\Pages\VehiclePage;
use AceRentals\Pages\LocationPage;
use SilverStripe\ORM\DataObject;

ini_set('display_errors', 0);


use SilverStripe\Control\Controller;
use SilverStripe\Control\HTTPRequest;

class CheckinController extends \PageController
{

    private static $allowed_actions = [
        'checkin',
        'bookinginfo',
        'changevehicle',
        'acebookGetVehicles',
        'changeextras',
        'changepersonaldetails',
        'paymentoption',
        'summary',
        'getInfoData',
        'getCharges',
        'getOptions',
        'getCovers'
    ];
     public function index(HTTPRequest $request) {
        return $this->renderWith("Page", array_merge($request->allParams(), array(
            'dir'=>ACEBOOK_DIR,
            'MetaTitle'=>'Checkin | Ace Rental Cars'
        )));
    }
     public function checkin(HTTPRequest $request) {
        return $this->renderWith("Page", array_merge($request->allParams(), array(
            'dir'=>ACEBOOK_DIR,
            'MetaTitle'=>'Checkin | Ace Rental Cars'
        )));
    }
    public function bookinginfo(HTTPRequest $request) {
        return $this->renderWith("Page", array_merge($request->allParams(), array(
            'dir'=>ACEBOOK_DIR,
            'MetaTitle'=>'Checkin | Ace Rental Cars'
        )));
    }
    public function changevehicle(HTTPRequest $request) {
        return $this->renderWith("Page", array_merge($request->allParams(), array(
            'dir'=>ACEBOOK_DIR,
            'MetaTitle'=>'Checkin | Ace Rental Cars'
        ), $request->postVars()));
    }
    public function changeextras(HTTPRequest $request) {
        return $this->renderWith("Page", array_merge($request->allParams(), array(
            'dir'=>ACEBOOK_DIR,
            'MetaTitle'=>'Checkin | Ace Rental Cars'
        ), $request->postVars()));
    }
    public function changepersonaldetails(HTTPRequest $request) {
        return $this->renderWith("Page", array_merge($request->allParams(), array(
            'dir'=>ACEBOOK_DIR,
            'MetaTitle'=>'Checkin | Ace Rental Cars'
        ), $request->postVars()));
    }

    public function paymentoption(HTTPRequest $request) {
        return $this->renderWith("Page", array_merge($request->allParams(), array(
            'dir'=>ACEBOOK_DIR,
            'MetaTitle'=>'Checkin | Ace Rental Cars'
        ), $request->postVars()));
    }


    public function summary(HTTPRequest $request) {
        return $this->renderWith("Page", array_merge($request->allParams(), array(
            'dir'=>ACEBOOK_DIR,
            'MetaTitle'=>'Checkin | Ace Rental Cars'
        ), $request->postVars()));
    }



    public static function isCheckin(){
        return true;
    }

    public static function LoadCheckin(){
        return true;
    }

    public function getLocale(){
        return $this->request->param('locale');
    }
    
    public function acebookGetVehicles() {
        $vehicles = VehiclePage::get();
        $cars = [];
        foreach($vehicles as $veh){
            $specs = $veh->Specifications();
            $arr_specs = [];
            foreach($specs as $sp){
                $arr_specs[] = [
                    'title'=>$sp->Title,
                    'icon'=>$sp->Icon()->getURL(), 
                    'fullwidth'=>$sp->FullWidth
                ];
            }
            $cars[$veh->VehicleClass] = [
                'title' => $veh->Title,
                'age' => $veh->Age,
                'specs'=>$arr_specs,
                'image'=>$veh->Image()->getURL(),  
                'upgradeimage'=>$veh->CarImage()->getURL(),      
            ];
        }

        return json_encode($cars);
    }

    public function getInfoData(){
        $locations = LocationPage::get();
        $infoData = [];
        foreach($locations as $location){
            $country=$location->Country=='New Zealand'? 'nz' : 'au';
            $infoData[$country][$location->RcmLocationID] = [
                'infoDesc' => $location->InfoBoxDescription,
                'address' => $location->Address,
                'image'=>$location->InfoBoxImage()->getURL(),    
            ];
        }
        return json_encode($infoData);
    }

    public function getCharges(){
        //DataObject::get_by_id('Team',$myPlayer->TeamID);
        $charges =DataObject::get("Option","Grouping='Charge'", "", "", "");
        
        $additionalCharges = [];
        foreach($charges as $charge){
            $additionalCharges[] = [
                'Code' => $charge->Code,
                'Description' => $charge->Description,
                'IncludedInTotal'=>$charge->IncludedInTotal,    
            ];
        }
        return json_encode($additionalCharges);
    }

    public function getOptions(){
        //DataObject::get_by_id('Team',$myPlayer->TeamID);
        $charges =DataObject::get("Option","Grouping='Option'", "", "", "");
        
        $additionalCharges = [];
        foreach($charges as $charge){
            $additionalCharges[] = [
                'Code' => $charge->Code,
                'Description' => $charge->Description,
                'RateType'=>$charge->RateType,    
            ];
        }
        return json_encode($additionalCharges);
    }

    public function getCovers(){
        //DataObject::get_by_id('Team',$myPlayer->TeamID);
        $charges =DataObject::get("Option","Grouping='Cover'", "", "", "");
        
        $additionalCharges = [];
        foreach($charges as $charge){
            $additionalCharges[] = [
                'Code' => $charge->Code,
                'Description' => $charge->Description,
                'Specifications'=>$charge->Specifications,    
            ];
        }
        return json_encode($additionalCharges);
    }

}