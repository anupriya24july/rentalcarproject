<?php
namespace AceRentals\Payments\Controllers;

ini_set('display_errors', 0);

use Omnipay\Omnipay;
use Omnipay\PaymentExpress\Message\PxPayAuthorizeResponse;
use Omnipay\PaymentExpress\Message\PxPayCompleteAuthorizeRequest;
use Omnipay\PaymentExpress\PxPostGateway;
use Omnipay\Common\Helper;

use SilverStripe\Control\Controller;
use SilverStripe\Control\HTTPRequest;
use Aws\Ses\SesClient;
use Aws\Ses\Exception\SesException;

class PaymentsController extends Controller
{

    private static $allowed_actions = [
        'purchase',
        'callback',
        'emailConfimation'
    ];

    public function purchase(HTTPRequest $request) {
        if($request->isPOST()){
            $gateway = $this->getPxPostGateway();

            $card = array(
                'firstName' => $request->postVar('name'),
                'lastName' => '',
                'number' => $request->postVar('num'),
                'expiryMonth' => explode("/",$request->postVar('exp'))[0],
                'expiryYear' => explode("/",$request->postVar('exp'))[1],
                'cvv' => $request->postVar('cvc'),
                'email' => $request->postVar('email')
            );

            $options = array(
                'description' => $request->postVar('resno'),
                'transactionId' => $request->postVar('resno'),
                'card' => $card,
                'amount' =>  $request->postVar('amount'),
                'returnUrl' => 'https://' . $_SERVER['SERVER_NAME'] . '/payments/callback'
            );

            $response = array(
                'isSuccessful' => false,
                'responseText' => 'Sorry, there was an error processing your payment. Please try again later.',
                'transaction' => array()
            );

            try {
                $response = $gateway->purchase($options)->send();
                // $transaction = $(array) $response->getData()->Transaction;
                $transaction = array(
                    'ReCo' => (string) $response->getData()->Transaction->ReCo,
                    'AcquirerDate' => (string) $response->getData()->Transaction->AcquirerDate,
                    'AcquirerTime' => (string) $response->getData()->Transaction->AcquirerTime,
                    'CardName' => (string) $response->getData()->Transaction->CardName,
                    'DpsTxnRef' => (string) $response->getData()->Transaction->DpsTxnRef,
                    'DpsBillingId' => (string) $response->getData()->Transaction->DpsBillingId,
                    'TestMode' => (string) $response->getData()->Transaction->TestMode,
                );

                if ($response->isSuccessful()) {
                    // payment was successful: confirm RCM payment
                    
                    return json_encode(array(
                        'isSuccessful' => true,
                        'responseText' => $response->getMessage(),
                        'Transaction' => $transaction,
                    ));
                } elseif ($response->isRedirect()) {
                    // redirect to offsite payment gateway
                    $response->redirect();
                } else {
                    // payment failed: display message to customer
                    return json_encode(array(
                        'isSuccessful' => false,
                        'responseText' => $response->getMessage(),
                        'Transaction' => $transaction,
                    ));
                }
            } catch (\Exception $e) {
                // internal error, log exception and display a generic message to the customer
                return json_encode(array(
                    'isSuccessful' => false,
                    'responseText' => $e->getMessage() .'. Sorry, there was an error processing your payment. Please try again later.',
                    'transaction' => array()
                ));
            }
            
        }
    }

    public function callback(){

    }

    public function emailConfimation(HTTPRequest $request){
                                                 
        // Replace sender@example.com with your "From" address. 
        // This address must be verified with Amazon SES.
        define('SENDER', 'Ace Rental Cars <info@acerentalcars.co.nz>');           

        // Replace recipient@example.com with a "To" address. If your account 
        // is still in the sandbox, this address must be verified.
        define('RECIPIENT', 'junjet@acerentals.co.nz');    

        // Replace us-west-2 with the AWS Region you're using for Amazon SES.
        define('REGION','ap-southeast-2'); 

        define('SUBJECT','Amazon SES test (AWS SDK for PHP)');

        define('HTMLBODY','<h1>AWS Amazon Simple Email Service Test Email</h1>'.
                          '<p>This email was sent with <a href="https://aws.amazon.com/ses/">'.
                          'Amazon SES</a> using the <a href="https://aws.amazon.com/sdk-for-php/">'.
                          'AWS SDK for PHP</a>.</p>');
        define('TEXTBODY','This email was send with Amazon SES using the AWS SDK for PHP.');

        define('CHARSET','UTF-8');

        $client = SesClient::factory(array(
            'version'=> 'latest',     
            'region' => 'us-east-1',
            'credentials' => array(
                'key'    => 'AKIAIQLJLL2L73SGCSLA',
                'secret' => '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',
            )
        ));

        try {
             $result = $client->sendEmail([
            'Destination' => [
                'ToAddresses' => [
                    RECIPIENT,
                ],
            ],
            'Message' => [
                'Body' => [
                    'Html' => [
                        'Charset' => CHARSET,
                        'Data' => HTMLBODY,
                    ],
                    'Text' => [
                        'Charset' => CHARSET,
                        'Data' => TEXTBODY,
                    ],
                ],
                'Subject' => [
                    'Charset' => CHARSET,
                    'Data' => SUBJECT,
                ],
            ],
            'Source' => SENDER,
        ]);
             $messageId = $result->get('MessageId');
             echo("Email sent! Message ID: $messageId"."\n");

        } catch (SesException $error) {
             echo("The email was not sent. Error message: ".$error->getAwsErrorMessage()."\n");
        }

    }

    public function getPxPostGateway(){

        $parts = explode(".", $_SERVER['HTTP_HOST']);
        $tld = end($parts);
        $env = ($parts[0] == 'www' || $parts[0] == 'live' )? 'live':'dev';
        $activecountry = $tld;

        //LG staging
        if($parts[0] == 'acerentals'){
            $env = 'live';
            $activecountry = 'nz';
        }
        if($parts[0] == 'acerentals-au'){
            $env = 'live';
            $activecountry = 'au';
        }

        $pxpost['live']['nz']['username'] = 'AceRentals';
        $pxpost['live']['nz']['password'] = 'fa2531e2';
        $pxpost['live']['nz']['currency'] = 'NZD';

        // $pxpost['live']['au']['username'] = 'AceRentalSyd7812';
        // $pxpost['live']['au']['password'] = 'ccc89063';
        // $pxpost['live']['au']['currency'] = 'AUD';

        $pxpost['live']['au']['username'] = 'AceTouristRentalsAU';
        $pxpost['live']['au']['password'] = 'df512bea';
        $pxpost['live']['au']['currency'] = 'AUD';

        $pxpost['dev']['nz']['username'] = 'AceRentalCarsDev25';
        $pxpost['dev']['nz']['password'] = 'test1234';
        $pxpost['dev']['nz']['currency'] = 'NZD';

        $pxpost['dev']['au']['username'] = 'AceRentalCarsDev25';
        $pxpost['dev']['au']['password'] = 'test1234';
        $pxpost['dev']['au']['currency'] = 'NZD';

        $gateway = Omnipay::create('\\Omnipay\PaymentExpress\\PxPostGateway');
        $gateway->setUsername($pxpost[$env][$activecountry]['username']);
        $gateway->setPassword($pxpost[$env][$activecountry]['password']);
        $gateway->setCurrency($pxpost[$env][$activecountry]['currency']);

        return $gateway;
    }
}
