<?php

namespace AceRentals\Bookings\Controllers;

use SilverStripe\Control\Controller;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\Core\Environment;
use AceRentals\Pages\VehiclePage;
use AceRentals\Pages\LocationPage;
use SilverStripe\ORM\DataObject;

use AceRentals\Pages\MiscellaneousPage;
use TractorCow\Fluent\Model\Locale;
use TractorCow\Fluent\State\FluentState;

class BookingsController extends \PageController
{

    private static $allowed_actions = [
        'index',
        'itinerary',
        'selectvehicle',
        'addextras',
        'personaldetails',
        'payment',
        'complete',
        'isAceBooking',
        'LoadAceBooking',
        'getLocale',
        'getLocalizedLabels',
        'acebookGetVehicles',
        'getInfoData',
        'search',
        'getCharges',
        'getOptions',
        'getCovers'
    ];

    public function Title(){
        return MiscellaneousPage::get()->first()->toMap()['BookingLabel'];
    }

    public static function isAceBookApp(){
        return true;
    }

    public static function LoadAceBooking(){
        return true;
    }

    public static function URLSegment(){
        return "bookings";
    }

    public function getLocale(){
        $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $uri_segments = explode('/', trim($uri_path,"/"));
        if(count($uri_segments) >= 2 && ($uri_segments[0] == 'bookings' || $uri_segments[1] == 'bookings')){
            return ($uri_segments[1] == 'bookings'? $uri_segments[0] : ($uri_segments[0] == 'bookings'? "" : Locale::getDefault(true)->getURLSegment()));
        }else{
            return "";
        }
    }

    public function getLocalizedLabels() {
        $locale = MiscellaneousPage::get()->first()->toMap();
        return json_encode($locale);
    }

    public function getLabels() {
        $currentLocale = Locale::getCurrentLocale()->Locale;
        return MiscellaneousPage::get()->first();
    }

    public function getLocaleSegment() {
        $urlSeg="";
        if(!Locale::getCurrentLocale()->IsGlobalDefault){
            $urlSeg = Locale::getCurrentLocale()->URLSegment.'/';
        }
        return $urlSeg;
    }

    public function acebookGetVehicles() {
        $vehicles = VehiclePage::get();
        $cars = [];
        foreach($vehicles as $veh){
            $specs = $veh->Specifications();
            $arr_specs = [];
            foreach($specs as $sp){
                $arr_specs[] = [
                    'title'=>$sp->Title,
                    'icon'=>$sp->Icon()->getURL(), 
                    'fullwidth'=>$sp->FullWidth
                ];
            }
            switch(Environment::getEnv('API')){
                case 'rcm':
                    $index = $veh->CategoryTypeID;
                    break;
                case 'plus':
                    $index = $veh->VehicleClass==''? 'noclass':$veh->VehicleClass;
                    break;
            }
            $cars[$index] = [
                'title' => $veh->Title,
                'age' => $veh->Age,
                'specs'=>$arr_specs,
                'image'=>$veh->Image()->getURL(),  
                'upgradeimage'=>$veh->CarImage()->getURL(),    
            ];
        }

        return json_encode($cars);
    }

    public function getInfoData(){
        $locations = LocationPage::get();
        $infoData = [];
        foreach($locations as $location){
            $country=$location->Country=='New Zealand'? 'nz' : 'au';
            $infoData[$country][$location->RcmLocationID] = [
                'infoDesc' => $location->InfoBoxDescription,
                'address' => $location->Address,
                'image'=>$location->InfoBoxImage()->getURL(),    
            ];
        }
        return json_encode($infoData);
    }

    public function getCharges(){
        //DataObject::get_by_id('Team',$myPlayer->TeamID);
        $charges =DataObject::get("Option","Grouping='Charge'", "", "", "");
        
        $additionalCharges = [];
        foreach($charges as $charge){
            $additionalCharges[] = [
                'Code' => $charge->Code,
                'Description' => $charge->Description,
                'IncludedInTotal'=>$charge->IncludedInTotal,    
            ];
        }
        return json_encode($additionalCharges);
    }

    public function getOptions(){
        //DataObject::get_by_id('Team',$myPlayer->TeamID);
        $charges =DataObject::get("Option","Grouping='Option'", "", "", "");
        
        $additionalCharges = [];
        foreach($charges as $charge){
            $additionalCharges[] = [
                'Code' => $charge->Code,
                'Description' => $charge->Description,
                'RateType'=>$charge->RateType,    
            ];
        }
        return json_encode($additionalCharges);
    }

    public function getCovers(){
        //DataObject::get_by_id('Team',$myPlayer->TeamID);
        $charges =DataObject::get("Option","Grouping='Cover'", "", "", "");
        
        $additionalCharges = [];
        foreach($charges as $charge){
            $additionalCharges[] = [
                'Code' => $charge->Code,
                'Description' => $charge->Description,
                'Specifications'=>$charge->Specifications,    
            ];
        }
        return json_encode($additionalCharges);
    }
}
