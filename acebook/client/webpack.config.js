const path = require('path');
const webpack = require('webpack');
const ES6Promise = require('es6-promise');
const CompressionPlugin = require("compression-webpack-plugin");

var config = {
	entry: {
		bundle_rcm: './src/acebook.rcm.js',
		bundle_plus: './src/acebook.plus.js',
		// checkin: './src/rcm/checkin/checkin.rcm.js',
	},
	output: {
		path: path.join(__dirname, 'dist'),
		filename: "[name].js",
		publicPath: `http://www.acerentalcars.dev:3030/`,
    	hotUpdateChunkFilename: 'hot/hot-update.js',
    	hotUpdateMainFilename: 'hot/hot-update.json'
	},
	module: {
		rules: [
			{
				test: /\.js?/,
				include: path.join(__dirname, 'src'),
				loader: "babel-loader",
				query: {
					presets: ["react", "es2015", "stage-2"]
				}
			}
		]
	},
	plugins: [
		new webpack.optimize.UglifyJsPlugin({ minimize: true }),
		new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
		new CompressionPlugin({
			asset: "[path].gz[query]",
			algorithm: "gzip",
			test: /\.js$|\.css$|\.html$/,
			threshold: 10240,
			minRatio: 1
		}),
		new webpack.DefinePlugin({
			'process.env': {
			  NODE_ENV: JSON.stringify('production'),
			},
		}),
		new webpack.ProvidePlugin({
			Promise: 'es6-promise-promise'
		})
	],
	devServer: {
    publicPath: '/',
    compress: true,
    port: '3030',
    hot: true,
    hotOnly: false,
    historyApiFallback: false,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  },
}
module.exports  = config;