import React from 'react';
import ReactSVG from 'react-svg';
import moment from 'moment';
import numeral from 'numeral';

export const BookingDetails = (props) => {
    let form = props.state.form;
    let bookingDetails = props.state.checkin.bookingInfo[0];
    let customerInfo = form.formCustomerData;
    let locp = form.formPickupLocation.split('-');

    const activeCountry = props.state[locp[0]];
    const p_location = activeCountry.rcmLocationInfo.filter(loc => loc.id == form.formPickupLocation.split('-')[1])[0];
    const d_location = activeCountry.rcmLocationInfo.filter(loc => loc.id == form.formDropoffLocation.split('-')[1])[0];

    const selectedCar = activeCountry.rcmAvailableCars.filter(car => car.carsizeid == form.selectedCarCategoryId)[0];
    let vehicle = cars ? cars : [];

    const datetimeformat = 'DD/MM/YYYY HH:mm';
    const p_datetime = moment(form.formPickupDate + ' ' + form.formPickupTime.replace('_', ':'), datetimeformat);
    const d_datetime = moment(form.formDropoffDate + ' ' + form.formDropoffTime.replace('_', ':'), datetimeformat);

    let days = selectedCar ? selectedCar.numofdays : 0;

    let chkid = "";
    let qtyid = "";
    let calcTotal = 0;
    let calcGst = calcTotal;
    let calcStampDuty = calcTotal;
    let calcTotOptExt = 0.0;
    let ItemVal = 0.0;
    let ItemObj = null;
    let qtyItems = 1;
    let CountryTax = 1.0 + activeCountry.rcmTaxRate;
    let StateTax = 1.0 + activeCountry.rcmStateTax;
    let UseStateTax = activeCountry.rcmStateTax > 0;
    let UseTax = activeCountry.rcmTaxRate > 0;

    let totalcost = 0;
    let selectedInsurance = {};
    if (form.selectedInsuranceId == '') {
        //error: there must be a problem in the initialization
    } else {
        selectedInsurance = props.state[locp[0]].rcmInsuranceOptions.filter(ins => ins.id == form.selectedInsuranceId)[0];
        if (!selectedInsurance) {
            selectedInsurance = props.state[locp[0]].rcmInsuranceOptions.filter(ins => ins.default == "True")[0];
        }
    }

    if (props.state[locp[0]].rcmInsuranceOptions.length == 0 || form.selectedInsuranceId == '') {
        return false;
    }

    if (activeCountry.rcmTaxInclusive == false) {
        calcGst = parseFloat(calcGst) * CountryTax;
        calcStampDuty = parseFloat(calcStampDuty) * StateTax;
    }

    // Insurance
    let calcInsurance = 0.0;
    activeCountry.rcmInsuranceOptions.map((ins, i) => {
        if (ins.type == "Daily") {
            ItemVal = (parseFloat(ins.numofdays) * parseFloat(ins.fees));
        } else if (ins.type == "Percentage") {
            if (ins.percentagetotalcost == "True" || ins.merchantfee == "True") {
                ItemVal = (parseFloat(calcTotal) * parseFloat(ins.fees) / 100);
            } else {
                ItemVal = (parseFloat(ratetotal) * parseFloat(ins.fees) / 100);
            }
        } else {
            ItemVal = parseFloat(ins.fees);
        }

        if (parseFloat(ins.maxprice) > 0 && ItemVal > parseFloat(ins.maxprice))
            ItemVal = parseFloat(ins.maxprice);
        if (form.selectedInsuranceId == ins.id) {
            if (activeCountry.rcmTaxInclusive == false) {
                calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
                if (ins.gst == "True")
                    calcGst = parseFloat(calcGst) + (parseFloat(ItemVal) * CountryTax);
                if (ins.stampduty == "True")
                    calcStampDuty = parseFloat(calcStampDuty) + (parseFloat(ItemVal) * StateTax);
            } else {
                calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
                if (ins.gst == "True")
                    calcGst = parseFloat(calcGst) + parseFloat(ItemVal);
                if (ins.stampduty == "True")
                    calcStampDuty = parseFloat(calcStampDuty) + parseFloat(ItemVal);
            }
            calcInsurance = parseFloat(calcTotOptExt) + parseFloat(ItemVal);
        }
    });
    // if(){
    //      if (activeCountry.rcmTaxInclusive == false) {
    //        calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
    //        if (ins.gst == "True") calcGst = parseFloat(calcGst) + (parseFloat(ItemVal) * CountryTax);
    //        if (ins.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + (parseFloat(ItemVal) * StateTax);
    //     } else {
    //        calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
    //        if (ins.gst == "True") calcGst = parseFloat(calcGst) + parseFloat(ItemVal);
    //        if (ins.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + parseFloat(ItemVal);
    //     }
    // }
    // Mandatory Fees
    let calcMandatoryFees = 0.0;
    activeCountry.rcmMandatoryFees.map((mandatoryfees, i) => {
        let ItemVal = 0.0;
        let OldVal = 0.0;
        if ((mandatoryfees.locationid == p_location.id || mandatoryfees.locationid == "0") && (mandatoryfees.vehiclesizeid == selectedCar.carsizeid || mandatoryfees.vehiclesizeid == "0")) {
            if (mandatoryfees.type == "Daily") {
                ItemVal = (parseFloat(mandatoryfees.numofdays) * parseFloat(mandatoryfees.fees));
            } else if (mandatoryfees.type == "Percentage") {
                if (mandatoryfees.percentagetotalcost == "True" || mandatoryfees.merchantfee == "True") {
                    ItemVal = (parseFloat(calcTotal) * parseFloat(mandatoryfees.fees) / 100);
                } else {
                    ItemVal = (parseFloat(ratetotal) * parseFloat(mandatoryfees.fees) / 100);
                }
            } else {
                ItemVal = parseFloat(mandatoryfees.fees);
            }
            if (parseFloat(mandatoryfees.maxprice) > 0 && ItemVal > parseFloat(mandatoryfees.maxprice))
                ItemVal = parseFloat(mandatoryfees.maxprice);
            if (activeCountry.rcmTaxInclusive == false) {
                calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
                if (mandatoryfees.gst == "True")
                    calcGst = parseFloat(calcGst) + (parseFloat(ItemVal) * CountryTax);
                if (mandatoryfees.stampduty == "True")
                    calcStampDuty = parseFloat(calcStampDuty) + (parseFloat(ItemVal) * StateTax);
            } else {
                calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
                if (mandatoryfees.gst == "True")
                    calcGst = parseFloat(calcGst) + ItemVal;
                if (mandatoryfees.stampduty == "True")
                    calcStampDuty = parseFloat(calcStampDuty) + ItemVal;
            }
        }
    });

    // Optional Extras
    let calcOptionalExtras = 0.0;
    form.selectedOptionalExtras.map((selex, i) => {
        ItemVal = 0.0;
        qtyItems = selex.qty;
        if (selex.ex.type == "Daily") {
            ItemVal = (parseFloat(selex.ex.numofdays) * parseFloat(selex.ex.fees));
        } else if (selex.ex.type == "Percentage") {
            if (selex.ex.percentagetotalcost == "True" || selex.ex.merchantfee == "True") {
                ItemVal = (parseFloat(calcTotal) * parseFloat(selex.ex.fees) / 100);
            } else {
                ItemVal = (parseFloat(ratetotal) * parseFloat(selex.ex.fees) / 100);
            }
        } else {
            ItemVal = parseFloat(selex.ex.fees);
        }
        if (parseFloat(selex.ex.maxprice) > 0 && ItemVal > parseFloat(selex.ex.maxprice))
            ItemVal = parseFloat(selex.ex.maxprice);
        ItemVal = ItemVal * qtyItems;

        if (activeCountry.rcmTaxInclusive == false) {
            calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
            if (selex.ex.gst == "True")
                calcGst = parseFloat(calcGst) + (parseFloat(ItemVal) * (CountryTax));
            if (selex.ex.stampduty == "True")
                calcStampDuty = parseFloat(calcStampDuty) + (parseFloat(ItemVal) * StateTax);
        } else {
            calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
            if (selex.ex.gst == "True")
                calcGst = parseFloat(calcGst) + parseFloat(ItemVal);
            if (selex.ex.stampduty == "True")
                calcStampDuty = parseFloat(calcStampDuty) + parseFloat(ItemVal);
        }
        calcOptionalExtras = parseFloat(calcOptionalExtras) + parseFloat(ItemVal);
    });

    // todo: implement Km charges.
    // Km Charges
    let calcKmChargesFees = 0.0;
    activeCountry.rcmKmCharges.map((kmcharges, i) => {
        let ItemVal = 0.0;
        // - KmCharges, kms Daily rate are based on NoofRate days, if 1.4 days charged, total kms daily rate will be 1.4x$10
        if (activeCountry.rcmKmCharges.length != 0) {
            if (kmcharges.dailyrate > 0) {
                ItemVal = (numofdays * parseFloat(kmcharges.dailyrate)).toFixed(2);
            }
        }

        // if (parseFloat(rcmKmCharges[j]["maxprice"]) > 0 && ItemVal > parseFloat(rcmKmCharges[j]["maxprice"])) ItemVal = parseFloat(rcmKmCharges[j]["maxprice"]);
        if (activeCountry.rcmTaxInclusive == false) {
            calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
            calcGst = parseFloat(calcGst) + (parseFloat(ItemVal) * CountryTax);
            calcStampDuty = parseFloat(calcStampDuty) + (parseFloat(ItemVal) * StateTax);
        } else {
            calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
            calcGst = parseFloat(calcGst) + parseFloat(ItemVal);
            calcStampDuty = parseFloat(calcStampDuty) + parseFloat(ItemVal);
        }
        calcKmChargesFees = parseFloat(calcTotOptExt) + parseFloat(ItemVal);
    });
    totalcost = +selectedCar.total + +calcTotal;

    let paymentamount = 0;
    if (form.isAmendment) {
        paymentamount = numeral(bookingDetails.payment).format('0.00');
    } else {
        paymentamount = numeral((form.selectedPaymentAmount == 'full') ? form.total : (form.total * props.deposit)).format('0.00');
    }
    let amountCheck = totalcost - paymentamount >= 0 ? true : false;
    return (
<div className="l-summary">
    <div className="container">
        <h1 className="l-summary__heading">{props.title}</h1>
        <div className="l-summary__content" style={{margin: '0 auto 20px', width:'100%'}}>
            {props.content}
            <div id="booking_number" data-booking-number={bookingDetails.reservationno} style={{fontSize:'30px', border:'1px solid', width:'200px', margin:'20px auto 0px'}}>{bookingDetails.reservationno}</div>
            <div style={{fontSize:'14px'}}>BOOKING NUMBER</div>
        </div>


    </div>
    <div className="l-summary__container">
        <div className="l-summary__itinerary">        
            <h2 className="l-summary__heading">Itinerary Details</h2>
            <div className="l-summary__itinerary_rows">
                <div className="l-summary__itinerary-item">
                    <div className="l-summary__itinerary-label">
                        Pick Up Location
                    </div>
                    {p_location.location}
                </div>

                <div className="l-summary__itinerary-item">
                    <div className="l-summary__itinerary-label">
                        Pick Up Date
                    </div>
                    {p_datetime.format('DD, MMM, YYYY, hh:mma')}
                </div>

                <div className="l-summary__itinerary-arrow">
                    <ReactSVG path="/themes/base/production/svg/itinerary-arrow.svg" className='svg-itinerary-arrow'/>
                </div>

                <div className="l-summary__itinerary-item">
                    <div className="l-summary__itinerary-label">
                        Drop Off Location
                    </div>
                    {d_location.location}
                </div>

                <div className="l-summary__itinerary-item">
                    <div className="l-summary__itinerary-label">
                        Drop Off Date
                    </div>
                    {d_datetime.format('DD, MMM, YYYY, hh:mma')}
                </div>
            </div> 

            <a href={"/checkin/bookinginfo"} className="btn btn--secondary l-summary__checkin-action">Change Itinerary</a>
        </div> 

        <div className="l-summary__vehicle">   
            <h2 className="l-summary__heading">Vehicle Details</h2>
            <div className="l-summary__vehicle-main">
                <div className="l-summary__vehicle-row">
                    <div className="l-summary__vehicle-image">
                        <img src={vehicle[selectedCar.carsizeid]? vehicle[selectedCar.carsizeid].image:''} alt={selectedCar.categoryfriendlydescription} />
                    </div> 
                    <div className="l-summary__vehicle-summary">
                    <h2 className="l-summary__sub-heading"></h2>
                    <div className="l-summary__vehicle-title">{selectedCar.categoryfriendlydescription}</div>
                    <div className="l-summary__vehicle-subtitle">{selectedCar.vehiclecategory}</div>
                    <div className="l-summary__summary-price">
                        <span className="l-summary__summary-price-math">{days} x {numeral(selectedCar.discounteddailyrate).format('$0,0.00')} per day =</span> {numeral(selectedCar.totalrateafterdiscount).format('$0,0.00')}
                    </div>

                    {selectedCar.totaldiscount != 0
                    ?
                    <div className="l-summary__summary-discount">
                        <div className="l-summary-label">{numeral(selectedCar.totaldiscount).format('$0,0.00')} discount included.</div>
                        <div className="l-summary-price">
                            <span className="l-summary-price-math">({form.formPromoCode})</span>
                        </div>
                    </div>
                    : ''
                    }
                    </div>
                </div>
            </div>
            <a href={"/checkin/changevehicle"} className="btn btn--secondary l-summary__checkin-action">Change Vehicle</a>
        </div>
        <div className="l-summary__extras">
            <h2 className="l-summary__heading">Extras Details</h2>

            <div className="l-summary__main">
                <div className="l-summary__summary-row">
                    <div className="l-summary__summary-label">{selectedInsurance.name}</div>
                    <div className="l-summary__summary-price">
                        <span className="l-summary__summary-price-math">{days} x {numeral(selectedInsurance.fees).format('$0,0.00')} per day =</span> {numeral(selectedInsurance.fees * days).format('$0,0.00')}
                    </div>
                </div>

                {activeCountry.rcmMandatoryFees.map((man, i) => (
                <div className="l-summary__summary-row" key={i}>
                    <div className="l-summary__summary-label">{man.name}</div>
                    <div className="l-summary__summary-price">
                        <span className="l-summary__summary-price-math">{numeral(man.fees).format('$0,0.00')} =</span> {numeral(man.fees).format('$0,0.00')}
                    </div>
                </div>
                    ))}

                {form.selectedOptionalExtras.map((selex, i) => (
                <div className="l-summary__summary-row" key={i}>
                    <div className="l-summary__summary-label">{selex.qty} {selex.ex.name}</div>
                    <div className="l-summary__summary-price">
                        <span className="l-summary__summary-price-math">{(selex.ex.type == 'Daily'? selex.qty : selex.qty)} x {numeral(selex.ex.fees).format('$0,0.00')} {selex.ex.type == 'Daily'? ' per day ' : ' '} =</span> {numeral(selex.ex.fees * (selex.ex.type == 'Daily'? selex.ex.numofdays : 1) * selex.qty).format('$0,0.00')}
                    </div>
                </div>
                    ))}
                {form.country == 'au'
                    ? <div className="l-summary__summary-row">
                    <div className="l-summary__summary-label">* GST on Total Price</div>
                    <div className="l-summary__summary-price">
                        ({numeral(totalcost * props.gst).format('$0,0.00')})
                    </div>
                </div>
                    : ''
                }
            </div>
            <a href={"/checkin/changeextras"} className="btn btn--secondary l-summary__checkin-action">Change Extras</a>
        </div>

        <div className="l-summary__personal">
            <h2 className="l-summary__heading">Personal Details</h2>
            <div className="summary__main">

                <div className="l-summary__summary-row">
                    <div className="l-summary__summary-label">First Name</div>
                    <div id="first_name" className="l-summary__summary-first-name">
                        {customerInfo.fnm}
                    </div>
                </div>
                <div className="l-summary__summary-row">
                    <div className="l-summary__summary-label">Last Name</div>
                    <div className="l-summary__summary-last-name">
                        {customerInfo.lnm}
                    </div>
                </div>
                <div className="l-summary__summary-row">
                    <div className="l-summary__summary-label">Email</div>
                    <div className="l-summary__summary-email">
                        {customerInfo.eml}
                    </div>
                </div>
                <div className="l-summary__summary-row">
                    <div className="l-summary__summary-label">Mobile</div>
                    <div id="first_name" className="l-summary__summary-mobile">
                        {customerInfo.mob}
                    </div>
                </div>
                <div className="l-summary__summary-row">
                    <div className="l-summary__summary-label">Contact</div>
                    <div id="first_name" className="l-summary__summary-contact">
                        {customerInfo.cnt}
                    </div>
                </div>
                <div className="l-summary__summary-row">
                    <div className="l-summary__summary-label">Notes</div>
                    <div id="first_name" className="l-summary__summary-notes">
                        {customerInfo.rmk}
                    </div>
                </div>
            </div>
            <a href={"/checkin/changepersonaldetails"} className="btn btn--secondary l-summary__checkin-action">Change Personal Details</a>
        </div>

        <div className="l-summary__payment">
            <div className="summary__main">

                <h2 className="l-summary__heading">Payment Details</h2>
                <div className="l-summary__summary-total">
                    <div className="l-summary__summary-total-label">Total Cost</div>
                    <div id="booking_total" data-booking-total={numeral(totalcost).format('00.00')} className="l-summary__summary-total-price">
                        {numeral(totalcost).format('$0,0.00')}
                    </div>
                </div>
                <div className="l-summary__summary-total" style={{margin:'0px', paddingTop:'0px'}}>
                    <div className="l-summary__summary-total-label">Amount Paid</div>
                    <div className="l-summary__summary-total-price">
                        {numeral(paymentamount).format('$0,0.00')}
                    </div>
                </div>
                <div className="l-summary__summary-total" style={{margin:'0px', paddingTop:'0px'}}>
                    <div className="l-summary__summary-total-label">Balance Owing</div>
                    <div className="l-summary__summary-total-price">
                        {numeral(totalcost - paymentamount).format('$0,0.00')}
                    </div>
                </div>
            </div>

            {amountCheck?
                <div className="l-summary__summary-total" style={{margin:'0px', paddingTop:'0px'}}>
            <button onClick={e => props.onPayBalance(e, numeral(totalcost - paymentamount).format('0.00'))} className="btn btn--secondary l-summary__checkin-payaction">Pay Balance Amount Now</button>
            </div>
                    : <span>Please contact support@acerentalcars.{(location.hostname.split('.').pop() == 'nz' ? 'co.nz' : 'com.au')} for Refund.</span>
            }
        </div>
        <div>
        <div className="l-summary__footer">
            <button type="submit" className="btn btn--secondary l-summary__checkin-action-confirm" onClick={
            e => props.onSubmit(e)}>Confirm Changes</button>
            <button type="submit" className="btn btn--secondary l-summary__checkin-action-cancel"  onClick={e => props.onCancel(e)}>Cancel Changes</button>
            
            </div>
        </div>
    </div>
</div>
            )
        }
