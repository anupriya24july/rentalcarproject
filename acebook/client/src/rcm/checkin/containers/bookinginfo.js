import React from 'react';
import ReactSVG from 'react-svg';

import superagent from 'superagent';
import jsonp from 'superagent-jsonp';
import moment from 'moment';
import pikaday from 'pikaday';

import { AceBookAPI as api } from '../../common/api/acebookapi';
import { BrowserRouter as Router } from 'react-router-dom';
import { connect } from 'react-redux';

import { SidebarWidget } from '../../common/components/sidebarwidget';

class BookingInfo extends React.Component {

    constructor(props) {
        super(props);
        this.country = api.defaultcountry;
        this.dateFormat = 'DD/MM/YYYY';
        this.today = moment(new Date());
        this.state = {
          ...this.props.state.form
        }
    }

    dateFields() {
        this.dateContainers = document.querySelectorAll('.js-date-container');
        if (this.dateContainers) {
            ;
            NodeList.prototype.forEach = Array.prototype.forEach;
            this.dateContainers.forEach((container, i) => {
                let dateInput = container.querySelector('.js-date');
                if (container.childElementCount == 2) {
                    $('#' + dateInput.id).data('pikaday', new pikaday({
                        container,
                        field: dateInput,
                        numberOfMonths: 1,
                        defaultDate: moment(dateInput.classList.contains('Pickup') ? this.props.state.form.formPickupDate : this.props.state.form.formDropoffDate, 'DD/MM/YYYY').toDate(),
                        setDefaultDate: true,
                        minDate: this.today.toDate(),
                        format: "DD, MMM, YYYY",
                        position: 'bottom right',
                        i18n: {
                            previousMonth: 'Previous Month',
                            nextMonth: 'Next Month',
                            months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December', ],
                            weekdays: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', ],
                            weekdaysShort: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', ],
                        },
                        onSelect: (selecteddate) => {
                            let datepickers = $('.js-date');
                            const props = this.props;
                            const dateformat = this.dateFormat
                            let dropoffdate = props.state.form.formDropoffDate;
                            let pickupdate = props.state.form.formPickupDate;
                            if (dateInput.classList.contains('Dropoff')) {
                                datepickers.each(function () {
                                    let el = $('#' + this.id);
                                    dropoffdate = moment(selecteddate).format(dateformat);
                                    if (el.hasClass('Dropoff')) {
                                        el.data('pikaday').setMoment(moment(selecteddate), true);
                                    }

                                    // daterange
                                    el.data('pikaday').setStartRange(moment(pickupdate, dateformat).toDate());
                                    el.data('pikaday').setEndRange(moment(dropoffdate, dateformat).toDate());
                                });
                                props.setFormState({
                                    formDropoffDate: moment(selecteddate).format(dateformat),
                                });
                            } else { //Pickup
                                // - return date can't be earlier than pickup date + minimum booking days
                                if (props.state.form.formPickupLocation != '') {
                                    let loc = props.state.form.formPickupLocation.split('-');
                                    let ploc = props.state[loc[0]].rcmLocationInfo.filter(l => l.id == loc[1])[0];


                                    datepickers.each(function () {
                                        let el = $('#' + this.id);
                                        pickupdate = moment(selecteddate).format(dateformat);
                                        if (moment(dropoffdate, dateformat).isBefore(moment(selecteddate).add(+ploc.minbookingday + 1, 'days'))) {
                                            dropoffdate = moment(selecteddate).add(+ploc.minbookingday + 1, 'days').format(dateformat);
                                        }
                                        if (el.hasClass('Pickup')) {
                                            el.data('pikaday').setMoment(moment(selecteddate), true);
                                        } else {
                                            el.data('pikaday').setMoment(moment(dropoffdate, dateformat));
                                            el.data('pikaday').setMinDate(moment(selecteddate).add(+ploc.minbookingday + 1, 'days').toDate());
                                        }

                                        // daterange
                                        el.data('pikaday').setStartRange(moment(pickupdate, dateformat).toDate());
                                        el.data('pikaday').setEndRange(moment(dropoffdate, dateformat).toDate());
                                    });

                                    props.setFormState({
                                        formPickupDate: pickupdate,
                                        formDropoffDate: dropoffdate,
                                        isChanges: true
                                    });
                                }
                            }
                        },
                        onClose: () => {
                        },
                    }));
                }
            });
        }
    }

    componentDidMount() {
        this.dateFields();
    }

    initStep() {
    }

    onSubmitSearch() {
        // pickup location
        let locp = this.props.state.form.formPickupLocation.split('-');
        let ploc = this.props.state[locp[0]].rcmLocationInfo.filter(l => l.id == locp[1])[0];

        // dropoff location
        let locd = this.props.state.form.formDropoffLocation.split('-');
        let dloc = this.props.state[locd[0]].rcmLocationInfo.filter(l => l.id == locd[1])[0];

        // pickup date
        let pdate = this.props.state.form.formPickupDate.replace(new RegExp('/', 'g'), '_');

        // dropoff date
        let ddate = this.props.state.form.formDropoffDate.replace(new RegExp('/', 'g'), '_');

        // todo: perform validations here

        this.props.setDataState({
            [locp[0]]: {
                ...this.props.state[locp[0]],
                rcmReservationRef: '',
                rcmReservationNo: '',
                rcmPaymentSaved: false
            }
        });

        // create permalink url with search data
        let urlseg = [
            0, // this.props.state.form.formCategoryType,
            locp[1],
            pdate,
            this.props.state.form.formPickupTime,
            locd[1],
            ddate,
            this.props.state.form.formDropoffTime,
            this.props.state.form.formMinimumAge,
            1,
            this.props.state.form.formPromoCode == '' ? '-' : this.props.state.form.formPromoCode
        ];
        window.location = '/checkin/summary';


    }
    onFormChange(event) {
        const newFormState = {...this.props.state.form, };
        switch (event.target.name) {
            case 'formPickupLocation':
                let loc = event.target.value.split('-');
                let ploc = this.props.state[loc[0]].rcmLocationInfo.filter(l => l.id == loc[1])[0];
                // Get notice required and minimum booking day
                let startPickup = moment(this.today).add(Math.ceil(ploc.noticerequired), 'days');
                let startDropOff = moment(this.today).add(Math.ceil(ploc.noticerequired) + +ploc.minbookingday, 'days');

                let PickUpDW = 0;
                let DropOffDW = 0;

                // set datepicker
                // window.inline_Pickup_Date.gotoDate(moment(startPickup).toDate());
                // window.inline_Pickup_Date.setMoment(moment(startPickup));
                // window.inline_Pickup_Date.setMinDate(moment(startPickup).toDate());
                // window.inline_Dropoff_Date.setMoment(moment(startDropOff));
                // window.inline_Dropoff_Date.setMinDate(moment(startDropOff).toDate());

                // todo: validate and set pickup and dropoff times

                if (loc[0] != newFormState.country) { // if pickup is in a different country always set dropoff same as pickup
                    newFormState.formDropoffLocation = event.target.value;
                } else {
                    if (!this.props.state.form.formDifferentDropOff) { // dropoff is a configured to be always same as pickup
                        newFormState.formDropoffLocation = event.target.value;
                    }
                }

                newFormState.formCategoryType = this.props.state[loc[0]].rcmCategoryTypeInfo[0].id;
                newFormState.formMinimumAge = this.props.state[loc[0]].rcmDriverAgesInfo[0].id;

                newFormState.country = loc[0]; // change country based on pickup location.

                newFormState[event.target.name] = event.target.value;
                break;
            case 'formDifferentDropOff':
                newFormState[event.target.name] = event.target.checked;
                newFormState['showDropoff'] = event.target.checked;
                break;
            default:
                newFormState[event.target.name] = event.target.value;
                break;
        }

        newFormState['isChanges'] = true;
        this.props.setFormState(newFormState);
    }

    render() {
        return (<SidebarWidget api={api} state={this.props.state} form={this.state}  onFormChange={e => this.onFormChange(e)} onSubmitSearch={e => this.onSubmitSearch(e)} />);
    }
}


const mapStateToProps = (state) => {
    return {
        state: {
            nz: state.data.nz,
            au: state.data.au,
            checkin: state.checkin,
            form: state.form,
        }
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setState: (state) => {
            dispatch({
                type: "SET_STATE",
                payload: state,
            });
        },
        setFormState: (state) => {
            dispatch({
                type: "SET_FORMSTATE",
                payload: state,
            });
        },
        setDataState: (state) => {
            dispatch({
                type: "SET_DATASTATE",
                payload: state,
            });
        }
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(BookingInfo);
