import React from 'react';
import ReactSVG from 'react-svg';

import superagent from 'superagent';
import jsonp from 'superagent-jsonp';
import moment from 'moment';
import pikaday from 'pikaday';

import { AceBookAPI as api } from '../../common/api/acebookapi';
import { AmendmentAPI as amendmentapi } from '../../common/api/aceamendmentapi';
import { BrowserRouter as Router } from 'react-router-dom';
import { connect } from 'react-redux';

class CheckinForm extends React.Component {

  constructor(props) {
    super(props);

    this.country = api.defaultcountry;
    this.openModal = props.openModal;
    this.today = moment(new Date(), 'DD/MMM/YYYY').format('DD/MM/YYYY');
        this.state = {
          ...this.props.state.form
        }
  }

  componentWillMount() {
    api.getStep1(api.getEndPoint(this.country), this.props, this.initStep.bind(this));
  }

  initStep() {
  }

  checkinStep() {
    if (this.props.state.checkin.length == 0) {
      alert("Error");
      window.location = "/checkin";
    } else {
      let bookingDetails = this.props.state.checkin.bookingInfo[0];
      if(moment(bookingDetails.spickupdate, 'DD/MMM/YYYY').add(-2, 'days').format('DD/MM/YYYY')==this.today){
        this.props.setState({
          ...this.props.state.checkin,
          bookingInfo: {},
          customerInfo: {},
          rateInfo: {},
          extraFees: {},
          paymentInfo: {},
          bookingRef: "", 
        });
        let msg="";
        let heading="Oops! Looks like something went wrong!";
        msg='Sorry you wont be able to make any changes to the booking as the pick up date is closer. For making changes please contact support@acerentalcars.' + (location.hostname.split('.').pop() == 'nz'? 'co.nz':'com.au');
        this.openModal(heading,msg,0);
      } else {
        let customerInfo = this.props.state.checkin.customerInfo[0];
        let insuranceInfo = this.props.state.checkin.extraFees.filter(ins => ins.insuranceextra == 1)[0];
        this.props.setFormState({
          step: 0,
          country: this.country,
          formDifferentDropOff: bookingDetails.pickuplocationid == bookingDetails.dropofflocationid ? true : false,
          formPickupLocation: this.country + '-' + bookingDetails.pickuplocationid,
          formDropoffLocation: this.country + '-' + bookingDetails.dropofflocationid,
          formPickupDate: moment(bookingDetails.spickupdate, 'DD/MMM/YYYY').format('DD/MM/YYYY'),
          formDropoffDate: moment(bookingDetails.sdropoffdate, 'DD/MMM/YYYY').format('DD/MM/YYYY'),
          formPickupTime: moment(bookingDetails.pickuptime, 'HH:mm').format('HH_mm'),
          formDropoffTime: moment(bookingDetails.dropofftime, 'HH:mm').format('HH_mm'),
          formCategoryType: '0',
          formMinimumAge: bookingDetails.driverageid,
          formPromoCode: '',
          formEmailOptin: true,
          formAgreedToTerms: '',
          formCustomerData: {
            fnm: customerInfo.acfirstname,
            lnm: customerInfo.aclastname,
            eml: customerInfo.acemail,
            mob: customerInfo.acmobile,
            cnt: (this.country == 'au' ? '7' : '2'),
            rmk: bookingDetails.customernote,
          },
          selectedCarCategoryId: bookingDetails.carsizeid,
          selectedInsuranceId: insuranceInfo.extrafeesid,
          selectedOptionalExtras: [],
          selectedPaymentAmount: 'branch',
          showDropoff: (this.country == 'au' ? true : false),
          isAmendment: true,
          isChanges: false,
          bookingRef:this.props.state.checkin.bookingRef,
          bookingNo:this.props.state.checkin.bookingNo,
          customerNo:customerInfo.acid,
          total: bookingDetails.totalcost,
          formAgreedToTerms: true
        });
        this.props.history.push('/checkin/summary');
      }
    }
  }
  getBookingData() {
    if(this.props.state.checkin.bookingRef){
      api.bookingInfo(api.getEndPoint(this.country), this.props.state.checkin.bookingRef + '/-', this.props, this.checkinStep.bind(this));
    }
  }
  onSubmit() {
    let data = 'bookingNo=' + this.props.state.checkin.bookingNo;
    let bookingRef = amendmentapi.getBookingRef(amendmentapi.getEndPoint(this.props.state.form.country), data, this.props, this.getBookingData.bind(this));
  }

  onFormChange(event) {
    this.props.setState({
      bookingNo: event.target.value
    });
  }

  render() {
    return (
      <div className="l-arrow-container">
      <div className="l-page">
      <div className="l-page__container">
      <h1 className="l-page__heading">  Booking Amendment</h1>
      <div className="l-page__content">
      <div className="l-form js-checkin-form" >      
      <div className="l-form__fields-checkin-form">
      <div className="l-form__field-checkin-form  ">
      <label htmlFor="bookingNo" className="l-form__label-checkin-form">
      Booking Number
      </label>
      <input className="l-summary-checkin-form" type="text" name="bookingNo" id="bookingNo" onChange={this.onFormChange.bind(this)}/>
      </div>
      </div>
      
      <div className="l-form__action-checkin-form">
      <button type="submit" className="btn btn--secondary  " onClick={e => this.onSubmit(e)}>
      GO
      </button>
      </div>
      </div>
      </div>
      </div>
      </div>
      </div>
      );
  }
}


const mapStateToProps = (state) => {
  return {
    state: {
      nz: state.data.nz,
      au: state.data.au,
      checkin: state.checkin,
      form: state.form,
    }
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setState: (state) => {
      dispatch({
        type: "SET_STATE",
        payload: state,
      });
    },
    setFormState: (state) => {
      dispatch({
        type: "SET_FORMSTATE",
        payload: state,
      });
    },
    setDataState: (state) => {
      dispatch({
        type: "SET_DATASTATE",
        payload: state,
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CheckinForm);
