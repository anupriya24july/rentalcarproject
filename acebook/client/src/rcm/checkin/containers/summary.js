import React from 'react';
import superagent from 'superagent';
import jsonp from 'superagent-jsonp';
import moment from 'moment';
import pikaday from 'pikaday';

import { AceBookAPI as api } from '../../common/api/acebookapi';
import { AmendmentAPI as amendmentapi } from '../../common/api/aceamendmentapi';
import { BrowserRouter as Router } from 'react-router-dom';
import { connect } from 'react-redux';
import base64 from 'base-64';

import { BookingDetails } from '../components/bookingdetails';
class Summary extends React.Component {
  constructor(props) {
    super(props);
    this.country = api.defaultcountry;
    this.openModal = props.openModal;
    if(!props.state.checkin.bookingRef){
      window.location = "/checkin"
    }
  }

  togglePanel(e) {
    e.preventDefault();
    window.scrollTo(0, 0);
    const panel = document.querySelector('.js-booking-panel')
    if (panel.classList.contains('is-open')) {
            //panel.classList.remove('is-open');
          } else {
            panel.classList.add('is-open');
          }
        }

        initStep3() {
          if (!this.props.state.form.isChanges) {
            let age = this.props.state[api.defaultcountry].rcmDriverAgesInfo.find(age => age.id == this.props.state.form.formMinimumAge);
            // load default insurance if none is selected.
            if (this.props.state.form.selectedInsuranceId == '' || this.props.state[api.defaultcountry].rcmInsuranceOptions.find(ins => ins.id == this.props.state.form.selectedInsuranceId) == undefined) {
                // let selectedInsurance = this.props.state[api.defaultcountry].rcmInsuranceOptions.find(ins => ins.fees == 0 && ins.fromage >= age.driverage);
                let selectedInsurance = this.props.state[api.defaultcountry].rcmInsuranceOptions.sort((a, b) => {
                  return +a.fees - +b.fees
                });
                this.props.setFormState({
                  selectedInsuranceId: selectedInsurance[0].id,
                });
              }
              this.props.state.checkin.extraFees.filter(ins => {
                if (ins.insuranceextra != 1)
                  return ins
              }).map((extras, i) => {
                if (this.props.state[api.defaultcountry].rcmOptionalFees.find((ex, i) => ex.id == extras.extrafeesid) && !this.props.state.form.selectedOptionalExtras.find((selected, i) => selected.ex.id == extras.extrafeesid)) {
                  this.props.state.form.selectedOptionalExtras.push({
                    qty: parseInt(extras.qty),
                    ex: this.props.state[api.defaultcountry].rcmOptionalFees.find((ex, i) => ex.id == extras.extrafeesid),
                  })
                }
              });
              this.props.setFormState({
                selectedOptionalExtras: this.props.state.form.selectedOptionalExtras,
                selectedExtras: this.props.state.form.selectedOptionalExtras,
              });
            // remove optional extras that are not compatible with the current vehicle.
            let newSelEx = this.props.state.form.selectedOptionalExtras;
            this.props.state.form.selectedOptionalExtras.map((selex, i) => {
              let thisex = this.props.state[api.defaultcountry].rcmOptionalFees.find(ex => selex.ex.id == ex.id);
              if (!thisex) {
                newSelEx.splice(newSelEx.indexOf(selex), 1);
              }
            });
            this.props.setFormState({
              selectedOptionalExtras: newSelEx,
              selectedExtras: newSelEx,
            });

          }

          let isIns = this.props.state[this.country].rcmInsuranceOptions.find(ins => ins.id == this.props.state.form.selectedInsuranceId) ? true : false;
          if (!isIns) {
            this.props.setFormState({
              selectedInsuranceId: this.props.state[this.country].rcmInsuranceOptions.find(ins => ins.default == 'True').id,
            });
          }

        }
        initStep() {
          let urlseg = [
            0, //this.props.state.form.formCategoryType,
            this.props.state.form.formPickupLocation.split('-')[1],
            moment(this.props.state.form.formPickupDate, 'DD/MM/YYYY').format('DD_MM_YYYY'),
            this.props.state.form.formPickupTime,
            this.props.state.form.formDropoffLocation.split('-')[1],
            moment(this.props.state.form.formDropoffDate, 'DD/MM/YYYY').format('DD_MM_YYYY'),
            this.props.state.form.formDropoffTime,
            this.props.state.form.formMinimumAge,
            this.props.state.form.selectedCarCategoryId,
            1,
            '_'
            ];
            api.getStep3(api.getEndPoint(api.defaultcountry), urlseg.join('/') + '#', this.props, this.initStep3.bind(this));

          }

          componentWillMount() {
            let urlseg = [
            0, //this.props.state.form.formCategoryType,
            this.props.state.form.formPickupLocation.split('-')[1],
            moment(this.props.state.form.formPickupDate, 'DD/MM/YYYY').format('DD_MM_YYYY'),
            this.props.state.form.formPickupTime,
            this.props.state.form.formDropoffLocation.split('-')[1],
            moment(this.props.state.form.formDropoffDate, 'DD/MM/YYYY').format('DD_MM_YYYY'),
            this.props.state.form.formDropoffTime,
            this.props.state.form.formMinimumAge,
            1,
            '-'
            ];

            api.getStep2(api.getEndPoint(this.country), urlseg.join('/') + '#', this.props, this.initStep.bind(this));

          }
          onSubmit() {
            let mandatoryFees = this.props.state[api.defaultcountry].rcmMandatoryFees;      
            this.props.setFormState({
              mandatoryFees: mandatoryFees,
            });
            amendmentapi.amendBooking(amendmentapi.getEndPoint(this.props.state.form.country),JSON.stringify(this.props.state.form));
          } 

          onPayBalance(e, amount) { 
            this.props.setFormState({
              balance: amount,
            });
            this.props.history.push('/checkin/paymentoption');
          } 

          onCancel() {
            let msg="";
            let heading="Just to let you know...";
            msg='Please confirm that you want to cancel the changes as the changes made will be lost.';
            this.openModal(heading,msg,2);
          } 

          render() {
            return (
              <BookingDetails deposit={api.deposit} gst={api.gst} state={this.props.state} title="Booking Summary" content="" onSubmit={e => this.onSubmit(e)} onPayBalance={this.onPayBalance.bind(this)}  onCancel={e => this.onCancel(e)} />
              );
          }
        }


        const mapStateToProps = (state) => {
          return {
            state: {
              nz: state.data.nz,
              au: state.data.au,
              checkin: state.checkin,
              form: state.form,
            }
          };
        };

        const mapDispatchToProps = (dispatch) => {
          return {
            setState: (state) => {
              dispatch({
                type: "SET_STATE",
                payload: state,
              });
            },
            setFormState: (state) => {
              dispatch({
                type: "SET_FORMSTATE",
                payload: state,
              });
            },
            setDataState: (state) => {
              dispatch({
                type: "SET_DATASTATE",
                payload: state,
              });
            }
          };
        };


        export default connect(mapStateToProps, mapDispatchToProps)(Summary);
