import React from 'react';
import moment from 'moment';
import pikaday from 'pikaday';

import { AceBookAPI as api } from '../../common/api/acebookapi';
import { BrowserRouter as Router } from 'react-router-dom';
import { connect } from 'react-redux';

import { VehicleList } from '../../common/components/vehiclelist';
import { VehiclePanel } from '../../common/components/partials/vehiclepanel';
import { StickyBar } from '../../common/components/partials/stickybar';
import { ProgressBar } from '../../common/components/partials/progressbar';

class ChangeVehicle extends React.Component {

  constructor(props) {
    super(props);
    this.openModal = props.openModal;
    this.country = api.defaultcountry;
        this.state = {
          ...this.props.state.form
        }
  }

  componentWillMount() {
    if (this.props.match.path == '/checkin/changevehicle') {
      let locp = this.props.state.form.formPickupLocation.split('-');
      let ploc = this.props.state[locp[0]].rcmLocationInfo.filter(l => l.id == locp[1])[0];
      let locd = this.props.state.form.formDropoffLocation.split('-');
      let dloc = this.props.state[locd[0]].rcmLocationInfo.filter(l => l.id == locd[1])[0];
      let pdate = this.props.state.form.formPickupDate.replace(new RegExp('/', 'g'), '_');
      let ddate = this.props.state.form.formDropoffDate.replace(new RegExp('/', 'g'), '_');

      let urlseg = [
          0, //this.props.state.form.formCategoryType,
          locp[1],
          pdate,
          this.props.state.form.formPickupTime,
          locd[1],
          ddate,
          this.props.state.form.formDropoffTime,
          this.props.state.form.formMinimumAge,
          1,
          this.props.state.form.formPromoCode == '' ? '-' : this.props.state.form.formPromoCode
          ];

          api.getStep2(api.getEndPoint(this.props.state.form.country), urlseg.join('/') + '#', this.props, this.initStep.bind(this));
    }
  }

  togglePanel(e) {
        e.preventDefault();
        window.scrollTo(0, 0);
        const panel = document.querySelector('.js-booking-panel')
        if (panel.classList.contains('is-open')) {
      // panel.classList.remove('is-open');
    } else {
      panel.classList.add('is-open');
    }
  }

  toggleMoreSpecs(e) {
    let specs = e.target.parentElement.parentElement.querySelectorAll('.toggle-active');
    if (specs.length) {
      specs.forEach((spec) => {
        spec.classList.remove('toggle-active');
        spec.classList.add('toggle-hidden');
      });
    } else {
      specs = e.target.parentElement.parentElement.querySelectorAll('.toggle-hidden');
      specs.forEach((spec) => {
        spec.classList.add('toggle-active');
        spec.classList.remove('toggle-hidden');
      });
    }
  }

  onSelectVehicle(catid, index, e) {
    e.preventDefault();
    if (this.props.match.path == '/checkin/changevehicle') {
      let selectedRate=this.props.state[this.country].rcmAvailableCars.filter(cars =>cars.carsizeid == this.props.state.checkin.bookingInfo[0].carsizeid)[0]['discounteddailyrate'];
      let changedRate=this.props.state[this.country].rcmAvailableCars[index]['discounteddailyrate'];
      if(changedRate>selectedRate)
      {
        this.props.setFormState({
          selectedCarCategoryId: catid,
          isChanges: true
        });
        this.props.history.push('/checkin/summary');
      }else{   
        let msg="";
        let heading="Oops! Looks like something went wrong!";
        msg='Sorry, To degrade car please contact support@acerentalcars.' + (location.hostname.split('.').pop() == 'nz'? 'co.nz':'com.au');
        this.openModal(heading,msg,0);
      }
    }
  }

  initStep() {
    let locp = this.props.state.form.formPickupLocation.split('-');
    if (this.props.state[locp[0]].rcmAvailableCars.length == 0) {
      //this.props.history.goBack();
      alert("Error: There are no cars available for the selected dates. Please try again.");
      window.location = "/checkin/summary";
      //this.props.history.push('/bookings/');
    }
  // window.txtPickupdate.setMoment(moment(this.props.state.form.formPickupDate,api.dateformat));
  // window.txtPickupdate.setMinDate(moment(this.props.state.form.formPickupDate,api.dateformat).toDate());
  // window.txtReturn.setMoment(moment(this.props.state.form.formDropoffDate,api.dateformat));
  // window.txtReturn.setMinDate(moment(this.props.state.form.formPickupDate,api.dateformat).toDate());
  }

  render() {
    if (this.props.state[this.props.state.form.country].rcmErrors.length != 0) {
      return null;
    }

    return (
      <div className="l-booking">
        <div className="l-booking__main">
          <div className="l-booking__container">
            <div className="l-booking__header">
              <h1 className="l-booking__heading">Make a Booking</h1>
            </div>
            <ProgressBar progress={0}/>
            <VehicleList gst={api.gst}cta="Book" state={this.props.state} onSelectVehicle={this.onSelectVehicle.bind(this)} toggleMoreSpecs={this.toggleMoreSpecs.bind(this)}/>
          </div>
        </div>
        <StickyBar  mode='checkin'  showtotal={false} state={this.props.state}  togglePanel={this.togglePanel.bind(this)}/>
      </div>
    );
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    let form = this.props.state.form;
    let impressions = [];
    let locp = this.props.state.form.formPickupLocation.split('-');
    let ploc = this.props.state[locp[0]].rcmLocationInfo.filter(l => l.id == locp[1])[0];

    this.props.state[locp[0]].rcmAvailableCars.map((item, i) => {
      let totalprice = +item.totalrateafterdiscount;
      this.props.state[form.country].rcmMandatoryFees.filter(man => {
        if (man.locationid == locp[1] && man.vehiclesizeid == item.carsizeid)
          return man
      }).map((man, i) =>
      totalprice += +man.fees
      )
      impressions.push({
        id: item.sippcodes,
        name: item.categoryfriendlydescription,
        brand: item.vehicledescription1,
        category: ploc.location + '/vehicle',
        position: i + 1,
        list: 'checkin/changevehicle',
        variant: 'vehicle',
        price: totalprice
      });
    });
    window.dataLayer.push({
      currencyCode: form.country == 'au' ? 'AUD' : 'NZD',
      impressions: impressions,
      event: 'AceRentals.ec',
      eventCategory: 'Ecommerce',
      eventAction: 'Product Impression'
    });
  }
}

const mapStateToProps = (state) => {
  return {
    state: {
      nz: state.data.nz,
      au: state.data.au,
      checkin: state.checkin,
      form: state.form,
    }
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setState: (state) => {
      dispatch({
        type: "SET_STATE",
        payload: state,
      });
    },
    setFormState: (state) => {
      dispatch({
        type: "SET_FORMSTATE",
        payload: state,
      });
    },
    setDataState: (state) => {
      dispatch({
        type: "SET_DATASTATE",
        payload: state,
      });
    }
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(ChangeVehicle);
