import React from 'react';
import superagent from 'superagent';
import jsonp from 'superagent-jsonp';
import moment from 'moment';
import pikaday from 'pikaday';

import { AceBookAPI as api } from '../../common/api/acebookapi';
import { BrowserRouter as Router } from 'react-router-dom';
import { connect } from 'react-redux';

import { Extras } from '../../common/components/extras';
import { VehiclePanel } from '../../common/components/partials/vehiclepanel';
import { StickyBar } from '../../common/components/partials/stickybar';
import { ProgressBar } from '../../common/components/partials/progressbar';

class ChangeExtras extends React.Component {

    constructor(props) {
        super(props);
        this.total = 0;
        this.openModal = props.openModal;
        this.state = {
          ...this.props.state.form
        }
    }

    componentWillMount() {
        if (this.props.match.path == '/checkin/changeextras') {
            // pickup location
            let locp = this.props.state.form.formPickupLocation.split('-');
            let ploc = this.props.state[locp[0]].rcmLocationInfo.filter(l => l.id == locp[1])[0];

            // dropoff location
            let locd = this.props.state.form.formDropoffLocation.split('-');
            let dloc = this.props.state[locd[0]].rcmLocationInfo.filter(l => l.id == locd[1])[0];

            // pickup date
            let pdate = this.props.state.form.formPickupDate.replace(new RegExp('/', 'g'), '_');

            // dropoff date
            let ddate = this.props.state.form.formDropoffDate.replace(new RegExp('/', 'g'), '_');

            let urlseg = [
                0, //this.props.state.form.formCategoryType,
                locp[1],
                pdate,
                this.props.state.form.formPickupTime,
                locd[1],
                ddate,
                this.props.state.form.formDropoffTime,
                this.props.state.form.formMinimumAge,
                this.props.state.form.selectedCarCategoryId,
                1,
                this.props.state.form.formPromoCode == '' ? '_' : this.props.state.form.formPromoCode
            ];
            api.getStep3(api.getEndPoint(api.defaultcountry), urlseg.join('/') + '#', this.props, this.initStep.bind(this));
        }

    }

    toggleMoreSpecs(e) {
        let specs = e.target.parentElement.parentElement.querySelectorAll('.toggle-active');
        if (specs.length) {
            specs.forEach((spec) => {
                spec.classList.remove('toggle-active');
                spec.classList.add('toggle-hidden');
            });
        } else {
            specs = e.target.parentElement.parentElement.querySelectorAll('.toggle-hidden');
            specs.forEach((spec) => {
                spec.classList.add('toggle-active');
                spec.classList.remove('toggle-hidden');
            });
        }
    }

    initStep() {
    }

    onContinue() {
        this.props.setFormState({
            total: this.total,
        });
        this.props.history.push('/checkin/summary');
    }

    onSelectInsurance(insID, e) {
        let locp = this.props.state.form.formPickupLocation.split('-');
        const activeCountry = this.props.state[locp[0]];

        let extras = this.props.state.form.selectedOptionalExtras;
        const insuranceoptions = activeCountry.rcmInsuranceOptions;
        if ((insuranceoptions.find((ins, i) => ins.id == insID).excessfee == 0)) {
            let additionaldriver = extras.find((selex, i) => selex.ex.name.match(/add.*driver.*/ig));
            if (additionaldriver) {
                extras.splice(extras.indexOf(additionaldriver), 1);
            }
        }
        this.props.setFormState({
            selectedInsuranceId: insID,
            selectedOptionalExtras: extras,
            isChanges: true
        });
    }

    onSelectOptionalExtras(extraFeeID, e) {
        e.stopPropagation();
        let locp = this.props.state.form.formPickupLocation.split('-');
        let extras = this.props.state.form.selectedOptionalExtras;
        let bookedExtras = this.props.state.form.selectedExtras.find((selex, i) => selex.ex.id == extraFeeID);
        let thisExtra = extras.find((selex, i) => selex.ex.id == extraFeeID);
        if (bookedExtras) {
            let msg="";
            let heading="Oops! Looks like something went wrong!";
            msg='Sorry, To change the extras you already selected please contact support@acerentalcars.' + (location.hostname.split('.').pop() == 'nz'? 'co.nz':'com.au');
            this.openModal(heading,msg,0);
        } else {
            if (thisExtra) {
                extras.splice(extras.indexOf(thisExtra), 1);
            } else {
                extras.push({
                    qty: parseInt(e.target.parentElement.querySelector('select') ? e.target.parentElement.querySelector('select').value : 1),
                    ex: this.props.state[locp[0]].rcmOptionalFees.find((ex, i) => ex.id == extraFeeID),
                })
            }
            this.props.setFormState({
                selectedOptionalExtras: extras,
                isChanges: true
            });
        }
    }

    removeJS(filename) {
        const tags = document.getElementsByTagName('script');
        for (var i = tags.length; i >= 0; i--) {
            if (tags[i] && tags[i].getAttribute('src') != null && tags[i].getAttribute('src').indexOf(filename) != -1)
                tags[i].parentNode.removeChild(tags[i]);
        }
    }

    togglePanel(e) {
        e.preventDefault();
        window.scrollTo(0, 0);
        const panel = document.querySelector('.js-booking-panel')
        if (panel.classList.contains('is-open')) {
            //panel.classList.remove('is-open');
        } else {
            panel.classList.add('is-open');
        }
    }

    onQtyChange(e, current_ex) {
        let selex = this.props.state.form.selectedExtras;
        let isChange=true;
        selex.map(item => {
            if (item.ex.id == current_ex.id) {
                if(item.qty>e.target.value){
                    let msg="";
                    let heading="Oops! Looks like something went wrong!";
                    msg='Sorry, To change the extras you already selected please contact support@acerentalcars.' + (location.hostname.split('.').pop() == 'nz'? 'co.nz':'com.au');
                    this.openModal(heading,msg,0);
                    isChange=false;
                }
            }
        });
        if(isChange){
            let changeselex = this.props.state.form.selectedOptionalExtras;
            changeselex.map(item => {
                if (item.ex.id == current_ex.id) {
                    item.qty = +e.target.value;
                        this.props.setFormState({
                            selectedOptionalExtras: changeselex,
                            isChanges: true
                    });
                }
            });
        }
    }

    render() {
        if (this.props.state[api.defaultcountry].rcmErrors.length != 0) {
            this.props.state[this.props.state.form.country].rcmErrors.map((item, index) => {
                console.log(item.error);
            })
            return null;
        }
        return (
                <div className="l-booking">
                    <VehiclePanel gst={api.gst} state={this.props.state} showtotal={true}/>
                    <div className="l-booking__main">
                        <div className="l-booking__container">
                            <div className="l-booking__header">
                                <h1 className="l-booking__heading">Make a Booking</h1>
                            </div>
                            <ProgressBar progress={25}/>
                            <Extras state={this.props.state} onSelectInsurance={this.onSelectInsurance.bind(this)} onSelectOptionalExtras={this.onSelectOptionalExtras.bind(this)} onContinue={this.onContinue.bind(this)} toggleMoreSpecs={this.toggleMoreSpecs.bind(this)} onQtyChange={this.onQtyChange.bind(this)}/>
                            <button className="l-booking__next btn btn--primary" onClick={this.onContinue.bind(this)}>
                                Next Step
                            </button>
                        </div>
                    </div>
                    <StickyBar gst={api.gst} showtotal={true} state={this.props.state} togglePanel={this.togglePanel.bind(this)} updateTotal={this.updateTotal.bind(this)}/>
                </div>
                );
    }

    updateTotal(total) {
        this.total = total;
    }

    componentDidMount() {
        window.scrollTo(0, 0)
    }
}



const mapStateToProps = (state) => {
    return {
        state: {
            nz: state.data.nz,
            au: state.data.au,
            checkin: state.checkin,
            form: state.form,
        }
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setState: (state) => {
            dispatch({
                type: "SET_STATE",
                payload: state,
            });
        },
        setFormState: (state) => {
            dispatch({
                type: "SET_FORMSTATE",
                payload: state,
            });
        },
        setDataState: (state) => {
            dispatch({
                type: "SET_DATASTATE",
                payload: state,
            });
        }
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(ChangeExtras);
