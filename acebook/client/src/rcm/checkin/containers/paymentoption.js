import React from 'react';
import moment from 'moment';
import pikaday from 'pikaday';
import superagent from 'superagent';
import numeral from 'numeral';
import Cleave from 'cleave.js';

import { AceBookAPI as api } from '../../common/api/acebookapi';
import { BrowserRouter as Router } from 'react-router-dom';
import { connect } from 'react-redux';

import { PaymentAmount } from '../../common/components/partials/paymentamount';
import { CreditCard } from '../../common/components/partials/creditcard';
import { VehiclePanel } from '../../common/components/partials/vehiclepanel';
import { StickyBar } from '../../common/components/partials/stickybar';
import { ProgressBar } from '../../common/components/partials/progressbar';

class PaymentOption extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            num: '',
            name: '',
            exp: '',
            cvc: '',
            error: '',
            isprocessing: false,
            number: ''
        }
        this.state = {
          ...this.props.state.form
        }
    }

    componentDidMount() {
        this.setState({
            isprocessing: true
        });
    }

    onCCUpdate(event) {
        const newFormState = {...this.state, };
        if (this.number.getRawValue()) {
            newFormState['number'] = this.number.properties.result;
            newFormState['num'] = this.number.getRawValue();
        }
        if (this.exp.getRawValue()) {
            newFormState['exp'] = this.exp.properties.result;
        }
        newFormState[event.target.name] = event.target.value;
        newFormState['isChanges'] = true;
        this.setState(newFormState);
    }

    togglePanel(e) {
        e.preventDefault();
        window.scrollTo(0, 0);
        const panel = document.querySelector('.js-booking-panel')
        if (panel.classList.contains('is-open')) {
            //panel.classList.remove('is-open');
        } else {
            panel.classList.add('is-open');
        }
    }

    onContinue() {
        this.createBooking();
    }

    onPaymentConfirmDone() {
        window.location.replace('/bookings/complete');
    }

    formatFields() {
        if (document.querySelector('.js-cc-num')) {
            this.number = new Cleave('.js-cc-num', {
                creditCard: true,
                delimiter: ' - ',
                onCreditCardTypeChanged: (type) => {
                    const visa = document.querySelector('.js-cc-visa');
                    const mastercard = document.querySelector('.js-cc-mastercard');
                    const unionpay = document.querySelector('.js-cc-unionpay');

                    visa.classList.remove('is-active');
                    mastercard.classList.remove('is-active');
                    unionpay.classList.remove('is-active');

                    if (type === 'visa') {
                        visa.classList.add('is-active');
                    } else if (type === 'mastercard') {
                        mastercard.classList.add('is-active');
                    } else {
                        visa.classList.add('is-active');
                        mastercard.classList.add('is-active');
                        unionpay.classList.add('is-active');
                    }
                },
            });
        }
        if (document.querySelector('.js-cc-exp')) {
            this.exp = new Cleave('.js-cc-exp', {
                date: true,
                datePattern: ['m', 'y'],
                delimiter: ' / ',
            });
        }
    }

    validate() {
        let error_message = "";
        if (this.state.name == "") {
            error_message = error_message + "\nPlease Enter Card Holder Name.";
        }
        if (this.state.num == "") {
            error_message = error_message + "\nPlease Enter the Card Number.";
        }
        if (this.state.exp == "") {
            error_message = error_message + "\nPlease Enter Expiry Date.";
        }
        if (this.state.cvc == "") {
            error_message = error_message + "\nPlease Enter CVC.";
        } else {
            let reg_cvc = /^[0-9]{3,4}$/;
            if (!reg_cvc.test(this.state.cvc)) {
                error_message = error_message + "\nPlease Enter a Valid CVC.";
            }
        }
        return error_message;
    }

    confirmPayment() {
        let form = this.props.state.form;
        let locp = form.formPickupLocation.split('-');
        let paymentamount = numeral((form.selectedPaymentAmount == 'branch') ? (form.balance).format('0.00') : (form.total * api.deposit)).format('0.00');

        let data = {
            resno: form.bookingRef,
            name: this.state.name,
            num: this.state.num,
            exp: this.state.exp,
            cvc: this.state.cvc,
            amount: '' + paymentamount
        }
        this.setState({
            'error': '',
            isprocessing: true
        });

        let cc = this.state
        superagent.post('/payments/purchase').send($.param(data)).end((err, res) => {
            // Calling the end function will send the request
            let jsonres = JSON.parse(res.text);
            if (jsonres.isSuccessful) {
                this.setState({
                    error: ''
                });

                let paymentdata = [
                    paymentamount,
                    '1', //success (1=yes, 0=no),
                    jsonres.Transaction.CardName, //cctype,
                    moment(jsonres.Transaction.AcquirerDate + ' ' + jsonres.Transaction.AcquirerTime, "YYYYMMDD HHmmss").format('DD-MMM-YYYY HH:mm'), // paymentdateacquired (DD-MMM-YYYY HH:mm),
                    '2', //paymentsupplierid (2=rcm),
                    '', // transactionbillingid,
                    '', // paymenttxnref,
                    cc.name.toUpperCase(), // cardholdername,
                    jsonres.Transaction.CardName.toUpperCase(), //paymentsource,
                    this.state.num.replace(/(\d{6})\d{8}(\d{2})/, "$1********$2"), // truncatedcardnumber,
                    this.state.exp, // ccexpiry,
                    '', // transtype (Empty=purchase, Auth=allow zero amount),
                    '0', // merchantfeeid (0=no fee)
                ];

                let urlseg = [
                    this.props.state[locp[0]].rcmReservationRef,
                    api.base64.encode(paymentdata.join(';') + '|' + api.getGUID())
                ];

                api.confirmPayment(api.getEndPoint(form.country), urlseg.join('/'), this.props, () => {
                    this.onPaymentConfirmDone()
                });
            } else {
                this.setState({
                    error: jsonres.responseText,
                    isprocessing: false
                });
                alert(jsonres.responseText);
            }
        });


    }

    createBooking() {
        // pickup location
        let msg = '';
        const state = this.props.state;
        const form = state.form;
        if (form.selectedPaymentAmount != 'branch') {
            if (this.number.getRawValue()) {
                this.setState({
                    number: this.number.properties.result,
                    num: this.number.getRawValue()
                });
            }
            if (this.exp.getRawValue()) {
                this.setState({
                    exp: this.exp.properties.result,
                });
            }
            msg = this.validate();
        }
        if (msg == '') {
            if (state[form.country].rcmReservationNo == '') {
                let locp = form.formPickupLocation.split('-');
                let ploc = state[locp[0]].rcmLocationInfo.filter(l => l.id == locp[1])[0];

                // dropoff location
                let locd = form.formDropoffLocation.split('-');
                let dloc = state[locd[0]].rcmLocationInfo.filter(l => l.id == locd[1])[0];

                // pickup date
                let pdate = form.formPickupDate.replace(new RegExp('/', 'g'), '_');

                // dropoff date
                let ddate = form.formDropoffDate.replace(new RegExp('/', 'g'), '_');


                this.setState({
                    'error': '',
                    isprocessing: true
                });
                if (form.selectedPaymentAmount == 'branch') {
                    let checkin = this.props.state.checkin;
                    let urlseg = [
                        checkin.bookingNo,
                        locp[1], // pickuplocation
                        2, // BookingType (1=qoute, 2=booking)
                        form.selectedInsuranceId, //insurance
                        0, // extra_kms_id
                        0, // transmission
                        0 // sendemail set to off since we have out own implementation on email confirmation
                    ];

                    let custdata = new Array();
                    let custFormData = form.formCustomerData;
                    let rmkArray = custFormData.rmk.match(/[a-zA-Z0-9\/\.@#?-\s\n]/g);
                    custFormData.rmk = rmkArray.join("");
                    Object.keys(custFormData).map(key => custdata.push(key + ':' + custFormData[key]));
                    let optionsdata = new Array();
                    form.selectedOptionalExtras.map(item => optionsdata.push(item.ex.id + ':' + item.qty));
                    let data = [
                        custdata.join(','),
                        optionsdata.join(','),
                        0, // referral_id
                        api.getGUID()
                    ];
                    api.bookingEdit(api.getEndPoint(form.country), urlseg.join('/') + '/?' + api.base64.encode(data.join('|')), this.props, () => {
                        alert('done');
                    });
                }
            } else {
                if (!this.state.isprocessing) {
                    this.setState({
                        isprocessing: true
                    });
                    // this.confirmPayment();
                }
            }
        } else {
            alert(msg);
        }
    }

    onFormChange(event) {
        const newFormState = {...this.props.state.form};
        switch (event.target.name) {
            default:
                newFormState.formCustomerData[event.target.name] = event.target.value;
                break;
        }

        this.props.setFormState(newFormState);
    }

    onSelectPaymentAmount(id) {
        this.props.setFormState({
            selectedPaymentAmount: id
        });
    }

    render() {
        return (
                <div className="l-booking">
                    <VehiclePanel gst={api.gst} state={this.props.state} showtotal={true}/>
                    <div className="l-booking__main">
                        <div className="l-booking__container">
                            <div className="l-booking__header">
                                <h1 className="l-booking__heading">Make a Booking</h1>
                            </div>
                            <ProgressBar progress={75}/>
                            <PaymentAmount mode="checkin" state={this.props.state} onSelectPaymentAmount={this.onSelectPaymentAmount.bind(this)}/>
                            <CreditCard state={this.state} onCCUpdate={this.onCCUpdate.bind(this)}/>
                            {this.state.isprocessing
                            ? <button className="l-booking__next btn">
                                Proccessing...
                            </button>
                            :
                                    <button className="l-booking__next btn btn--primary" onClick={this.onContinue.bind(this)}>
                                        Continue
                                    </button>
                            }
                        </div>
                    </div>
                    <StickyBar gst={api.gst} showtotal={true} state={this.props.state} togglePanel={this.togglePanel.bind(this)}/>
                </div>
                );
    }

    componentDidMount() {
        this.formatFields();
        window.scrollTo(0, 0);
        this.setState({
            isprocessing: false
        });
    }
}
const mapStateToProps = (state) => {
    return {
        state: {
            nz: state.data.nz,
            au: state.data.au,
            checkin: state.checkin,
            form: state.form,
        }
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setState: (state) => {
            dispatch({
                type: "SET_STATE",
                payload: state,
            });
        },
        setFormState: (state) => {
            dispatch({
                type: "SET_FORMSTATE",
                payload: state,
            });
        },
        setDataState: (state) => {
            dispatch({
                type: "SET_DATASTATE",
                payload: state,
            });
        }
    };
};



export default connect(mapStateToProps, mapDispatchToProps)(PaymentOption);
