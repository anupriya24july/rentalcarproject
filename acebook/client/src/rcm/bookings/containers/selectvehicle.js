import React from 'react';
import moment from 'moment';
import pikaday from 'pikaday';
import numeral from 'numeral';

import { AceBookAPI as api } from '../../common/api/acebookapi';
import { BrowserRouter as Router } from 'react-router-dom'; 
import { connect } from 'react-redux';

import { VehicleList } from '../../common/components/vehiclelist';
import { VehiclePanel } from '../../common/components/partials/vehiclepanel';
import { StickyBar } from '../../common/components/partials/stickybar';
import { ProgressBar } from '../../common/components/partials/progressbar';

class SelectVehicle extends React.Component {

  pushAnalytics () {
    let form = this.props.state.form;
    let impressions = [];
    let products = [];
    let locp = this.props.state.form.formPickupLocation.split('-');
    let ploc = this.props.state[locp[0]].rcmLocationInfo.filter(l => l.id == locp[1] )[0];

    let locd = this.props.state.form.formDropoffLocation.split('-');
    let dloc = this.props.state[locp[0]].rcmLocationInfo.filter(l => l.id == locd[1] )[0];

    const datetimeformat = 'DD/MM/YYYY HH:mm';
    const p_datetime = moment(form.formPickupDate + ' ' + form.formPickupTime.replace('_',':'), datetimeformat);
    const d_datetime = moment(form.formDropoffDate + ' ' + form.formDropoffTime.replace('_',':'), datetimeformat);

    let age = this.props.state[locp[0]].rcmDriverAgesInfo.filter(age => age.id == this.props.state.form.formMinimumAge)[0];

    let days = this.props.state[locp[0]].rcmAvailableCars[0].numofdays;

    let customDimensions = {
       'dimension1': days // Days
      ,'metric1': days // Days
      ,'dimension2': ploc.location // Pickup Location
      ,'dimension3': p_datetime.format('DD, MMM, YYYY, hh:mm a') // Pickup DateTime
      ,'dimension4': dloc.location // DropOff Location
      ,'dimension5': d_datetime.format('DD, MMM, YYYY, hh:mm a') // DropOff DateTime
      ,'dimension6': age.driverage // Age
      ,'dimension7': form.formPromoCode // Promocode
    }

    this.props.state[locp[0]].rcmAvailableCars.map((item,i) => {
      
      let product = {
        ...customDimensions,
        'id': item.sippcodes,
        'name': item.categoryfriendlydescription,
        'brand': item.vehicledescription1,
        'category': 'bookings/selectvehicle (' + ploc.location + ')',
        'variant':'Vehicle',
        'price': item.totalrateafterdiscount + '',
        'quantity': 1 // always set to item qty for vehicles
      };

      products.push(product);

      impressions.push({
        ...product,
        'position': i+1,
        'list': 'bookings/selectvehicle (' + ploc.location + ')',
      });

      // Availability
      window.dataLayer.push({
        'event': 'AceRentals.ec',
        'eventCategory': 'Availability Search',
        'eventAction': p_datetime.format('YYYY-MM-DD hh:mm a') + ' - ' + d_datetime.format('YYYY-MM-DD hh:mm a') + ' ( ' + item.numofdays + 'd ) / ' + ploc.location + ' - ' + dloc.location + ' / ' + age.driverage + ' / ' + form.formPromoCode,
        'eventLabel': item.vehiclecategory + ' / ( ' + item.discounteddailyrate + 'pd ) ' +  item.totalrateafterdiscount + ' / ' + item.availablemsg,
        'eventValue': +item.available
      });
    });
    
    // Vehicle Impressions
    window.dataLayer.push({
      'ecommerce':{
        'currencyCode': form.country=='au'? 'AUD':'NZD',
        'impressions': impressions
      },
      'event': 'AceRentals.ec',
      'eventCategory': 'Ecommerce',
      'eventAction': 'Product Impression'
    });

    // Checkout
    window.dataLayer.push({
      'ecommerce': {
        'checkout': {
          'actionField': {'step': 1},
          'products': products
        }
      },
      'event': 'AceRentals.ec',
      'eventCategory': 'Ecommerce',
      'eventAction': 'Checkout'
    });
  }

  constructor(props) {
    super(props);
    this.openModal = props.openModal;
    this.props.setFormState({
      isprocessing: false
    });
    this.country = api.defaultcountry;

    this.state = {
      ...this.props.state.form
    }
  }

  componentWillMount() {
    if(this.props.match.path == window.locale + '/bookings/selectvehicle'){
       // pickup location
      let locp = this.props.state.form.formPickupLocation.split('-');
      let ploc = this.props.state[locp[0]].rcmLocationInfo.filter(l => l.id == locp[1] )[0];

      // dropoff location
      let locd = this.props.state.form.formDropoffLocation.split('-');
      let dloc = this.props.state[locd[0]].rcmLocationInfo.filter(l => l.id == locd[1] )[0];

      // pickup date
      let pdate = this.props.state.form.formPickupDate.replace(new RegExp('/', 'g'), '_');

      // dropoff date
      let ddate = this.props.state.form.formDropoffDate.replace(new RegExp('/', 'g'), '_');

      let urlseg = [
        0, //this.props.state.form.formCategoryType,
        locp[1],
        pdate,
        this.props.state.form.formPickupTime,
        locd[1],
        ddate,
        this.props.state.form.formDropoffTime,
        this.props.state.form.formMinimumAge,
        1,
        this.props.state.form.formPromoCode == '' ? '-': this.props.state.form.formPromoCode
      ];
      
      api.getStep2(api.getEndPoint(this.props.state.form.country),urlseg.join('/')+'#',this.props, this.initStep.bind(this));
    }
  }

  togglePanel(e) {
    e.preventDefault();
    window.scrollTo(0, 0);
    const panel = document.querySelector('.js-booking-panel')
    if (panel.classList.contains('is-open')) {
      // panel.classList.remove('is-open');
    } else {
      panel.classList.add('is-open');
    }
  }

  hideMoreSpecs(e) {
    let specs = e.target.parentElement.parentElement.querySelectorAll('.details.active');
    if(specs.length){
      specs.forEach((spec) => {
        spec.classList.remove('active');
        spec.classList.add('hidden');
      });
    }
  }

  toggleMoreSpecs(e) {
    let specs = e.target.parentElement.parentElement.querySelectorAll('.details.hidden');
    if(specs.length){
      specs.forEach((spec) => {
        spec.classList.remove('hidden');
        spec.classList.add('active');
      });
    }else{
      specs = e.target.parentElement.parentElement.querySelectorAll('.details.active');
      specs.forEach((spec) => {
        spec.classList.add('hidden');
        spec.classList.remove('active');
      });
    }
  }

   onSelectVehicle(catid, upgradeindex, e) {
    this.props.setFormState({
      isprocessing: true
    });
    e.preventDefault();
    this.props.state[this.country].rcmAvailableCars.sort(function(obj1, obj2) {  return  obj2.available-obj1.available || obj1.totrate-obj2.totrate ; });
    let selectedVehicle=this.props.state[this.country].rcmAvailableCars.filter(item => catid == item.carsizeid)[0];
    let selectedTotalRate=selectedVehicle['totalrateafterdiscount'];
    let upgradeVehicleId = "";
    let upgradeVehicle = "";
    let rate = "";
    let description = "";
    let totalRate="";
    if(this.country=='nz'){
      if(this.props.state[this.country].rcmAvailableCars[upgradeindex]){
        let upgradeVehicle=this.props.state[this.country].rcmAvailableCars[upgradeindex];
        if(upgradeVehicle['available'] == 1){
          upgradeVehicleId=upgradeVehicle['carsizeid'];
          rate=upgradeVehicle['discounteddailyrate'];
          totalRate=upgradeVehicle['totalrateafterdiscount'];
          description=upgradeVehicle['categoryfriendlydescription'];
        }
      }
    } else {
      let caroffer = this.props.state[this.country].rcmMandatoryFees.filter(item => item.name.indexOf('Relocation') >= 0 && item.fees==0 && item.vehiclesizeid==selectedVehicle['carsizeid']);
      if(caroffer.length<=0){
        let availablevehicles=this.props.state[this.country].rcmAvailableCars.filter(item => parseInt(item.totalrateafterdiscount)>parseInt(selectedVehicle['totalrateafterdiscount'])  && item.available == 1);
        availablevehicles.sort(function(obj1, obj2) {  return  obj1.totrate-obj2.totrate ; });
        let upgradeVehicle=availablevehicles[0]!='undefined'? availablevehicles[0] : '';
        if(upgradeVehicle){
          upgradeVehicleId=upgradeVehicle['carsizeid'];
          rate=upgradeVehicle['discounteddailyrate'];
          totalRate=upgradeVehicle['totalrateafterdiscount'];
          description=upgradeVehicle['categoryfriendlydescription'];
        }
      }
    }
    if(this.props.match.path == window.locale + '/bookings/selectvehicle'){
      this.props.setFormState({
        selectedCarCategoryId: catid,
        upgradeCar: {
                    categoryId: upgradeVehicleId,
                    description: description,
                    rate: rate,
                    amntDiff: totalRate-selectedTotalRate,
                    previousCar: {
                        categoryId: '',
                        description: '',
                        rate: '',
                    }
                },
      });
      window.location = window.locale + '/bookings/addextras';
    }
  }


  initStep() {
    let locp = this.props.state.form.formPickupLocation.split('-');
    if(this.props.state[locp[0]].rcmAvailableCars.length == 0){
      //this.props.history.goBack();
      let heading="Just to let you know...";
      let msg="There are no cars available for the selected dates. Please try again.";
      this.openModal(heading,msg,0);      
      this.props.setFormState({
        isprocessing: false
      });
      window.location = "/bookings";
      //this.props.history.push('/bookings/');
    }else{
      this.pushAnalytics();
    }
    // window.txtPickupdate.setMoment(moment(this.props.state.form.formPickupDate,api.dateformat));
    // window.txtPickupdate.setMinDate(moment(this.props.state.form.formPickupDate,api.dateformat).toDate());
    // window.txtReturn.setMoment(moment(this.props.state.form.formDropoffDate,api.dateformat));
    // window.txtReturn.setMinDate(moment(this.props.state.form.formPickupDate,api.dateformat).toDate());
  }


  shouldComponentUpdate(nextProps, nextState) {
    return !this.props.state.form.isprocessing;
  }
  
  render() {
    return (
      <div className="l-booking">
        <div className="l-booking__main">
          <div className="l-booking__container">
            <div className="l-booking__header">
              <h1 className="l-booking__heading">{window.labels.BookingLabel}</h1>
            </div>
            <ProgressBar progress={0}/>
            <VehicleList gst={api.gst} cta="Book" state={this.props.state} onSelectVehicle={this.onSelectVehicle.bind(this)} toggleMoreSpecs={this.toggleMoreSpecs.bind(this)} hideMoreSpecs={this.hideMoreSpecs.bind(this)}/>
          </div>
        </div>
        <StickyBar api={api} showtotal={false} state={this.props.state} togglePanel={this.togglePanel.bind(this)}/>
      </div>
    );
  }
  
  componentDidMount () {
    window.scrollTo(0, 0);
  }
}

const mapStateToProps = (state) => {
  return {
    state: {
      nz: state.data.nz,
      au: state.data.au,
      form: state.form,
    }
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setFormState: (state) =>{
      dispatch({
        type: "SET_FORMSTATE",
        payload: state, 
      });
    },
    setDataState: (state) =>{
      dispatch({
        type: "SET_DATASTATE",
        payload: state, 
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectVehicle);
