import React from 'react';
import superagent from 'superagent';
import jsonp from 'superagent-jsonp';
import moment from 'moment';
import pikaday from 'pikaday';

import { AceBookAPI as api } from '../../common/api/acebookapi';
import { BrowserRouter as Router } from 'react-router-dom'; 
import { connect } from 'react-redux';

import { SidebarWidget } from '../../common/components/sidebarwidget';
import { InlineWidget } from '../../common/components/inlinewidget';

class Itinerary extends React.Component {

  constructor(props) {
    super(props);

    this.country = api.defaultcountry;
    this.fetchCountry = api.defaultcountry;
    this.dropOffSameAsPickup = true;
    this.dateFormat = 'DD/MM/YYYY';
    this.today = moment(new Date());
    this.openModal = props.openModal;
    this.mode = props.mode;
    this.props.setFormState({
      isprocessing: false
    });

    this.state = {
      ...this.props.state.form
    }
  }

  detectPresets(){

    if (!String.prototype.startsWith) {
      String.prototype.startsWith = function(searchString, position) {
        position = position || 0;
        return this.indexOf(searchString, position) === position;
      };
    }

    let form = this.props.state.form;
    let locationBook = document.querySelector('.l-location__book');
    if(locationBook && location.pathname.startsWith('/locations') && document.querySelector('.l-page__heading')){
      let currentloc = document.querySelector('.l-location__book').dataset.locationid;
      let currentcountry = document.querySelector('.l-location__book').dataset.country == 'New Zealand'? 'nz':'au';

      locationBook.addEventListener('click', (e) => {
        e.preventDefault();

        const country =  this.props.state[currentcountry];
        const p_location = country.rcmLocationInfo.filter(loc => loc.id == currentloc)[0];

        this.setState({
          formPickupLocation: currentcountry + '-' + p_location.id,
          formDropoffLocation: currentcountry + '-' + p_location.id,
        });
      });
    }

    let promoBook = document.querySelector('.l-promotion__book');
    if(promoBook && (location.pathname.startsWith('/deals')||location.pathname.startsWith('/rent-the-experience')) && document.querySelector('.l-promotion__code')){
      let promocode = document.querySelector('.l-promotion__code').innerHTML.split('/div>')[1].trim();
      promoBook.addEventListener('click', (e) => {
        e.preventDefault();
        this.setState({
          formPromoCode: promocode
        });
      });
    }

    let reloBook = document.querySelectorAll('.c-relocation-card__action');
    if(reloBook && location.pathname.startsWith('/deals/relocations')){
      reloBook.forEach((reloBtn) => {
        reloBtn.addEventListener('click', (e) => {
          e.preventDefault();
          this.setState({
            formPromoCode: this.country=='nz'? 'RELOCATION':'',
            formPickupLocation: this.country + '-' + e.target.dataset.pl,
            formDropoffLocation: this.country + '-' + e.target.dataset.dl,
            selectedCarCategoryId: e.target.dataset.car,
          });
        });
      });
    }
    
  }

  togglePanel(e) {
    e.stopPropagation();
    window.scrollTo(0, 0);
    let panel = document.querySelector('.js-booking-panel');
    if (panel.classList.contains('is-open')) {
      panel.classList.remove('is-open');
    } else {
      panel.classList.add('is-open');
    }
  }

  componentWillMount() {
    window.rcmStep1Ready = () => {
      if (this.fetchCountry === 'nz') {
        this.props.setDataState({
          nz: {
            ...this.props.state.nz,
            rcmLocationInfo: window.rcmLocationInfo,
            rcmOfficeTimes: window.rcmOfficeTimes,
            rcmCategoryTypeInfo: window.rcmCategoryTypeInfo,
            rcmDriverAgesInfo: window.rcmDriverAgesInfo,
            rcmHolidays: window.rcmHolidays,
            rcmErrors: window.rcmErrors,
          },
        }, function(){alert('hit')});

        if(api.defaultcountry == 'nz'){
          this.fetchCountry = 'au';
          superagent.get(api.getEndPoint(this.fetchCountry) + '/step1/#').use(jsonp).end((error, response) => {});
        }
        this.removeJS(api.getEndPoint(this.fetchCountry) + '/step1/#');
       
      }

      if(this.fetchCountry === 'au'){
        this.props.setDataState({
          au: {
            ...this.props.state.au,
            rcmLocationInfo: window.rcmLocationInfo,
            rcmOfficeTimes: window.rcmOfficeTimes,
            rcmCategoryTypeInfo: window.rcmCategoryTypeInfo,
            rcmDriverAgesInfo: window.rcmDriverAgesInfo,
            rcmHolidays: window.rcmHolidays,
            rcmErrors: window.rcmErrors,
          },
        });

        //  - clean up unwanted global variables and callback function
        delete window.rcmLocationInfo;
        delete window.rcmOfficeTimes;
        delete window.rcmCategoryTypeInfo;
        delete window.rcmDriverAgesInfo;
        delete window.rcmHolidays;
        delete window.rcmErrors;

        if(api.defaultcountry == 'au'){
          this.fetchCountry = 'nz';
          superagent.get(api.getEndPoint(this.fetchCountry) + '/step1/#').use(jsonp).end((error, response) => {});
        }
        this.removeJS(api.getEndPoint(this.fetchCountry) + '/step1/#');
      }

      this.initTime(this.state);
    };

    // if(this.props.match.path == '/' || this.props.match.path == '/bookings'){
    superagent.get(api.getEndPoint(this.fetchCountry) + '/step1/#').use(jsonp).end((error, response) => {});
    // }

    this.detectPresets();
  }

  dateFields() {
    this.dateContainers = document.querySelectorAll('.js-date-container');
    const parent = this;
    if (this.dateContainers) {
      NodeList.prototype.forEach = Array.prototype.forEach;
      this.dateContainers.forEach((container, i) => {
        let dateInput = container.querySelector('.js-date');
        $('#'+dateInput.id).data('pikaday', new pikaday({
          container, 
          field: dateInput, 
          numberOfMonths: 1, 
          defaultDate: moment(dateInput.classList.contains('Pickup')? this.state.formPickupDate : this.state.formDropoffDate,this.dateFormat).toDate(),
          setDefaultDate: true,
          keyboardInput: false,
          minDate: this.today.toDate(),
          format: "DD, MMM, YYYY",
          position: 'bottom left',
          reposition: false,
          i18n: {
            previousMonth: 'Previous Month',
            nextMonth: 'Next Month',
            months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December', ], 
            weekdays: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', ],
            weekdaysShort: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', ],
          },
          onSelect: (selecteddate) => { 
            let datepickers = $('.js-date');
            const props = this.props;
            const dateformat = this.dateFormat
            let dropoffdate = parent.state.formDropoffDate;
            let pickupdate = parent.state.formPickupDate;
            if(dateInput.classList.contains('Dropoff')){
              datepickers.each(function(){
                let el = $('#'+this.id);
                dropoffdate = moment(selecteddate).format(dateformat);
                if(el.hasClass('Dropoff')){
                  el.data('pikaday').setMoment(moment(selecteddate),true);
                }

                // daterange
                el.data('pikaday').setStartRange(moment(pickupdate,dateformat).toDate());
                el.data('pikaday').setEndRange(moment(dropoffdate,dateformat).toDate());
              });
              parent.setState({
                formDropoffDate: moment(selecteddate).format(dateformat),
              });
            }else{ //Pickup
              // - return date can't be earlier than pickup date + minimum booking days
              if(parent.state.formPickupLocation != ''){
                let loc = parent.state.formPickupLocation.split('-');
                let ploc = props.state[loc[0]].rcmLocationInfo.filter(l => l.id == loc[1] )[0];
                
               
                datepickers.each(function(){
                  let el = $('#'+this.id);
                  pickupdate = moment(selecteddate).format(dateformat);
                  if(moment(dropoffdate,dateformat).isBefore(moment(selecteddate).add(+ploc.minbookingday+1,'days'))){
                    dropoffdate = moment(selecteddate).add(+ploc.minbookingday+1,'days').format(dateformat);
                  }
                  if(el.hasClass('Pickup')){
                    el.data('pikaday').setMoment(moment(selecteddate),true);
                  }else{
                    el.data('pikaday').setMoment(moment(dropoffdate,dateformat));
                    el.data('pikaday').setMinDate(moment(selecteddate).add(+ploc.minbookingday+1,'days').toDate());
                  }

                  // daterange
                  el.data('pikaday').setStartRange(moment(pickupdate,dateformat).toDate());
                  el.data('pikaday').setEndRange(moment(dropoffdate,dateformat).toDate());
                });

                parent.setState({
                  formPickupDate: pickupdate,
                  formDropoffDate: dropoffdate,
                });
              }
            }
          },
          onClose: () => {
            // if(!window.inline_Dropoff_Date.isVisible()){
            //   // open dropoff date calendar immediately after selecting pickup date
            // }
          },
        }));
      });
    }
  }

  toggleOptions() {  
    let toggle=!this.state.showDropoff;
     this.setState({showDropoff:toggle});
  }

  componentDidMount(){   
    this.dateFields();    
    this.setState({
      isprocessing: false
    });
  }

  initTime(newFormState) {
    let pickupTime=[];
    let dropoffTime=[];
    let pickupDate= moment(newFormState.formPickupDate, 'DD/MM/YYYY').day()+1;
    let dropOffDate= moment(newFormState.formDropoffDate, 'DD/MM/YYYY').day()+1;
    let ploc = newFormState.formPickupLocation.split('-');
    let dloc = newFormState.formDropoffLocation.split('-');

    let officeState=this.props.state[ploc[0]].rcmOfficeTimes;
    
    //if(officeState.length){
      let pOfficeTime=officeState.filter(l => l.locid == ploc[1]  && l.wd ==pickupDate )[0];
      let dOfficeTime=officeState.filter(l => l.locid == dloc[1]  && l.wd ==dropOffDate )[0];
      let startPickup=moment(pOfficeTime.startpickup, 'HH:mm');
      let endPickup=moment(pOfficeTime.endpickup, 'HH:mm');

      /* Pickup TimeArray */
      if(pOfficeTime){
        let officeEndPickup=endPickup.format('HH:mm')==startPickup.format('HH:mm')?endPickup.add(1, 'day').format('YYYY-MM-DD HH:mm'):endPickup.format('YYYY-MM-DD HH:mm');
       
        for (let startTime=startPickup;startTime.format('YYYY-MM-DD HH:mm')<=officeEndPickup;startTime.add(15, 'minutes')) {
              pickupTime.push(startTime.format('HH_mm') + ','+ startTime.format('hh:mm A') );
        } 
        if(startPickup.format('HH:mm')!=endPickup.format('HH:mm')){
          pickupTime.push(endPickup.format('HH_mm') + ','+ endPickup.format('hh:mm A'));
        }
      }

      /* DropOff TimeArray */
      if(dOfficeTime){
        let startDropoff=moment(dOfficeTime.startdropoff, 'HH:mm');
        let endDropoff=moment(dOfficeTime.enddropoff, 'HH:mm'); 
        let officeEndDropoff=endDropoff.format('HH:mm')==startDropoff.format('HH:mm')?endDropoff.add(1, 'day').format('YYYY-MM-DD HH:mm'):endDropoff.format('YYYY-MM-DD HH:mm');

         for (let startTime=startDropoff;startTime.format('YYYY-MM-DD HH:mm')<=officeEndDropoff;startTime.add(15, 'minutes')) {
          dropoffTime.push(startTime.format('HH_mm')+ ','+ startTime.format('hh:mm A') );
        }
        if(startDropoff.format('HH:mm')!=endDropoff.format('HH:mm')){
          dropoffTime.push(endDropoff.format('HH_mm')+ ','+ endDropoff.format('hh:mm A') );
        }
       
      }

      let selectedPickupTime= newFormState.formPickupTime+','+moment(newFormState.formPickupTime, 'HH_mm').format('hh:mm A');
      if(pickupTime.indexOf(selectedPickupTime)<0){
        if(moment(newFormState.formPickupTime,'h:mma').isBefore(moment(pOfficeTime.startpickup, 'HH:mm'))) {          
          this.setState({
            formPickupTime: moment(pOfficeTime.startpickup, 'HH:mm').format('HH_mm')
          });
        }else{
          this.setState({
            formPickupTime: moment(pOfficeTime.endpickup, 'HH:mm').format('HH_mm')
          });
        }
      }
      let selectedDropoffTime= newFormState.formDropoffTime+','+moment(newFormState.formDropoffTime, 'HH_mm').format('hh:mm A');
      if(dropoffTime.indexOf(selectedDropoffTime)<0){
        this.setState({
          formDropoffTime: moment(pOfficeTime.startdropoff, 'HH:mm').format('HH_mm')
        });
      }

      this.setState({
        dropoffTimeArr: dropoffTime.filter((val,id,array) => array.indexOf(val) == id),
        pickupTimeArr : pickupTime.filter((val,id,array) => array.indexOf(val) == id)
      });
   // }
  }

  onFormChange(event){
    //const newFormState = {...this.props.state.form, };
    const newFormState = {...this.state, };
    let heading="Just to let you know...";
    let msg="";
    switch(event.target.name){
      case 'formPickupLocation':
        let loc = event.target.value.split('-');
        let ploc = this.props.state[loc[0]].rcmLocationInfo.filter(l => l.id == loc[1] )[0];
        // Get notice required and minimum booking day
        let startPickup = moment(this.today).add(Math.ceil(ploc.noticerequired),'days');
        let startDropOff = moment(this.today).add(Math.ceil(ploc.noticerequired) + +ploc.minbookingday,'days');

        let PickUpDW = 0;
        let DropOffDW = 0;

        // set datepicker
        // window.inline_Pickup_Date.gotoDate(moment(startPickup).toDate());
        // window.inline_Pickup_Date.setMoment(moment(startPickup));
        // window.inline_Pickup_Date.setMinDate(moment(startPickup).toDate());
        // window.inline_Dropoff_Date.setMoment(moment(startDropOff));
        // window.inline_Dropoff_Date.setMinDate(moment(startDropOff).toDate());

        // todo: validate and set pickup and dropoff times

        if(loc[0] != newFormState.country){ // if pickup is in a different country always set dropoff same as pickup
          newFormState.formDropoffLocation = event.target.value;
        }else{
          if(!this.props.state.form.formDifferentDropOff){ // dropoff is a configured to be always same as pickup
            newFormState.formDropoffLocation = event.target.value;
          }
        }

        newFormState.country = loc[0]; // change country based on pickup location.

        newFormState[event.target.name] = event.target.value;
        break;
      case 'formDifferentDropOff':
        newFormState[event.target.name] = event.target.checked; 
        newFormState['showDropoff'] = event.target.checked;
        break;

      default:
        newFormState[event.target.name] = event.target.value;
        break;
    }
    this.setState(newFormState);
    this.initTime(newFormState);
  }

  checkAvailability() {
    if(this.props.state[this.country].rcmAvailableCars.length == 0){
      let heading="Just to let you know...";
      let msg="There are no cars available for the selected dates. Please try again.";
      this.openModal(heading,msg,0);
      this.setState({
        isprocessing: false
      });
    }else{
      if(location.pathname.startsWith('/deals/relocations')){
        // pickup location
        let locp = this.props.state.form.formPickupLocation.split('-');
        let ploc = this.props.state[locp[0]].rcmLocationInfo.filter(l => l.id == locp[1] )[0];

        if(this.props.state[locp[0]].rcmAvailableCars.filter(c => c.carsizeid == this.props.state.form.selectedCarCategoryId)[0].available == 1){
          window.location.href = '/bookings/addextras';
        }else{
          let heading="Just to let you know...";
          let msg="The selected car is not available for the selected dates. Please try again.";         
          this.openModal(heading,msg,0); 
          this.setState({
            isprocessing: false
          });
        }
      }else{
        window.location.href = window.locale + '/bookings/selectvehicle';
      }
    }
  }

  validate(){
     let msg="";
     if(this.state.formMinimumAge == 0){
       msg="Please Select Driver's Age";
    }
    return msg;
  }

  onSubmitSearch() {
    this.setState({
        isprocessing: true
    });
     // pickup location
    let locp = this.state.formPickupLocation.split('-');
    let ploc = this.props.state[locp[0]].rcmLocationInfo.filter(l => l.id == locp[1] )[0];

    // dropoff location
    let locd = this.state.formDropoffLocation.split('-');
    let dloc = this.props.state[locd[0]].rcmLocationInfo.filter(l => l.id == locd[1] )[0];

    // pickup date
    let pdate = this.state.formPickupDate.replace(new RegExp('/', 'g'), '_');

    // dropoff date
    let ddate = this.state.formDropoffDate.replace(new RegExp('/', 'g'), '_');
    // create permalink url with search data
    let urlseg = [
      0, // this.props.state.form.formCategoryType,
      api.defaultcountry == locp[0] ? locp[1] : this.state.formPickupLocation,
      pdate,
      this.state.formPickupTime,
      api.defaultcountry == locp[0] ? locd[1] : this.state.formDropoffLocation,
      ddate,
      this.state.formDropoffTime,
      this.state.formMinimumAge,
      1,
      this.state.formPromoCode == '' ? '-': this.state.formPromoCode
    ];

    if(this.mode=='experience'){
     window.location.href = 'https://agents.acerentalcars' + (locp[0]=='nz'? '.co.nz':'.com.au' )+ '/?cdp=uber/bookings/search/#' + urlseg.join('/');
    }else {
      let msg=this.validate();
      if (msg){
        let heading="Oops! Looks like something is missing!";
        this.openModal(heading,msg,0);
        this.setState({
          isprocessing: false
        });
      }
      else {
        // todo: perform validations here
        this.props.setDataState({
          [locp[0]]: {
              ...this.props.state[locp[0]],
              rcmReservationRef:'',
              rcmReservationNo:'',
              rcmPaymentSaved:false
          }
        });

        this.props.setFormState({
          ...this.state,
          isprocessing: true
        });
        this.props.setFormState({
         dropoffTimeArr:{},
         pickupTimeArr:{}
        });


        //Afterhour pickup check
        let pcountry = this.state.formPickupLocation.split('-');
        let officeTime=this.props.state[pcountry[0]].rcmOfficeTimes;
          let pickupDate= moment(this.state.formPickupDate, 'DD/MM/YYYY').day()+1;
          let pOfficeTime=officeTime.filter(l => l.locid == pcountry[1]  && l.wd ==pickupDate )[0];
          let pOpeningTime=moment(pOfficeTime['openingtime'], 'H:mm').format('h:mma');
          let pClosingTime=moment(pOfficeTime['closingtime'], 'H:mm').format('h:mma');
          let pickupTime=moment(this.state.formPickupTime, 'H_mm').format('h:mma');
          if(moment(pickupTime,'h:mma').isBefore(moment(pOpeningTime,'h:mma')) || moment(pickupTime,'h:mma').isAfter(moment(pClosingTime,'h:mma'))){
            this.props.setFormState({
              afterHourPick: true
            });
          }

        if(api.defaultcountry == locp[0]){
          api.getStep2(api.getEndPoint(this.state.country),urlseg.join('/')+'#',this.props, this.checkAvailability.bind(this));
        }else{
          window.location.href = (api.enviroment == 'dev'? 'http://dev':'https://www') + '.acerentalcars' + (locp[0]=='nz'? '.co.nz':'.com.au' ) + window.locale + '/bookings/search/#' + urlseg.join('/');
        }
      }
    }
  }

  removeJS(filename){
    const tags = document.getElementsByTagName('script');
      for (var i = tags.length; i >= 0; i--){
      if (tags[i] && tags[i].getAttribute('src') != null && tags[i].getAttribute('src').indexOf(filename) != -1)
      tags[i].parentNode.removeChild(tags[i]);
    }
  }

  render() {
    let widget = null;
    switch(this.props.widget){
      case 'sidebar':
        widget = <SidebarWidget api={api} state={this.props.state} form={this.state} onClick={e => this.toggleOptions()} onFormChange={e => this.onFormChange(e)} onSubmitSearch={e => this.onSubmitSearch(e)} onTogglePanel={e => this.togglePanel(e)}/>;
        break;
      case 'inline':
        widget = <InlineWidget api={api} state={this.props.state}  form={this.state} onClick={e => this.toggleOptions()}  onFormChange={e => this.onFormChange(e)} onSubmitSearch={e => this.onSubmitSearch(e)}/>;
        break;
      default:
        widget = <SidebarWidget api={api} state={this.props.state}  form={this.state} onClick={e => this.toggleOptions()}  onFormChange={e => this.onFormChange(e)} onSubmitSearch={e => this.onSubmitSearch(e)} onTogglePanel={e => this.togglePanel(e)}/>;
        break;
    }
    return widget;
  }
}

const mapStateToProps = (state) => {
  return {
    state: {
      nz: state.data.nz,
      au: state.data.au,
      form: state.form,
    }
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setFormState: (state) =>{
      dispatch({
        type: "SET_FORMSTATE",
        payload: state, 
      });
    },
    setDataState: (state) =>{
      dispatch({
        type: "SET_DATASTATE",
        payload: state, 
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Itinerary);
