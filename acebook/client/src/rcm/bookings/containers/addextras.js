import React from 'react';
import superagent from 'superagent';
import jsonp from 'superagent-jsonp';
import moment from 'moment';
import pikaday from 'pikaday';

import { AceBookAPI as api } from '../../common/api/acebookapi';
import { BrowserRouter as Router } from 'react-router-dom'; 
import { connect } from 'react-redux';

import { Extras } from '../../common/components/extras';
import { VehiclePanel } from '../../common/components/partials/vehiclepanel';
import { StickyBar } from '../../common/components/partials/stickybar';
import { ProgressBar } from '../../common/components/partials/progressbar';

class AddExtras extends React.Component {

  pushAnalytics () {
    let form = this.props.state.form;
    const activeCountry =  this.props.state[api.defaultcountry];
    let impressions = [];
    let products = [];
    let locp = this.props.state.form.formPickupLocation.split('-');
    let ploc = activeCountry.rcmLocationInfo.filter(l => l.id == locp[1] )[0];

    let locd = this.props.state.form.formDropoffLocation.split('-');
    let dloc = activeCountry.rcmLocationInfo.filter(l => l.id == locd[1] )[0];

    const datetimeformat = 'DD/MM/YYYY HH:mm';
    const p_datetime = moment(form.formPickupDate + ' ' + form.formPickupTime.replace('_',':'), datetimeformat);
    const d_datetime = moment(form.formDropoffDate + ' ' + form.formDropoffTime.replace('_',':'), datetimeformat);

    let age = activeCountry.rcmDriverAgesInfo.filter(age => age.id == this.props.state.form.formMinimumAge)[0];

    let customDimensions = {
       'dimension1': activeCountry.rcmAvailableCars[0].numofdays // Days
      ,'metric1': activeCountry.rcmAvailableCars[0].numofdays // Days
      ,'dimension2': ploc.location // Pickup Location
      ,'dimension3': p_datetime.format('DD, MMM, YYYY, hh:mma') // Pickup DateTime
      ,'dimension4': dloc.location // DropOff Location
      ,'dimension5': d_datetime.format('DD, MMM, YYYY, hh:mma') // DropOff DateTime
      ,'dimension6': age.driverage // Age
      ,'dimension7': form.formPromoCode // Promocode
    }

    const insuranceoptions = activeCountry.rcmInsuranceOptions.filter(ins => { 
      if(age.driverage >= ins.fromage  && (age.driverage <= ins.toage || ins.toage == 0) ) return ins
    });

    insuranceoptions.map((ins,i) => {
      let product = {
        ...customDimensions,
        'id': ins.id,
        'name': ins.name,
        'brand': ins.extradesc,
        'category': 'bookings/addextras (' + ploc.location + ')',
        'variant':'Insurance',
        'price': ins.fees + '',
        'quantity': ins.numofdays
      };

      products.push(product);

      impressions.push({
        ...product,
        'position': i+1,
        'list': 'bookings/addextras (' + ploc.location + ')',
      });
    });

    activeCountry.rcmOptionalFees.map((ex,i) => {
      let product = {
        ...customDimensions,
        'id': ex.id,
        'name': ex.name,
        'brand': ex.extradesc,
        'category': 'bookings/addextras (' + ploc.location + ')',
        'variant':'Optional Extras',
        'price': ex.fees + '',
        'quantity': (ex.type == 'Daily' ? ex.numofdays: 1)
      };

      products.push(product);

      impressions.push({
        ...product,
        'position': i+1,
        'list': 'bookings/addextras (' + ploc.location + ')',
      });
    });

    let selectedCar = activeCountry.rcmAvailableCars[0];
    
    let carproduct = [{
      ...customDimensions,
      'id': selectedCar.sippcodes,
      'name': selectedCar.categoryfriendlydescription,
      'brand': selectedCar.vehicledescription1,
      'category': 'bookings/selectvehicle (' + ploc.location + ')',
      'variant':'Vehicle',
      'price': selectedCar.totalrateafterdiscount + '',
      'quantity': 1 // always set to item qty for vehicles
    }];

    // Product Click Vehicle
    window.dataLayer.push({
      'ecommerce': {
        'click': {
          'actionField': {'list': 'bookings/selectvehicle (' + ploc.location + ')'},
          'products': carproduct
        }
      },
      'event': 'AceRentals.ec',
      'eventCategory': 'Ecommerce',
      'eventAction': 'productClick'
    });

    // Add to Cart Vehicle
    window.dataLayer.push({
      'ecommerce': {
        'currencyCode': form.country=='au'? 'AUD':'NZD',
        'add': {
          'products': carproduct
        }
      },
      'event': 'AceRentals.ec',
      'eventCategory': 'Ecommerce',
      'eventAction': 'addToCart'
    });

    // Extras Impressions
    window.dataLayer.push({
      'ecommerce':{
        'currencyCode': form.country=='au'? 'AUD':'NZD',
        'impressions': impressions
      },
      'event': 'AceRentals.ec',
      'eventCategory': 'Ecommerce',
      'eventAction': 'Product Impression'
    });

    // Checkout
    window.dataLayer.push({
      'ecommerce': {
        'checkout': {
          'actionField': {'step': 2},
          'products': products
        }
      },
      'event': 'AceRentals.ec',
      'eventCategory': 'Ecommerce',
      'eventAction': 'Checkout'
    });
  }

  constructor(props) {
    super(props);
    this.total = 0;
    this.handleScroll = this.handleScroll.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.openModal = props.openModal;
    this.props.setFormState({
      isprocessing: false
    });
    
    this.state = {
      ...this.props.state.form
    }
   }

   loadApi(catID){

    window.rcmStep3Ready = () => {

      let locp = this.props.state.form.formPickupLocation.split('-');
      let msg="";
      // - check for errors
      if (window.rcmErrors.length != 0){
        window.rcmErrors.map((item, index) => {             
          let heading="Just to let you know...";
          msg=item.error;
          this.openModal(heading,msg,0);
        })
        window.rcmErrors = [];
      }

      this.props.setDataState({
        [locp[0]]: {
          ...this.props.state[locp[0]],
          rcmDriverAgesInfo: window.rcmDriverAgesInfo,
          rcmLocationFees: window.rcmLocationFees,
          rcmAvailableCarDetails: window.rcmAvailableCarDetails,
          rcmAvailableCars: window.rcmAvailableCars,
          rcmMandatoryFees: window.rcmMandatoryFees,
          rcmOptionalFees: window.rcmOptionalFees,
          rcmInsuranceOptions: window.rcmInsuranceOptions,
          rcmKmCharges: window.rcmKmCharges,
          rcmRentalSource: window.rcmRentalSource,
          rcmCountries: window.rcmCountries,
          rcmAreaOfUse: window.rcmAreaOfUse,
          rcmTaxInclusive: window.rcmTaxInclusive,
          rcmTaxRate: window.rcmTaxRate,
          rcmStateTax: window.rcmStateTax,
          rcmErrors: window.rcmErrors,
        },
      });

      //  - clean up unwanted global variables and callback function
      delete window.rcmDriverAgesInfo;
      delete window.rcmLocationFees;
      delete window.rcmAvailableCarDetails;
      delete window.rcmAvailableCars;
      delete window.rcmMandatoryFees;
      delete window.rcmOptionalFees;
      delete window.rcmInsuranceOptions;
      delete window.rcmKmCharges;
      delete window.rcmRentalSource;
      delete window.rcmCountries;
      delete window.rcmAreaOfUse;
      delete window.rcmTaxInclusive;
      delete window.rcmTaxRate;
      delete window.rcmStateTax;
      delete window.rcmErrors;

      if(!msg){
        this.initStep();
      }

      this.removeJS(this.nzStep3);

    };

    if(this.props.match.path == window.locale + '/bookings/addextras'){
       // pickup location
      let locp = this.props.state.form.formPickupLocation.split('-');
      let ploc = this.props.state[locp[0]].rcmLocationInfo.filter(l => l.id == locp[1] )[0];

      // dropoff location
      let locd = this.props.state.form.formDropoffLocation.split('-');
      let dloc = this.props.state[locd[0]].rcmLocationInfo.filter(l => l.id == locd[1] )[0];

      // pickup date
      let pdate = this.props.state.form.formPickupDate.replace(new RegExp('/', 'g'), '_');

      // dropoff date
      let ddate = this.props.state.form.formDropoffDate.replace(new RegExp('/', 'g'), '_');

      let urlseg = [
        0, //this.props.state.form.formCategoryType,
        locp[1],
        pdate,
        this.props.state.form.formPickupTime,
        locd[1],
        ddate,
        this.props.state.form.formDropoffTime,
        this.props.state.form.formMinimumAge,
        catID,
        1,
        this.props.state.form.formPromoCode == '' ? '_': this.props.state.form.formPromoCode
      ];

      superagent.get(api.getEndPoint(this.props.state.form.country) + '/step3' + '/' +urlseg.join('/') + '#').use(jsonp).end((error, response) => {});
    }
   }

  componentWillMount() {
    this.loadApi(this.props.state.form.selectedCarCategoryId);
  }

  toggleMoreSpecs(e) {
    let specs = e.target.parentElement.parentElement.querySelectorAll('.toggle-active');
    if(specs.length){
      specs.forEach((spec) => {
        spec.classList.remove('toggle-active');
        spec.classList.add('toggle-hidden');
      });
    }else{
      specs = e.target.parentElement.parentElement.querySelectorAll('.toggle-hidden');
      specs.forEach((spec) => {
        spec.classList.add('toggle-active');
        spec.classList.remove('toggle-hidden');
      });
    }
  }

  initStep(){
    let locp = this.props.state.form.formPickupLocation.split('-');
    let age = this.props.state[locp[0]].rcmDriverAgesInfo.filter(age => age.id == this.props.state.form.formMinimumAge)[0];
    const activeCountry =  this.props.state[locp[0]];
    
    // load default insurance if none is selected.
    if(api.defaultcountry == 'au'){
      if(this.props.state.form.selectedInsuranceId == '' || activeCountry.rcmInsuranceOptions.filter(ins => ins.id == this.props.state.form.selectedInsuranceId)[0] == undefined){
        // let selectedInsurance = activeCountry.rcmInsuranceOptions.find(ins => ins.fees == 0 && ins.fromage >= age.driverage);
        let selectedInsurance = activeCountry.rcmInsuranceOptions.sort((a,b)=>{return +a.fees - +b.fees});
        this.props.setFormState({
          selectedInsuranceId: selectedInsurance[0].id,
        });
      }
    }

    let newSelEx = this.props.state.form.selectedOptionalExtras;

    this.props.state.form.selectedOptionalExtras.map((selex,i) => {
      // remove optional extras that are not compatible with the current vehicle.
      let thisex = activeCountry.rcmOptionalFees.filter(ex => selex.ex.id == ex.id)[0];
      if(!thisex){
        newSelEx.splice(newSelEx.indexOf(selex),1);
      }

      // refresh aceplus discounts if there is any.
      if(selex.ex.sgroupname.match(/ace plus discount|bundled/ig)){
        newSelEx.splice(newSelEx.indexOf(selex),1);
      }
    });

    //load bundled extras.
    let bundles = activeCountry.rcmMandatoryFees.filter(man => man.name.match(/bundle/ig))[0];
    if (bundles && bundles.extradesc3.startsWith('bundle')){
      bundles = bundles.extradesc3.split(':')[1].split(',');
    }else{
      bundles = [];
    }

    bundles.map(b => {
      let thebundle = activeCountry.rcmOptionalFees.filter(ex => ex.id == b)[0];
      if(thebundle){
        let bundle = this.props.state.form.selectedOptionalExtras.filter(selex => selex.ex.id == b)[0];
        if(! bundle){
          newSelEx.push({
            qty: 1,
            ex: thebundle,
          })
        }
      }
    });

    this.onSelectInsurance(this.props.state.form.selectedInsuranceId);

    this.props.setFormState({
      selectedOptionalExtras: newSelEx,
    });

    this.pushAnalytics();
  }

  onContinue(){
    this.props.setFormState({
      isprocessing: true
    });
    if(this.props.state.form.selectedInsuranceId == '' && api.defaultcountry == 'nz'){      
      let heading="Oops! Looks like something is missing!";
      let msg="Please choose an insurance option.";
      this.openModal(heading,msg,0);
    }else{
      this.props.setFormState({
        total: this.total,
      });
      window.location= window.locale + '/bookings/personaldetails';
    }
  }

  onSelectInsurance(insID, e){
    let locp = this.props.state.form.formPickupLocation.split('-');
    const activeCountry =  this.props.state[locp[0]];

    let extras = this.props.state.form.selectedOptionalExtras;
    const insuranceoptions = activeCountry.rcmInsuranceOptions;
    let aceplusdiscount = null;

    //load bundled cover
    let bundles = activeCountry.rcmMandatoryFees.filter(man => man.name.match(/bundle/ig))[0];
    if (bundles && bundles.extradesc3.startsWith('bundle')){
      bundles = bundles.extradesc3.split(':')[1].split(',');
    }else{
      bundles = [];
    }

    if( insuranceoptions.filter((ins,i) => ins.id == insID)[0] && insuranceoptions.filter((ins,i) => ins.id == insID)[0].excessfee == 0){
      let additionaldriver = extras.filter((selex,i) => selex.ex.name.match(/add.*driver.*/ig))[0];
      if(additionaldriver){
        extras.splice(extras.indexOf(additionaldriver),1);
      }

      //add aceplus discount if there is any.
      aceplusdiscount = activeCountry.rcmOptionalFees.filter((ex,i) => ex.sgroupname.match(/ace plus discount/ig))[0];
      if(aceplusdiscount){
        let apd = extras.filter((thisex,i) => thisex.ex.sgroupname.match(/ace plus discount/ig))[0];
        if(!apd){
          extras.push({
            qty: 1,
            ex: aceplusdiscount,
          })
        }
      }else{
        bundles.map(b => {
          let thebundle = activeCountry.rcmOptionalFees.filter(ex => ex.id == b && ex.sgroupname.match(/bundled cover/ig))[0];
          if(thebundle){
            let bundle = this.props.state.form.selectedOptionalExtras.filter(selex => (selex.ex.id == b && selex.ex.sgroupname.match(/bundled cover/ig)))[0];
            if(! bundle){
              extras.push({
                qty: 1,
                ex: thebundle,
              })
            }
          }
        });
      }
    }else{
      aceplusdiscount = extras.filter((ex,i) => ex.ex.sgroupname.match(/ace plus discount/ig))[0];
      if(aceplusdiscount){
        extras.splice(extras.indexOf(aceplusdiscount),1);
      }

      bundles.map(b => {
        let thebundle = activeCountry.rcmOptionalFees.filter(ex => ex.id == b)[0];
        if(thebundle){
          let bundle = this.props.state.form.selectedOptionalExtras.filter(selex => (selex.ex.id == b && selex.ex.sgroupname.match(/bundled cover/ig)))[0];
          if(bundle){
            extras.splice(extras.indexOf(thebundle),1);
          }
        }
      });
    }

    this.props.setFormState({
      selectedInsuranceId: insID,
      selectedOptionalExtras: extras,
    });
  }

  onSelectOptionalExtras(extraFeeID, e){
    e.stopPropagation();
    let locp = this.props.state.form.formPickupLocation.split('-');
    let extras = this.props.state.form.selectedOptionalExtras;
    let thisExtra = extras.filter((selex,i) => selex.ex.id == extraFeeID)[0];
    if(thisExtra){
      extras.splice(extras.indexOf(thisExtra),1);
    }else{
      extras.push({
        qty: parseInt(e.target.parentElement.querySelector('select')? e.target.parentElement.querySelector('select').value: 1),
        ex: this.props.state[locp[0]].rcmOptionalFees.filter((ex,i) => ex.id == extraFeeID)[0],
      })
    }
    this.props.setFormState({
      selectedOptionalExtras: extras,
    });
  }


  onUpgrade(catid, prevcarid, mode, e) {
    e.preventDefault();
    if(mode==1) {
      this.props.setFormState({
          selectedCarCategoryId: catid,
          upgradeCar: {
                      previousCar: {
                        categoryId: prevcarid,
                        description: this.props.state[api.defaultcountry].rcmAvailableCars[0].categoryfriendlydescription,
                        rate: this.props.state[api.defaultcountry].rcmAvailableCars[0].discounteddailyrate,
                      } 
                  },
      });    
    }else {
      this.props.setFormState({
          selectedCarCategoryId: catid,
          upgradeCar: {
                      previousCar: {
                        categoryId: '',
                        description: '',
                        rate: '',
                      }
                  },
      });    

    }
     this.loadApi(catid);
  }


  removeJS(filename){
    const tags = document.getElementsByTagName('script');
      for (var i = tags.length; i >= 0; i--){
      if (tags[i] && tags[i].getAttribute('src') != null && tags[i].getAttribute('src').indexOf(filename) != -1)
      tags[i].parentNode.removeChild(tags[i]);
    }
  }

  togglePanel(e) {
    e.preventDefault();
    window.scrollTo(0, 0);
    const panel = document.querySelector('.js-booking-panel')
    if (panel.classList.contains('is-open')) {
      //panel.classList.remove('is-open');
    } else {
      panel.classList.add('is-open');
    }
  }

  onQtyChange(e, current_ex){
    let selex = this.props.state.form.selectedOptionalExtras;
    selex.map(item=>{
      if(item.ex.id == current_ex.id){
        item.qty = +e.target.value;
      }
    });
    this.props.setFormState({
      selectedOptionalExtras: selex,
    });
  }

  render() {
    if (this.props.state[this.props.state.form.country].rcmErrors.length != 0){
      // this.props.state[this.props.state.form.country].rcmErrors.map((item, index) => {
      //   console.log(item.error);
      // })
      return null;
    }
    return (
      <div className="l-booking">
        <VehiclePanel api={api} state={this.props.state} showtotal={true} showUpgrade={true}  onUpgrade={this.onUpgrade.bind(this)} />
        <div className="l-booking__main">
          <div className="l-booking__container">
            <div className="l-booking__header">
              <h1 className="l-booking__heading">{window.labels.BookingLabel}</h1>
            </div>
            <ProgressBar progress={25}/>
            <Extras state={this.props.state} onSelectInsurance={this.onSelectInsurance.bind(this)} onSelectOptionalExtras={this.onSelectOptionalExtras.bind(this)} onContinue={this.onContinue.bind(this)} toggleMoreSpecs={this.toggleMoreSpecs.bind(this)} onQtyChange={this.onQtyChange.bind(this)}/>
            { this.props.state.form.isprocessing
                ? <button className="l-booking__next btn" onClick={this.onContinue.bind(this)} disabled>
                    {window.labels.ProcessingLabel}...
                  </button>
                : <button className="l-booking__next btn btn--primary" onClick={this.onContinue.bind(this)}>
                    {window.labels.NextStepLabel}
                  </button>
            }
          </div>
        </div>
        <StickyBar api={api} showtotal={true} state={this.props.state} togglePanel={this.togglePanel.bind(this)} updateTotal={this.updateTotal.bind(this)}/>
      </div>
    );
  }

  updateTotal(total){
    this.total = total;
  }
    handleClick(event){
        if(event.target.classList.contains("leadin-button")){
            let acePlus= this.props.state[api.defaultcountry].rcmInsuranceOptions.filter(ins => ins.default  == 'True')[0];
                this.props.setFormState({
                selectedInsuranceId: acePlus.id
                });                
            document.getElementsByClassName('l-booking__options')[0].scrollIntoView();
        }
        document.getElementsByClassName("leadinModal-form")[0].style.display = "none";    
    }

    handleScroll() {
        var isPopUp= document.getElementsByClassName("leadinModal-preview")[0] ? true :false;
        var isAcePlus=false;
        if(isPopUp){  
            document.getElementsByClassName("leadin-preview-wrapper")[0].addEventListener('click',this.handleClick);
            if(this.props.state.form.selectedInsuranceId == '' && api.defaultcountry == 'nz'){
            document.getElementsByClassName("leadinModal-preview")[0].style.display = "none";
            document.getElementsByClassName("leadin-preview-wrapper")[0].removeEventListener('click', this.handleClick);
        }else{
            var isMin=  document.getElementsByClassName("pos2")[0].classList.contains("is-selected") ? true :false;
            if (isMin) {
            document.getElementsByClassName("leadinModal-preview")[0].style.display = "block";    
            } else {
            document.getElementsByClassName("leadinModal-preview")[0].style.display = "none";
            document.getElementsByClassName("leadin-preview-wrapper")[0].removeEventListener('click', this.handleClick);
                }
            }    
        }

        document.getElementsByClassName("leadinModal-form")[0] ?document.getElementsByClassName("leadinModal-form")[0].style.display = "none":"";   
    }
    
  componentDidMount () {
    window.scrollTo(0, 0);
    window.addEventListener('scroll', this.handleScroll,this.props.state);
    document.querySelector('.l-booking__insurance-video').addEventListener('click', (e) => {
      this.openModal('','',1);
    });
  }
}


const mapStateToProps = (state) => {
  return {
    state: {
      nz: state.data.nz,
      au: state.data.au,
      form: state.form,
    }
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    replaceFormState: (state) =>{
      dispatch({
        type: "REPLACE_FORMSTATE",
        payload: state, 
      });
    },
    setFormState: (state) =>{
      dispatch({
        type: "SET_FORMSTATE",
        payload: state, 
      });
    },
    setDataState: (state) =>{
      dispatch({
        type: "SET_DATASTATE",
        payload: state, 
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddExtras);
