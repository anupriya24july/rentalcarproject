import React from 'react';
import moment from 'moment';

import { AceBookAPI as api } from '../../common/api/acebookapi';
import { BrowserRouter as Router } from 'react-router-dom'; 
import { connect } from 'react-redux';

class Search extends React.Component {

  componentWillMount() {
    let hash = location.hash.substring(1).split("/");
    const country = hash[1].split('-')[0];
    this.props.setFormState({
      step:0,
      country: country,
      formdropOffSameAsPickup: this.dropOffSameAsPickup,
      formPickupLocation: hash[1],
      formDropoffLocation: hash[4],
      formPickupDate: (hash[2] == '-'? moment().format('DD/MM/YYYY'):hash[2].replace(/_/g,"/")),
      formDropoffDate: (hash[5] == '-'? moment().add(1, 'days').format('DD/MM/YYYY'):hash[5].replace(/_/g,"/")),
      formPickupTime: (hash[3] == '-'? moment().add(75-(moment().minutes() % 15), 'minutes').format('HH_mm'):hash[3]),
      formDropoffTime: (hash[6] == '-'? moment().add(75-(moment().minutes() % 15), 'minutes').format('HH_mm'):hash[6]),
      formCategoryType: 0,
      formMinimumAge: hash[7],
      formPromoCode: hash[9] == '-' ? '': hash[9] , 
    });

    this.props.setDataState({
      [country]: {
          ...this.props.state[country],
          rcmReservationRef:'',
          rcmReservationNo:'',
          rcmPaymentSaved:false
      }
    });

    window.location.href = window.locale + '/bookings/selectvehicle';
  }

  render(){
    return false;
  }

}

const mapStateToProps = (state) => {
  return {
    state: {
      nz: state.data.nz,
      au: state.data.au,
      form: state.form,
    }
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setFormState: (state) =>{
      dispatch({
        type: "SET_FORMSTATE",
        payload: state, 
      });
    },
    setDataState: (state) =>{
      dispatch({
        type: "SET_DATASTATE",
        payload: state, 
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);
