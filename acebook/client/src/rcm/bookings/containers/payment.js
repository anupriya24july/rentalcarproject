import React from 'react';
import moment from 'moment';
import pikaday from 'pikaday';
import superagent from 'superagent';
import numeral from 'numeral';

import { AceBookAPI as api } from '../../common/api/acebookapi';
import { BrowserRouter as Router } from 'react-router-dom'; 
import { connect } from 'react-redux';

import { PaymentAmount } from '../../common/components/partials/paymentamount';
import { CreditCard } from '../../common/components/partials/creditcard';
import { VehiclePanel } from '../../common/components/partials/vehiclepanel';
import { StickyBar } from '../../common/components/partials/stickybar';
import { ProgressBar } from '../../common/components/partials/progressbar';

class Payment extends React.Component {

  pushAnalytics () {
    let form = this.props.state.form;
    const activeCountry =  this.props.state[api.defaultcountry];
    let impressions = [];
    let products = [];
    let locp = form.formPickupLocation.split('-');
    let ploc = activeCountry.rcmLocationInfo.filter(l => l.id == locp[1] )[0];

    let locd = form.formDropoffLocation.split('-');
    let dloc = activeCountry.rcmLocationInfo.filter(l => l.id == locd[1] )[0];

    const datetimeformat = 'DD/MM/YYYY HH:mm';
    const p_datetime = moment(form.formPickupDate + ' ' + form.formPickupTime.replace('_',':'), datetimeformat);
    const d_datetime = moment(form.formDropoffDate + ' ' + form.formDropoffTime.replace('_',':'), datetimeformat);
    let selectedCar = activeCountry.rcmAvailableCars[0];

    let promocode = activeCountry.rcmAvailableCarDetails.filter(cd => { if (cd.carsizeid == selectedCar.carsizeid) return cd })[0].discountcode;

    let age = activeCountry.rcmDriverAgesInfo.filter(age => age.id == form.formMinimumAge)[0];

    // Selected Vehicle
    products.push({
      'id': selectedCar.sippcodes,
      'name': selectedCar.categoryfriendlydescription,
      'brand': selectedCar.vehicledescription1,
      'category': 'bookings/selectvehicle (' + ploc.location + ')',
      'variant':'Vehicle',
      'price': selectedCar.totalrateafterdiscount + '',
      'quantity': 1 // always set to item qty for vehicles
    });

    // Mandatory Fees
    this.props.state[form.country].rcmMandatoryFees.filter(man => { if(man.locationid == locp[1] && man.vehiclesizeid == selectedCar.carsizeid) return man }).map((man,i) =>
       products.push({
        'id': man.id,
        'name': man.name,
        'brand': man.extradesc1,
        'category': 'bookings/selectvehicle (' + ploc.location + ')',
        'variant':'Madatory Fee',
        'price': man.fees + '',
        'quantity': 1 // always set to item qty for mandatory fee
      })
    );

    // Insurance
    let selectedInsurance = this.props.state[form.country].rcmInsuranceOptions.filter(ins => ins.id == form.selectedInsuranceId)[0];
    products.push({
      'id': selectedInsurance.id,
      'name': selectedInsurance.name,
      'brand': selectedInsurance.extradesc,
      'category': 'bookings/addextras (' + ploc.location + ')',
      'variant':'Insurance',
      'price': selectedInsurance.fees + '',
      'quantity': selectedInsurance.numofdays
    });

    // Optional Extras
    form.selectedOptionalExtras.map((selex,i) => {
      products.push({
        'id': selex.ex.id,
        'name': selex.ex.name,
        'brand': selex.ex.extradesc,
        'category': 'bookings/addextras (' + ploc.location + ')',
        'variant':'Optional Extras',
        'price': selex.ex.fees + '',
        'quantity': selex.qty + (selex.ex.type == 'Daily' ? selex.ex.numofdays: 1)
      });
    });

    // Checkout
    window.dataLayer.push({
      'ecommerce': {
        'checkout': {
          'actionField': {'step': 4},
          'products': products
        }
      },
      'event': 'AceRentals.ec',
      'eventCategory': 'Ecommerce',
      'eventAction': 'Checkout'
    });
  }

  constructor(props) {
    super(props);
    this.state = {
      num:'',
      name:'',
      exp:'',
      cvc:'',
      error: '',
      isprocessing: false,
      number:''
    }
    this.openModal = props.openModal;

    this.state = {
      ...this.props.state.form
    }
   }

  componentWillMount(){
    let form = this.props.state.form;
    let locp = form.formPickupLocation.split('-');
    
    const activeCountry =  this.props.state[locp[0]];

    this.props.setDataState({
      [locp[0]]: {
          ...this.props.state[locp[0]],
          rcmReservationRef: '',
          rcmPaymentSaved: false
      }
    });

    let selectedCar = activeCountry.rcmAvailableCars[0];

    if(selectedCar.available == 2){
      this.createBooking();
    }
  }

  onCCUpdate(event){
    const newState = {...this.state, };
    if(event.target.name=='num'){
      newState[event.target.name] = event.target.rawValue;
    }else{
      newState[event.target.name] = event.target.value;
    }
    this.setState(newState);
  }
  

  togglePanel(e) {
    e.preventDefault();
    window.scrollTo(0, 0);
    const panel = document.querySelector('.js-booking-panel')
    if (panel.classList.contains('is-open')) {
      //panel.classList.remove('is-open');
    } else {
      panel.classList.add('is-open');
    }
  }

  onContinue(){
    this.createBooking();
  }

  onPaymentConfirmDone(){
    // check if payment is saved.
    // if payment saved.
    //this.props.history.push('/bookings/complete'); // instead of just rendering a new view redirect to erase the payment post request.
    window.location.replace( window.locale + '/bookings/complete');
  }

  validate(){ 
    let error_message="";
    if(this.props.state.form.formPickupTime<moment(new Date()).format("HH_mm") && this.props.state.form.formPickupDate==moment(new Date()).format("DD/MM/YYYY")){
      error_message=error_message+"<br>Sorry the Pickup time selected has lapsed. Please select a different Pickup time to book.";
    }else{
    if(this.state.name  == ""){
      error_message=error_message+"<br>Please Enter Card Holder Name.";
    }
    if(this.state.num == ""){
      error_message=error_message+"<br>Please Enter the Card Number.";
    }
    if(this.state.exp == ""){
      error_message=error_message+"<br>Please Enter Expiry Date.";
    }
    if(this.state.cvc == ""){
      error_message=error_message+"<br>Please Enter CVC.";
    }else{
      let reg_cvc = /^[0-9]{3,4}$/ ;
      if(!reg_cvc.test(this.state.cvc)){
        error_message=error_message+"<br>Please Enter a Valid CVC.";
      }
    }
  }
  return error_message;
  }

  confirmPayment(){
    let form = this.props.state.form;
    let locp = form.formPickupLocation.split('-');

    let selectedCar = this.props.state[form.country].rcmAvailableCars[0];

    if(selectedCar.available == 2){
      this.onPaymentConfirmDone();
    }else{
      let paymentamount = numeral((form.selectedPaymentAmount == 'full')? form.total : (form.total * api.deposit)).format('0.00');

      let data = {
          resno: this.props.state[form.country].rcmReservationNo,
          name: this.state.name, 
          num: this.state.num,
          exp: this.state.exp,
          cvc: this.state.cvc,
          amount: '' + paymentamount,
          email: form.formCustomerData.eml
      }
      this.setState({
        'error': '',
        isprocessing: true
      });

      let cc = this.state;
      superagent.post('/payments/purchase').send($.param(data)).end((err, res) => {
        // Calling the end function will send the request
        let jsonres = JSON.parse(res.text);
        if(jsonres.isSuccessful){
          this.setState({
            error: ''
          });

          let paymentdata = [
            paymentamount,
            '1', //success (1=yes, 0=no),
            jsonres.Transaction.CardName, //cctype,
            moment(jsonres.Transaction.AcquirerDate + ' ' + jsonres.Transaction.AcquirerTime,"YYYYMMDD HHmmss").format('DD-MMM-YYYY HH:mm'), // paymentdateacquired (DD-MMM-YYYY HH:mm),
            '2', //paymentsupplierid (2=rcm),
            '', // transactionbillingid,
            '', // paymenttxnref,
            cc.name.toUpperCase(), // cardholdername,
            jsonres.Transaction.CardName.toUpperCase(), //paymentsource,
            this.state.num.replace(/(\d{6})\d{8}(\d{2})/, "$1********$2"), // truncatedcardnumber,
            this.state.exp, // ccexpiry,
            '', // transtype (Empty=purchase, Auth=allow zero amount),
            '0', // merchantfeeid (0=no fee)
          ];

          let urlseg = [
            this.props.state[locp[0]].rcmReservationRef,
            api.base64.encode(paymentdata.join(';') + '|' + api.getGUID()) 
          ];

          api.confirmPayment(api.getEndPoint(form.country),urlseg.join('/'), this.props, () => { this.onPaymentConfirmDone() });
        }else{

          this.setState({
            error: jsonres.responseText,
            isprocessing: false
          });
     
          let heading="Oops! Looks like something went wrong!";
          let msg=jsonres.responseText + ' Please contact support@acerentalcars.' + (location.hostname.split('.').pop() == 'nz'? 'co.nz':'com.au');
          this.openModal(heading,msg,0);
          if($zopim){
            $zopim.livechat.say('My payment has failed for Reservation No ' + this.props.state[form.country].rcmReservationNo + '. Could you please contact me?');
          }
        }
      });
    }
    
  }

  createBooking(){
    // pickup location
    const state = this.props.state;
    const form = state.form; 

    let selectedCar = this.props.state[form.country].rcmAvailableCars[0];

    let msg = this.validate();
    if(msg == '' || selectedCar.available == 2){ // don't validate onrequest
      if(state[form.country].rcmReservationRef == ''){
        let locp = form.formPickupLocation.split('-');
        let ploc = state[locp[0]].rcmLocationInfo.filter(l => l.id == locp[1] )[0];

        // dropoff location
        let locd = form.formDropoffLocation.split('-');
        let dloc = state[locd[0]].rcmLocationInfo.filter(l => l.id == locd[1] )[0];

        // pickup date
        let pdate = form.formPickupDate.replace(new RegExp('/', 'g'), '_');

        // dropoff date
        let ddate = form.formDropoffDate.replace(new RegExp('/', 'g'), '_');

        let urlseg = [
          0, //form.formCategoryType,
          locp[1], // pickuplocation
          pdate, // pickupdate
          form.formPickupTime, // pickuptime
          locd[1], // dropofflocation
          ddate, // pickupdate
          form.formDropoffTime, // pickuptime
          form.formMinimumAge, // age
          form.selectedCarCategoryId, //car category id
          (selectedCar.available == 2 ? 0:2), // BookingType (1=qoute, 2=booking)
          form.selectedInsuranceId, //insurance
          0, // extra_kms_id
          0, // transmission
          0 // sendemail set to off since we send our own custom booking confirmation email
        ];

        let custdata = new Array();
        let custFormData=form.formCustomerData;
        let rmkArray=custFormData.rmk.match(/[a-zA-Z0-9\/\.@#?-\s\n]/g);
        custFormData.rmk= (rmkArray? rmkArray.join(''):'');
        Object.keys(custFormData).map(key => custdata.push(key+':'+custFormData[key]));
        let optionsdata = new Array();
        form.selectedOptionalExtras.map(item => optionsdata.push(item.ex.id+':'+item.qty)) ;
        let data = [
          custdata.join(','),
          optionsdata.join(','),
          0, // referral_id
          form.formPromoCode == '' ? '-': form.formPromoCode,
          9, // website agent code
          'Online Booking 2.0', // website agent name
          form.formEmailOptin? 'True':'False',
          'Online Booking 2.0',
          '', // agentemail
          api.getGUID()
        ];

        this.setState({
          'error': '',
          isprocessing: true
        });
        
        this.pushAnalytics();
        api.createBooking(api.getEndPoint(form.country),urlseg.join('/') + '/?' + api.base64.encode(data.join('|')) , this.props, () => { this.confirmPayment() }, this);
      }else{
        if(!this.state.isprocessing){
          this.setState({
            isprocessing: true
          });
          this.confirmPayment();
        }
      }
    }else{
      let heading="Oops! Looks like something is missing!";
      this.openModal(heading,msg,0);
    }
  }

  onFormChange(event){
    const newFormState = {...this.props.state.form};
    switch(event.target.name){
      default:
        newFormState.formCustomerData[event.target.name] = event.target.value;
        break;
    }

    this.props.setFormState(newFormState);
  }

  onSelectPaymentAmount(id){
    this.props.setFormState({
      selectedPaymentAmount: id
    });
  }

  render() {
    let form = this.props.state.form;
    let locp = form.formPickupLocation.split('-');
    const activeCountry =  this.props.state[locp[0]];
    const selectedCar = activeCountry.rcmAvailableCars[0];
    if(selectedCar.available == 2){
      return false;
    }
    return (
      <div className="l-booking">
        <VehiclePanel api={api} state={this.props.state} showtotal={true}  showUpgrade={false} />
        <div className="l-booking__main">
          <div className="l-booking__container">
            <div className="l-booking__header">
              <h1 className="l-booking__heading">Make a Booking</h1>
            </div>
            <ProgressBar progress={75}/>
            <PaymentAmount state={this.props.state} onSelectPaymentAmount={this.onSelectPaymentAmount.bind(this)}/>
            <CreditCard state={this.state} onCCUpdate={this.onCCUpdate.bind(this)}/>
            {this.state.isprocessing
            ? <button className="l-booking__next btn">
               {window.labels.ProcessingLabel}...
              </button>
            :
              <button className="l-booking__next btn btn--primary" onClick={this.onContinue.bind(this)}>
                {window.labels.ContinueLabel}
              </button>
            }
          </div>
        </div>
        <StickyBar api={api} showtotal={true} state={this.props.state} togglePanel={this.togglePanel.bind(this)}/>
      </div>
    );
  }

  componentDidMount () {
    // this.formatFields();
    window.scrollTo(0, 0);
    this.setState({
      isprocessing: false
    });
  }
}

const mapStateToProps = (state) => {
  return {
    state: {
      nz: state.data.nz,
      au: state.data.au,
      form: state.form,
    }
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setFormState: (state) =>{
      dispatch({
        type: "SET_FORMSTATE",
        payload: state, 
      });
    },
    setDataState: (state) =>{
      dispatch({
        type: "SET_DATASTATE",
        payload: state, 
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Payment);
