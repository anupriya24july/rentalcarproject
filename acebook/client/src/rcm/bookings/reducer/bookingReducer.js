import { REHYDRATE } from 'redux-persist/constants';
import moment from 'moment';

const defaultdata = {
  rcmLocationInfo: [],
  rcmOfficeTimes: [],
  rcmCategoryTypeInfo: [],
  rcmDriverAgesInfo: [],
  rcmLocationFees: [],
  rcmAvailableCarDetails: [],
  rcmAvailableCars: [],
  rcmMandatoryFees: [],
  rcmOptionalFees: [],
  rcmInsuranceOptions: [],
  rcmKmCharges: [],
  rcmHolidays: [],
  rcmLocationFees: [],
  rcmRentalSource: [],
  rcmCountries: [],
  rcmAreaOfUse: [],
  rcmErrors: [],
  rcmTaxInclusive: '',
  rcmTaxRate: '',
  rcmStateTax: '',
  rcmReservationRef:'',
  rcmReservationNo:'',
}

const tld = location.hostname.split('.').pop();

const defaultform = {
  mode: 'booking',
  step: 0,
  country: tld,
  formDifferentDropOff: false,
  formPickupLocation: tld + '-' + (tld=='au'? '21':'16'),
  formDropoffLocation: tld + '-' + (tld=='au'? '21':'16'),
  formPickupDate: moment().add(195-(moment().minutes() % 15), 'minutes').format('DD/MM/YYYY'),
  formDropoffDate: moment().add(195-(moment().minutes() % 15), 'minutes').add(1,'days').format('DD/MM/YYYY'),
  formPickupTime:  moment().add(195-(moment().minutes() % 15), 'minutes').format('HH_mm'),
  formDropoffTime: moment().add(195-(moment().minutes() % 15), 'minutes').format('HH_mm'),
  formCategoryType: '0',
  formMinimumAge:(tld=='au'? '0':'1'),
  formPromoCode: '',
  formEmailOptin:false,
  formAgreedToTerms:'',
  formCustomerData:{
    fnm:'',
    lnm:'',
    eml:'',
    mob:'',
    cnt: (tld=='au'? '7':'2'),
    rmk:'',
  },
  selectedCarCategoryId: '', 
  upgradeCar: {
    categoryId: '',
    description: '',
    rate: '',
    amntDiff:'',
    previousCar: {
                    categoryId: '',
                    description: '',
                    rate: '',
                },
   },
  selectedInsuranceId: '',
  selectedOptionalExtras: [],
  selectedPaymentAmount:'deposit',
  showDropoff: (tld=='au'? true:false),
  afterHourPick: false,
}

export const initialState = {
  data:{
    nz: {
      ...defaultdata,
    },
    au: {
      ...defaultdata,
    },
  },
  form:{
    ...defaultform,
  },
}

export const formReducer = (previousState = initialState, action) => {
  let newstate = { ...previousState }
  switch (action.type) {
    case 'SET_FORMSTATE':
      $.extend(true, newstate, action.payload);
      break;
    case 'REPLACE_FORMSTATE':
      $.extend(false, newstate, action.payload);
      break;
    default:
      newstate = previousState;
      break;
  }
  return newstate;
};

export const dataReducer = (previousState = initialState, action) => {
  let newstate = { ...previousState, nz: { ...previousState.nz }, au: { ...previousState.au } };
  switch (action.type){
    case 'SET_DATASTATE':
      if(action.payload.hasOwnProperty('nz')){
        newstate.nz = action.payload.nz;
      }
      if(action.payload.hasOwnProperty('au')){
        newstate.au = action.payload.au;
      }
      break;
    default:
      newstate = previousState;
      break;
  }
  return newstate;
}

export const rehydrateReducer = (previousState = initialState, action) => {
  switch (action.type) {
    case REHYDRATE:
      return action.payload;
      break;
    default:
      return previousState;
      break;
  }
};
