import React from 'react';
import ReactSVG from 'react-svg';
import { OptionCard }  from './partials/optioncard';

export const Extras = (props) => {
  let form = props.state.form;
  let locp = form.formPickupLocation.split('-');
  
  const activeCountry =  props.state[locp[0]];
  const age = activeCountry.rcmDriverAgesInfo.find(age => age.id == form.formMinimumAge);
  const selectedInsurance = form.selectedInsuranceId;
  const insuranceoptions = activeCountry.rcmInsuranceOptions.filter(ins => { 
    if(age.driverage >= ins.fromage  && (age.driverage <= ins.toage || ins.toage == 0) ) return ins
  }).sort((a, b) => { return b.fees - a.fees });

  const insurancefeatures = window.insurance;

  let hasaceplusdiscount = false;
  //bundled cover
  let bundles = activeCountry.rcmMandatoryFees.filter(man => man.name.match(/bundle/ig))[0];
  if (bundles && bundles.extradesc3.startsWith('bundle')){
    bundles = bundles.extradesc3.split(':')[1].split(',');
  }else{
    bundles = [];
  }

  const optionalextras = activeCountry.rcmOptionalFees.filter(item => { 
    if(item.sgroupname.match(/ace plus discount/ig)){
      hasaceplusdiscount  = true;
    }
    if(item.sgroupname.match(/bundled cover/ig) && bundles.filter(b => b == item.id)[0] ){
      hasaceplusdiscount  = true;
    }
    if(item.name.match(/add.*driver.*/ig)){
      let selins = insuranceoptions.filter((ins,i) => ins.id == selectedInsurance)[0];
      if(selins && selins.excessfee == 0){
        return false;
      }else{
        return item;
      }
    }else{
      return item;
    }
  });
  return (
    <div>
      <div className="l-booking__step">
        <h2 className="l-booking__step-heading">
          1. {window.labels.CoverageInstructons}
        {/*
          <a href="#" className="l-booking__video-link">
            <ReactSVG path="/themes/base/production/svg/video-link.svg" classNameName='svg-video-link'/>
            Insurance Explainer Video
          </a>
        */}
        </h2>
        <div className="l-booking__options">
        {insuranceoptions.map((ins,i) => (
          <OptionCard key={i} order={i} special={ins.excessfee==0 && hasaceplusdiscount} showdesc3={ins.excessfee!=0} Option={ins} Features={insurancefeatures[ins.id]} Type="lg" Selected={ins.id == selectedInsurance} onClick={props.onSelectInsurance} toggleMoreSpecs={props.toggleMoreSpecs}/>
        ))}
          <div style={{fontStyle: 'italic', width: '100%', margin: '0px 20px 40px 20px'}}>
            <p>{window.labels.ExcessBlurb}</p>
            <p>{window.labels.BondBlurb}</p>
          </div>
          <div className="l-booking__insurance-video"><span className="l-booking__insurance-video-svg"><ReactSVG style={{maxWidth: '25px'}} path="/themes/base/production/svg/video-icon.svg"/></span><span><b>Click here for a short video explaining our cover options</b></span></div>
        </div>
      </div>
      <div className="l-booking__step">
        <h2 className="l-booking__step-heading">
          2. {window.labels.OptionsInstructions}
        </h2>

        <div className="l-booking__options">
        {optionalextras.map((ex,i) => (!ex.sgroupname.match(/ace plus discount|bundled/ig)
            ? <OptionCard key={i} showGST={form.country=='au'} Option={ex} Selected={form.selectedOptionalExtras.find(sel_ex => sel_ex.ex.id == ex.id)} onClick={props.onSelectOptionalExtras} onQtyChange={props.onQtyChange}/>
            : ''
        ))}
        </div>
      </div>
    </div>
  );
}

