import React from 'react';
import ReactSVG from 'react-svg';
import moment from 'moment';
import { DateTimeField }  from './partials/datetimefield';

export const SidebarWidget = (props) => {
  let pickupTime=[];
  let dropoffTime=[];
  let pickupDate= moment(props.form.formPickupDate, 'DD/MM/YYYY').day()+1;
  let dropOffDate= moment(props.form.formDropoffDate, 'DD/MM/YYYY').day()+1;
  let ploc = props.form.formPickupLocation.split('-');
  let dloc = props.form.formDropoffLocation.split('-');
  let officeState=props.state[ploc[0]].rcmOfficeTimes;
  let pOfficeTime=officeState.filter(l => l.locid == ploc[1]  && l.wd ==pickupDate )[0];
  let dOfficeTime=officeState.filter(l => l.locid == dloc[1]  && l.wd ==dropOffDate )[0];

  /* Pickup TimeArray */
  if(pOfficeTime){
    let startPickup=moment(pOfficeTime.startpickup, 'HH:mm');
    let endPickup=moment(pOfficeTime.endpickup, 'HH:mm');
    let officeEndPickup=endPickup.format('HH:mm')==startPickup.format('HH:mm')?endPickup.add(1, 'day').format('YYYY-MM-DD HH:mm'):endPickup.format('YYYY-MM-DD HH:mm');
   
    for (let startTime=startPickup;startTime.format('YYYY-MM-DD HH:mm')<=officeEndPickup;startTime.add(15, 'minutes')) {
          pickupTime.push(startTime.format('HH_mm') + ','+ startTime.format('hh:mm A') );
    } 
    if(startPickup.format('HH:mm')!=endPickup.format('HH:mm')){
      pickupTime.push(endPickup.format('HH_mm') + ','+ endPickup.format('hh:mm A'));
    }
  }

    /* DropOff TimeArray */
  if(dOfficeTime){
    let startDropoff=moment(dOfficeTime.startdropoff, 'HH:mm');
    let endDropoff=moment(dOfficeTime.enddropoff, 'HH:mm'); 
    let officeEndDropoff=endDropoff.format('HH:mm')==startDropoff.format('HH:mm')?endDropoff.add(1, 'day').format('YYYY-MM-DD HH:mm'):endDropoff.format('YYYY-MM-DD HH:mm');

     for (let startTime=startDropoff;startTime.format('YYYY-MM-DD HH:mm')<=officeEndDropoff;startTime.add(15, 'minutes')) {
      dropoffTime.push(startTime.format('HH_mm')+ ','+ startTime.format('hh:mm A') );
    }
    if(startDropoff.format('HH:mm')!=endDropoff.format('HH:mm')){
      dropoffTime.push(endDropoff.format('HH_mm')+ ','+ endDropoff.format('hh:mm A') );
    }
   
  }
  const dropoffTimeArr = dropoffTime.filter((val,id,array) => array.indexOf(val) == id);
  const pickupTimeArr =pickupTime.filter((val,id,array) => array.indexOf(val) == id);
  return (
    <div id="sidebar_container" style={{backgroundColor:'#fff'}}>
      <div className="l-booking-panel__overlay js-booking-panel-toggle"></div>
        <div className="l-booking-panel__header">
          <h2 className="l-booking-panel__heading">{window.labels.BookingLabel}</h2>
          <button className="l-booking-panel__close js-booking-panel-toggle" onClick={props.onTogglePanel}>
            <ReactSVG path="/themes/base/production/svg/close.svg" className='svg-close'/>
          </button>
        </div>

        <div className="l-booking-panel__main">
          {props.preset
          ?
            <div>
              <div className="l-booking-panel__summary">
                <div className="l-booking-panel__summary-label">Booking:</div>
                Wellington to Auckland Relocation (Suzuki Swift)
              </div>
              <div className="l-booking-panel__split"></div>
            </div>
          :''
          }

          <div className="l-booking-panel__form">
            <div className="l-form__field l-form__field--select">
              <label className="l-form__label">
                {window.labels.PickupLocationLabel}
              </label>
              <div className="l-form__select notranslate">
                <select className="l-form__input l-form__input--select" name="formPickupLocation" value={props.form.formPickupLocation} onChange={e => props.onFormChange(e)} disabled={props.form.isAmendment}>
                  {props.state[props.api.defaultcountry].rcmLocationInfo.length != 0
                    ? <optgroup label={props.api.defaultcountry=='nz'? 'New Zealand':'Australia'}>
                        {props.state[props.api.defaultcountry].rcmLocationInfo.map(loc => <option value={props.api.defaultcountry + '-' + loc.id} key={props.api.defaultcountry + '-' + loc.id}>{loc.location}</option>)}
                      </optgroup>
                    : ''
                  }
                  {props.state[props.api.defaultcountry=='nz'? 'au':'nz'].rcmLocationInfo.length != 0
                    ? <optgroup label={props.api.defaultcountry=='nz'? 'Australia':'New Zealand'}>
                        {props.state[props.api.defaultcountry=='nz'? 'au':'nz'].rcmLocationInfo.map(loc => <option value={(props.api.defaultcountry=='nz'? 'au':'nz') + '-' + loc.id} key={(props.api.defaultcountry=='nz'? 'au':'nz') + '-' + loc.id}>{loc.location}</option>)}
                      </optgroup>
                    : ''
                  }
                </select>
                <ReactSVG path="/themes/base/production/svg/defaults/select-arrow.svg" className='svg-defaults/select-arrow'/>
              </div>
            </div>

            <div className="l-form__field l-form__field--select">
              <label className="l-form__label">
                {window.labels.DropoffLocationLabel}
              </label>

              <div className="l-form__select  notranslate">
                <select className="l-form__input l-form__input--select"  name="formDropoffLocation" value={props.form.formDropoffLocation} onChange={e => props.onFormChange(e)} disabled={props.form.isAmendment}>
                  {props.state[props.form.country].rcmLocationInfo.length != 0
                    ? <optgroup label={props.form.country=='nz'? 'New Zealand':'Australia'}>
                        {props.state[props.form.country].rcmLocationInfo.map(loc => <option value={props.form.country + '-' + loc.id} key={props.form.country + '-' + loc.id}>{loc.location}</option>)}
                      </optgroup>
                    : ''
                  }
                </select>
                <ReactSVG path="/themes/base/production/svg/defaults/select-arrow.svg" className='svg-defaults/select-arrow'/>
              </div>
            </div>

            <div className="l-booking-panel__split"></div>

            <DateTimeField timeArr={pickupTimeArr?pickupTimeArr:""} labeldate={window.labels.PickupDateLabel} prefix="sidebar" name="Pickup" onFormChange={props.onFormChange} value={props.form.formPickupTime}/>
            <DateTimeField timeArr={dropoffTimeArr?dropoffTimeArr:""}  labeldate={window.labels.DropoffDateLabel} prefix="sidebar" name="Dropoff" onFormChange={props.onFormChange} value={props.form.formDropoffTime}/>

            <div className="l-booking-panel__split"></div>

            <div className="l-form__field l-form__field--select">
              <label className="l-form__label">
                {window.labels.DriverAgeLabel}
              </label>

              <div className="l-form__select">
                <select id="cmbAge" name="formMinimumAge" className="l-form__input l-form__input--select" value={props.form.formMinimumAge} onChange={e => props.onFormChange(e)}>
                  {props.form.country=='au'? <option value="0">Select Age</option> : ''}
                  {props.state[props.form.country].rcmDriverAgesInfo.map(age => <option value={age.id} key={age.id}>{age.driverage}</option>)}
                </select>
                <ReactSVG path="/themes/base/production/svg/defaults/select-arrow.svg" className='svg-defaults/select-arrow'/>
              </div>
            </div>

            <div className="l-form__field ">
              <label className="l-form__label">
                {window.labels.PromocodeLabel}
              </label>
              <input className="l-form__input" type="text" name="formPromoCode" value={props.form.formPromoCode} id={props.prefix + '_Promo'} onChange={e => props.onFormChange(e)} disabled={props.form.isAmendment} />
            </div>
              {props.form.isprocessing
                ? <button type="submit" className="l-booking-panel__submit btn" onClick={e => props.onSubmitSearch(e)} disabled>
                    {window.labels.ProcessingLabel}...
                  </button>
                :
                  <button type="submit" className="l-booking-panel__submit btn btn--secondary" onClick={e => props.onSubmitSearch(e)} className="l-booking-form__submit btn btn--secondary" >
                    {window.labels.ItinerarySearchLabel}
                  </button>
              }
          </div>
        </div>
    </div>
  );
}
