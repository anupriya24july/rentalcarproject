import React from 'react';
import ReactSVG from 'react-svg';
import numeral from 'numeral';

export const VehicleList = (props) => {
  let form = props.state.form;
  let locp = form.formPickupLocation.split('-');
  let vehicle = cars? cars:[];
  let totalmandatoryfee = [];
  let selectedCar=form.selectedCarCategoryId?form.selectedCarCategoryId : "";
  props.state[props.state.form.country].rcmAvailableCars.map((item, index) =>{
    props.state[props.state.form.country].rcmMandatoryFees.filter(man => { 
      if(man.locationid == locp[1] && man.vehiclesizeid == item.carsizeid) 
        return man 
    }).map((man,i)=>{
        totalmandatoryfee[item.carsizeid] = +man.fees + (totalmandatoryfee[item.carsizeid]? totalmandatoryfee[item.carsizeid]:0);
      })
  });
  props.state.form.country=="nz" ? props.state[props.state.form.country].rcmAvailableCars.sort(function(obj1, obj2) {  return  obj2.available-obj1.available || obj1.totrate-obj2.totrate ; }) : props.state[props.state.form.country].rcmAvailableCars.sort(function(obj1, obj2) {  return  obj1.categoryfriendlydescription == "SUV" ? -1 : obj2.categoryfriendlydescription == "SUV" ? 1 : 0 || obj2.available-obj1.available || obj1.totrate-obj2.totrate ; });
  return (
    <div className="l-cars__cards">
{props.state[props.state.form.country].rcmAvailableCars.map((item, index) => (
      <div key={index} className="c-vehicle-card c-vehicle-card__js-vehicle-card" onMouseLeave={(e) => props.hideMoreSpecs(e)}>
        <div className="c-vehicle-card__title notranslate">{vehicle[item.carsizeid]? vehicle[item.carsizeid].title: item.categoryfriendlydescription}</div>
        <div className="c-vehicle-card__subtitle">{vehicle[item.carsizeid]? vehicle[item.carsizeid].age: item.vehiclecategory}</div>
        <div className="c-vehicle-card__image">
          <img src={vehicle[item.carsizeid]? vehicle[item.carsizeid].image:''} alt="" />
        </div>
          <ul className="c-vehicle-card__specifications details hidden">
            {(vehicle[item.carsizeid] && vehicle[item.carsizeid].specs)
              ? vehicle[item.carsizeid].specs.filter(specs => { if(specs.fullwidth=='0') return specs}).map((specs,i) =>
                  <li key={i} className={'c-vehicle-card__specification js-vehicle-spec' + ((specs.fullwidth=='1')? ' c-vehicle-card__specification--fw':'') + ' js-vehicle-spec-more toggle-active'}>
                    {specs.icon
                      ? <img className="c-vehicle-card__specification-icon" src={specs.icon} alt={specs.title} />
                      : ''
                    }
                    {specs.title}
                  </li>
                )
              : ''
            }
            <li className="c-vehicle-card__specification js-vehicle-spec c-vehicle-card__specification--fw js-vehicle-spec-more toggle-active" style={{textAlign:'center'}}><i>* {window.labels.TotalPriceLabel}:</i></li>
            {form.country=='au'
              ? <li className='c-vehicle-card__specification js-vehicle-spec c-vehicle-card__specification--fw js-vehicle-spec-more toggle-active'>
                  {numeral(item.totalrateafterdiscount * (1 - props.gst)).format('$0,0.00')} ({numeral(item.discounteddailyrate * (1 -  props.gst)).format('$0,0.00')} / {window.labels.DayLabel}) vehicle cost
                </li>
              : <li className='c-vehicle-card__specification js-vehicle-spec c-vehicle-card__specification--fw js-vehicle-spec-more toggle-active'>
                  {numeral(item.discounteddailyrate).format('$0,0.00')} x {item.numofdays} {window.labels.DayLabel}(s)
                </li>
            }
            {form.country=='au'
              ? <li className='c-vehicle-card__specification js-vehicle-spec c-vehicle-card__specification--fw js-vehicle-spec-more toggle-active'>
                  {numeral(item.totalrateafterdiscount * props.gst).format('$0,0.00')} GST
                </li>
              : ''
            }
            {item.discountrate!=0
              ? <li className='c-vehicle-card__specification js-vehicle-spec c-vehicle-card__specification--fw js-vehicle-spec-more toggle-active' style={{'color':'green', 'fontWeight':'bold'}}>
                  {numeral(item.totaldiscount).format('$0,0.00')} discount ({props.state[props.state.form.country].rcmAvailableCarDetails.filter(cd => { if (cd.carsizeid == item.carsizeid) return cd })[0].discountname})
                </li>
              : ''
            }
            {props.state[props.state.form.country].rcmMandatoryFees.filter(man => { if(man.locationid == locp[1] && man.vehiclesizeid == item.carsizeid) return man }).map((man,i) => man.fees > 0
              ? <li key={i} className='c-vehicle-card__specification js-vehicle-spec c-vehicle-card__specification--fw js-vehicle-spec-more toggle-active'>
                  {numeral(man.fees).format('$0,0.00')} {man.name}
                </li>
              : <li key={i} className='c-vehicle-card__specification js-vehicle-spec c-vehicle-card__specification--fw js-vehicle-spec-more toggle-active' style={{'color':'green', 'fontWeight':'bold'}}>
                  {man.extradesc}
                </li>
            )}
          </ul>

          <button className="c-vehicle-card__specifications-more" onClick={(e) => props.toggleMoreSpecs(e)}>
            <span>Click here to view vehicle & rate details</span>
          </button>

      { props.cta =='Book' && item.totalrateafterdiscount
      ?           
        <div className="c-vehicle-card__footer c-vehicle-card__footer--book">
          {item.available == 0 && !(item.availablemsg.indexOf('Unavailable') >= 0)
            ? <div className="c-vehicle-card__pricing" style={{fontSize: 'smaller', textAlign: 'center', color: '#0056a7'}}>
                {item.availablemsg}
              </div>
            : <div className="c-vehicle-card__pricing">
                <div className="c-vehicle-card__price-label">
                  {window.labels.TotalPriceLabel}
                  {form.country=='au'
                    ? <span style={{textTransform: 'none', fontSize: 'smaller'}}> (incl. GST)</span>
                    : ''
                  }
                </div>
                <div className="c-vehicle-card__price">
                  {numeral(+item.totalrateafterdiscount + (totalmandatoryfee[item.carsizeid]? totalmandatoryfee[item.carsizeid]:0)).format('$0,0.00')} <span>{form.country=='au'? 'AUD':'NZD'}</span>
                </div>
                <div style={{fontSize: '12px'}}>@ {numeral(item.discounteddailyrate).format('$0,0.00')} per day</div>
              </div>
          }
          {item.available == 2
            ? (props.state.form.isprocessing
              ? <button className="c-vehicle-card__action btn " onClick={ e => props.onSelectVehicle(item.carsizeid, index+1, e)} disabled>
                  {props.state.form.selectedCarCategoryId==item.carsizeid? window.labels.ProcessingLabel + "..." : window.labels.SelectLabel}
                </button> 
              : <button className="c-vehicle-card__action btn btn--secondary" onClick={ e => props.onSelectVehicle(item.carsizeid, index+1, e)}>
                  REQUEST
                </button>)   
            : ''
          }
          {item.available == 1
            ? (props.state.form.isprocessing
              ? <button className="c-vehicle-card__action btn " onClick={ e => props.onSelectVehicle(item.carsizeid, index+1, e)} disabled>
                  {props.state.form.selectedCarCategoryId==item.carsizeid? window.labels.ProcessingLabel + "..." : window.labels.SelectLabel}
                </button> 
              : <button className="c-vehicle-card__action btn btn--primary" onClick={ e => props.onSelectVehicle(item.carsizeid, index+1, e)}>
                  {window.labels.SelectLabel}
                </button>)   
            : ''
          }
          {item.available == 0 && (item.availablemsg.indexOf('Unavailable') >= 0)
            ? <a href="#" className="btn" style={{backgroundColor: '#ccc', borderColor: '#ccc'}}>
                SOLD OUT
              </a>
            : ''
          }
        </div>
      : <div className="c-vehicle-card__footer">
          <a href="{$Link}" className={'c-vehicle-card__more btn' + (props.type == 'lg')? ' btn--lg':' btn--primary'}>
            Learn More
          </a>
        </div>
      }
      </div>
    ))}
    </div>
  );
}