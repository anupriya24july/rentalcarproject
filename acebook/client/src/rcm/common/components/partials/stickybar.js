import React from 'react';
import ReactSVG from 'react-svg';
import moment from 'moment';
import numeral from 'numeral';

export const StickyBar = (props) => {
  let form = props.state.form;
  let locp = form.formPickupLocation.split('-');
  let locd = form.formDropoffLocation.split('-');
  
  const activeCountry =  props.state[props.api.defaultcountry];

  const p_location = activeCountry.rcmLocationInfo.filter(loc => loc.id == locp[1])[0];
  const d_location = activeCountry.rcmLocationInfo.filter(loc => loc.id == locd[1])[0];
  const selectedCar = activeCountry.rcmAvailableCars.filter(car => car.carsizeid == form.selectedCarCategoryId)[0];


  let locationInfo= locInfo? locInfo: '';
  let pickupInfo=locationInfo[locp[0]][locp[1]];
  let dropoffInfo=locationInfo[locd[0]][locd[1]];

  const datetimeformat = 'DD/MM/YYYY HH:mm';
  const p_datetime = moment(form.formPickupDate + ' ' + form.formPickupTime.replace('_',':'), datetimeformat);
  const d_datetime = moment(form.formDropoffDate + ' ' + form.formDropoffTime.replace('_',':'), datetimeformat);

  let days = selectedCar ? selectedCar.numofdays: 0;

  let chkid = "";
  let qtyid = "";
  let calcTotal = 0;
  let calcGst = calcTotal;
  let calcStampDuty = calcTotal;
  let calcTotOptExt = 0.0;
  let ItemVal = 0.0;
  let ItemObj = null;
  let qtyItems = 1;
  let CountryTax = 1.0 + activeCountry.rcmTaxRate;
  let StateTax = 1.0 + activeCountry.rcmStateTax;
  let UseStateTax = activeCountry.rcmStateTax > 0;
  let UseTax = activeCountry.rcmTaxRate > 0;

  let totalcost = 0;
  let selectedInsurance = null;

  if(props.showtotal){
    if(form.selectedInsuranceId != ''){
      if(form.selectedInsuranceId == ''){
        //error: there must be a problem in the initialization
      }else{
        selectedInsurance = props.state[form.country].rcmInsuranceOptions.filter(ins => ins.id == form.selectedInsuranceId)[0];
      }

      if(props.state[form.country].rcmInsuranceOptions.length == 0 || form.selectedInsuranceId == ''){
        return false;
      }

      if(activeCountry.rcmTaxInclusive == false){
        calcGst = parseFloat(calcGst) * CountryTax;
        calcStampDuty = parseFloat(calcStampDuty) * StateTax;
      }

      // Insurance
      let calcInsurance = 0.0;
      activeCountry.rcmInsuranceOptions.map((ins,i) => {
        if (ins.type == "Daily") {
          ItemVal = (parseFloat(days) * parseFloat(ins.fees));
        } else if (ins.type == "Percentage") {
         if (ins.percentagetotalcost == "True" || ins.merchantfee == "True") {
             ItemVal = (parseFloat(calcTotal) * parseFloat(ins.fees) / 100);
           } else {
             ItemVal = (parseFloat(ratetotal) * parseFloat(ins.fees) / 100);
           }
        } else {
          ItemVal = parseFloat(ins.fees);
        }

        if (parseFloat(ins.maxprice) > 0 && ItemVal > parseFloat(ins.maxprice)) ItemVal = parseFloat(ins.maxprice);
        if (form.selectedInsuranceId == ins.id) {
          if (activeCountry.rcmTaxInclusive == false) {
             calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
             if (ins.gst == "True") calcGst = parseFloat(calcGst) + (parseFloat(ItemVal) * CountryTax);
             if (ins.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + (parseFloat(ItemVal) * StateTax);
          } else {
             calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
             if (ins.gst == "True") calcGst = parseFloat(calcGst) + parseFloat(ItemVal);
             if (ins.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + parseFloat(ItemVal);
          }
          calcInsurance = parseFloat(calcTotOptExt) + parseFloat(ItemVal);
        }
      });
    }

    // Mandatory Fees
    let calcMandatoryFees = 0.0;
    activeCountry.rcmMandatoryFees.map((mandatoryfees,i) => {
      let ItemVal = 0.0;
      let OldVal = 0.0;
      if ((mandatoryfees.locationid == p_location.id || mandatoryfees.locationid == "0") && (mandatoryfees.vehiclesizeid == selectedCar.carsizeid || mandatoryfees.vehiclesizeid == "0")) {
        if (mandatoryfees.type == "Daily") {
          ItemVal = ( parseFloat(days) * parseFloat(mandatoryfees.fees));
        } else if (mandatoryfees.type == "Percentage") {
          if (mandatoryfees.percentagetotalcost == "True" || mandatoryfees.merchantfee == "True") {
            ItemVal = (parseFloat(calcTotal) * parseFloat(mandatoryfees.fees) / 100);
          } else {
            ItemVal = (parseFloat(ratetotal) * parseFloat(mandatoryfees.fees) / 100);
          }
        } else {
          ItemVal = parseFloat(mandatoryfees.fees);
        }
        if (parseFloat(mandatoryfees.maxprice) > 0 && ItemVal > parseFloat(mandatoryfees.maxprice)) ItemVal = parseFloat(mandatoryfees.maxprice);
        if (activeCountry.rcmTaxInclusive == false) {
          calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
          if (mandatoryfees.gst == "True") calcGst = parseFloat(calcGst) + (parseFloat(ItemVal) * CountryTax);
          if (mandatoryfees.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + (parseFloat(ItemVal) * StateTax);
        } else {
          calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
          if (mandatoryfees.gst == "True") calcGst = parseFloat(calcGst) + ItemVal;
          if (mandatoryfees.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + ItemVal;
        }
      }
    });

    // Optional Extras
    let calcOptionalExtras = 0.0;
    form.selectedOptionalExtras.map((selex,i) => {
      ItemVal = 0.0;
      qtyItems = selex.qty;
      if (selex.ex.type == "Daily") {
        ItemVal = (parseFloat(days) * parseFloat(selex.ex.fees));
      } else if (selex.ex.type == "Percentage") {
        if (selex.ex.percentagetotalcost == "True" || selex.ex.merchantfee == "True") {
          ItemVal = (parseFloat(calcTotal) * parseFloat(selex.ex.fees) / 100);
        } else {
          ItemVal = (parseFloat(ratetotal) * parseFloat(selex.ex.fees) / 100);
        }
      } else {
        ItemVal = parseFloat(selex.ex.fees);
      }
      if (parseFloat(selex.ex.maxprice) > 0 && ItemVal > parseFloat(selex.ex.maxprice)) ItemVal = parseFloat(selex.ex.maxprice);
      ItemVal = ItemVal * qtyItems;

      if (activeCountry.rcmTaxInclusive == false) {
        calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
        if (selex.ex.gst == "True") calcGst = parseFloat(calcGst) + (parseFloat(ItemVal) * (CountryTax));
        if (selex.ex.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + (parseFloat(ItemVal) * StateTax);
      } else {
        calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
        if (selex.ex.gst == "True") calcGst = parseFloat(calcGst) + parseFloat(ItemVal);
        if (selex.ex.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + parseFloat(ItemVal);
      }
      calcOptionalExtras = parseFloat(calcOptionalExtras) + parseFloat(ItemVal);
    });

    // todo: implement Km charges.
    // Km Charges
    let calcKmChargesFees = 0.0;
    activeCountry.rcmKmCharges.map((kmcharges,i) => {
      let ItemVal = 0.0;
      // - KmCharges, kms Daily rate are based on NoofRate days, if 1.4 days charged, total kms daily rate will be 1.4x$10
      if (activeCountry.rcmKmCharges.length != 0) {
        if (kmcharges.dailyrate > 0) {
           ItemVal = (days * parseFloat(kmcharges.dailyrate)).toFixed(2);
        }
      }
      
      // if (parseFloat(rcmKmCharges[j]["maxprice"]) > 0 && ItemVal > parseFloat(rcmKmCharges[j]["maxprice"])) ItemVal = parseFloat(rcmKmCharges[j]["maxprice"]);
      if (activeCountry.rcmTaxInclusive == false) {
          calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
          calcGst = parseFloat(calcGst) + (parseFloat(ItemVal) * CountryTax);
          calcStampDuty = parseFloat(calcStampDuty) + (parseFloat(ItemVal) * StateTax);
      } else {
          calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
          calcGst = parseFloat(calcGst) + parseFloat(ItemVal);
          calcStampDuty = parseFloat(calcStampDuty) + parseFloat(ItemVal);
      }
      calcKmChargesFees = parseFloat(calcTotOptExt) + parseFloat(ItemVal);
    });

    totalcost = +selectedCar.total + +calcTotal;

    if(props.updateTotal){
      props.updateTotal(numeral(totalcost).format('0.00'));
    }

  /*
    if (document.getElementById("displTot")) {
      if (rcmTaxInclusive == false) {
        document.getElementById("displTot").innerHTML = (parseFloat(calcTotal) + (parseFloat(calcGst) - parseFloat(calcGst) / CountryTax) + (parseFloat(calcStampDuty) - parseFloat(calcStampDuty) / StateTax)).toFixed(2);
        if (UseTax == true) document.getElementById("displGST").innerHTML = (parseFloat(calcGst) - parseFloat(calcGst) / CountryTax).toFixed(2);
        if (UseStateTax == true) document.getElementById("displState").innerHTML = (parseFloat(calcStampDuty) - parseFloat(calcStampDuty) / StateTax).toFixed(2);
      } else {
        document.getElementById("displTot").innerHTML = parseFloat(calcTotal).toFixed(2);
        if (UseTax == true) document.getElementById("displGST").innerHTML = (parseFloat(calcGst) - parseFloat(calcGst) / (CountryTax)).toFixed(2);
        if (UseStateTax == true) document.getElementById("displState").innerHTML = (parseFloat(calcStampDuty) - parseFloat(calcStampDuty) / StateTax).toFixed(2);
      }
      document.getElementById("TotOptionalExtras").innerHTML = parseFloat(calcTotOptExt).toFixed(2);
    }
  */
  }

  return (
    <div className="l-booking-summary-bar">
      <div className="l-booking-summary-bar__blocks">
        <div className="l-booking-summary-bar__block ">
          <div className="l-booking-summary-bar__block-label">{window.labels.PickupLabel}</div>
          <div className="l-booking-summary-bar__block-location">{p_location? p_location.location:''}
          {pickupInfo?
            <span className='js-tooltip'  title={'<img src="'+ pickupInfo.image +'"><br>' + pickupInfo.address + '<br>' + pickupInfo.infoDesc} ><ReactSVG path="/themes/base/production/svg/info.svg"/></span>
            :""
          }
          </div>
          <div className="l-booking-summary-bar__block-date">{p_datetime.format('DD, MMM, YYYY, hh:mma')}</div>
        </div>

        <div className="l-booking-summary-bar__block">
          <div className="l-booking-summary-bar__block-label">{window.labels.DropoffLabel}</div>
          <div className="l-booking-summary-bar__block-location">{d_location? d_location.location:''}
          {dropoffInfo?
            <span className='js-tooltip'  title={'<img src="'+ dropoffInfo.image +'"><br>' + dropoffInfo.address + '<br>' + dropoffInfo.infoDesc} ><ReactSVG path="/themes/base/production/svg/info.svg"/></span>
          :""
          }
        </div>
          <div className="l-booking-summary-bar__block-date">{d_datetime.format('DD, MMM, YYYY, hh:mma')}</div>
        </div>
      </div>

      <a href="#" className="l-booking-summary-bar__modify" onClick={props.togglePanel}>
      { props.state.form.isAmendment? "" :  window.labels.ModifyTripDetailsLabel}
      </a>

      { props.showtotal
       ?  (<div className="l-booking-summary-bar__total">
            <div className="l-booking-summary-bar__total-label">{window.labels.TotalPriceLabel} 
              {form.country=='au'
                ? <span style={{textTransform: 'none', fontSize: 'smaller'}}> (incl. GST)</span>
                : ''
              }
            </div>
            <div className="l-booking-summary-bar__total-price">{numeral(totalcost).format('$0,0.00')} <span>{form.country=='au'? 'AUD':'NZD'}</span></div>

            <div className="l-booking-summary-bar__total-details">
              <div className="l-booking-summary-bar__total-details-trigger">
                <ReactSVG path="/themes/base/production/svg/large-arrow.svg" className='svg-large-arrow'/>
              </div>

              <div className="l-booking-summary-bar__total-details-panel">
                
                <div className="l-booking-summary-bar__total-item"> 
                  <div className="l-booking-summary-bar__total-item-label">{window.labels.VehicleCostLabel}</div>
                  <div>
                    <div className="l-booking-summary-bar__total-item-name">{selectedCar.categoryfriendlydescription}</div>
                    <div className="l-booking-summary-bar__total-item-totals">
                      <div className="l-booking-summary-bar__total-item-subtotal ">{numeral(selectedCar.discounteddailyrate).format('$0,0.00')} x {days} {window.labels.DayLabel}(s)</div>
                      <div className="l-booking-summary-bar__total-item-total ">{numeral(selectedCar.totalrateafterdiscount).format('$0,0.00')}</div>
                    </div>
                    {selectedCar.totaldiscount!=0
                      ? <div className="l-booking-summary-bar__total-item-totals">
                          <div className="l-booking-summary-bar__total-item-subtotal">{numeral(selectedCar.totaldiscount).format('$0,0.00')} discount included.</div>
                          <div className="l-booking-summary-bar__total-item-total">({activeCountry.rcmAvailableCarDetails.filter(cd => { if (cd.carsizeid == selectedCar.carsizeid) return cd })[0].discountname})</div>
                        </div>
                      : ''
                    }
                  </div>
                </div>
                
                <div className="l-booking-summary-bar__total-item" style={{display:activeCountry.rcmMandatoryFees.length ? 'block':'none'}}>  
                  <div className="l-booking-summary-bar__total-item-label">{window.labels.MandatoryFeesLabel}</div>
                  {activeCountry.rcmMandatoryFees.map((man,i) => (
                    man.vehiclesizeid == selectedCar.carsizeid
                      ? <div key={i}>
                          <div className="l-booking-summary-bar__total-item-name">{man.name}</div>
                          <div className="l-booking-summary-bar__total-item-totals">
                            <div className="l-booking-summary-bar__total-item-subtotal">{numeral(man.fees).format('$0,0.00')} </div>
                            <div className="l-booking-summary-bar__total-item-total">{numeral(man.fees).format('$0,0.00')}</div>
                          </div>
                        </div>
                      : ''
                  ))}
                </div>
                {selectedInsurance
                ? <div className="l-booking-summary-bar__total-item"> 
                    <div className="l-booking-summary-bar__total-item-label">{window.labels.CoverageLabel}</div>
                    <div>
                      <div className="l-booking-summary-bar__total-item-name">{selectedInsurance.name}</div>
                      { selectedInsurance.maxprice > 0 && (selectedInsurance.fees * days) > selectedInsurance.maxprice
                        ? <div className="l-booking-summary-bar__total-item-totals">
                            <div className="l-booking-summary-bar__total-item-subtotal">{numeral(selectedInsurance.maxprice).format('$0,0.00')}</div>
                            <div className="l-booking-summary-bar__total-item-total">{numeral(selectedInsurance.maxprice).format('$0,0.00')}</div>
                          </div>
                        : <div className="l-booking-summary-bar__total-item-totals">
                            <div className="l-booking-summary-bar__total-item-subtotal">{numeral(selectedInsurance.fees).format('$0,0.00')} x {days} {window.labels.DayLabel}(s)</div>
                            <div className="l-booking-summary-bar__total-item-total">{numeral(selectedInsurance.fees * days).format('$0,0.00')}</div>
                          </div>
                      }
                    </div>
                  </div>
                :''
                }
                <div className="l-booking-summary-bar__total-item" style={{display:form.selectedOptionalExtras.length ? 'block':'none'}}> 
                  <div className="l-booking-summary-bar__total-item-label">{window.labels.AncillaryOptionsLabel}</div>
                  {form.selectedOptionalExtras.map((selex,i) => (
                  <div key={i}>
                    <div className="l-booking-summary-bar__total-item-name">{selex.ex.name}</div>
                    <div className="l-booking-summary-bar__total-item-totals">
                      <div className="l-booking-summary-bar__total-item-subtotal">{numeral(selex.ex.fees).format('$0,0.00') + ' x ' + selex.qty} {selex.ex.type=='Daily'? ' x ' + days + window.labels.DayLabel +' (s)' : ''}</div>
                      <div className="l-booking-summary-bar__total-item-total">{numeral(selex.ex.fees * (selex.ex.type=='Daily'? days : 1) * selex.qty).format('$0,0.00')}</div>
                    </div>
                  </div>
                  ))}
                </div>
                {form.country=='au'
                  ? <div className="l-booking-summary-bar__total-item"> 
                      <div className="l-booking-summary-bar__total-item-totals">
                        <div className="l-booking-summary-bar__total-item-subtotal" style={{float:'left'}}>* GST on {window.labels.TotalPriceLabel}</div>
                        <div className="l-booking-summary-bar__total-item-total" style={{float:'right'}}>({numeral(totalcost * props.api.gst).format('$0,0.00')})</div>
                      </div>
                    </div>
                  : ''
                }
              </div>
            </div>
          </div>)
        : ''
      }
    </div>
  );
}