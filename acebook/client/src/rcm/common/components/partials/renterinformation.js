import React from 'react';
import ReactSVG from 'react-svg';

export const RenterInformation = (props) => {
  const activeCountry =  props.state[props.state.form.country];
  return (
    <div className="l-booking__step">
        <form>
        <h2 className="l-booking__step-heading">
          1. {window.labels.RenterLabel}
        </h2>

        <div className="l-booking__form l-form">
          <div className="l-form__field">
            <label className="l-form__label" htmlFor="FirstName">{window.labels.FirstNameLabel} *</label>
            <input className="l-form__input autocompletefix" type="text" id="FirstName" name="fnm" required value={props.state.form.formCustomerData.fnm} onChange={e => props.onFormChange(e)} disabled={props.state.form.isAmendment}/>
          </div>
          <div className="l-form__field">
            <label className="l-form__label" htmlFor="LastName">{window.labels.LastNameLabel} *</label>
            <input className="l-form__input autocompletefix" type="text" id="LastName" name="lnm" required value={props.state.form.formCustomerData.lnm} onChange={e => props.onFormChange(e)} disabled={props.state.form.isAmendment}/>
          </div>
          <div className="l-form__field">
            <label className="l-form__label" htmlFor="ContactNumber">{window.labels.ContactNumberLabel} *</label>
            <input className="l-form__input autocompletefix" type="text" id="ContactNumber" name="mob" required value={props.state.form.formCustomerData.mob} onChange={e => props.onFormChange(e)}/>
          </div>
          <div className="l-form__field">
            <label className="l-form__label" htmlFor="Email">{window.labels.EmailLabel} *</label>
            <input className="l-form__input autocompletefix" type="text" id="Email" name="eml" required value={props.state.form.formCustomerData.eml} onChange={e => props.onFormChange(e)}/>
          </div>
          <div className="l-form__field l-form__field--select">
            <label htmlFor="Booking_Country" className="l-form__label">
              {window.labels.CountryLabel}
            </label>
            <div className="l-form__select">
              <select className="l-form__input l-form__input--select  notranslate" name="cnt" id="Booking_Country" defaultValue={props.state.form.formCustomerData.cnt} onChange={e => props.onFormChange(e)} disabled={props.state.form.isAmendment}>
                {activeCountry.rcmCountries.map((c,i) => (
                  <option key={i} value={c.id}>{c.country}</option>
                ))}
              </select>
              <ReactSVG path="/themes/base/production/svg/defaults/select-arrow.svg" className='svg-defaults/select-arrow'/>
            </div>
          </div>
        </div>
        <div className="l-booking__step">
          <h2 className="l-booking__step-heading">
            2. {window.labels.AdditionalInfoLabel}
          </h2>
          <div className="l-booking__step-note">
            {window.labels.AdditionalInfoDescription}
          </div>

          <div className="l-booking__form l-form">
          {/*
            <div className="l-form__field l-form__field--select">
              <label htmlFor="Booking_TravelReason" className="l-form__label">
                Reason for Travelling
              </label>
              <div className="l-form__select">
                <select className="l-form__input l-form__input--select" required name="Booking_TravelReason" id="Booking_TravelReason">
                  <option value="" selected disabled>Select a reason for travelling</option>
                  <option value="New Zealand">New Zealand</option>
                </select>
                <ReactSVG path="/themes/base/production/svg/defaults/select-arrow.svg" className='svg-defaults/select-arrow'/>
              </div>
            </div>
          */}
            <div className="l-form__field--fw">
              <div className="l-form__field l-form__field--notes">
                <label htmlFor="Booking_Notes" className="l-form__label">{window.labels.NotesLabel}</label>
                <textarea maxLength="100" className="l-form__input l-form__input--textarea" name="rmk" id="Booking_Notes" value={props.state.form.formCustomerData.rmk} onChange={e => props.onFormChange(e)} placeholder="Please write any extra relevant info here..."></textarea>
              </div>
            </div>

            <div className="l-form__field l-form__field--checkbox l-form__field--fw">
              <input className="l-form__input js-booking__dropoff-location-toggle" type="checkbox" name="formEmailOptin" id="Booking_Offers" checked={props.state.form.formEmailOptin} onChange={e => props.onFormChange(e)}/>
              <label htmlFor="Booking_Offers" className="l-form__label">
                {window.labels.SignUpLabel}
              </label>
            </div>

            <div className="l-form__field l-form__field--checkbox l-form__field--fw">
              <input className="l-form__input js-booking__dropoff-location-toggle" type="checkbox" name="formAgreedToTerms" id="Booking_Terms" checked={props.state.form.formAgreedToTerms} onChange={e => props.onFormChange(e)}/>
              <label htmlFor="Booking_Terms" className="l-form__label">
                {window.labels.AgreeLabel}&nbsp;<a href="/terms" target="_blank">{window.labels.TermsLabel}</a>
              </label>
            </div>
          </div>
        </div>
        </form>
    </div>
  );
}
