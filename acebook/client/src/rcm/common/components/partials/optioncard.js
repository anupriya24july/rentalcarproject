import React from 'react';
import ReactSVG from 'react-svg';
import numeral from 'numeral';

export const OptionCard = (props) => {

  return (
    <div className={"pos" + props.order + " c-option-card" + (props.Type? " c-option-card--" + props.Type : "") + (props.Selected? " is-selected":"")} onClick={(e) => props.onClick(props.Option.id, e)} >
      {props.Option.default == 'True'
        ? <div className="c-option-card__label">
            Most Popular
          </div>
        : ""
      }

      <div className="c-option-card__main  notranslate">
        <div className="c-option-card__title" style={{margin:'0px 20px'}}>{props.Option.name.split('(')[0]}</div>
        <div className="c-option-card__price">
          <span style={{textDecorationLine: props.Option.excessfee == 0 && props.special ? 'line-through': 'inherit'}}>
          {props.Option.fees == 0
            ? window.labels.FreeLabel
            : (props.isTotal? '':'+ ') + numeral(props.Option.fees).format('$0,0.00') +
              ((props.Option.type == 'Daily') ? " / " + window.labels.DayLabel :"")
          }
          </span>
          { props.Option.excessfee == 0 && props.special
            ? <div style={{fontWeight:'bold', fontSize:'17px', color: '#0056a7'}}>SELECT TO SAVE 10%</div>
            : ''
          }
          {props.showGST ? <span style={{textTransform: 'none', fontSize: '12px'}}> (incl. GST)</span> : '' }
        </div>
        {props.Note ? <div className="c-option-card__note">{props.Note}</div> : ''}
        {props.Option.extranotes ? props.Option.extranotes : '' }
      </div>
      {props.Option.qtyapply == 'True' 
        ? <div className="c-option-card__qty">
            <div className="l-form__field l-form__field--fw l-form__field--select">
              <div className="l-form__select">
                <select className="l-form__input l-form__input--select" onClick={e=>{e.stopPropagation()}} onChange={e=> props.onQtyChange(e, props.Option)} defaultValue={props.Selected? props.Selected.qty:0}>
                  {Array(10).join('-').split('-').map( (o,i) =>
                    <option key={i+1} value={i+1}>{i+1}</option>
                  )}
                </select>
                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="8" viewBox="0 0 12 8" className="svg-defaults/select-arrow"><path d="M1.712.298L.288 1.702l5.637 5.712 5.777-5.702L10.298.288 5.944 4.586" fill="#004E95"></path></svg>
              </div>
            </div>
          </div>
        :''
      }
      {props.Features
        ? <ul className="c-option-card__features">
            {props.Features.map((f,i) => 
              <li key={i} className={'c-option-card__feature' + ( i >= 5 ? ' toggle-active':'')}>
                <div className="c-option-card__feature-title">{f[0].toUpperCase()}</div>
                <div className="c-option-card__feature-detail">
                  {f[1]===true
                    ? <ReactSVG path="/themes/base/production/svg/option-feature-check-gray.svg" classNameName='svg-option-feature-check-gray'/>
                    : f[1]===false
                      ? <ReactSVG path="/themes/base/production/svg/option-feature-cross.svg" classNameName='svg-option-feature-cross'/>
                      : f[1].toUpperCase()
                  }
                </div>
              </li>
            )}
          </ul>
        :''
      }

      {props.Details
        ? <button className="c-option-card__more js-option-card-details-trigger" onClick={(e) => props.toggleMoreSpecs(e)}>
            <ReactSVG path="/themes/base/production/svg/more-plus.svg" classNameName='svg-more-plus'/> See more details
          </button>
        : ""
      }

      {props.showdesc3
        ? <div style={{padding:'25px'}}>{props.Option.extradesc3}</div>
        : ''
      }

      {props.Option.excessfee == 0 && props.special
        ? <div style={{textAlign: 'center', fontSize: '20px', background: '#ffdd00', padding: '10px 25px', color: '#0056a7', margin:'5px'}}><b>LIMITED OFFER *</b> <br /> Save 10% on Ace PLUS</div>
        : ''
      }
    </div>
  );
}
