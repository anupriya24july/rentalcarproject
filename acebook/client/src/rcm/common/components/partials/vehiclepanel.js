import React from 'react';
import ReactSVG from 'react-svg';
import moment from 'moment';
import numeral from 'numeral';

export const VehiclePanel = (props) => {
  let form = props.state.form;
  let locp = form.formPickupLocation.split('-');
  
  const activeCountry =  props.state[props.api.defaultcountry];

  const p_location = activeCountry.rcmLocationInfo.filter(loc => loc.id == form.formPickupLocation.split('-')[1])[0];
  const d_location = activeCountry.rcmLocationInfo.filter(loc => loc.id == form.formDropoffLocation.split('-')[1])[0];
  let selectedCar = activeCountry.rcmAvailableCars.filter(car => car.carsizeid == form.selectedCarCategoryId)[0];
  let upgradeCar = form.upgradeCar;
  let vehicle = cars? cars:[];
  const datetimeformat = 'DD/MM/YYYY HH:mm';
  const p_datetime = moment(form.formPickupDate + ' ' + form.formPickupTime.replace('_',':'), datetimeformat);
  const d_datetime = moment(form.formDropoffDate + ' ' + form.formDropoffTime.replace('_',':'), datetimeformat);

  let days = selectedCar ? selectedCar.numofdays: 0;

  let chkid = "";
  let qtyid = "";
  let calcTotal = 0;
  let calcGst = calcTotal;
  let calcStampDuty = calcTotal;
  let calcTotOptExt = 0.0;
  let ItemVal = 0.0;
  let ItemObj = null;
  let qtyItems = 1;
  let CountryTax = 1.0 + activeCountry.rcmTaxRate;
  let StateTax = 1.0 + activeCountry.rcmStateTax;
  let UseStateTax = activeCountry.rcmStateTax > 0;
  let UseTax = activeCountry.rcmTaxRate > 0;

  let totalcost = 0;
  let selectedInsurance = null;

  if(props.showtotal){
    if(form.selectedInsuranceId != ''){
      if(form.selectedInsuranceId == ''){
        //error: there must be a problem in the initialization
      }else{
        selectedInsurance = props.state[locp[0]].rcmInsuranceOptions.filter(ins => ins.id == form.selectedInsuranceId)[0];
      }

      if(props.state[form.country].rcmInsuranceOptions.length == 0 || form.selectedInsuranceId == ''){
        return false;
      }

      if(activeCountry.rcmTaxInclusive == false){
        calcGst = parseFloat(calcGst) * CountryTax;
        calcStampDuty = parseFloat(calcStampDuty) * StateTax;
      }

      // Insurance
      let calcInsurance = 0.0;
      activeCountry.rcmInsuranceOptions.map((ins,i) => {
        if (ins.type == "Daily") {
          ItemVal = (parseFloat(days) * parseFloat(ins.fees));
        } else if (ins.type == "Percentage") {
         if (ins.percentagetotalcost == "True" || ins.merchantfee == "True") {
             ItemVal = (parseFloat(calcTotal) * parseFloat(ins.fees) / 100);
           } else {
             ItemVal = (parseFloat(ratetotal) * parseFloat(ins.fees) / 100);
           }
        } else {
          ItemVal = parseFloat(ins.fees);
        }

        if (parseFloat(ins.maxprice) > 0 && ItemVal > parseFloat(ins.maxprice)) ItemVal = parseFloat(ins.maxprice);
        if (form.selectedInsuranceId == ins.id) {
          if (activeCountry.rcmTaxInclusive == false) {
             calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
             if (ins.gst == "True") calcGst = parseFloat(calcGst) + (parseFloat(ItemVal) * CountryTax);
             if (ins.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + (parseFloat(ItemVal) * StateTax);
          } else {
             calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
             if (ins.gst == "True") calcGst = parseFloat(calcGst) + parseFloat(ItemVal);
             if (ins.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + parseFloat(ItemVal);
          }
          calcInsurance = parseFloat(calcTotOptExt) + parseFloat(ItemVal);
        }
      });
    }

    // Mandatory Fees
    let calcMandatoryFees = 0.0;
    activeCountry.rcmMandatoryFees.map((mandatoryfees,i) => {
      let ItemVal = 0.0;
      let OldVal = 0.0;
      if ((mandatoryfees.locationid == p_location.id || mandatoryfees.locationid == "0") && (mandatoryfees.vehiclesizeid == selectedCar.carsizeid || mandatoryfees.vehiclesizeid == "0")) {
        if (mandatoryfees.type == "Daily") {
          ItemVal = ( parseFloat(days) * parseFloat(mandatoryfees.fees));
        } else if (mandatoryfees.type == "Percentage") {
          if (mandatoryfees.percentagetotalcost == "True" || mandatoryfees.merchantfee == "True") {
            ItemVal = (parseFloat(calcTotal) * parseFloat(mandatoryfees.fees) / 100);
          } else {
            ItemVal = (parseFloat(ratetotal) * parseFloat(mandatoryfees.fees) / 100);
          }
        } else {
          ItemVal = parseFloat(mandatoryfees.fees);
        }
        if (parseFloat(mandatoryfees.maxprice) > 0 && ItemVal > parseFloat(mandatoryfees.maxprice)) ItemVal = parseFloat(mandatoryfees.maxprice);
        if (activeCountry.rcmTaxInclusive == false) {
          calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
          if (mandatoryfees.gst == "True") calcGst = parseFloat(calcGst) + (parseFloat(ItemVal) * CountryTax);
          if (mandatoryfees.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + (parseFloat(ItemVal) * StateTax);
        } else {
          calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
          if (mandatoryfees.gst == "True") calcGst = parseFloat(calcGst) + ItemVal;
          if (mandatoryfees.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + ItemVal;
        }
      }
    });

    // Optional Extras
    let calcOptionalExtras = 0.0;
    form.selectedOptionalExtras.map((selex,i) => {
      ItemVal = 0.0;
      qtyItems = selex.qty;
      if (selex.ex.type == "Daily") {
        ItemVal = (parseFloat(days) * parseFloat(selex.ex.fees));
      } else if (selex.ex.type == "Percentage") {
        if (selex.ex.percentagetotalcost == "True" || selex.ex.merchantfee == "True") {
          ItemVal = (parseFloat(calcTotal) * parseFloat(selex.ex.fees) / 100);
        } else {
          ItemVal = (parseFloat(ratetotal) * parseFloat(selex.ex.fees) / 100);
        }
      } else {
        ItemVal = parseFloat(selex.ex.fees);
      }
      if (parseFloat(selex.ex.maxprice) > 0 && ItemVal > parseFloat(selex.ex.maxprice)) ItemVal = parseFloat(selex.ex.maxprice);
      ItemVal = ItemVal * qtyItems;

      if (activeCountry.rcmTaxInclusive == false) {
        calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
        if (selex.ex.gst == "True") calcGst = parseFloat(calcGst) + (parseFloat(ItemVal) * (CountryTax));
        if (selex.ex.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + (parseFloat(ItemVal) * StateTax);
      } else {
        calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
        if (selex.ex.gst == "True") calcGst = parseFloat(calcGst) + parseFloat(ItemVal);
        if (selex.ex.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + parseFloat(ItemVal);
      }
      calcOptionalExtras = parseFloat(calcOptionalExtras) + parseFloat(ItemVal);
    });

    // todo: implement Km charges.
    // Km Charges
    let calcKmChargesFees = 0.0;
    activeCountry.rcmKmCharges.map((kmcharges,i) => {
      let ItemVal = 0.0;
      // - KmCharges, kms Daily rate are based on NoofRate days, if 1.4 days charged, total kms daily rate will be 1.4x$10
      if (activeCountry.rcmKmCharges.length != 0) {
        if (kmcharges.dailyrate > 0) {
           ItemVal = (days * parseFloat(kmcharges.dailyrate)).toFixed(2);
        }
      }
      
      // if (parseFloat(rcmKmCharges[j]["maxprice"]) > 0 && ItemVal > parseFloat(rcmKmCharges[j]["maxprice"])) ItemVal = parseFloat(rcmKmCharges[j]["maxprice"]);
      if (activeCountry.rcmTaxInclusive == false) {
          calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
          calcGst = parseFloat(calcGst) + (parseFloat(ItemVal) * CountryTax);
          calcStampDuty = parseFloat(calcStampDuty) + (parseFloat(ItemVal) * StateTax);
      } else {
          calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
          calcGst = parseFloat(calcGst) + parseFloat(ItemVal);
          calcStampDuty = parseFloat(calcStampDuty) + parseFloat(ItemVal);
      }
      calcKmChargesFees = parseFloat(calcTotOptExt) + parseFloat(ItemVal);
    });

    totalcost = +selectedCar.total + +calcTotal;

    if(props.updateTotal){
      props.updateTotal(numeral(totalcost).format('0.00'));
    }

  /*
    if (document.getElementById("displTot")) {
      if (rcmTaxInclusive == false) {
        document.getElementById("displTot").innerHTML = (parseFloat(calcTotal) + (parseFloat(calcGst) - parseFloat(calcGst) / CountryTax) + (parseFloat(calcStampDuty) - parseFloat(calcStampDuty) / StateTax)).toFixed(2);
        if (UseTax == true) document.getElementById("displGST").innerHTML = (parseFloat(calcGst) - parseFloat(calcGst) / CountryTax).toFixed(2);
        if (UseStateTax == true) document.getElementById("displState").innerHTML = (parseFloat(calcStampDuty) - parseFloat(calcStampDuty) / StateTax).toFixed(2);
      } else {
        document.getElementById("displTot").innerHTML = parseFloat(calcTotal).toFixed(2);
        if (UseTax == true) document.getElementById("displGST").innerHTML = (parseFloat(calcGst) - parseFloat(calcGst) / (CountryTax)).toFixed(2);
        if (UseStateTax == true) document.getElementById("displState").innerHTML = (parseFloat(calcStampDuty) - parseFloat(calcStampDuty) / StateTax).toFixed(2);
      }
      document.getElementById("TotOptionalExtras").innerHTML = parseFloat(calcTotOptExt).toFixed(2);
    }
  */
  }
  return (
    <div className="l-vehicle-panel">
     <div className="l-vehicle-panel__details-tab">CAR DETAILS </div>
     { !upgradeCar.previousCar.categoryId && upgradeCar.categoryId?
       <div className="l-vehicle-panel__upgrade-tab">UPGRADE </div>
       : "" 
    }
      <div className="l-vehicle-panel__inner">
        <h2 className="l-vehicle-panel__title notranslate">{selectedCar.categoryfriendlydescription}</h2>
        <h3 className="l-vehicle-panel__subtitle">{selectedCar.vehiclecategory}</h3>
        <img className="l-vehicle-panel__image" src={vehicle[selectedCar.carsizeid]? vehicle[selectedCar.carsizeid].image:''} alt={selectedCar.categoryfriendlydescription} />
    {/*     <div className="l-vehicle-panel__summary">
          {selectedCar.vehicledescription2}
        </div>
        <ul className="l-vehicle-panel__specifications">
          {vehicle[selectedCar.carsizeid].specs.map((specs,i) =>
            <li key={i} className={"l-vehicle-panel__specification"} style={{display:(specs.fullwidth=='1'? 'none':'')}}>
              {specs.icon
                ? <img src={specs.icon} alt={specs.title} />
                : ''
              }
              {specs.title}
            </li>
          )}
        </ul>
      */}

        { props.showtotal
          ? <div className="l-vehicle-panel__total">
              <div style={{fontSize:'larger', marginTop:'20px'}}>{window.labels.YourQuoteLabel}</div>
              <hr/>
              <div className="l-vehicle-panel__total-details">
                <div className="l-vehicle-panel__total-details-panel">
                  
                  <div className="l-vehicle-panel__total-item"> 
                    <div className="l-vehicle-panel__total-item-label">{window.labels.VehicleCostLabel}</div>
                    <div>
                      <div className="l-vehicle-panel__total-item-name">{selectedCar.categoryfriendlydescription}</div>
                      <div className="l-vehicle-panel__total-item-totals">
                        <div className="l-vehicle-panel__total-item-subtotal notranslate" style={{float:'left'}}>{numeral(selectedCar.discounteddailyrate).format('$0,0.00')} x {days} {window.labels.DayLabel}(s)</div>
                        <div className="l-vehicle-panel__total-item-total notranslate" style={{float:'right'}}>{numeral(selectedCar.totalrateafterdiscount).format('$0,0.00')}</div>
                      </div>
                      <br/>
                      {selectedCar.totaldiscount!=0
                        ? <div className="l-vehicle-panel__total-item-totals">
                            <div className="l-vehicle-panel__total-item-subtotal notranslate" style={{float:'left'}}>{numeral(selectedCar.totaldiscount).format('$0,0.00')} discount included.</div>
                            <div className="l-vehicle-panel__total-item-total" style={{float:'right'}}>({activeCountry.rcmAvailableCarDetails.filter(cd => { if (cd.carsizeid == selectedCar.carsizeid) return cd })[0].discountname})</div>
                            <br/>
                          </div>
                        : ''
                      }
                    </div>
                  </div>
                  <div className="l-vehicle-panel__total-item" style={{display:activeCountry.rcmMandatoryFees.length ? 'block':'none'}}>  
                    <div className="l-vehicle-panel__total-item-label">{window.labels.MandatoryFeesLabel}</div>
                    {activeCountry.rcmMandatoryFees.map((man,i) => (
                      man.vehiclesizeid == selectedCar.carsizeid
                      ? <div key={i}>
                          <div className="l-vehicle-panel__total-item-name">{man.name}</div>
                          <div className="l-vehicle-panel__total-item-totals">
                            <div className="l-vehicle-panel__total-item-subtotal notranslate" style={{float:'left'}}>{numeral(man.fees).format('$0,0.00')} </div>
                            <div className="l-vehicle-panel__total-item-total notranslate" style={{float:'right'}}>{numeral(man.fees).format('$0,0.00')}</div>
                          </div>
                          <br/>
                        </div>
                      : ''
                    ))}
                    <br/>
                  </div>
                  {selectedInsurance
                  ? <div className="l-vehicle-panel__total-item"> 
                      <div className="l-vehicle-panel__total-item-label">{window.labels.CoverageLabel}</div>
                      <div>
                        <div className="l-vehicle-panel__total-item-name notranslate">{selectedInsurance.name}</div>
                        { selectedInsurance.maxprice > 0 && (selectedInsurance.fees * days) > selectedInsurance.maxprice
                          ? <div className="l-vehicle-panel__total-item-totals">
                              <div className="l-vehicle-panel__total-item-subtotal notranslate" style={{float:'left'}}>{numeral(selectedInsurance.maxprice).format('$0,0.00')}</div>
                              <div className="l-vehicle-panel__total-item-total notranslate" style={{float:'right'}}>{numeral(selectedInsurance.maxprice).format('$0,0.00')}</div>
                            </div>
                          : <div className="l-vehicle-panel__total-item-totals">
                              <div className="l-vehicle-panel__total-item-subtotal notranslate" style={{float:'left'}}>{numeral(selectedInsurance.fees).format('$0,0.00')} x {days} {window.labels.DayLabel}(s)</div>
                              <div className="l-vehicle-panel__total-item-total notranslate" style={{float:'right'}}>{numeral(selectedInsurance.fees * days).format('$0,0.00')}</div>
                            </div>
                        }
                      </div>
                      <br/><br/>
                    </div>
                  :''
                  }
                  <div className="l-vehicle-panel__total-item" style={{display:form.selectedOptionalExtras.length ? 'block':'none'}}> 
                    <div className="l-vehicle-panel__total-item-label">{window.labels.AncillaryOptionsLabel}</div>
                    {form.selectedOptionalExtras.map((selex,i) => (
                    <div key={i}>
                      <div className="l-vehicle-panel__total-item-name">{selex.ex.name}</div>
                      <div className="l-vehicle-panel__total-item-totals">
                        <div className="l-vehicle-panel__total-item-subtotal notranslate" style={{float:'left'}}>{numeral(selex.ex.fees).format('$0,0.00') + ' x ' + selex.qty} {selex.ex.type=='Daily'? ' x ' + days +  window.labels.DayLabel +'(s)' : ''}</div>
                        <div className="l-vehicle-panel__total-item-total notranslate" style={{float:'right'}}>{numeral(selex.ex.fees * (selex.ex.type=='Daily'? days : 1) * selex.qty).format('$0,0.00')}</div>
                      </div>
                      <br/>
                    </div>
                    ))}
                    <br/>
                  </div>
                  {form.country=='au'
                    ? <div className="l-vehicle-panel__total-item"> 
                        <div className="l-vehicle-panel__total-item-totals">
                          <div className="l-vehicle-panel__total-item-subtotal" style={{float:'left'}}>* GST on Total Price</div>
                          <div className="l-vehicle-panel__total-item-total notranslate" style={{float:'right'}}>({numeral(totalcost * props.api.gst).format('$0,0.00')})</div>
                        </div>
                        <br/>
                      </div>
                    : ''
                  }
                </div>
              </div>
              <hr/>
              <div className="l-vehicle-panel__total-label" style={{float:'left'}}>{window.labels.TotalPriceLabel} 
                {form.country=='au'
                  ? <span style={{textTransform: 'none', fontSize: 'smaller'}}> (incl. GST)</span>
                  : ''
                }
              </div>
              <div className="l-vehicle-panel__total-price notranslate" style={{float:'right'}}>{numeral(totalcost).format('$0,0.00')} <span>{form.country=='au'? 'AUD':'NZD'}</span></div>
            </div>
          : ''
          
        }
        {form.afterHourPick ?
          <span className="l-vehicle-panel__afterhour">
          <b>* After Hour Booking </b>
           <br/>The pickup time of {moment(form.formPickupTime,'HH:mm').format('hh:mm A')} you have selected is close to or outside our {p_location.location} operating hours. There is an additional fee that will be charged for after-hours pickups the fee will be added to your rental and a staff member will contact you to make the necessary arrangements.
           </span>
          : ''}
        {props.showUpgrade?
          upgradeCar.previousCar.categoryId?    
            <div className="l-vehicle-panel__degrade"> 
              <button className="c-vehicle-card__action btn btn--secondary "  onClick={ e => props.onUpgrade(upgradeCar.previousCar.categoryId, props.state.form.selectedCarCategoryId, 0, e)} >
                    Change to Previous Car
                </button> 
            </div>   
          : (props.state.form.isAmendment ? "" 
            : (upgradeCar.categoryId ?
              <div className="l-vehicle-panel__upgrade" >
              <div className="l-vehicle-panel__label">Upgrade</div>
                <h3 className="l-vehicle-panel__upgrade-subtitle">Upgrade to a {upgradeCar.description}</h3>
                <img className="l-vehicle-panel__image" src={vehicle[upgradeCar.categoryId]? vehicle[upgradeCar.categoryId].upgradeimage:''} alt={upgradeCar.description} />
                <div className="c-vehicle-card__footer c-vehicle-card__footer--book l-vehicle-panel__upgrade-footer">
                  <div className="c-vehicle-card__pricing">
                      <div className="c-vehicle-card__price-label l-vehicle-panel__upgrade-label" style={{textAlign: 'left'}}>
                          <span style={{textTransform: 'none'}}>For an extra </span>
                      </div>
                      <div className="c-vehicle-card__price l-vehicle-panel__upgrade-price">
                        {numeral(upgradeCar.amntDiff).format('$0,0.00')}
                      </div> 
                    </div>
                    <button className="c-vehicle-card__action btn btn--secondary "  onClick={ e => props.onUpgrade(upgradeCar.categoryId, props.state.form.selectedCarCategoryId, 1, e)} >
                        {props.state.form.selectedCarCategoryId==upgradeCar.categoryId? window.labels.ProcessingLabel + "..." : "Upgrade"}
                    </button> 
                </div>
              </div>
            : "")
          ) :""
        }
      </div>
    </div>
  );
}