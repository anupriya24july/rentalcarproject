import React from 'react';
import ReactSVG from 'react-svg';
import numeral from 'numeral';
import { OptionCard }  from './optioncard';

export const PaymentAmount = (props) => {
  let form = props.state.form;
  let mode=props.mode?props.mode:"";
  let pay_options = mode ?  [
    {
      type:'branch',
      note:'',
      name:'Pay Balance Amount',
      fees:props.state.form.balance,
      id:'branch'
    }
  ] : [
    {
      type:'fixed',
      note:'* + ' + numeral(props.state.form.total * 0.10).format('$0,0.00') + ' on pickup',
      name: window.labels.PayPercentLabel,
      fees: props.state.form.total * 0.10,
      id:'deposit',
      extranotes: (form.country=='au')? <div className="c-option-card__note" style={{marginBottom:'15px'}}>Balance subject to credit card surcharges <br />(See Below for details)</div> :false
    },
    {
      type:'fixed',
      note:'',
      name: window.labels.PayFullLabel,
      fees:props.state.form.total,
      id:'full',
      extranotes: (form.country=='au')? <div className="c-option-card__note">No Credit card Surcharge applies</div>:false
    }
  ]
  return (
    <div className="l-booking__step">
        <h2 className="l-booking__step-heading">
          1. {window.labels.PaymentAmountLabel}
        </h2>

        <div className="l-booking__options">
          {pay_options.map((po,i) => (
            <OptionCard key={i} Option={po} isTotal={true} Note={po.note} Selected={po.id == props.state.form.selectedPaymentAmount} onClick={()=>props.onSelectPaymentAmount(po.id)}/>
          ))}
        </div>
        {form.country=="au"
          ? <div style={{fontStyle: 'italic', maxWidth: '740px', margin: '20px 0 0'}}>
              <p>For all in-branch payments a credit card surcharge of 1.35% applies when payment is made by Visa, MasterCard, and Unionpay. For Amex and Diners Cards a surcharge of 2.97% applies.</p>
            </div>
          : ''
        }
    </div>
  )
}
