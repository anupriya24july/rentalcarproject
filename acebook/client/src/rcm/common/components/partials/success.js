import React from 'react';
import ReactSVG from 'react-svg';
import moment from 'moment';
import numeral from 'numeral';

export const Success = (props) => {
  let form = props.state.form;
  let locp = form.formPickupLocation.split('-');
  
  const activeCountry =  props.state[locp[0]];

  const p_location = activeCountry.rcmLocationInfo.filter(loc => loc.id == form.formPickupLocation.split('-')[1])[0];
  const d_location = activeCountry.rcmLocationInfo.filter(loc => loc.id == form.formDropoffLocation.split('-')[1])[0];

  let locationInfo= locInfo? locInfo: '';
  let pickupInfo=locationInfo[form.formPickupLocation.split('-')[0]][form.formPickupLocation.split('-')[1]];
  let dropoffInfo=locationInfo[form.formPickupLocation.split('-')[0]][form.formDropoffLocation.split('-')[1]];

  const selectedCar = activeCountry.rcmAvailableCars.filter(car => car.carsizeid == form.selectedCarCategoryId)[0];
  let vehicle = cars? cars:[];

  const datetimeformat = 'DD/MM/YYYY HH:mm';
  const p_datetime = moment(form.formPickupDate + ' ' + form.formPickupTime.replace('_',':'), datetimeformat);
  const d_datetime = moment(form.formDropoffDate + ' ' + form.formDropoffTime.replace('_',':'), datetimeformat);

  let days = selectedCar ? selectedCar.numofdays: 0;

  let chkid = "";
  let qtyid = "";
  let calcTotal = 0;
  let calcGst = calcTotal;
  let calcStampDuty = calcTotal;
  let calcTotOptExt = 0.0;
  let ItemVal = 0.0;
  let ItemObj = null;
  let qtyItems = 1;
  let CountryTax = 1.0 + activeCountry.rcmTaxRate;
  let StateTax = 1.0 + activeCountry.rcmStateTax;
  let UseStateTax = activeCountry.rcmStateTax > 0;
  let UseTax = activeCountry.rcmTaxRate > 0;

  let totalcost = 0;
  let selectedInsurance = {};

  if(form.selectedInsuranceId == ''){
    //error: there must be a problem in the initialization
  }else{
    selectedInsurance = props.state[form.country].rcmInsuranceOptions.filter(ins => ins.id == form.selectedInsuranceId)[0];
  }

  if(props.state[form.country].rcmInsuranceOptions.length == 0 || form.selectedInsuranceId == ''){
    return false;
  }

  if(activeCountry.rcmTaxInclusive == false){
    calcGst = parseFloat(calcGst) * CountryTax;
    calcStampDuty = parseFloat(calcStampDuty) * StateTax;
  }

  // Insurance
  let calcInsurance = 0.0;
  activeCountry.rcmInsuranceOptions.map((ins,i) => {
    if (ins.type == "Daily") {
      ItemVal = (parseFloat(days) * parseFloat(ins.fees));
    } else if (ins.type == "Percentage") {
     if (ins.percentagetotalcost == "True" || ins.merchantfee == "True") {
         ItemVal = (parseFloat(calcTotal) * parseFloat(ins.fees) / 100);
       } else {
         ItemVal = (parseFloat(ratetotal) * parseFloat(ins.fees) / 100);
       }
    } else {
      ItemVal = parseFloat(ins.fees);
    }

    if (parseFloat(ins.maxprice) > 0 && ItemVal > parseFloat(ins.maxprice)) ItemVal = parseFloat(ins.maxprice);
    if (form.selectedInsuranceId == ins.id) {
      if (activeCountry.rcmTaxInclusive == false) {
         calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
         if (ins.gst == "True") calcGst = parseFloat(calcGst) + (parseFloat(ItemVal) * CountryTax);
         if (ins.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + (parseFloat(ItemVal) * StateTax);
      } else {
         calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
         if (ins.gst == "True") calcGst = parseFloat(calcGst) + parseFloat(ItemVal);
         if (ins.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + parseFloat(ItemVal);
      }
      calcInsurance = parseFloat(calcTotOptExt) + parseFloat(ItemVal);
    }
  });

  // Mandatory Fees
  let calcMandatoryFees = 0.0;
  activeCountry.rcmMandatoryFees.map((mandatoryfees,i) => {
    let ItemVal = 0.0;
    let OldVal = 0.0;
    if ((mandatoryfees.locationid == p_location.id || mandatoryfees.locationid == "0") && (mandatoryfees.vehiclesizeid == selectedCar.carsizeid || mandatoryfees.vehiclesizeid == "0")) {
      if (mandatoryfees.type == "Daily") {
        ItemVal = ( parseFloat(days) * parseFloat(mandatoryfees.fees));
      } else if (mandatoryfees.type == "Percentage") {
        if (mandatoryfees.percentagetotalcost == "True" || mandatoryfees.merchantfee == "True") {
          ItemVal = (parseFloat(calcTotal) * parseFloat(mandatoryfees.fees) / 100);
        } else {
          ItemVal = (parseFloat(ratetotal) * parseFloat(mandatoryfees.fees) / 100);
        }
      } else {
        ItemVal = parseFloat(mandatoryfees.fees);
      }
      if (parseFloat(mandatoryfees.maxprice) > 0 && ItemVal > parseFloat(mandatoryfees.maxprice)) ItemVal = parseFloat(mandatoryfees.maxprice);
      if (activeCountry.rcmTaxInclusive == false) {
        calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
        if (mandatoryfees.gst == "True") calcGst = parseFloat(calcGst) + (parseFloat(ItemVal) * CountryTax);
        if (mandatoryfees.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + (parseFloat(ItemVal) * StateTax);
      } else {
        calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
        if (mandatoryfees.gst == "True") calcGst = parseFloat(calcGst) + ItemVal;
        if (mandatoryfees.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + ItemVal;
      }
    }
  });

  // Optional Extras
  let calcOptionalExtras = 0.0;
  form.selectedOptionalExtras.map((selex,i) => {
    ItemVal = 0.0;
    qtyItems = selex.qty;
    if (selex.ex.type == "Daily") {
      ItemVal = (parseFloat(days) * parseFloat(selex.ex.fees));
    } else if (selex.ex.type == "Percentage") {
      if (selex.ex.percentagetotalcost == "True" || selex.ex.merchantfee == "True") {
        ItemVal = (parseFloat(calcTotal) * parseFloat(selex.ex.fees) / 100);
      } else {
        ItemVal = (parseFloat(ratetotal) * parseFloat(selex.ex.fees) / 100);
      }
    } else {
      ItemVal = parseFloat(selex.ex.fees);
    }
    if (parseFloat(selex.ex.maxprice) > 0 && ItemVal > parseFloat(selex.ex.maxprice)) ItemVal = parseFloat(selex.ex.maxprice);
    ItemVal = ItemVal * qtyItems;

    if (activeCountry.rcmTaxInclusive == false) {
      calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
      if (selex.ex.gst == "True") calcGst = parseFloat(calcGst) + (parseFloat(ItemVal) * (CountryTax));
      if (selex.ex.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + (parseFloat(ItemVal) * StateTax);
    } else {
      calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
      if (selex.ex.gst == "True") calcGst = parseFloat(calcGst) + parseFloat(ItemVal);
      if (selex.ex.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + parseFloat(ItemVal);
    }
    calcOptionalExtras = parseFloat(calcOptionalExtras) + parseFloat(ItemVal);
  });

  // todo: implement Km charges.
  // Km Charges
  let calcKmChargesFees = 0.0;
  activeCountry.rcmKmCharges.map((kmcharges,i) => {
    let ItemVal = 0.0;
    // - KmCharges, kms Daily rate are based on NoofRate days, if 1.4 days charged, total kms daily rate will be 1.4x$10
    if (activeCountry.rcmKmCharges.length != 0) {
      if (kmcharges.dailyrate > 0) {
         ItemVal = (days * parseFloat(kmcharges.dailyrate)).toFixed(2);
      }
    }
    
    // if (parseFloat(rcmKmCharges[j]["maxprice"]) > 0 && ItemVal > parseFloat(rcmKmCharges[j]["maxprice"])) ItemVal = parseFloat(rcmKmCharges[j]["maxprice"]);
    if (activeCountry.rcmTaxInclusive == false) {
        calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
        calcGst = parseFloat(calcGst) + (parseFloat(ItemVal) * CountryTax);
        calcStampDuty = parseFloat(calcStampDuty) + (parseFloat(ItemVal) * StateTax);
    } else {
        calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
        calcGst = parseFloat(calcGst) + parseFloat(ItemVal);
        calcStampDuty = parseFloat(calcStampDuty) + parseFloat(ItemVal);
    }
    calcKmChargesFees = parseFloat(calcTotOptExt) + parseFloat(ItemVal);
  });

  totalcost = +selectedCar.total + +calcTotal;

  let paymentamount = numeral((form.selectedPaymentAmount == 'full')? form.total : (form.total * props.deposit)).format('0.00');
  let countryArray=[182,7,2,183,24];
  return (
    <div className="l-success">
      <div className="container">
        <h1 className="l-success__heading">{props.title}</h1>
        <div className="l-success__content" style={{margin: '0 auto 20px', width:'100%'}}>
          {props.content}
          <div id="booking_number" data-booking-number={activeCountry.rcmReservationNo} style={{fontSize:'30px',border:'1px solid',width:'200px',margin:'20px auto 0px'}}>{activeCountry.rcmReservationNo}</div>
          <div style={{fontSize:'14px'}}>REFERENCE NUMBER</div>
        </div>


      </div>
      <div className="l-success__container">
        <div className="l-success__itinerary">
          <div className="l-success__itinerary-item">
            <div className="l-success__itinerary-label">
              Pick Up Location
            </div>
            <div className="l-success__itinerary-location">
                  {p_location.location}
                  <span className='js-tooltip'  title={'<img src="'+ pickupInfo.image +'"><br>' + pickupInfo.address + '<br>' + pickupInfo.infoDesc} ><ReactSVG path="/themes/base/production/svg/info.svg"/></span>
            </div>
          </div>

          <div className="l-success__itinerary-item">
            <div className="l-success__itinerary-label">
              Pick Up Date
            </div>
            {p_datetime.format('DD, MMM, YYYY, hh:mma')}
          </div>

          <div className="l-success__itinerary-arrow">
            <ReactSVG path="/themes/base/production/svg/itinerary-arrow.svg" className='svg-itinerary-arrow'/>
          </div>

          <div className="l-success__itinerary-item">
            <div className="l-success__itinerary-label">
              Drop Off Location
            </div>
            <div className="l-success__itinerary-location">
              {d_location.location}
              <span className='js-tooltip'  title={'<img src="'+ dropoffInfo.image +'"><br>' + dropoffInfo.address + '<br>' + dropoffInfo.infoDesc} ><ReactSVG path="/themes/base/production/svg/info.svg"/></span>
            </div>
          </div>

          <div className="l-success__itinerary-item">
            <div className="l-success__itinerary-label">
              Drop Off Date
            </div>
            {d_datetime.format('DD, MMM, YYYY, hh:mma')}
          </div>
        </div>
        <div className="l-success__main">
          <div className="l-success__vehicle">
            <h2 className="l-success__sub-heading">Customer Details</h2>
            <div className="l-success__vehicle-info js-vehicle-card">
              <div className="l-success__vehicle-title">{form.formCustomerData.fnm + ' ' + form.formCustomerData.lnm}</div>
              <div className="l-success__vehicle-subtitle">{form.formCustomerData.eml}</div>
              <div className="l-success__vehicle-subtitle">{form.formCustomerData.mob}</div>
              <div className="l-success__vehicle-subtitle">{form.formCustomerData.rmk}</div>
            </div>
            <br /><br />
            <h2 className="l-success__sub-heading">Vehicle</h2>
            <div className="l-success__vehicle-info js-vehicle-card">
              <div className="l-success__vehicle-title">{selectedCar.categoryfriendlydescription}</div>
              <div className="l-success__vehicle-subtitle">{selectedCar.vehiclecategory}</div>
              <div className="l-success__vehicle-image">
                <img src={vehicle[selectedCar.carsizeid]? vehicle[selectedCar.carsizeid].image:''} alt={selectedCar.categoryfriendlydescription} />
              </div>
            </div>
          </div>
          <div className="l-success__summary">
            <h2 className="l-success__sub-heading">Pricing Summary</h2>

            <div className="l-success__summary-rows">
              <div className="l-success__summary-row">
                <div className="l-success__summary-label">Vehicle Cost</div>
                <div className="l-success__summary-price">
                  <span className="l-success__summary-price-math">{days} x {numeral(selectedCar.discounteddailyrate).format('$0,0.00')} per day =</span> {numeral(selectedCar.totalrateafterdiscount).format('$0,0.00')}
                </div>
              </div>

              {selectedCar.totaldiscount!=0
              ? <div className="l-success__summary-row">
                  <div className="l-success__summary-label">{numeral(selectedCar.totaldiscount).format('$0,0.00')} discount included.</div>
                  <div className="l-success__summary-price">
                    <span className="l-success__summary-price-math">({activeCountry.rcmAvailableCarDetails.filter(cd => { if (cd.carsizeid == selectedCar.carsizeid) return cd })[0].discountname})</span>
                  </div>
                </div>
              : ''
              }
              <div className="l-success__summary-row">
                <div className="l-success__summary-label">{selectedInsurance.name}</div>
                { selectedInsurance.maxprice > 0 && (selectedInsurance.fees * days) > selectedInsurance.maxprice
                  ? <div className="l-success__summary-price">
                      <span className="l-success__summary-price-math">{numeral(selectedInsurance.maxprice).format('$0,0.00')}</span> {numeral(selectedInsurance.maxprice).format('$0,0.00')}
                    </div>
                  : <div className="l-success__summary-price">
                      <span className="l-success__summary-price-math">{days} x {numeral(selectedInsurance.fees).format('$0,0.00')} per day =</span> {numeral(selectedInsurance.fees * days).format('$0,0.00')}
                    </div>
                }
              </div>

              {activeCountry.rcmMandatoryFees.map((man,i) => (
                man.vehiclesizeid == selectedCar.carsizeid
                  ? <div className="l-success__summary-row" key={i}>
                      <div className="l-success__summary-label">{man.name.indexOf('After Hour')>=0? <b>*</b> : ''}{man.name}</div>
                      <div className="l-success__summary-price">
                        <span className="l-success__summary-price-math">{numeral(man.fees).format('$0,0.00')} =</span> {numeral(man.fees).format('$0,0.00')}
                      </div>
                    </div>
                  : ''
              ))}

              {form.selectedOptionalExtras.map((selex,i) => (
              <div className="l-success__summary-row" key={i}>
                <div className="l-success__summary-label">{selex.qty} {selex.ex.name}</div>
                <div className="l-success__summary-price">
                  <span className="l-success__summary-price-math">{(selex.ex.type=='Daily'? days : 1)} x {numeral(selex.ex.fees).format('$0,0.00')} {selex.ex.type=='Daily'? ' per day ' : ' '} =</span> {numeral(selex.ex.fees * (selex.ex.type=='Daily'? days : 1) * selex.qty).format('$0,0.00')}
                </div>
              </div>
              ))}

              {form.country=='au'
                ? <div className="l-success__summary-row">
                    <div className="l-success__summary-label">* GST on Total Price</div>
                    <div className="l-success__summary-price">
                      ({numeral(totalcost * props.gst).format('$0,0.00')})
                    </div>
                  </div>
                : ''
              }
            </div>
            <div className="l-success__summary-total">
              <div className="l-success__summary-total-label">Total Cost</div>
              <div id="booking_total" data-booking-total={numeral(totalcost).format('00.00')} className="l-success__summary-total-price">
                {numeral(totalcost).format('$0,0.00')}
              </div>
            </div>
            { selectedCar.available == 1
              ? <div className="l-success__summary-total" style={{margin:'0px', paddingTop:'0px'}}>
                  <div className="l-success__summary-total-label">Amount Paid</div>
                  <div className="l-success__summary-total-price">
                    {numeral(paymentamount).format('$0,0.00')}
                  </div>
                </div>
              : ''
            }
            { selectedCar.available == 1
              ? <div className="l-success__summary-total" style={{margin:'0px', paddingTop:'0px'}}>
                  <div className="l-success__summary-total-label">Balance Owing</div>
                  <div className="l-success__summary-total-price">
                    {numeral(totalcost - paymentamount).format('$0,0.00')}
                  </div>
                </div>
              : ''
            }

            {form.afterHourPick ?
              <span className="l-vehicle-panel__afterhour">
                <b>* After Hour Booking </b>
                <br/>The pickup time of {moment(form.formPickupTime,'HH:mm').format('hh:mm A')} you have selected is close to or outside our {p_location.location} operating hours. There is an additional fee that will be charged for after-hours pickups the fee will be added to your rental and a staff member will contact you to make the necessary arrangements.
              </span>
              : ''
            }
            {countryArray.indexOf(form.formCustomerData.cnt) > -1 ?
              '' :
              <span className="l-vehicle-panel__afterhour">
                <b> Important information </b>
                <br/>If your original license is not in English, it is a NZ requirement that you also bring along a copy of your IDP or an authorised translation when collecting your vehicle from the branch. For more info [<a target="_blank" href="https://www.nzta.govt.nz/driver-licences/new-residents-and-visitors/approved-translators/">click here</a>].
              </span>
            }
          </div>
        </div>

        <div className="l-success__checkin">
          <h3 className="l-success__checkin-title">
            Want to save time during pickup? Check in Early!
          </h3>
           {form.country=="nz"? <a href="https://secure.acerentalcars.co.nz" className="btn btn--secondary l-success__checkin-action">
            Check in now
          </a>
            : <a href="https://secure.acerentalcars.com.au" className="btn btn--secondary l-success__checkin-action">
            Check in now
          </a>
          }
          
        </div>

        <div className="l-success__footer">
        {/*
          <a href="#" className="l-success__apple-wallet">
            <img src="/themes/base/production/images/apple-wallet.png" alt="Add to Apple Wallet" />
          </a>
        */}
          <a href="javascript:window.print();" className="l-success__pdf">
            Print Rental Summary
          </a>
        </div>
      </div>
  </div>
  )
}
