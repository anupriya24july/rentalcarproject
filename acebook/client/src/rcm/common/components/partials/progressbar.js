import React from 'react';
export const ProgressBar = (props) => {
	return (
    <div className="l-booking__progress">
      <div className="l-booking__progress-labels">
        <div className="l-booking__progress-label">{window.labels.SelectVehicleLabel}</div>
        <div className="l-booking__progress-label">{window.labels.AddExtrasLabel}</div>
        <div className="l-booking__progress-label">{window.labels.PersonalDetailsLabel}</div>
        <div className="l-booking__progress-label">{window.labels.PaymentLabel}</div>
        <div className="l-booking__progress-label">{window.labels.CompleteLabel}</div>
      </div>
      <div className={'l-booking__progress-bar l-booking__progress-bar--' + props.progress} />
    </div>
  );
}