import React from 'react';
import moment from 'moment';
import numeral from 'numeral';

export const RentalSummary = (props) => {

  const activeCountry =  props.state.nz;
  const form = props.state.form;

  const p_location = activeCountry.rcmLocationInfo.filter(loc => loc.id == form.formPickupLocation.split('-')[1])[0];
  const d_location = activeCountry.rcmLocationInfo.filter(loc => loc.id == form.formDropoffLocation.split('-')[1])[0];
  const selectedCar = activeCountry.rcmAvailableCars.filter(car => car.carsizeid == form.selectedCarCategoryId)[0];

  const datetimeformat = 'DD/MM/YYYY HH:mm';
  const p_datetime = moment(form.formPickupDate + ' ' + form.formPickupTime.replace('_',':'), datetimeformat);
  const d_datetime = moment(form.formDropoffDate + ' ' + form.formDropoffTime.replace('_',':'), datetimeformat);

  let days = selectedCar.numofdays;

  let chkid = "";
  let qtyid = "";
  let calcTotal = 0;
  let calcGst = calcTotal;
  let calcStampDuty = calcTotal;
  let calcTotOptExt = 0.0;
  let ItemVal = 0.0;
  let ItemObj = null;
  let qtyItems = 1;
  let CountryTax = 1.0 + activeCountry.rcmTaxRate;
  let StateTax = 1.0 + activeCountry.rcmStateTax;
  let UseStateTax = activeCountry.rcmStateTax > 0;
  let UseTax = activeCountry.rcmTaxRate > 0;

  let selectedInsurance = {};
  if(form.selectedInsuranceId == ''){
    //error: there must be a problem in the initialization
  }else{
    selectedInsurance = props.state.nz.rcmInsuranceOptions.filter(ins => ins.id == form.selectedInsuranceId)[0];
  }

  if(props.state.nz.rcmInsuranceOptions.length == 0 || form.selectedInsuranceId == ''){
    return false;
  }

  if(activeCountry.rcmTaxInclusive == false){
    calcGst = parseFloat(calcGst) * CountryTax;
    calcStampDuty = parseFloat(calcStampDuty) * StateTax;
  }

  // Insurance
  let calcInsurance = 0.0;
  activeCountry.rcmInsuranceOptions.map((ins,i) => {
    if (ins.type == "Daily") {
      ItemVal = (parseFloat(ins.numofdays) * parseFloat(ins.fees));
    } else if (ins.type == "Percentage") {
     if (ins.percentagetotalcost == "True" || ins.merchantfee == "True") {
         ItemVal = (parseFloat(calcTotal) * parseFloat(ins.fees) / 100);
       } else {
         ItemVal = (parseFloat(ratetotal) * parseFloat(ins.fees) / 100);
       }
    } else {
      ItemVal = parseFloat(ins.fees);
    }

    if (parseFloat(ins.maxprice) > 0 && ItemVal > parseFloat(ins.maxprice)) ItemVal = parseFloat(ins.maxprice);
    if (form.selectedInsuranceId == ins.id) {
      if (activeCountry.rcmTaxInclusive == false) {
         calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
         if (ins.gst == "True") calcGst = parseFloat(calcGst) + (parseFloat(ItemVal) * CountryTax);
         if (ins.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + (parseFloat(ItemVal) * StateTax);
      } else {
         calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
         if (ins.gst == "True") calcGst = parseFloat(calcGst) + parseFloat(ItemVal);
         if (ins.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + parseFloat(ItemVal);
      }
      calcInsurance = parseFloat(calcTotOptExt) + parseFloat(ItemVal);
    }
  });

  // Mandatory Fees
  let calcMandatoryFees = 0.0;
  activeCountry.rcmMandatoryFees.map((mandatoryfees,i) => {
    let ItemVal = 0.0;
    let OldVal = 0.0;
    if ((mandatoryfees.locationid == p_location.id || mandatoryfees.locationid == "0") && (mandatoryfees.vehiclesizeid == selectedCar.carsizeid || mandatoryfees.vehiclesizeid == "0")) {
      if (mandatoryfees.type == "Daily") {
        ItemVal = ( parseFloat(mandatoryfees.numofdays) * parseFloat(mandatoryfees.fees));
      } else if (mandatoryfees.type == "Percentage") {
        if (mandatoryfees.percentagetotalcost == "True" || mandatoryfees.merchantfee == "True") {
          ItemVal = (parseFloat(calcTotal) * parseFloat(mandatoryfees.fees) / 100);
        } else {
          ItemVal = (parseFloat(ratetotal) * parseFloat(mandatoryfees.fees) / 100);
        }
      } else {
        ItemVal = parseFloat(mandatoryfees.fees);
      }
      if (parseFloat(mandatoryfees.maxprice) > 0 && ItemVal > parseFloat(mandatoryfees.maxprice)) ItemVal = parseFloat(mandatoryfees.maxprice);
      if (activeCountry.rcmTaxInclusive == false) {
        calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
        if (mandatoryfees.gst == "True") calcGst = parseFloat(calcGst) + (parseFloat(ItemVal) * CountryTax);
        if (mandatoryfees.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + (parseFloat(ItemVal) * StateTax);
      } else {
        calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
        if (mandatoryfees.gst == "True") calcGst = parseFloat(calcGst) + ItemVal;
        if (mandatoryfees.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + ItemVal;
      }
    }
  });

  // Optional Extras
  let calcOptionalExtras = 0.0;
  form.selectedOptionalExtras.map((selex,i) => {
    ItemVal = 0.0;
    qtyItems = 1;
    if (selex.ex.type == "Daily") {
      ItemVal = (parseFloat(selex.ex.numofdays) * parseFloat(selex.ex.fees));
    } else if (selex.ex.type == "Percentage") {
      if (selex.ex.percentagetotalcost == "True" || selex.ex.merchantfee == "True") {
        ItemVal = (parseFloat(calcTotal) * parseFloat(selex.ex.fees) / 100);
      } else {
        ItemVal = (parseFloat(ratetotal) * parseFloat(selex.ex.fees) / 100);
      }
    } else {
      ItemVal = parseFloat(selex.ex.fees);
    }
    if (parseFloat(selex.ex.maxprice) > 0 && ItemVal > parseFloat(selex.ex.maxprice)) ItemVal = parseFloat(selex.ex.maxprice);
    ItemVal = ItemVal * qtyItems;

    if (activeCountry.rcmTaxInclusive == false) {
      calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
      if (selex.ex.gst == "True") calcGst = parseFloat(calcGst) + (parseFloat(ItemVal) * (CountryTax));
      if (selex.ex.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + (parseFloat(ItemVal) * StateTax);
    } else {
      calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
      if (selex.ex.gst == "True") calcGst = parseFloat(calcGst) + parseFloat(ItemVal);
      if (selex.ex.stampduty == "True") calcStampDuty = parseFloat(calcStampDuty) + parseFloat(ItemVal);
    }
    calcOptionalExtras = parseFloat(calcOptionalExtras) + parseFloat(ItemVal);
  });

  // todo: implement Km charges.
  // Km Charges
  let calcKmChargesFees = 0.0;
  activeCountry.rcmKmCharges.map((kmcharges,i) => {
    let ItemVal = 0.0;
    // - KmCharges, kms Daily rate are based on NoofRate days, if 1.4 days charged, total kms daily rate will be 1.4x$10
    if (activeCountry.rcmKmCharges.length != 0) {
      if (kmcharges.dailyrate > 0) {
         ItemVal = (numofdays * parseFloat(kmcharges.dailyrate)).toFixed(2);
      }
    }
    
    // if (parseFloat(rcmKmCharges[j]["maxprice"]) > 0 && ItemVal > parseFloat(rcmKmCharges[j]["maxprice"])) ItemVal = parseFloat(rcmKmCharges[j]["maxprice"]);
    if (activeCountry.rcmTaxInclusive == false) {
        calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
        calcGst = parseFloat(calcGst) + (parseFloat(ItemVal) * CountryTax);
        calcStampDuty = parseFloat(calcStampDuty) + (parseFloat(ItemVal) * StateTax);
    } else {
        calcTotal = parseFloat(calcTotal) + parseFloat(ItemVal);
        calcGst = parseFloat(calcGst) + parseFloat(ItemVal);
        calcStampDuty = parseFloat(calcStampDuty) + parseFloat(ItemVal);
    }
    calcKmChargesFees = parseFloat(calcTotOptExt) + parseFloat(ItemVal);
  });

  let totalcost = +selectedCar.total + +calcTotal;

/*
  if (document.getElementById("displTot")) {
    if (rcmTaxInclusive == false) {
      document.getElementById("displTot").innerHTML = (parseFloat(calcTotal) + (parseFloat(calcGst) - parseFloat(calcGst) / CountryTax) + (parseFloat(calcStampDuty) - parseFloat(calcStampDuty) / StateTax)).toFixed(2);
      if (UseTax == true) document.getElementById("displGST").innerHTML = (parseFloat(calcGst) - parseFloat(calcGst) / CountryTax).toFixed(2);
      if (UseStateTax == true) document.getElementById("displState").innerHTML = (parseFloat(calcStampDuty) - parseFloat(calcStampDuty) / StateTax).toFixed(2);
    } else {
      document.getElementById("displTot").innerHTML = parseFloat(calcTotal).toFixed(2);
      if (UseTax == true) document.getElementById("displGST").innerHTML = (parseFloat(calcGst) - parseFloat(calcGst) / (CountryTax)).toFixed(2);
      if (UseStateTax == true) document.getElementById("displState").innerHTML = (parseFloat(calcStampDuty) - parseFloat(calcStampDuty) / StateTax).toFixed(2);
    }
    document.getElementById("TotOptionalExtras").innerHTML = parseFloat(calcTotOptExt).toFixed(2);
  }
*/
  return (
    <div style={{padding:'20px'}}>
      <div className="row" style={{marginTop:'20px'}}>
        <div className="col-sm-6 text-left">Total Cost</div>
        <div className="col-sm-6 text-right">{numeral(totalcost).format('$0,0.00') + ' NZD'}</div>
      </div>
      <hr style={{borderTop: '2px solid #ccc'}}/>
      <div className="row">
          <div className="col-sm-12">
            <div className="lead" >YOUR ITINERARY</div>
          </div>
      </div>
      <div className="row">
          <div className="col-sm-12">
            <label>Pickup Location</label>
            <br/><span style={{fontSize:'16px'}}>{p_location.location}</span>
          </div>
      </div>
      <div className="row">
          <div className="col-sm-12">
            <label>Pickup Date</label>
            <br/><span style={{fontSize:'16px'}}>{p_datetime.format('DD MMMM, YYYY - hh:mm A')}</span>
          </div>
      </div>
      <hr/>
      <div className="row">
          <div className="col-sm-12">
            <label>Dropoff Location</label>
            <br/><span style={{fontSize:'16px'}}>{d_location.location}</span>
          </div>
      </div>
      <div className="row">
          <div className="col-sm-12">
            <label>Dropoff Date</label>
            <br/><span style={{fontSize:'16px'}}>{d_datetime.format('DD MMMM, YYYY - hh:mm A')}</span>
          </div>
      </div>
      <hr style={{borderTop: '2px solid #ccc'}}/>
      <div className="row" style={{marginTop:'20px'}}>
          <div className="col-sm-12">
            <div className="lead">RENTAL SUMMARY</div>
          </div>
      </div>
      <div className="row">
          <div className="col-sm-12">
            {selectedCar.categoryfriendlydescription}
          </div>
      </div>
      <div className="row">
          <div className="col-sm-12">
            <img src={selectedCar.imagename} style={{width:'60%'}}/>
          </div>
      </div>
      <hr/>
      <div className="row">
        <div className="col-sm-12"><label>Vehicle Cost</label></div>
      </div>
      <div className="row">
        <div className="col-sm-7 text-left">
          {numeral(selectedCar.avgrate).format('$0,0.00')} daily
        </div>
        <div className="col-sm-2 text-left">
          x {days}
        </div>
        <div className="col-sm-3 text-right">
          {numeral(selectedCar.totrate).format('$0,0.00')}
        </div>
      </div>

      <div className="row">
        <div className="col-sm-7 text-left">
          {selectedInsurance.name}
        </div>
        <div className="col-sm-2 text-left">
          x {days}
        </div>
        <div className="col-sm-3 text-right">
          {numeral(selectedInsurance.fees).format('$0,0.00')}
        </div>
      </div>

      {activeCountry.rcmMandatoryFees.map((man,i) => (
      <div className="row" key={i}>
        <div className="col-sm-7 text-left">
          {man.name}
        </div>
        <div className="col-sm-2 text-left">
        </div>
        <div className="col-sm-3 text-right">
          {numeral(man.fees).format('$0,0.00')}
        </div>
      </div>
      ))}

      {form.selectedOptionalExtras.map((selex,i) => (
      <div className="row" key={i}>
        <div className="col-sm-7 text-left">
          {selex.ex.name}
        </div>
        <div className="col-sm-2 text-left">
        </div>
        <div className="col-sm-3 text-right">
          {numeral(selex.ex.fees).format('$0,0.00')}
        </div>
      </div>
      ))}

      <hr style={{borderTop: '2px solid #ccc'}}/>
      <div className="row" style={{marginTop:'20px'}}>
        <div className="col-sm-6 text-left">Total Cost</div>
        <div className="col-sm-6 text-right">{numeral(totalcost).format('$0,0.00') + ' NZD'}</div>
      </div>
    </div>

  );
}
