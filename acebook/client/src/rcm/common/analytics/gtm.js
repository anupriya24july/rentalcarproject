import React from 'react';
import moment from 'moment';

pushAnalytics () {
  let form = this.props.state.form;
  let impressions = [];
  let products = [];
  let locp = this.props.state.form.formPickupLocation.split('-');
  let ploc = this.props.state[locp[0]].rcmLocationInfo.filter(l => l.id == locp[1] )[0];

  let locd = this.props.state.form.formDropoffLocation.split('-');
  let dloc = this.props.state[locp[0]].rcmLocationInfo.filter(l => l.id == locd[1] )[0];

  const datetimeformat = 'DD/MM/YYYY HH:mm';
  const p_datetime = moment(form.formPickupDate + ' ' + form.formPickupTime.replace('_',':'), datetimeformat);
  const d_datetime = moment(form.formDropoffDate + ' ' + form.formDropoffTime.replace('_',':'), datetimeformat);

  let age = this.props.state[locp[0]].rcmDriverAgesInfo.filter(age => age.id == this.props.state.form.formMinimumAge)[0];

  let customDimensions = {
     'dimension1': this.props.state[locp[0]].rcmAvailableCars[0].numofdays // Days
    ,'metric1': this.props.state[locp[0]].rcmAvailableCars[0].numofdays // Days
    ,'dimension2': ploc.location // Pickup Location
    ,'dimension3': p_datetime.format('DD, MMM, YYYY, hh:mma') // Pickup DateTime
    ,'dimension4': dloc.location // DropOff Location
    ,'dimension5': d_datetime.format('DD, MMM, YYYY, hh:mma') // DropOff DateTime
    ,'dimension6': age.driverage // Age
    ,'dimension7': form.formPromoCode // Promocode
  }

  this.props.state[locp[0]].rcmAvailableCars.map((item,i) => {
    
    let product = {
      ...customDimensions,
      'id': item.sippcodes,
      'name': item.categoryfriendlydescription,
      'brand': item.vehicledescription1,
      'category': 'bookings/selectvehicle (' + ploc.location + ')',
      'variant':'Vehicle',
      'price': item.totalrateafterdiscount + '',
      'quantity': 1 // always set to item qty for vehicles
    };

    products.push(product);

    impressions.push({
      ...product,
      'position': i+1,
      'list': 'bookings/selectvehicle (' + ploc.location + ')',
    });
  });
  
  // Vehicle Impressions
  window.dataLayer.push({
    'ecommerce':{
      'currencyCode': form.country=='au'? 'AUD':'NZD',
      'impressions': impressions
    },
    'event': 'AceRentals.ec',
    'eventCategory': 'Ecommerce',
    'eventAction': 'Product Impression'
  });

  // Checkout
  window.dataLayer.push({
    'ecommerce': {
      'checkout': {
        'actionField': {'step': 1},
        'products': products
      }
    },
    'event': 'AceRentals.ec',
    'eventCategory': 'Ecommerce',
    'eventAction': 'Checkout'
  });
}

const gtm = {
  enviroment: enviroment,
};

export default gtm;