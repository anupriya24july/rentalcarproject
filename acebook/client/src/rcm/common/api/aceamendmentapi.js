import base64 from 'base-64';
import superagent from 'superagent';
import amendBooking from './custom/amendbooking';
import getBookingRef from './custom/getbookingref';

const enviroment = 'live';
const defaultcountry = location.hostname.split('.').pop();
const endpoint = {
    live: {
        nz: 'http://api.acerentalcars.co.nz/?db=NZAce118',
        au: 'http://api.acerentalcars.co.nz/?db=AceAU',
    }
}

const AceAmendmentAPI = {
    enviroment: enviroment,
    defaultcountry: defaultcountry,
    dateformat: 'DD/MM/YYYY',
    amendBooking: amendBooking,
    getBookingRef: getBookingRef,
    enpoint: endpoint[enviroment][defaultcountry],
    getEndPoint: (country) => {
        return endpoint[enviroment][country];
    },
    base64: base64
};
export default AceAmendmentAPI;

export const AmendmentAPI = AceAmendmentAPI;