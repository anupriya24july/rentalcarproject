import superagent from 'superagent';
import jsonp from 'superagent-jsonp';

const step4 = (endpoint, data, props, callback, parent) => {
    window.rcmBookingReady = () => {
      
      let locp = props.state.form.formPickupLocation.split('-');

      let openModal=props.openModal;
      let msg="";

      // - check for errors
      let error = '';
      if (window.rcmErrors.length != 0){
        window.rcmErrors.map((item, index) => {
          let heading="Oops! Looks like something went wrong!";
          msg=item.error + ' Please contact support@acerentalcars.' + (location.hostname.split('.').pop() == 'nz'? 'co.nz':'com.au');
          openModal(heading,msg,0);
          if($zopim){
            $zopim.livechat.say('My booking returned an error: "' + item.error + '" Could you please contact me?');
          }
          error = item.error;
        })
      }
      
      props.setDataState({
        [locp[0]]: {
          ...props.state[locp[0]],
          rcmReservationRef: (window.rcmReservationRef? window.rcmReservationRef:''),
          rcmReservationNo: (window.rcmReservationNo? window.rcmReservationNo:''),
          rcmErrors: window.rcmErrors,
        }
      });

      //  - clean up unwanted global variables and callback function
      delete window.rcmReservationRef;
      delete window.rcmReservationNo;
      delete window.rcmErrors;

      const tags = document.getElementsByTagName('script');
      for (var i = tags.length; i >= 0; i--){
        if (tags[i] && tags[i].getAttribute('src') != null && tags[i].getAttribute('src').indexOf(endpoint + '/booking/' + data) != -1)
        tags[i].parentNode.removeChild(tags[i]);
      }

      if(callback && error == '' ){
        callback();
      }else{
         parent.setState({
          'error': error,
          isprocessing: false
        });
      }
    };

    superagent.get(endpoint + '/booking/' + data).use(jsonp).end((error, response) => {});
  }

  export default step4;