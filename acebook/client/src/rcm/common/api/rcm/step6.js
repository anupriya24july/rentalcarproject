import superagent from 'superagent';
import jsonp from 'superagent-jsonp';

const step6 = (endpoint, data, props, callback) => {
    window.rcmBookingInfoReady = () => {
      // - check for errors
      if (window.rcmErrors.length != 0){
        window.rcmErrors.map((item, index) => {
          alert(item.error);
        })
        window.rcmErrors = [];
      }
      props.setState({
          ...props.state.checkin,
          bookingInfo: window.rcmBookingInfo,
          customerInfo: window.rcmCustomerInfo,
          rateInfo: window.rcmRateInfo,
          extraFees: window.rcmExtraFees,
          paymentInfo: window.rcmPaymentInfo,
      });
      //  - clean up unwanted global variables and callback function
      delete window.rcmErrors;

      const tags = document.getElementsByTagName('script');
      for (var i = tags.length; i >= 0; i--){
        if (tags[i] && tags[i].getAttribute('src') != null && tags[i].getAttribute('src').indexOf(endpoint + '/bookinginfo/' + data) != -1)
        tags[i].parentNode.removeChild(tags[i]);
      }

      if(callback){
        callback();
      }
    };

    superagent.get(endpoint + '/bookinginfo/' + data + '#').use(jsonp).end((error, response) => {});
  }

  export default step6;