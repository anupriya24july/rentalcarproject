import superagent from 'superagent';
import jsonp from 'superagent-jsonp';
import { AceBookAPI as api } from '../../api/acebookapi';

const step1 = (endpoint, props, callback) => {

  let openModal=props.openModal;
  let msg="";

  window.rcmStep1Ready = () => {  
    if (window.rcmErrors.length != 0){
      window.rcmErrors.map((item, index) => {
          let heading="Just to let you know...";
          msg=item.error;
          openModal(heading,msg,0);
        window.location = "/bookings";
      })
      window.rcmErrors = [];
    }

    props.setDataState({
      [api.defaultcountry]: {
        ...props.state[api.defaultcountry],
        rcmLocationInfo: window.rcmLocationInfo,
        rcmOfficeTimes: window.rcmOfficeTimes,
        rcmCategoryTypeInfo: window.rcmCategoryTypeInfo,
        rcmDriverAgesInfo: window.rcmDriverAgesInfo,
        rcmHolidays: window.rcmHolidays,
        rcmErrors: window.rcmErrors,
      },
    });

      //  - clean up unwanted global variables and callback function
      delete window.rcmLocationInfo;
      delete window.rcmOfficeTimes;
      delete window.rcmCategoryTypeInfo;
      delete window.rcmDriverAgesInfo;
      delete window.rcmHolidays;
      delete window.rcmErrors;

      const tags = document.getElementsByTagName('script');
      for (var i = tags.length; i >= 0; i--){
        if (tags[i] && tags[i].getAttribute('src') != null && tags[i].getAttribute('src').indexOf(endpoint + '/step1/#') != -1)
        tags[i].parentNode.removeChild(tags[i]);
      }
      if(!msg){
        callback();
      }

    };
    superagent.get(endpoint + '/step1/#').use(jsonp).end((error, response) => {});
  }

  export default step1;