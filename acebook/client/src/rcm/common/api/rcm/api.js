import base64 from 'base-64';
import step1 from './step1';
import step2 from './step2';
import step3 from './step3';
import step4 from './step4';
import step5 from './step5';
import step6 from './step6';

const enviroment = (location.hostname.split('.').reverse().pop()!= 'dev') ? 'live':'dev';
const defaultcountry = location.hostname.split('.').pop();
const endpoint = {
  dev:{
    nz:'https://booking.acerentals.co.nz/api/3.1/Ym9va2luZy5hY2VyZW50YWxzLmNvLm56',
    au:'https://booking.acerentals.co.nz/api/3.1/Ym9va2luZy5hY2VyZW50YWxzLmNvbS5hdQ==',
  },
  live:{
    nz:'https://booking.acerentals.co.nz/api/3.1/Ym9va2luZy5hY2VyZW50YWxzLmNvLm56',
    au:'https://booking.acerentals.co.nz/api/3.1/Ym9va2luZy5hY2VyZW50YWxzLmNvbS5hdQ==',
  }
}
const RCMAPI = {
  enviroment: enviroment,
  defaultcountry: defaultcountry,
  dateformat: 'DD/MM/YYYY',
  enpoint: endpoint[enviroment][defaultcountry],
  getEndPoint: (country) => {
    return endpoint[enviroment][country];
  },
  getStep1: step1, // step1,
  getStep2: step2,
  getStep3: step3, // step3,
  createBooking: step4,
  confirmPayment: step5, // step5,
  bookingInfo: step6, // step6,
  getGUID: () => Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1),
  base64: base64,
  deposit:0.10,
  gst: defaultcountry=='nz'? 0.15 : 0.10 
};
export default RCMAPI;
