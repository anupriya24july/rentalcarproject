import superagent from 'superagent';
import jsonp from 'superagent-jsonp';
import { AceBookAPI as api } from '../../api/acebookapi';

const step3 = (endpoint,  data, props, callback) => {

  let openModal=props.openModal;
  let msg="";


    window.rcmStep3Ready = () => {
      // - check for errors
      if (window.rcmErrors.length != 0){
        window.rcmErrors.map((item, index) => {
          let heading="Just to let you know...";
          msg=item.error;
          openModal(heading,msg,0);
        })
        window.rcmErrors = [];
      }
      props.setDataState({
        [api.defaultcountry]: {
          ...props.state[api.defaultcountry],
          rcmDriverAgesInfo: window.rcmDriverAgesInfo,
          rcmLocationFees: window.rcmLocationFees,
          rcmAvailableCarDetails: window.rcmAvailableCarDetails,
          rcmMandatoryFees: window.rcmMandatoryFees,
          rcmOptionalFees: window.rcmOptionalFees,
          rcmInsuranceOptions: window.rcmInsuranceOptions,
          rcmKmCharges: window.rcmKmCharges,
          rcmRentalSource: window.rcmRentalSource,
          rcmCountries: window.rcmCountries,
          rcmAreaOfUse: window.rcmAreaOfUse,
          rcmTaxInclusive: window.rcmTaxInclusive,
          rcmTaxRate: window.rcmTaxRate,
          rcmStateTax: window.rcmStateTax,
          rcmErrors: window.rcmErrors,
        },
      });

      //  - clean up unwanted global variables and callback function
      delete window.rcmDriverAgesInfo;
      delete window.rcmLocationFees;
      delete window.rcmAvailableCarDetails;
      delete window.rcmAvailableCars;
      delete window.rcmMandatoryFees;
      delete window.rcmOptionalFees;
      delete window.rcmInsuranceOptions;
      delete window.rcmKmCharges;
      delete window.rcmRentalSource;
      delete window.rcmCountries;
      delete window.rcmAreaOfUse;
      delete window.rcmTaxInclusive;
      delete window.rcmTaxRate;
      delete window.rcmStateTax;
      delete window.rcmErrors;
      const tags = document.getElementsByTagName('script');
      for (var i = tags.length; i >= 0; i--){
        if (tags[i] && tags[i].getAttribute('src') != null && tags[i].getAttribute('src').indexOf(endpoint + '/step1/#') != -1)
        tags[i].parentNode.removeChild(tags[i]);
      }
      if(!msg){
        callback();
      }
    };
      superagent.get(endpoint + '/step3/' + data).use(jsonp).end((error, response) => {});
    }

  export default step3;