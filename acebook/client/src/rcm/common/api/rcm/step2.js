import superagent from 'superagent';
import jsonp from 'superagent-jsonp';

const step2 = (endpoint, data, props, callback) => {
    window.rcmStep2Ready = () => {
      
      let locp = props.state.form.formPickupLocation.split('-');
      let openModal=props.openModal;
      let msg="";
      // - check for errors
      if (window.rcmErrors.length != 0){
        window.rcmErrors.map((item, index) => {
          let heading="Just to let you know...";
          msg=item.error;
          openModal(heading,msg,0);
        })
      }
      props.setDataState({
        [locp[0]]: {
          ...props.state[locp[0]],
          rcmLocationInfo: window.rcmLocationInfo,
          rcmOfficeTimes: window.rcmOfficeTimes,
          rcmCategoryTypeInfo: window.rcmCategoryTypeInfo,
          rcmDriverAgesInfo: window.rcmDriverAgesInfo,
          rcmLocationFees: window.rcmLocationFees,
          rcmAvailableCarDetails: window.rcmAvailableCarDetails.filter(c => c.cattypeid != (locp[0]=='au'? 7:9999)), // exclude uber cattype
          rcmAvailableCars: window.rcmAvailableCars.filter(c => c.cattypeid != (locp[0]=='au'? 7:9999)), // exclude uber cattype
          rcmMandatoryFees: window.rcmMandatoryFees,
          rcmOptionalFees: window.rcmOptionalFees,
          rcmInsuranceOptions: window.rcmInsuranceOptions,
          rcmKmCharges: window.rcmKmCharges,
          rcmHolidays: window.rcmHolidays,
          rcmErrors: window.rcmErrors,
        },
      });

      //  - clean up unwanted global variables and callback function
      delete window.rcmLocationInfo;
      delete window.rcmOfficeTimes;
      delete window.rcmCategoryTypeInfo;
      delete window.rcmDriverAgesInfo;
      delete window.rcmLocationFees;
      delete window.rcmAvailableCarDetails;
      delete window.rcmAvailableCars;
      delete window.rcmMandatoryFees;
      delete window.rcmOptionalFees;
      delete window.rcmInsuranceOptions;
      delete window.rcmKmCharges;
      delete window.rcmHolidays;
      delete window.rcmErrors;

      const tags = document.getElementsByTagName('script');
      for (var i = tags.length; i >= 0; i--){
        if (tags[i] && tags[i].getAttribute('src') != null && tags[i].getAttribute('src').indexOf(endpoint + '/step2/' + data) != -1)
        tags[i].parentNode.removeChild(tags[i]);
      }
      if(!msg){
        callback();
      }
    };
    superagent.get(endpoint + '/step2/' + data).use(jsonp).end((error, response) => {});
  }

  export default step2;