import superagent from 'superagent';
import jsonp from 'superagent-jsonp';
import base64 from 'base-64';


const getBookingRef = (endpoint, data , props, callback) => {
    superagent.post(endpoint+'&mod=bookinginfo&a=getBookingInfo')
    .send(data).end((error, response) => {
        if(response.text){
        	 props.setState({
              ...props.state.checkin,
              bookingRef: response.text,        
          });      
          callback();
        } else{
          let openModal=props.openModal;
          let msg="";
          let heading="Oops! Looks like something went wrong!";
          msg='Sorry you wont be able to make any changes to the booking as the pick up date is closer. For making changes please contact support@acerentalcars.' + (location.hostname.split('.').pop() == 'nz'? 'co.nz':'com.au');
          openModal(heading,msg,0);
        }
    	});
  }

  export default getBookingRef;

