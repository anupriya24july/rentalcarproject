import Author from "./rcm/common/components/author";
import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Provider } from 'react-redux';

import { createStore, combineReducers, compose } from "redux";
import { persistStore, autoRehydrate } from 'redux-persist';

import { formReducer as form, dataReducer as data, rehydrateReducer as rehydrate, initialState } from "./rcm/bookings/reducer/bookingReducer";

import Search from './rcm/bookings/containers/search';
import Itinerary from './rcm/bookings/containers/itinerary';
import SelectVehicle from './rcm/bookings/containers/selectvehicle';

import AddExtras from './rcm/bookings/containers/addextras';
import PersonalDetails from './rcm/bookings/containers/personaldetails';
import Payment from './rcm/bookings/containers/payment';
import Complete from './rcm/bookings/containers/complete';

const routes = [
  { path: window.locale + '/bookings',
    exact: true,
    step: Itinerary,
  },
  { path: window.locale + '/bookings/selectvehicle',
    step: SelectVehicle,
  },
  { path: window.locale + '/bookings/addextras',
    step: AddExtras,
  },
  { path: window.locale + '/bookings/personaldetails',
    step: PersonalDetails,
  },
  { path: window.locale + '/bookings/payment',
    step: Payment,
  },
  { path: window.locale + '/bookings/complete',
    step: Complete,
  },
  { path: window.locale + '/bookings/search',
    step: Search,
  },
]

if (!String.prototype.endsWith ) {
  String.prototype.endsWith  = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
  };
}
let enhancer = {};
if(location.hostname.split('.').reverse().pop()=='dev' && (/Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor))){
  enhancer = compose(
    autoRehydrate({
      log:true
    }),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  );
}else{
  enhancer = compose(
    autoRehydrate({
      log:false
    }),
  );
}

const config = {
  whitelist:['form','data'],
};

const onPersist = () => {

  // only render after rehydrate is completed.
  if(document.getElementById('acebook-app') !== null){
    render( 
      <Provider store={store}>
        <Router>
          <div>
            {routes.map((route, index) => (<Route key={index} path={route.path} exact={route.exact}  render={ props  => <route.step openModal={openModal}  mode='home'  {...props}   />} openModal={openModal}  /> ))}
          </div>
        </Router>
      </Provider>, document.getElementById('acebook-app')
    );
  }


  if(document.getElementById('acebook-inline-widget') !== null && window.getComputedStyle(document.getElementById('acebook-inline-widget').parentElement).display != 'none'){ 
    render(
      <Provider store={store}>
        <Router><Route path={window.locale == '' ? '/': window.locale} exact={true} render={ props  => <Itinerary openModal={openModal} widget="inline" mode='home'  {...props}   />}/></Router>
      </Provider>, document.getElementById('acebook-inline-widget')
    );    
  }

  if(document.getElementById('acebook-sidebar-widget') !== null && (document.getElementById('acebook-inline-widget') === null || window.getComputedStyle(document.getElementById('acebook-inline-widget').parentElement).display == 'none') && document.getElementById('sidebar_container') === null){ 
    render(
      <Provider store={store}>
        <Router><Route path={'/'}  exact={false} render={ props  => <Itinerary openModal={openModal}  widget="sidebar" mode='home'   {...props} />}/></Router>
      </Provider>, document.getElementById('acebook-sidebar-widget')
    );    
  }

  if(document.getElementById('acebook-inline-widget') !== null && window.getComputedStyle(document.getElementById('acebook-inline-widget').parentElement).display != 'none' && window.location.href.indexOf("/experience/") !== -1){ 
    render(
      <Provider store={store}>
        <Router>
          <Route path='/experience/:portal' exact={true} render={ props  => <Itinerary openModal={openModal} widget="inline"  mode='experience'  {...props} />}/>
        </Router>
      </Provider>, document.getElementById('acebook-inline-widget')
    );    
  }

}

const store = createStore(
  combineReducers({
    form, 
    data, 
    rehydrate
  }),
  initialState,
  enhancer
);

if(location.pathname == '/' || window.location.href.indexOf("/experience/") > -1){
  persistStore(store, config , onPersist).purge();
}else{
  persistStore(store, config , onPersist);
}

// Start of Zendesk Chat Script
(function(d,s){
  var z=function(c){
    z._.push(c)
  },
  $=z.s=d.createElement(s),
  e=d.getElementsByTagName(s)[0];
  z.set=function(o){
    z.set._.push(o)
  };
  z._=[];
  z.set._=[];
  $.async=!0;
  $.setAttribute("charset","utf-8");
  $.onreadystatechange = function() {
    if (this.readyState == 'complete') {
      initializeZopim();
    }
  } 
  $.onload = initializeZopim;
  $.src="https://v2.zopim.com/?34mP3wfK04QcDZNtNjf8zFMAtNDJCdY1";
  z.t=+new Date;
  $.type="text/javascript";
  e.parentNode.insertBefore($,e)
})(document,"script");

function initializeZopim(){
  $zopim.livechat.button.setOffsetVertical(9000000000000);
  $zopim.livechat.button.setOffsetVerticalMobile(9000000000000);
  $zopim.livechat.window.setPosition('tr');
}

// End of Zendesk Chat Script

function openModal(heading,msg,mode) {
  if(mode){
    document.querySelector('.modal-message-title').innerHTML = 'Our Cover Options';
    document.querySelector('.modal-message-content').classList.add('modal-iframe-wrapper');
    document.querySelector('.modal-message-content').innerHTML = '<iframe class="modal-iframe" src="https://www.youtube.com/embed/HB1XlOBIT6c?autoplay=1" frameborder="0" allowfullscreen></iframe>';
  } else {
    document.querySelector('.modal-message-title').innerHTML=heading;
    document.querySelector('.modal-message-content').innerHTML=msg;       
    this.props.setFormState({
      isprocessing: false
    });    
  }
  document.querySelector('.js-modal').classList.remove('is-hidden');
}