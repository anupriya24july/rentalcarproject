import React from 'react';
import ReactSVG from 'react-svg';

export const Filters = (props) => {
  return (
    <div className="l-cars__filter hidden">
      <div className="l-vehicle-filter">
        <div className="l-vehicle-filter__label">Filters:</div>
        <div className="l-vehicle-filter__types">
            <div className="l-vehicle-filter__type l-form__field--checkbox">
              <input className="l-form__input" type="checkbox" name="Filter_" id="Filter_" />
              <label htmlFor="Filter_" className="l-form__label">title</label>
            </div>
        </div>
        <div className="l-vehicle-filter__sort">
          <div className="l-vehicle-filter__sort-field l-form__select">
            <select className="l-form__input l-form__input--select" name="Filter_Sort" id="Filter_Sort">
              <option value="Popularity">Ordered by Price</option>
            </select>
            <ReactSVG path="/themes/base/production/svg/defaults/select-arrow.svg" className='svg-defaults/select-arrow'/>
          </div>
        </div>
      </div>
    </div>
  );
}