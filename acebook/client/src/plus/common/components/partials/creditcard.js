import React from 'react';
import ReactSVG from 'react-svg';
import numeral from 'numeral';
import Cleave from 'cleave.js/react';

export const CreditCard = (props) => {
  return (
    <div className="l-booking__step">
      <h2 className="l-booking__step-heading">
        2. {window.labels.CreditCardLabel}
        <div className="l-booking__cc-brands">
          <img className="l-booking__cc-brand is-active js-cc-visa" src="/themes/base/production/images/cc-brands/visa.png" alt="Visa" />
          <img className="l-booking__cc-brand is-active js-cc-mastercard" src="/themes/base/production/images/cc-brands/mastercard.png" alt="MasterCard" />
          <img className="l-booking__cc-brand is-active js-cc-unionpay" src="/themes/base/production/images/cc-brands/unionpay.png" alt="UnionPay" />
        </div>
      </h2>
      {/*
      <div className="alert alert-danger">
        <strong>Warning!</strong> {props.state.error}
      </div>
      */}
      <div className="l-booking__form l-form">
        <div className="l-form__field">
          <label htmlFor="Booking_CardHolder" className="l-form__label">{window.labels.CardHolderLabel}*</label>
          <input className="l-form__input" type="text" name="name" value={props.state.name}  onChange={props.onCCUpdate}/>
        </div>
        <div className="l-form__field">
          <label htmlFor="Booking_CCNum" className="l-form__label">{window.labels.CardNumberLabel}*</label>
          <Cleave options={{creditCard: true,
        delimiter: ' - '}} className="l-form__input js-cc-num" type="text" name="num" value={props.state.num} onChange={props.onCCUpdate}/>
        </div>

        <div className="l-form__field--double">
          <div className="l-form__field">
            <label htmlFor="Booking_CCNum" className="l-form__label">{window.labels.ExpiryDateLabel}*</label>
            <Cleave  options={{date: true,
        datePattern: ['m', 'y'],
        delimiter: ' / '}} className="l-form__input js-cc-exp" type="text" name="exp" placeholder="MM/YY" value={props.state.exp} onChange={props.onCCUpdate}/>
          </div>
          <div className="l-form__field">
            <label htmlFor="Booking_CCNum" className="l-form__label">
              {window.labels.CvcLabel}*
              {/*
              <button className="l-form__label-tooltip js-tooltip" title="I'm a tooltip!">
                <ReactSVG path="/themes/base/production/svg/tooltip.svg" classNameName='svg-tooltip' />
              </button>
              */}
            </label>
            <input className="l-form__input js-cc-cvc" type="text" maxLength="4" name="cvc" value={props.state.cvc} onChange={props.onCCUpdate} />
          </div>
        </div>
      </div>
    </div>
  )
}
