import React from 'react';
import ReactSVG from 'react-svg';
import moment from 'moment';
import numeral from 'numeral';

export const VehiclePanel = (props) => {
  let form = props.state.form;
  let locp = form.formPickupLocation.split('-');
  
  const activeCountry =  props.state[locp[0]];

  const p_location = activeCountry.Locations.find(loc => loc.id == form.formPickupLocation.split('-')[1]);
  const d_location = activeCountry.Locations.find(loc => loc.id == form.formDropoffLocation.split('-')[1]);

  const vehicle = cars? cars:[];
  const selectedCar = vehicle[form.selectedCarClass];

  const datetimeformat = 'YYYY-MM-DDTHH:mm:ss';
  const p_datetime = moment(form.formPickupDate + 'T' + form.formPickupTime, datetimeformat);
  const d_datetime = moment(form.formDropoffDate + 'T' + form.formDropoffTime, datetimeformat);

  const estimate = activeCountry.Estimate.RenterEstimate;


  const allCharges = additionalCharges? additionalCharges:[];

  const allCovers = covers? covers:[];

  const allOptions = options? options:[];
  return (
    <div className="l-vehicle-panel">
      <div className="l-vehicle-panel__inner">
        <h2 className="l-vehicle-panel__title">{selectedCar.title}</h2>
        <h3 className="l-vehicle-panel__subtitle">{selectedCar.age}</h3>
        <img className="l-vehicle-panel__image" src={selectedCar? selectedCar.image:''} alt={selectedCar.title} />
        <ul className="l-vehicle-panel__specifications">
          {selectedCar.specs.map((specs,i) =>
            <li key={i} className={"l-vehicle-panel__specification"} style={{display:(specs.fullwidth=='1'? 'none':'')}}>
              {specs.icon
                ? <img src={specs.icon} alt={specs.title} />
                : ''
              }
              {specs.title}
            </li>
          )}
        </ul>

        { props.showtotal && activeCountry.Estimate.hasOwnProperty('@attributes')
          ? <div className="l-vehicle-panel__total">
              <div style={{fontSize:'larger', marginTop:'20px'}}>{window.labels.YourQuoteLabel}</div>
              <hr/>
              <div className="l-vehicle-panel__total-details">
                <div className="l-vehicle-panel__total-details-panel">
                {estimate.Charge.map((ch,i) => 
                  ch['@attributes'].desc=='DAYS' ?
                  <div>
                    <div class="l-vehicle-panel__total-item-label">Vehicle Cost</div>
                      <div key={i} className="l-vehicle-panel__total-item"> 
                        <div>
                          <div className="l-vehicle-panel__total-item-name"></div>
                          <div className="l-vehicle-panel__total-item-totals">
                            <div className="l-vehicle-panel__total-item-subtotal" style={{float:'left'}}>{ch['@attributes'].quantity} Day(s) </div>
                            <div className="l-vehicle-panel__total-item-total" style={{float:'right'}}>{numeral(ch['@attributes'].total).format('$0,0.00')}</div>
                          </div>
                        </div>
                        <br/>
                      </div>
                      <br/>
                    </div>
                  : ""
                  )}
                  {estimate.Charge.map((ch,i) => 
                    allCharges.map((charg,i) =>
                      charg.Code==ch['@attributes'].desc && charg.Code!='GST' ?
                        <div>
                          <div class="l-vehicle-panel__total-item-label">Mandatory Fees</div>
                          <div key={i} className="l-vehicle-panel__total-item"> 
                            <div>
                              <div className="l-vehicle-panel__total-item-name"></div>
                              <div className="l-vehicle-panel__total-item-totals">
                                <div className="l-vehicle-panel__total-item-subtotal" style={{float:'left'}}>{ch['@attributes'].desc} x {ch['@attributes'].quantity}</div>
                                <div className="l-vehicle-panel__total-item-total" style={{float:'right'}}>{numeral(ch['@attributes'].total).format('$0,0.00')}</div>
                              </div>
                            </div>
                            <br/>
                          </div>
                          <br/>
                        </div>
                        : ""
                  ))}
                  {estimate.Charge.map((ch,i) => 
                    allCovers.map((cover,i) =>
                      cover.Code==ch['@attributes'].code ?
                        <div>
                          <div class="l-vehicle-panel__total-item-label">Cover</div>
                          <div key={i} className="l-vehicle-panel__total-item"> 
                          <div>
                            <div className="l-vehicle-panel__total-item-name"></div>
                            <div className="l-vehicle-panel__total-item-totals">
                              <div className="l-vehicle-panel__total-item-subtotal" style={{float:'left'}}>{ch['@attributes'].desc} x {ch['@attributes'].quantity}</div>
                              <div className="l-vehicle-panel__total-item-total" style={{float:'right'}}>{numeral(ch['@attributes'].total).format('$0,0.00')}</div>
                            </div>
                          </div>
                          <br/>
                        </div>
                      <br/>
                      </div>
                        : ""
                  ))}
                  {form.selectedAncillaries.length>0
                    ?
                      <div class="l-vehicle-panel__total-item-label">Ancillary Options</div>
                    :""
                   }
                  {estimate.Charge.map((ch,i) => 
                    allOptions.map((option,i) =>
                      option.Code==ch['@attributes'].code ?
                        <div>
                          <div key={i} className="l-vehicle-panel__total-item"> 
                            <div>
                              <div className="l-vehicle-panel__total-item-name"></div>
                              <div className="l-vehicle-panel__total-item-totals">
                                <div className="l-vehicle-panel__total-item-subtotal" style={{float:'left'}}>{ch['@attributes'].desc} x {ch['@attributes'].quantity}</div>
                                <div className="l-vehicle-panel__total-item-total" style={{float:'right'}}>{numeral(ch['@attributes'].total).format('$0,0.00')}</div>
                              </div>
                            </div>
                            <br/>
                          </div>
                        </div>
                      : ""
                  ))}
                <br/>
                  {estimate.Charge.map((ch,i) => 
                      ch['@attributes'].desc=='GST' ?
                        <div>
                          <div class="l-vehicle-panel__total-item-label"></div>
                          <div key={i} className="l-vehicle-panel__total-item"> 
                            <div>
                              <div className="l-vehicle-panel__total-item-name"></div>
                              <div className="l-vehicle-panel__total-item-totals">
                                <div className="l-vehicle-panel__total-item-subtotal" style={{float:'left'}}>* GST on Total Price</div>
                                <div className="l-vehicle-panel__total-item-total" style={{float:'right'}}>({numeral(ch['@attributes'].total).format('$0,0.00')})</div>
                              </div>
                            </div>
                            <br/>
                          </div>
                        </div>
                        : ""
                  )}
                </div>
              </div>
              <hr/>
              <div className="l-vehicle-panel__total-label" style={{float:'left'}}>{window.labels.TotalPriceLabel} 
                {form.country=='au'
                  ? <span style={{textTransform: 'none', fontSize: 'smaller'}}> (incl. GST)</span>
                  : ''
                }
              </div>
              <div className="l-vehicle-panel__total-price" style={{float:'right'}}>{numeral(estimate['@attributes'].total).format('$0,0.00')} <span>{form.country=='au'? 'AUD':'NZD'}</span></div>
            </div>
          : ''
        }
      </div>
    </div>
  );
}