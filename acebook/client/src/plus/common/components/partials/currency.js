import React from 'react';
import ReactSVG from 'react-svg';

export const Currency = (props) => {
  return (
    <div className="l-booking__currency">
      <div className="l-booking__currency-label">Currency:</div>
      <div className="l-booking__currency-toggle">
        <div className="l-booking__currency-selected" role="button" tabIndex="0">
          <ReactSVG path="/themes/base/production/svg/flags/nz.svg" className='svg-flags/nz l-booking__currency-flag'/>
          <div className="l-booking__currency-acronym">NZD</div>
          <ReactSVG path="/themes/base/production/svg/mini-arrow.svg" className='svg-mini-arrow'/>
        </div>
        <ul className="l-booking__currency-options">
          <li className="l-booking__currency-option">
            <ReactSVG path="/themes/base/production/svg/flags/au.svg" className='svg-flags/au l-booking__currency-flag'/>
            AUD
          </li>
          <li className="l-booking__currency-option">
            <ReactSVG path="/themes/base/production/svg/flags/nz.svg" className='svg-flags/nz l-booking__currency-flag'/>
            NZD
          </li>
        </ul>
      </div>
    </div>
  );
}