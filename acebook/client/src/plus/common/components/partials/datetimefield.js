import React from 'react';
import ReactSVG from 'react-svg';

export const DateTimeField = (props) => {
   return (
    <div className="l-form__field l-form__field--datetime notranslate">
      <label htmlFor={props.prefix + "_" + props.name + "_Date"} className="l-form__label">
        {props.labeldate}
      </label>
      <div className="l-form__datetime">
        <div className="l-form__date js-date-container">
          <input type="text" className={"l-form__input l-form__input--date js-date " + props.name} id={props.prefix + "_" + props.name + "_Date"}  name={"form" + props.name + "Date"} readOnly/>
          <ReactSVG path="/themes/base/production/svg/calendar.svg" classNameName='svg-calendar'/>
        </div>
        <div className="l-form__time">
          <label htmlFor={props.prefix + "_" + props.name + "_Time"} className="sr-only">
            {props.labeltime}
          </label>
          <select className="l-form__input l-form__input--time l-form__input--select" id={props.prefix + "_" + props.name + "_cmbPickupTime"} name={"form" + props.name + "Time"} value={props.value} onChange={e => props.onFormChange(e)}>
           {props.timeArr
                ?  props.timeArr.map((t,i) => <option value={t.split(',')[0]} key={i}>{t.split(',')[1]}</option>)
                  : ''}
          </select>
          <ReactSVG path="/themes/base/production/svg/clock.svg" className='svg-clock'/>
        </div>
      </div>
    </div>
  );
}