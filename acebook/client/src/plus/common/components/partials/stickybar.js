import React from 'react';
import ReactSVG from 'react-svg';
import moment from 'moment';
import numeral from 'numeral';

export const StickyBar = (props) => {

  let form = props.state.form;
  let locp = form.formPickupLocation.split('-');
  
  const activeCountry =  props.state[locp[0]];

  const p_location = activeCountry.Locations.find(loc => loc.id == form.formPickupLocation.split('-')[1]);
  const d_location = activeCountry.Locations.find(loc => loc.id == form.formDropoffLocation.split('-')[1]);
  const vehicle = cars? cars:[];
  const selectedCar = vehicle[form.selectedCarClass];

  const datetimeformat = 'YYYY-MM-DDTHH:mm:ss';
  const p_datetime = moment(form.formPickupDate + 'T' + form.formPickupTime, datetimeformat);
  const d_datetime = moment(form.formDropoffDate + 'T' + form.formDropoffTime, datetimeformat);

  const estimate = activeCountry.Estimate.RenterEstimate;


  const allCharges = additionalCharges? additionalCharges:[];

  const allCovers = covers? covers:[];

  const allOptions = options? options:[];

  return (
    <div className="l-booking-summary-bar">
      <div className="l-booking-summary-bar__blocks">
        <div className="l-booking-summary-bar__block">
          <div className="l-booking-summary-bar__block-label">{window.labels.PickupLabel}</div>
          <div className="l-booking-summary-bar__block-location">{p_location.location}</div>
          <div className="l-booking-summary-bar__block-date">{p_datetime.format('DD, MMM, YYYY, hh:mma')}</div>
        </div>

        <div className="l-booking-summary-bar__block">
          <div className="l-booking-summary-bar__block-label">{window.labels.DropoffLabel}</div>
          <div className="l-booking-summary-bar__block-location">{d_location.location}</div>
          <div className="l-booking-summary-bar__block-date">{d_datetime.format('DD, MMM, YYYY, hh:mma')}</div>
        </div>
      </div>

      <a href="#" className="l-booking-summary-bar__modify" onClick={props.togglePanel}>
        {window.labels.ModifyTripDetailsLabel}
      </a>
      { props.showtotal && activeCountry.Estimate.hasOwnProperty('@attributes')
       ?  (<div className="l-booking-summary-bar__total">
            <div className="l-booking-summary-bar__total-label">{window.labels.TotalPriceLabel} 
              {form.country=='au'
                ? <span style={{textTransform: 'none', fontSize: 'smaller'}}> (incl. GST)</span>
                : ''
              }
            </div>
            <div className="l-booking-summary-bar__total-price">{numeral(estimate['@attributes'].total).format('$0,0.00')} <span>{form.country=='au'? 'AUD':'NZD'}</span></div>

            <div className="l-booking-summary-bar__total-details">
              <div className="l-booking-summary-bar__total-details-trigger">
                <ReactSVG path="/themes/base/production/svg/large-arrow.svg" className='svg-large-arrow'/>
              </div>

              <div className="l-booking-summary-bar__total-details-panel">
                
                {estimate.Charge.map((ch,i) =>
                  ch['@attributes'].desc=='DAYS' ?
                    <div key={i} className="l-booking-summary-bar__total-item"> 
                      <div className="l-booking-summary-bar__total-item-label">Vehicle Cost</div>
                      <div className="l-booking-summary-bar__total-item-totals">
                        <div className="l-booking-summary-bar__total-item-subtotal">{ch['@attributes'].quantity} Day(s) </div>
                        <div className="l-booking-summary-bar__total-item-total ">{numeral(ch['@attributes'].total).format('$0,0.00')}</div>
                      </div>
                    </div>
                    : ""
                )}
                {estimate.Charge.map((ch,i) =>
                    allCharges.map((charg,i) =>
                      charg.Code==ch['@attributes'].desc && charg.Code!='GST' ?
                      <div key={i} className="l-booking-summary-bar__total-item"> 
                        <div className="l-booking-summary-bar__total-item-label">Mandatory Fees</div>
                        <div className="l-booking-summary-bar__total-item-totals">
                          <div className="l-booking-summary-bar__total-item-subtotal">{ch['@attributes'].desc} x {ch['@attributes'].quantity}</div>
                          <div className="l-booking-summary-bar__total-item-total ">{numeral(ch['@attributes'].total).format('$0,0.00')}</div>
                        </div>
                      </div>
                      :""
                ))}
                {estimate.Charge.map((ch,i) =>
                    allCovers.map((cover,i) =>
                      cover.Code==ch['@attributes'].code ?
                      <div key={i} className="l-booking-summary-bar__total-item"> 
                        <div className="l-booking-summary-bar__total-item-label">Cover</div>
                        <div className="l-booking-summary-bar__total-item-totals">
                          <div className="l-booking-summary-bar__total-item-subtotal">{ch['@attributes'].desc} x {ch['@attributes'].quantity}</div>
                          <div className="l-booking-summary-bar__total-item-total ">{numeral(ch['@attributes'].total).format('$0,0.00')}</div>
                        </div>
                      </div>
                      :""
                ))} 
                {form.selectedAncillaries.length>0
                    ?                        
                  <div className="l-booking-summary-bar__total-item"><div className="l-booking-summary-bar__total-item-label">Ancillary Options</div>
                    
                  {estimate.Charge.map((ch,i) =>
                      allOptions.map((option,i) =>
                        option.Code==ch['@attributes'].code ?
                          <div key={i} className="l-booking-summary-bar__total-item-totals">
                            <div className="l-booking-summary-bar__total-item-subtotal">{ch['@attributes'].desc} x {ch['@attributes'].quantity}</div>
                            <div className="l-booking-summary-bar__total-item-total ">{numeral(ch['@attributes'].total).format('$0,0.00')}</div>
                          </div>
                        :""
                  ))}
                  </div>
                  :""}
                {estimate.Charge.map((ch,i) =>
                    ch['@attributes'].desc=='GST' ?
                      <div key={i} className="l-booking-summary-bar__total-item"> 
                        <div className="l-booking-summary-bar__total-item-label"></div>
                        <div className="l-booking-summary-bar__total-item-totals">
                          <div className="l-booking-summary-bar__total-item-subtotal">* GST on TOTAL PRICE</div>
                          <div className="l-booking-summary-bar__total-item-total ">({numeral(ch['@attributes'].total).format('$0,0.00')})</div>
                        </div>
                      </div>
                      :""
                )}
              </div>  
            </div>
          </div>)
        : ''
      }
    </div>
  );
}