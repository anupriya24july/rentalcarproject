import React from 'react';
import ReactSVG from 'react-svg';
import numeral from 'numeral';
import { OptionCard }  from './optioncard';

export const PaymentAmount = (props) => {
  let form = props.state.form;
  let locp = form.formPickupLocation.split('-');
  
  const activeCountry =  props.state[locp[0]];
  let mode=props.mode? props.mode:"";
  const total = activeCountry.Estimate.RenterEstimate['@attributes'].total;
  let pay_options = mode ?  [
    {
      '@attributes': {
        type: 'fixed',
        note: '',
        name: 'Pay Balance Amount',
        rate: total,
        id: 'full'
      }
    }
  ] : [
    {
      '@attributes': {
        type: 'fixed',
        note: '* + ' + numeral(total * 0.10).format('$0,0.00') + ' on pickup',
        name: window.labels.PayPercentLabel,
        rate: total * 0.10,
        id:'deposit',
        extranotes: (form.country=='au')? <div className="c-option-card__note" style={{marginBottom:'15px'}}>Balance subject to credit card surcharges <br />(See Below for details)</div> :false
      }
    },
    {
      '@attributes': {
        type: 'fixed',
        note: '',
        name: window.labels.PayFullLabel,
        rate: total,
        id: 'full',
        extranotes: (form.country=='au')? <div className="c-option-card__note">No Credit card Surcharge applies</div>:false
      }
    }
  ]
  return (
    <div className="l-booking__step">
        <h2 className="l-booking__step-heading">
          1. {window.labels.PaymentAmountLabel}
        </h2>

        <div className="l-booking__options">
          {pay_options.map((po,i) => (
            <OptionCard key={i} Option={po} isTotal={true} Note={po['@attributes'].note} Selected={po['@attributes'].id == props.state.form.selectedPaymentAmount} onClick={()=>props.onSelectPaymentAmount(po['@attributes'].id)}/>
          ))}
        </div>
        {form.country=="au"
          ? <div style={{fontStyle: 'italic', maxWidth: '740px', margin: '20px 0 0'}}>
              <p>For all in-branch payments a credit card surcharge of 1.35% applies when payment is made by Visa, MasterCard, and Unionpay. For Amex and Diners Cards a surcharge of 2.97% applies.</p>
            </div>
          : ''
        }
    </div>
  )
}
