import React from 'react';
import ReactSVG from 'react-svg';
import moment from 'moment';
import numeral from 'numeral';

export const Success = (props) => {
  let form = props.state.form;
  let locp = form.formPickupLocation.split('-');
  
  const activeCountry =  props.state[locp[0]];

  const p_location = activeCountry.Locations.find(loc => loc.id == form.formPickupLocation.split('-')[1]);
  const d_location = activeCountry.Locations.find(loc => loc.id == form.formDropoffLocation.split('-')[1]);
  const vehicle = cars? cars:[];
  const selectedCar = vehicle[form.selectedCarClass];

  const datetimeformat = 'YYYY-MM-DDTHH:mm:ss';
  const p_datetime = moment(form.formPickupDate + 'T' + form.formPickupTime, datetimeformat);
  const d_datetime = moment(form.formDropoffDate + 'T' + form.formDropoffTime, datetimeformat);

  const estimate = activeCountry.Estimate.RenterEstimate;

  const total = activeCountry.Estimate.RenterEstimate['@attributes'].total;
  const paymentamount = numeral((form.selectedPaymentAmount == 'full')? total : (total * props.deposit)).format('0.00')


  const allCharges = additionalCharges? additionalCharges:[];

  const allCovers = covers? covers:[];

  const allOptions = options? options:[];

  return (
    <div className="l-success">
      <div className="container">
        <h1 className="l-success__heading">{props.title}</h1>
        <div className="l-success__content" style={{margin: '0 auto 20px', width:'100%'}}>
          {props.content}
          <div id="booking_number" data-booking-number={activeCountry.ReservationNo} style={{fontSize:'30px',border:'1px solid',width:'200px',margin:'20px auto 0px'}}>{activeCountry.ReservationNo}</div>
          <div style={{fontSize:'14px'}}>BOOKING NUMBER</div>
        </div>


      </div>
      <div className="l-success__container">
        <div className="l-success__itinerary">
          <div className="l-success__itinerary-item">
            <div className="l-success__itinerary-label">
              Pick Up Location
            </div>
            {p_location.location}
          </div>

          <div className="l-success__itinerary-item">
            <div className="l-success__itinerary-label">
              Pick Up Date
            </div>
            {p_datetime.format('DD, MMM, YYYY, hh:mma')}
          </div>

          <div className="l-success__itinerary-arrow">
            <ReactSVG path="/themes/base/production/svg/itinerary-arrow.svg" className='svg-itinerary-arrow'/>
          </div>

          <div className="l-success__itinerary-item">
            <div className="l-success__itinerary-label">
              Drop Off Location
            </div>
            {d_location.location}
          </div>

          <div className="l-success__itinerary-item">
            <div className="l-success__itinerary-label">
              Drop Off Date
            </div>
            {d_datetime.format('DD, MMM, YYYY, hh:mma')}
          </div>
        </div>
        <div className="l-success__main">
          <div className="l-success__vehicle">
            <h2 className="l-success__sub-heading">Customer Details</h2>
            <div className="l-success__vehicle-info js-vehicle-card">
              <div className="l-success__vehicle-title">{form.formCustomerData.fnm + ' ' + form.formCustomerData.lnm}</div>
              <div className="l-success__vehicle-subtitle">{form.formCustomerData.eml}</div>
              <div className="l-success__vehicle-subtitle">{form.formCustomerData.mob}</div>
              <div className="l-success__vehicle-subtitle">{form.formCustomerData.rmk}</div>
            </div>
            <br /><br />
            <h2 className="l-success__sub-heading">Vehicle</h2>
            <div className="l-success__vehicle-info js-vehicle-card">
              <div className="l-success__vehicle-title">{selectedCar.title}</div>
              <div className="l-success__vehicle-subtitle">{selectedCar.age}</div>
              <div className="l-success__vehicle-image">
                <img src={selectedCar? selectedCar.image:''} alt={selectedCar.title} />
              </div>
            </div>
          </div>

          <div className="l-success__summary">
            <h2 className="l-success__sub-heading">Pricing Summary</h2>








            {estimate.Charge.map((ch,i) =>
                  ch['@attributes'].desc=='DAYS' ?
            <div key={i} className="l-success__summary-rows">
              <div className="l-success__summary-row">
                <div className="l-success__summary-label">Vehicle Cost</div>
                <div className="l-success__summary-price">
                  <span className="l-success__summary-price-math"></span>x {ch['@attributes'].quantity} Day(s) = {numeral(ch['@attributes'].total).format('$0,0.00')}
                </div>
              </div>
            </div> :""
            )}

            {estimate.Charge.map((ch,i) =>
                    allCharges.map((charg,i) =>
                      charg.Code==ch['@attributes'].desc && charg.Code!='GST' ?
            <div key={i} className="l-success__summary-rows">
              <div className="l-success__summary-row">
                <div className="l-success__summary-label">Mandatory Fees - {ch['@attributes'].desc}</div>
                <div className="l-success__summary-price">
                  <span className="l-success__summary-price-math"></span>x {ch['@attributes'].quantity} = {numeral(ch['@attributes'].total).format('$0,0.00')}
                </div>
              </div>
            </div> :""
            ))}

            {estimate.Charge.map((ch,i) =>
                    allCovers.map((cover,i) =>
                      cover.Code==ch['@attributes'].code ?
            <div key={i} className="l-success__summary-rows">
              <div className="l-success__summary-row">
                <div className="l-success__summary-label">Cover - {ch['@attributes'].desc}</div>
                <div className="l-success__summary-price">
                  <span className="l-success__summary-price-math"></span>x {ch['@attributes'].quantity} = {numeral(ch['@attributes'].total).format('$0,0.00')}
                </div>
              </div>
            </div> :""
            ))}
            {estimate.Charge.map((ch,i) =>
                      allOptions.map((option,i) =>
                        option.Code==ch['@attributes'].code ?
            <div key={i} className="l-success__summary-rows">
              <div className="l-success__summary-row">
                <div className="l-success__summary-label">{ch['@attributes'].desc} </div>
                <div className="l-success__summary-price">
                  <span className="l-success__summary-price-math"></span>x {ch['@attributes'].quantity} = {numeral(ch['@attributes'].total).format('$0,0.00')}
                </div>
              </div>
            </div> :""
            ))}
            {estimate.Charge.map((ch,i) =>
                    ch['@attributes'].desc=='GST' ?
            <div key={i} className="l-success__summary-rows">
              <div className="l-success__summary-row">
                <div className="l-success__summary-label">* GST on TOTAL PRICE</div>
                <div className="l-success__summary-price">
                  <span className="l-success__summary-price-math"></span> {numeral(ch['@attributes'].total).format('$0,0.00')}
                </div>
              </div>
            </div> :""
            )}


            <div className="l-success__summary-total">
              <div className="l-success__summary-total-label">Total Cost</div>
              <div id="booking_total" data-booking-total={numeral(total).format('00.00')} className="l-success__summary-total-price">
                {numeral(total).format('$0,0.00')}
              </div>
            </div>
            <div className="l-success__summary-total" style={{margin:'0px', paddingTop:'0px'}}>
              <div className="l-success__summary-total-label">Amount Paid</div>
              <div className="l-success__summary-total-price">
                {numeral(paymentamount).format('$0,0.00')}
              </div>
            </div>
            <div className="l-success__summary-total" style={{margin:'0px', paddingTop:'0px'}}>
              <div className="l-success__summary-total-label">Balance Owing</div>
              <div className="l-success__summary-total-price">
                {numeral(total - paymentamount).format('$0,0.00')}
              </div>
            </div>
          </div>
        </div>

        <div className="l-success__checkin">
          <h3 className="l-success__checkin-title">
            Want to save time during pickup? Check in Early!
          </h3>
           {form.country=="nz"? <a href="https://secure.acerentalcars.co.nz" className="btn btn--secondary l-success__checkin-action">
            Check in now
          </a>
            : <a href="https://secure.acerentalcars.com.au" className="btn btn--secondary l-success__checkin-action">
            Check in now
          </a>
          }
          
        </div>

        <div className="l-success__footer">
        {/*
          <a href="#" className="l-success__apple-wallet">
            <img src="/themes/base/production/images/apple-wallet.png" alt="Add to Apple Wallet" />
          </a>
        */}
          <a href="javascript:window.print();" className="l-success__pdf">
            Print Rental Summary
          </a>
        </div>
      </div>
  </div>
  )
}
