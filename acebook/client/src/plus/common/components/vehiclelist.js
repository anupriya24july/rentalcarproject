import React from 'react';
import ReactSVG from 'react-svg';
import numeral from 'numeral';

export const VehicleList = (props) => {
  let form = props.state.form;
  let curentCountry = props.state[form.country];
  let locp = form.formPickupLocation.split('-');
  let vehicle = cars? cars:[];
  let totalmandatoryfee = [];
  return (
    <div className="l-cars__cards">
  {curentCountry.Rates.filter(item => {return vehicle[item.Class]? true:false }).map((item, index) => (
      <div key={index} className="c-vehicle-card c-vehicle-card__js-vehicle-card" onMouseLeave={(e) => props.hideMoreSpecs(e)}>
      { item.Class== form.selectedCarClass
        ? <div className="c-option-card__label">
            Selected Vehicle
          </div>
        : ""
      }
        <div className="c-vehicle-card__title notranslate">{vehicle[item.Class]? vehicle[item.Class].title:''}</div>
        <div className="c-vehicle-card__subtitle">{vehicle[item.Class]? vehicle[item.Class].age:''}</div>
        <div className="c-vehicle-card__image">
          <img src={vehicle[item.Class]? vehicle[item.Class].image:''} alt="" />
        </div>
        <ul className="c-vehicle-card__specifications details hidden">
          {(vehicle[item.Class] && vehicle[item.Class].specs)
            ? vehicle[item.Class].specs.filter(specs => { if(specs.fullwidth=='0') return specs}).map((specs,i) =>
                <li key={i} className={'c-vehicle-card__specification js-vehicle-spec' + ((specs.fullwidth=='1')? ' c-vehicle-card__specification--fw':'') + ' js-vehicle-spec-more toggle-active'}>
                  {specs.icon
                    ? <img className="c-vehicle-card__specification-icon" src={specs.icon} alt={specs.title} />
                    : ''
                  }
                  {specs.title}
                </li>
              )
            : ''
          }
        </ul>
        <button className="c-vehicle-card__specifications-more" onClick={(e) => props.toggleMoreSpecs(e)}>
          <span>Click here to view vehicle & rate details</span>
        </button>
      { props.cta =='Book' && item.Estimate
      ?           
        <div className="c-vehicle-card__footer c-vehicle-card__footer--book">
          {item.available == 0 && !(item.availablemsg.indexOf('Unavailable') >= 0)
            ? <div className="c-vehicle-card__pricing" style={{fontSize: 'smaller', textAlign: 'center', color: '#0056a7'}}>
                {item.availablemsg}
              </div>
            : <div className="c-vehicle-card__pricing">
                <div className="c-vehicle-card__price-label">
                  {window.labels.TotalPriceLabel}
                  {form.country=='au'
                    ? <span style={{textTransform: 'none', fontSize: 'smaller'}}> (incl. GST)</span>
                    : ''
                  }
                </div>
                <div className="c-vehicle-card__price">
                  {numeral(item.Estimate:0).format('$0,0.00')} <span>{form.country=='au'? 'AUD':'NZD'}</span>
                </div>
              </div>
          }
          {item.Availability == 'Available'
            ? (props.state.form.isprocessing
              ? <button className="c-vehicle-card__action btn "  disabled>
                  {props.state.form.selectedCarClass==item.Class? window.labels.ProcessingLabel + "..." : window.labels.SelectLabel}
                </button> 
              : 

              (form.isAmendment ?
                 <button className="c-vehicle-card__action btn btn--primary" onClick={ e => props.onSelectVehicle(item, index+1, e)}>
                  Change
                </button>   :
              <button className="c-vehicle-card__action btn btn--primary" onClick={ e => props.onSelectVehicle(item, index+1, e)}>
                  {window.labels.SelectLabel}
                </button>)  ) 
            : ''
          }
          {item.Availability != 'Available'
            ? <a href="#" className="btn" style={{backgroundColor: '#ccc', borderColor: '#ccc'}}>
                SOLD OUT
              </a>
            : ''
          }
        </div>
      : <div className="c-vehicle-card__footer">
          <a href="{$Link}" className={'c-vehicle-card__more btn' + (props.type == 'lg')? ' btn--lg':' btn--primary'}>
            Learn More
          </a>
        </div>
      }
      </div>
    ))}
    </div>
  );
}