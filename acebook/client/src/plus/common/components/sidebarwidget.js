import React from 'react';
import ReactSVG from 'react-svg';
import moment from 'moment';
import { DateTimeField }  from './partials/datetimefield';

export const SidebarWidget = (props) => {
  return (
    <div style={{backgroundColor:'#fff'}}>
      <div className="l-booking-panel__overlay js-booking-panel-toggle"></div>
        <div className="l-booking-panel__header">
          <h2 className="l-booking-panel__heading">{window.labels.BookingLabel}</h2>
          <button className="l-booking-panel__close js-booking-panel-toggle" onClick={props.onTogglePanel}>
            <ReactSVG path="/themes/base/production/svg/close.svg" className='svg-close'/>
          </button>
        </div>

        <div className="l-booking-panel__main">
          {props.preset
          ?
            <div>
              <div className="l-booking-panel__summary">
                <div className="l-booking-panel__summary-label">Booking:</div>
                Wellington to Auckland Relocation (Suzuki Swift)
              </div>
              <div className="l-booking-panel__split"></div>
            </div>
          :''
          }

          <div className="l-booking-panel__form">
            <div className="l-form__field l-form__field--select">
              <label className="l-form__label">
                {window.labels.PickupLocationLabel}
              </label>
              <div className="l-form__select notranslate">
                <select className="l-form__input l-form__input--select" name="formPickupLocation" value={props.state.form.formPickupLocation} onChange={e => props.onFormChange(e)} disabled={props.state.form.isAmendment}>>
                  {props.state.nz.Locations.length != 0
                    ? <optgroup label="New Zealand">
                        {props.state.nz.Locations.map(loc => <option value={'nz-' + loc.id} key={'nz-' + loc.id}>{loc.location}</option>)}
                      </optgroup>
                    : ''
                  }
                  {props.state.au.Locations.length != 0
                    ? <optgroup label="Australia">
                        {props.state.au.Locations.map(loc => <option value={'au-' + loc.id} key={'au-' + loc.id}>{loc.location}</option>)}
                      </optgroup>
                    : ''
                  }
                </select>
                <ReactSVG path="/themes/base/production/svg/defaults/select-arrow.svg" className='svg-defaults/select-arrow'/>
              </div>
            </div>

            <div className="l-form__field l-form__field--select">
              <label className="l-form__label">
                {window.labels.DropoffLocationLabel}
              </label>

              <div className="l-form__select  notranslate">
                <select className="l-form__input l-form__input--select"  name="formDropoffLocation" value={props.state.form.formDropoffLocation} onChange={e => props.onFormChange(e)} disabled={props.state.form.isAmendment}>>
                  {props.state[props.state.form.country].Locations.length != 0
                    ? <optgroup label={props.state.form.country=='nz'? 'New Zealand':'Australia'}>
                        {props.state[props.state.form.country].Locations.map(loc => <option value={props.state.form.country + '-' + loc.id} key={props.state.form.country + '-' + loc.id}>{loc.location}</option>)}
                      </optgroup>
                    : ''
                  }
                </select>
                <ReactSVG path="/themes/base/production/svg/defaults/select-arrow.svg" className='svg-defaults/select-arrow'/>
              </div>
            </div>

            <div className="l-booking-panel__split"></div>

            <DateTimeField timeArr={props.pickupTimeArr? props.pickupTimeArr:""} labeldate={window.labels.PickupDateLabel} prefix="sidebar" name="Pickup" onFormChange={props.onFormChange} value={props.state.form.formPickupTime}/>
            <DateTimeField timeArr={props.dropoffTimeArr? props.dropoffTimeArr:""}  labeldate={window.labels.DropoffDateLabel} prefix="sidebar" name="Dropoff" onFormChange={props.onFormChange} value={props.state.form.formDropoffTime}/>

            <div className="l-booking-panel__split"></div>

            <div className="l-form__field l-form__field--select">
              <label className="l-form__label">
                {window.labels.DriverAgeLabel}
              </label>

              <div className="l-form__select">
                <select id="cmbAge" name="formMinimumAge" className="l-form__input l-form__input--select" value={props.state.form.formMinimumAge} onChange={e => props.onFormChange(e)}>
                  {props.state.form.country=='au'? <option value="0">Select Age</option> : ''}
                  {props.state[props.state.form.country].DriverAges.map(age => <option value={age.id} key={age.id}>{age.driverage}</option>)}
                </select>
                <ReactSVG path="/themes/base/production/svg/defaults/select-arrow.svg" className='svg-defaults/select-arrow'/>
              </div>
            </div>

            <div className="l-form__field ">
              <label className="l-form__label">
                {window.labels.PromocodeLabel}
              </label>
              <input className="l-form__input" type="text" name="formPromoCode" value={props.state.form.formPromoCode} id={props.prefix + '_Promo'} onChange={e => props.onFormChange(e)} />
            </div>
              {props.state.form.isprocessing
                ? <button type="submit" className="l-booking-panel__submit btn" onClick={e => props.onSubmitSearch(e)} disabled>
                    {window.labels.ProcessingLabel}...
                  </button>
                :
                props.state.form.isAmendment?
                <button type="submit" className="l-booking-panel__submit btn btn--secondary" onClick={e => props.onSubmitSearch(e)} className="l-booking-form__submit btn btn--secondary" >
                    Change
                  </button>
                  :
                  <button type="submit" className="l-booking-panel__submit btn btn--secondary" onClick={e => props.onSubmitSearch(e)} className="l-booking-form__submit btn btn--secondary" >
                    {window.labels.ItinerarySearchLabel}
                  </button>
              }
          </div>
        </div>
    </div>
  );
}
