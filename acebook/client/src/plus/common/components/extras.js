import React from 'react';
import ReactSVG from 'react-svg';
import { OptionCard }  from './partials/optioncard';

export const Extras = (props) => {
  let form = props.state.form;
  let locp = form.formPickupLocation.split('-');
  
  const activeCountry =  props.state[locp[0]];
  const age = activeCountry.DriverAges.find(age => age.id == form.formMinimumAge);
  const insurancefeatures = window.insurance_plus;
  const Options = activeCountry.Options.Option? activeCountry.Options.Option:[];
  return (
    <div>
      <div className="l-booking__step">
        <h2 className="l-booking__step-heading">
          1. {window.labels.CoverageInstructons}
        </h2>
        <div className="l-booking__options">
        {Options.filter(opt => opt['@attributes'].hasOwnProperty('liability') && opt['@attributes'].available == 'true').map((cover,i) => (
          <OptionCard key={i} order={i} special={cover.excessfee==0 && hasaceplusdiscount} showdesc3={cover.excessfee!=0} Option={cover} Features={insurancefeatures[cover['@attributes'].code]} Type="lg" Selected={(form.selectedCover.hasOwnProperty('@attributes') && cover['@attributes'].code == form.selectedCover['@attributes'].code)} onClick={props.onSelectCover} toggleMoreSpecs={props.toggleMoreSpecs}/>
        ))}
          <div style={{fontStyle: 'italic', width: '100%', margin: '0px 20px 40px 20px'}}>
            <p>{window.labels.ExcessBlurb}</p>
            <p>{window.labels.BondBlurb}</p>
          </div>
        </div>
      </div>
      <div className="l-booking__step">
        <h2 className="l-booking__step-heading">
          2. {window.labels.OptionsInstructions}
        </h2>

        <div className="l-booking__options">
        {Options.filter(opt => !opt['@attributes'].hasOwnProperty('liability') && opt['@attributes'].available == 'true').map((option,i) =>
          <OptionCard key={i} showGST={form.country=='au'} Option={option} Selected={form.selectedAncillaries.find(item => item.opt['@attributes'].code == option['@attributes'].code)} onClick={props.onSelectAncillaries} onQtyChange={props.onQtyChange}/>
        )}
        </div>
      </div>
    </div>
  );
}

