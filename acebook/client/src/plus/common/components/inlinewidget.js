import React from 'react';
import ReactSVG from 'react-svg';
import moment from 'moment';
import { DateTimeField }  from './partials/datetimefield';

export const InlineWidget = (props) => {
  const prefix = 'Hero_Booking';
  return (
    <div className="l-form js-booking-form" prefix={prefix}>      
      <div className="l-booking-form__fields">
        <div className="l-form__field l-form__field--select">
          <label htmlFor={prefix + '_PickupLocation'} className="l-form__label">
            {window.labels.PickupLocationLabel}
          </label>

          <div className="l-form__select notranslate">
            <select className="l-form__input l-form__input--select" name="formPickupLocation" value={props.state.form.formPickupLocation} onChange={e => props.onFormChange(e)}>
              {props.state.nz.Locations.length != 0
                ? <optgroup label="New Zealand">
                    {props.state.nz.Locations.map(loc => <option value={'nz-' + loc.id} key={'nz-' + loc.id}>{loc.location}</option>)}
                  </optgroup>
                : ''
              }
              {props.state.au.Locations.length != 0
                ? <optgroup label="Australia">
                    {props.state.au.Locations.map(loc => <option value={'au-' + loc.id} key={'au-' + loc.id}>{loc.location}</option>)}
                  </optgroup>
                : ''
              }
            </select>
            <ReactSVG path="/themes/base/production/svg/defaults/select-arrow.svg" className='svg-defaults/select-arrow'/>
          </div>
        </div>
        
        <DateTimeField timeArr={props.pickupTimeArr? props.pickupTimeArr:""} labeldate={window.labels.PickupDateLabel} prefix="inline" name="Pickup" onFormChange={props.onFormChange} value={props.state.form.formPickupTime}/>
        <DateTimeField timeArr={props.dropoffTimeArr? props.dropoffTimeArr:""} labeldate={window.labels.DropoffDateLabel} prefix="inline" name="Dropoff" onFormChange={props.onFormChange} value={props.state.form.formDropoffTime}/>

        <div className="l-form__field--fw l-booking-form__toggles">
          
          <div className="l-form__field l-form__field--checkbox">
            <input className="l-form__input js-booking__dropoff-location-toggle" type="checkbox" name='formDifferentDropOff'  id='formDifferentDropOff' checked={ props.state.form.formDifferentDropOff}  onChange={e => props.onFormChange(e)}/>
            <label htmlFor='formDifferentDropOff' className="l-form__label">
              {window.labels.DifferentDropoffLabel}
            </label>
          </div>
          { <button className="js-booking__advanced-toggle l-booking-form__toggle " id="toggleButton" onClick={props.onClick} >{props.state.form.showDropoff? "- " + window.labels.LessOptionsLabel:"+ " + window.labels.MoreOptionsLabel}</button> }
        </div>

        
        <div className= {props.state.form.showDropoff? "l-form__field l-form__field--select js-booking__dropoff-location js-booking__advanced-field notranslate ":"l-form__field l-form__field--select js-booking__dropoff-location js-booking__advanced-field is-hidden notranslate"}>
          <label htmlFor={prefix + '_DropoffLocation'} className="l-form__label">
            {window.labels.DropoffLocationLabel}
          </label>
          <div className="l-form__select">
            <select className="l-form__input l-form__input--select"  name="formDropoffLocation" value={props.state.form.formDropoffLocation} onChange={e => props.onFormChange(e)}>
              {props.state[props.state.form.country].Locations.length != 0
                ? <optgroup label={props.state.form.country=='nz'? 'New Zealand':'Australia'}>
                    {props.state[props.state.form.country].Locations.map(loc => <option value={props.state.form.country + '-' + loc.id} key={props.state.form.country + '-' + loc.id}>{loc.location}</option>)}
                  </optgroup>
                : ''
              }
            </select>
            <ReactSVG path="/themes/base/production/svg/defaults/select-arrow.svg" className='svg-defaults/select-arrow'/>
          </div>
        </div>

        
        <div className={props.state.form.showDropoff? "l-form__field l-form__field--select js-booking__advanced-field":"l-form__field l-form__field--select js-booking__advanced-field is-hidden"}>
          <label htmlFor="cmbAge" className="l-form__label">
            {window.labels.DriverAgeLabel}
          </label>

          <div className="l-form__select">
            <select id="cmbAge" name="formMinimumAge" className="l-form__input l-form__input--select" value={props.state.form.formMinimumAge} onChange={e => props.onFormChange(e)}>
              {props.state.form.country=='au'? <option value="0">Select Age</option> : ''}
              {props.state[props.state.form.country].DriverAges.map(age => <option value={age.id} key={age.id}>{age.driverage}</option>)}
            </select>
            <ReactSVG path="/themes/base/production/svg/defaults/select-arrow.svg" className='svg-defaults/select-arrow'/>
          </div>
        </div>
        <div className={props.state.form.showDropoff? "l-form__field js-booking__advanced-field ":"l-form__field js-booking__advanced-field is-hidden"}>
          <label htmlFor={props.prefix + '_Promo'} className="l-form__label">
            {window.labels.PromocodeLabel}
          </label>
          <input className="l-form__input" type="text" name="formPromoCode" id={props.prefix + '_Promo'} value={props.state.form.formPromoCode} onChange={e => props.onFormChange(e)} />
        </div>
      </div>
      <div className="l-booking-form__action">
        {props.state.form.isprocessing
          ? <button type="submit" className="l-booking-form__submit btn" onClick={e => props.onSubmitSearch(e)} disabled>
              {window.labels.ProcessingLabel}...
            </button>
          :
            <button type="submit" className="l-booking-form__submit " onClick={e => props.onSubmitSearch(e)} className="l-booking-form__submit btn btn--secondary" >
              {window.labels.ItinerarySearchLabel}
            </button>
        }
      </div>
    </div>
  );
}
