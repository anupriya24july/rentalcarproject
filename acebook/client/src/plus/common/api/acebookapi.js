import base64 from 'base-64';
import superagent from 'superagent';
import moment from 'moment';
const environment = (location.hostname.split('.').reverse().pop()!= 'dev') ? 'live':'live';
const defaultcountry = location.hostname.split('.').pop();
const today = moment(new Date(), 'DD/MMM/YYYY').format('DD-MM-YYYY');
export const AceBookAPI = {
  environment: environment,
  defaultcountry: defaultcountry,
  dateformat: 'YYYY-MM-DD',
  initBooking: initBooking,
  getRates: getRates,
  getEstimate: getEstimate,
  getOptions: getOptions,
  createNewReservation: createNewReservation,
  updateReservation: updateReservation,
  retrieveReservation: retrieveReservation,
  retrieveEstimate: retrieveEstimate,
  deposit:0.10,
  gst: defaultcountry=='nz'? 0.15 : 0.10,
  getGUID: getGUID,
  currency: defaultcountry=='nz'? 'NZD' : 'AUD',
};

function openModal(heading,msg,mode) {
    if(mode == 1){
        document.querySelector('.modal-message-title').innerHTML = 'Our Cover Options';
        document.querySelector('.modal-message-content').classList.add('modal-iframe-wrapper');
        document.querySelector('.modal-message-content').innerHTML = '<iframe class="modal-iframe" src="https://www.youtube.com/embed/HB1XlOBIT6c?autoplay=1" frameborder="0" allowfullscreen></iframe>';
    } else if (mode == 2) {
        document.querySelector('.modal-message-title').innerHTML=heading;
        document.querySelector('.modal-message-content').innerHTML=msg;  
        document.querySelector('.modal-footer').classList.remove('is-hidden');
    } else {
        document.querySelector('.modal-message-title').innerHTML=heading;
        document.querySelector('.modal-message-content').innerHTML=msg;  
    }
  document.querySelector('.js-modal').classList.remove('is-hidden');
}

function getGUID(){
	let token = "";
	let codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	codeAlphabet += "abcdefghijklmnopqrstuvwxyz";
	codeAlphabet += "0123456789";
	let max = codeAlphabet.length;

  for (let i=0; i < 10; i++) { // 10 characters long
      token += codeAlphabet[Math.floor(Math.random() * max)];
  }

  return token;
}

function gracefulResponse(response, callback){
	let errormsg = '';
	if(response['@attributes'].success == 'false'){
		errormsg = response.Messages.Message.Text;
	}

  if(errormsg == '' ){
  	if(callback) callback();
  }else{
  	let heading="Oops! Looks like something went wrong!";
  	openModal(heading,errormsg,0);
  	// window.location = "/bookings";
  }
}

function initBooking(parent, callback){
	superagent.get('/api/InitBooking').end((error, response) => {
		parent.props.setDataState({
      [defaultcountry]: {
        ...parent.props.state[defaultcountry],
        Locations : response.body.Locations,
        DriverAges : response.body.DriverAges,
        Countries  : response.body.Countries
      },
    });

    gracefulResponse(response.body, callback);

  });
}

function getRates(data, parent, callback){
	const base64data = base64.encode(Object.entries(data).map(e => encodeURIComponent(e[0])+"="+encodeURIComponent(e[1])).join('&'));
	superagent.get('/api/getRates/' + base64data).end((error, response) => {

		if(response.body.ResRates['@attributes'].success == 'true'){
			parent.props.setDataState({
	      [defaultcountry]: {
	        ...parent.props.state[defaultcountry],
	        ReservationRef:'',
	        ReservationNo:'',
	        PaymentSaved:false,
	        Rates : response.body.ResRates.Rate,
	      },
	    });
		}

		gracefulResponse(response.body.ResRates, callback);

  });
}

function getEstimate(data, parent, callback){
	const base64data = base64.encode(Object.entries(data).map(e => encodeURIComponent(e[0])+"="+encodeURIComponent(e[1])).join('&'));
	superagent.get('/api/getEstimate/' + base64data).end((error, response) => {

		if(response.body.ResEstimate['@attributes'].success == 'true'){
			parent.props.setDataState({
	      [defaultcountry]: {
	        ...parent.props.state[defaultcountry],
	        Estimate: response.body.ResEstimate,
	      },
	    });
		}

		gracefulResponse(response.body.ResEstimate, callback);

  });
}

function getOptions(data, parent, callback){
	const base64data = base64.encode(Object.entries(data).map(e => encodeURIComponent(e[0])+"="+encodeURIComponent(e[1])).join('&'));
	superagent.get('/api/getOptions/' + base64data).end((error, response) => {

		if(response.body.ResOptions['@attributes'].success == 'true'){
			parent.props.setDataState({
	      [defaultcountry]: {
	        ...parent.props.state[defaultcountry],
	        Options: response.body.ResOptions,
	      },
	    });
		}

		gracefulResponse(response.body.ResOptions, callback);

  });
}

function createNewReservation(data, parent, callback){
	const base64data = base64.encode(Object.entries(data).map(e => encodeURIComponent(e[0])+"="+encodeURIComponent(e[1])).join('&'));
	superagent.get('/api/createNewReservation/' + base64data).end((error, response) => {

		if(response.body.NewReservationResponse['@attributes'].success == 'true'){
			parent.props.setDataState({
	      [defaultcountry]: {
	        ...parent.props.state[defaultcountry],
	        ReservationNo: response.body.NewReservationResponse['@attributes'].reservationNumber,
	        ReservationRef: data.cn,
	      },
	    });
		}

		gracefulResponse(response.body.NewReservationResponse, callback);

  });
}

function updateReservation(data, parent, callback){
	const base64data = base64.encode(Object.entries(data).map(e => encodeURIComponent(e[0])+"="+encodeURIComponent(e[1])).join('&'));
	superagent.get('/api/updateReservation/' + base64data).end((error, response) => {

		if(response.body.UpdateReservationResponse['@attributes'].success == 'true'){
			parent.props.setDataState({
	      [defaultcountry]: {
	        ...parent.props.state[defaultcountry],
	        ReservationNo: response.body.UpdateReservationResponse['@attributes'].reservationNumber,
	        PaymentSaved: true,
	      },
	    });
		}

		gracefulResponse(response.body.UpdateReservationResponse, callback);

  });
}

function retrieveReservation(data, parent, callback){
	const base64data = base64.encode(Object.entries(data).map(e => encodeURIComponent(e[0])+"="+encodeURIComponent(e[1])).join('&'));
	superagent.get('/api/retrieveReservation/' + base64data).end((error, response) => {
		if(response.body.RetrieveReservationResponse['@attributes'].success == 'true'){
			let pickupCheck=moment(response.body.RetrieveReservationResponse['Res']['Pickup']['@attributes']['dateTime'], 'YYYY-MM-DDTHH:mm:ss').add(-2, 'days').format('DD-MM-YYYY');
			if(pickupCheck<=today || response.body.RetrieveReservationResponse['Res']['@attributes']['status']!='O'){
				let heading="Oops! Looks like something went wrong!";
        		let msg='Sorry you wont be able to make any changes to the booking as the pick up date is closer. For making changes please contact support@acerentalcars.' + (location.hostname.split('.').pop() == 'nz'? 'co.nz':'com.au');
        		openModal(heading,msg,0);
			} else{
				parent.props.setState({
			        ...parent.props.state.checkin,
			        Pickup: response.body.RetrieveReservationResponse['Res']['Pickup']['@attributes'],
			        Return: response.body.RetrieveReservationResponse['Res']['Return']['@attributes'],
			        Option: response.body.RetrieveReservationResponse['Res']['Option'],
			        RenterAddress: response.body.RetrieveReservationResponse['Res']['Renter']['Address'],
			        RenterName: response.body.RetrieveReservationResponse['Res']['Renter']['RenterName']['@attributes'],
			        RenterID: response.body.RetrieveReservationResponse['Res']['Renter']['@attributes'].customerNumber,
			        Source: response.body.RetrieveReservationResponse['Res']['Source']['@attributes'],
			        Vehicle: response.body.RetrieveReservationResponse['Res']['Vehicle']['@attributes']['classCode'],
			        Notes: response.body.RetrieveReservationResponse['Res']['ReservationMainNote']['@attributes'],
		    	});
			}
		}

		gracefulResponse(response.body.RetrieveReservationResponse, callback);

  });
}

function retrieveEstimate(data, parent, callback){
	const base64data = base64.encode(Object.entries(data).map(e => encodeURIComponent(e[0])+"="+encodeURIComponent(e[1])).join('&'));
	superagent.get('/api/retrieveEstimate/' + base64data).end((error, response) => {
		if(response.body.ResEstimate['@attributes'].success == 'true'){
			parent.props.setState({
		        ...parent.props.state.checkin,
		        Total: response.body.ResEstimate['RenterEstimate']['@attributes'].total,
		        Charge: response.body.ResEstimate['RenterEstimate']['Charge']
	    	});
		}

		gracefulResponse(response.body.ResEstimate, callback);

  });
}