import React from 'react';
import moment from 'moment';
import pikaday from 'pikaday';
import superagent from 'superagent';
import numeral from 'numeral';

import { AceBookAPI as api } from '../../common/api/acebookapi';
import { BrowserRouter as Router } from 'react-router-dom'; 
import { connect } from 'react-redux';

import { PaymentAmount } from '../../common/components/partials/paymentamount';
import { CreditCard } from '../../common/components/partials/creditcard';
import { VehiclePanel } from '../../common/components/partials/vehiclepanel';
import { StickyBar } from '../../common/components/partials/stickybar';
import { ProgressBar } from '../../common/components/partials/progressbar';

class Payment extends React.Component {

  pushAnalytics () {
    let form = this.props.state.form;
    let impressions = [];
    let products = [];
    let locp = this.props.state.form.formPickupLocation.split('-');
    let ploc = this.props.state[locp[0]].rcmLocationInfo.filter(l => l.id == locp[1] )[0];

    let locd = this.props.state.form.formDropoffLocation.split('-');
    let dloc = this.props.state[locp[0]].rcmLocationInfo.filter(l => l.id == locd[1] )[0];

    const datetimeformat = 'DD/MM/YYYY HH:mm';
    const p_datetime = moment(form.formPickupDate + ' ' + form.formPickupTime.replace('_',':'), datetimeformat);
    const d_datetime = moment(form.formDropoffDate + ' ' + form.formDropoffTime.replace('_',':'), datetimeformat);
    let selectedCar = this.props.state[locp[0]].rcmAvailableCars[0];

    let promocode = this.props.state[locp[0]].rcmAvailableCarDetails.filter(cd => { if (cd.carsizeid == selectedCar.carsizeid) return cd })[0].discountcode;

    let age = this.props.state[locp[0]].rcmDriverAgesInfo.find(age => age.id == this.props.state.form.formMinimumAge);

    // Selected Vehicle
    products.push({
      'id': selectedCar.sippcodes,
      'name': selectedCar.categoryfriendlydescription,
      'brand': selectedCar.vehicledescription1,
      'category': 'bookings/selectvehicle (' + ploc.location + ')',
      'variant':'Vehicle',
      'price': selectedCar.totalrateafterdiscount + '',
      'quantity': 1 // always set to item qty for vehicles
    });

    // Mandatory Fees
    this.props.state[form.country].rcmMandatoryFees.filter(man => { if(man.locationid == locp[1] && man.vehiclesizeid == selectedCar.carsizeid) return man }).map((man,i) =>
       products.push({
        'id': man.id,
        'name': man.name,
        'brand': man.extradesc1,
        'category': 'bookings/selectvehicle (' + ploc.location + ')',
        'variant':'Madatory Fee',
        'price': man.fees + '',
        'quantity': 1 // always set to item qty for mandatory fee
      })
    );

    // Insurance
    let selectedInsurance = this.props.state[form.country].rcmInsuranceOptions.find(ins => ins.id == form.selectedInsuranceId);
    products.push({
      'id': selectedInsurance.id,
      'name': selectedInsurance.name,
      'brand': selectedInsurance.extradesc,
      'category': 'bookings/addextras (' + ploc.location + ')',
      'variant':'Insurance',
      'price': selectedInsurance.fees + '',
      'quantity': selectedInsurance.numofdays
    });

    // Optional Extras
    form.selectedOptionalExtras.map((selex,i) => {
      products.push({
        'id': selex.ex.id,
        'name': selex.ex.name,
        'brand': selex.ex.extradesc,
        'category': 'bookings/addextras (' + ploc.location + ')',
        'variant':'Optional Extras',
        'price': selex.ex.fees + '',
        'quantity': selex.qty + (selex.ex.type == 'Daily' ? selex.ex.numofdays: 1)
      });
    });

    // Checkout
    window.dataLayer.push({
      'ecommerce': {
        'checkout': {
          'actionField': {'step': 4},
          'products': products
        }
      },
      'event': 'AceRentals.ec',
      'eventCategory': 'Ecommerce',
      'eventAction': 'Checkout'
    });
  }

  constructor(props) {
    super(props);
    this.state = {
      num:'',
      name:'',
      exp:'',
      cvc:'',
      error: '',
      isprocessing: false,
      number:''
    }
    this.openModal = props.openModal;
   }

  componentWillMount(){
    let form = this.props.state.form;
    let locp = form.formPickupLocation.split('-');
    
    const activeCountry =  this.props.state[locp[0]];

    this.props.setDataState({
      [locp[0]]: {
          ...this.props.state[locp[0]],
          ReservationRef: '',
          PaymentSaved: false
      }
    });
  }

  onCCUpdate(event){
    const newState = {...this.state, };
    if(event.target.name=='num'){
      newState[event.target.name] = event.target.rawValue;
    }else{
      newState[event.target.name] = event.target.value;
    }
    this.setState(newState);
  }
  

  togglePanel(e) {
    e.preventDefault();
    window.scrollTo(0, 0);
    const panel = document.querySelector('.js-booking-panel')
    if (panel.classList.contains('is-open')) {
      //panel.classList.remove('is-open');
    } else {
      panel.classList.add('is-open');
    }
  }

  onContinue(){
    this.createBooking();
  }

  onPaymentConfirmDone(){
    // check if payment is saved.
    // if payment saved.
    //this.props.history.push('/bookings/complete'); // instead of just rendering a new view redirect to erase the payment post request.
    window.location.replace( window.locale + '/bookings/complete');
  }

  validate(){ 
    let error_message="";
    if(this.props.state.form.formPickupTime<moment(new Date()).format("HH_mm") && this.props.state.form.formPickupDate==moment(new Date()).format("DD/MM/YYYY")){
      error_message=error_message+"<br>Sorry the Pickup time selected has lapsed. Please select a different Pickup time to book.";
    }else{
    if(this.state.name  == ""){
      error_message=error_message+"<br>Please Enter Card Holder Name.";
    }
    if(this.state.num == ""){
      error_message=error_message+"<br>Please Enter the Card Number.";
    }
    if(this.state.exp == ""){
      error_message=error_message+"<br>Please Enter Expiry Date.";
    }
    if(this.state.cvc == ""){
      error_message=error_message+"<br>Please Enter CVC.";
    }else{
      let reg_cvc = /^[0-9]{3,4}$/ ;
      if(!reg_cvc.test(this.state.cvc)){
        error_message=error_message+"<br>Please Enter a Valid CVC.";
      }
    }
  }
  return error_message;
  }

  confirmPayment(){
    let form = this.props.state.form;
    let locp = form.formPickupLocation.split('-');
    let activeCountry = this.props.state[form.country];
    const total = activeCountry.Estimate.RenterEstimate['@attributes'].total;
    const paymentamount = numeral((form.selectedPaymentAmount == 'full')? total : (total * api.deposit)).format('0.00');

    let data = {
        resno: this.props.state[form.country].ReservationNo,
        name: this.state.name, 
        num: this.state.num,
        exp: this.state.exp,
        cvc: this.state.cvc,
        amount: '' + paymentamount,
        email: form.formCustomerData.eml
    }
    this.setState({
      'error': '',
      isprocessing: true
    });

    let cc = this.state;
    superagent.post('/payments/purchase').send($.param(data)).end((err, res) => {

      let jsonres = JSON.parse(res.text);
      if(jsonres.isSuccessful){
        this.setState({
          error: ''
        });

        // Todo: must inform Carsplus that payment was captured successfully.

        let payment = {
          ...data,
          num: data.num.replace(/(\d{4})\d{8}(\d{4})/, "$100000000$2"), // truncatedcardnumber,
          currency: api.currency,
          payment: false
        };

        // modify the booking and convert it to open status.
        api.updateReservation(this.encodeBooking('O'/* Open status */, payment), this, this.onPaymentConfirmDone.bind(this));
      }else{

        this.setState({
          error: jsonres.responseText,
          isprocessing: false
        });
   
        let heading="Oops! Looks like something went wrong!";
        let msg=jsonres.responseText + ' Please contact support@acerentalcars.' + (location.hostname.split('.').pop() == 'nz'? 'co.nz':'com.au');
        this.openModal(heading,msg,0);
        if($zopim){
          $zopim.livechat.say('My payment has failed for Reservation No ' + activeCountry.ReservationNo + '. Could you please contact me?');
        }
      }
    });    
  }

  encodeBooking(status, payment = {}){

    const state = this.props.state;
    const form = state.form; 
    let activeCountry = this.props.state[form.country];

    // pickup location
    let locp = form.formPickupLocation.split('-');
    let ploc = this.props.state[locp[0]].Locations.find(l => l.id == locp[1] );

    // dropoff location
    let locd = form.formDropoffLocation.split('-');
    let dloc = this.props.state[locd[0]].Locations.find(l => l.id == locd[1] );

    let options = [];
    if(form.selectedCover.hasOwnProperty('@attributes')){
      options.push(form.selectedCover['@attributes'].code + ':' + 1);
    }
    form.selectedAncillaries.map(item => {
      options.push(item.opt['@attributes'].code + ':' + item.qty);
    });

    let data = {
      pl:locp[1],
      rl:locd[1],
      pd:form.formPickupDate + 'T' + form.formPickupTime,
      rd:form.formDropoffDate + 'T' + form.formDropoffTime,
      dob:moment().subtract(form.formMinimumAge, 'years').format('YYYY-MM-DD'),
      cid: form.formPromoCode == '' ? 'ACEWEB': form.formPromoCode,
      rid:form.selectedRateID,
      car:form.selectedCarClass,
      opt:options.join('|'),
      cn:(status == 'Q') ? api.getGUID() : activeCountry.ReservationRef,
      st:status,
      resno:activeCountry.ReservationNo,
      age: form.formMinimumAge,
      ...form.formCustomerData,
      ...payment
    };

    return data;
  }

  createBooking(){
    // pickup location
    const state = this.props.state;
    const form = state.form; 
    let msg = this.validate();
    if(msg==''){
      if(state[form.country].ReservationRef == ''){
        this.setState({
          'error': '',
          isprocessing: true
        });
        
        //this.pushAnalytics();
        api.createNewReservation(this.encodeBooking('Q'/* Quote status */), this, this.confirmPayment.bind(this));
      }else{
        if(!this.state.isprocessing){
          this.setState({
            isprocessing: true
          });
          this.confirmPayment();
        }
      }
    }else{
      let heading="Oops! Looks like something is missing!";
      this.openModal(heading,msg,0);
    }
  }

  onFormChange(event){
    const newFormState = {...this.props.state.form};
    switch(event.target.name){
      default:
        newFormState.formCustomerData[event.target.name] = event.target.value;
        break;
    }

    this.props.setFormState(newFormState);
  }

  onSelectPaymentAmount(id){
    this.props.setFormState({
      selectedPaymentAmount: id
    });
  }

  render() {
    return (
      <div className="l-booking">
        <VehiclePanel gst={api.gst} state={this.props.state} showtotal={true}/>
        <div className="l-booking__main">
          <div className="l-booking__container">
            <div className="l-booking__header">
              <h1 className="l-booking__heading">Make a Booking</h1>
            </div>
            <ProgressBar progress={75}/>
            <PaymentAmount state={this.props.state} onSelectPaymentAmount={this.onSelectPaymentAmount.bind(this)}/>
            <CreditCard state={this.state} onCCUpdate={this.onCCUpdate.bind(this)}/>
            {this.state.isprocessing
            ? <button className="l-booking__next btn">
               {window.labels.ProcessingLabel}...
              </button>
            :
              <button className="l-booking__next btn btn--primary" onClick={this.onContinue.bind(this)}>
                {window.labels.ContinueLabel}
              </button>
            }
          </div>
        </div>
        <StickyBar gst={api.gst} showtotal={true} state={this.props.state} togglePanel={this.togglePanel.bind(this)}/>
      </div>
    );
  }

  componentDidMount () {
    // this.formatFields();
    window.scrollTo(0, 0);
    this.setState({
      isprocessing: false
    });
  }
}

const mapStateToProps = (state) => {
  return {
    state: {
      nz: state.data.nz,
      au: state.data.au,
      form: state.form,
    }
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setFormState: (state) =>{
      dispatch({
        type: "SET_FORMSTATE",
        payload: state, 
      });
    },
    setDataState: (state) =>{
      dispatch({
        type: "SET_DATASTATE",
        payload: state, 
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Payment);
