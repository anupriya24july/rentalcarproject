import React from 'react';
import superagent from 'superagent';
import jsonp from 'superagent-jsonp';
import moment from 'moment';
import pikaday from 'pikaday';

import { AceBookAPI as api } from '../../common/api/acebookapi';
import { BrowserRouter as Router } from 'react-router-dom'; 
import { connect } from 'react-redux';

import { Success } from '../../common/components/partials/success';
import { Currency } from '../../common/components/partials/currency';
import { ProgressBar } from '../../common/components/partials/progressbar';

class Complete extends React.Component {

  pushAnalytics () {
    let form = this.props.state.form;
    let impressions = [];
    let products = [];
    let locp = this.props.state.form.formPickupLocation.split('-');
    let ploc = this.props.state[locp[0]].rcmLocationInfo.filter(l => l.id == locp[1] )[0];

    let locd = this.props.state.form.formDropoffLocation.split('-');
    let dloc = this.props.state[locp[0]].rcmLocationInfo.filter(l => l.id == locd[1] )[0];

    const datetimeformat = 'DD/MM/YYYY HH:mm';
    const p_datetime = moment(form.formPickupDate + ' ' + form.formPickupTime.replace('_',':'), datetimeformat);
    const d_datetime = moment(form.formDropoffDate + ' ' + form.formDropoffTime.replace('_',':'), datetimeformat);
    let selectedCar = this.props.state[locp[0]].rcmAvailableCars[0];

    let promocode = this.props.state[locp[0]].rcmAvailableCarDetails.filter(cd => { if (cd.carsizeid == selectedCar.carsizeid) return cd })[0].discountcode;

    let age = this.props.state[locp[0]].rcmDriverAgesInfo.find(age => age.id == this.props.state.form.formMinimumAge);

    let customDimensions = {
       'dimension1': this.props.state[locp[0]].rcmAvailableCars[0].numofdays // Length Of Keep
      ,'metric1': this.props.state[locp[0]].rcmAvailableCars[0].numofdays // Length Of Keep
      ,'dimension2': ploc.location // Pickup Location
      ,'dimension3': p_datetime.format('DD, MMM, YYYY, hh:mma') // Pickup DateTime
      ,'dimension4': dloc.location // DropOff Location
      ,'dimension5': d_datetime.format('DD, MMM, YYYY, hh:mma') // DropOff DateTime
      ,'dimension6': age.driverage // Age
      ,'dimension7': promocode // Promocode
    }

    // Selected Vehicle
    products.push({
      ...customDimensions,
      'id': selectedCar.sippcodes,
      'name': selectedCar.categoryfriendlydescription,
      'brand': selectedCar.vehicledescription1,
      'category': 'bookings/selectvehicle (' + ploc.location + ')',
      'variant':'Vehicle',
      'price': selectedCar.totalrateafterdiscount + '',
      'quantity': 1 // always set to item qty for vehicles
    });

    // Mandatory Fees
    this.props.state[form.country].rcmMandatoryFees.filter(man => { if(man.locationid == locp[1] && man.vehiclesizeid == selectedCar.carsizeid) return man }).map((man,i) =>
       products.push({
        ...customDimensions,
        'id': man.id,
        'name': man.name,
        'brand': man.extradesc1,
        'category': 'bookings/selectvehicle (' + ploc.location + ')',
        'variant':'Madatory Fee',
        'price': man.fees + '',
        'quantity': 1 // always set to item qty for mandatory fee
      })
    );

    // Insurance
    let selectedInsurance = this.props.state[form.country].rcmInsuranceOptions.find(ins => ins.id == form.selectedInsuranceId);
    products.push({
      ...customDimensions,
      'id': selectedInsurance.id,
      'name': selectedInsurance.name,
      'brand': selectedInsurance.extradesc,
      'category': 'bookings/addextras (' + ploc.location + ')',
      'variant':'Insurance',
      'price': selectedInsurance.fees + '',
      'quantity': selectedInsurance.numofdays
    });

    // Optional Extras
    form.selectedOptionalExtras.map((selex,i) => {
      products.push({
        ...customDimensions,
        'id': selex.ex.id,
        'name': selex.ex.name,
        'brand': selex.ex.extradesc,
        'category': 'bookings/addextras (' + ploc.location + ')',
        'variant':'Optional Extras',
        'price': selex.ex.fees + '',
        'quantity': selex.qty + (selex.ex.type == 'Daily' ? selex.ex.numofdays: 1)
      });
    });

    // Purchase
    window.dataLayer.push({
      'ecommerce': {
        'purchase': {
          'actionField': {
            'id': this.props.state[locp[0]].rcmReservationNo,
            'affiliation': 'Online Booking 2.0',
            'revenue': form.total,
            'tax': form.total * api.gst,
            'shipping': '0.00',
            'coupon': promocode
          },
          'products': products
        }
      },
      'event': 'AceRentals.ec',
      'eventCategory': 'Ecommerce',
      'eventAction': 'purchase'
    });

    // Checkout
    window.dataLayer.push({
      'ecommerce': {
        'checkout': {
          'actionField': {'step': 5},
          'products': products
        }
      },
      'event': 'AceRentals.ec',
      'eventCategory': 'Ecommerce',
      'eventAction': 'Checkout'
    });
  }

  togglePanel(e) {
    e.preventDefault();
    window.scrollTo(0, 0);
    const panel = document.querySelector('.js-booking-panel')
    if (panel.classList.contains('is-open')) {
      //panel.classList.remove('is-open');
    } else {
      panel.classList.add('is-open');
    }
  }

  onFormChange(event){
    const newFormState = {...this.props.state.form};
    switch(event.target.name){
      default:
        newFormState.formCustomerData[event.target.name] = event.target.value;
        break;
    }

    this.props.setFormState(newFormState);
  }

  componentWillMount() {
    if(this.props.state[this.props.state.form.country].rcmReservationNo == ''){
      window.location.replace("/"); // redirect back to homepage.
    }
  }

  render() {
    return (
      <Success deposit={api.deposit} gst={api.gst} state={this.props.state} onFormChange={this.onFormChange.bind(this)} title="Payment Successful" content="Thank you for booking with us. Please refer below for a summary of your booking details."/>
    );
  }

  componentDidMount () {
    window.scrollTo(0, 0);
    //this.pushAnalytics();

    let form = this.props.state.form;
    let locp = form.formPickupLocation.split('-');
    
    const activeCountry =  this.props.state[locp[0]];

    this.props.setDataState({
      [locp[0]]: {
          ...this.props.state[locp[0]],
          ReservationRef: '',
          PaymentSaved: false
      }
    });
  }
}

const mapStateToProps = (state) => {
  return {
    state: {
      nz: state.data.nz,
      au: state.data.au,
      form: state.form,
    }
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setFormState: (state) =>{
      dispatch({
        type: "SET_FORMSTATE",
        payload: state, 
      });
    },
    setDataState: (state) =>{
      dispatch({
        type: "SET_DATASTATE",
        payload: state, 
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Complete);
