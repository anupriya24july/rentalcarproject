import React from 'react';
import superagent from 'superagent';
import moment from 'moment';
import pikaday from 'pikaday';

import { AceBookAPI as api } from '../../common/api/acebookapi';
import { BrowserRouter as Router } from 'react-router-dom'; 
import { connect } from 'react-redux';

import { SidebarWidget } from '../../common/components/sidebarwidget';
import { InlineWidget } from '../../common/components/inlinewidget';

class Itinerary extends React.Component {

  constructor(props) {
    super(props);

    this.country = api.defaultcountry;
    this.fetchCountry = api.defaultcountry;
    this.dropOffSameAsPickup = true;
    this.dateFormat = 'YYYY-MM-DD';
    this.today = moment(new Date());
    this.resetFormData={};
    this.openModal = props.openModal;
    this.props.setFormState({
      isprocessing: true
    });
  }

  detectPresets(){

    if (!String.prototype.startsWith) {
      String.prototype.startsWith = function(searchString, position) {
        position = position || 0;
        return this.indexOf(searchString, position) === position;
      };
    }

    let form = this.props.state.form;
    let locationBook = document.querySelector('.l-location__book');
    if(locationBook && location.pathname.startsWith('/locations') && document.querySelector('.l-page__heading')){
      let currentloc = document.querySelector('.l-page__heading').innerHTML;
      locationBook.addEventListener('click', (e) => {
        e.preventDefault();

        const activeCountry =  this.props.state[this.country];
        const p_location = activeCountry.Locations.find(loc => loc.location == currentloc);

        this.props.setFormState({
          formPickupLocation: this.country + '-' + p_location.id,
          formDropoffLocation: this.country + '-' + p_location.id,
        });
      });
    }

    let promoBook = document.querySelector('.l-promotion__book');
    if(promoBook && location.pathname.startsWith('/deals') && document.querySelector('.l-promotion__code')){
      let promocode = document.querySelector('.l-promotion__code').innerHTML.split('/div>')[1].trim();
      promoBook.addEventListener('click', (e) => {
        e.preventDefault();
        this.props.setFormState({
          formPromoCode: promocode
        });
      });
    }

    let reloBook = document.querySelectorAll('.c-relocation-card__action');
    if(reloBook && location.pathname.startsWith('/deals/relocations')){
      reloBook.forEach((reloBtn) => {
        reloBtn.addEventListener('click', (e) => {
          e.preventDefault();
          this.props.setFormState({
            formPromoCode: this.country=='nz'? 'RELOCATION':'',
            formPickupLocation: this.country + '-' + e.target.dataset.pl,
            formDropoffLocation: this.country + '-' + e.target.dataset.dl,
            selectedCarCategoryId: e.target.dataset.car,
          });
        });
      });
    }
    
    this.resetFormData= {...this.props.state.form, };

    this.props.setFormState({
      isprocessing: false
    });
  }

  toggleOptions() {  
    let toggle=!this.props.state.form.showDropoff;
     this.props.setFormState({showDropoff:toggle});
  }

  togglePanel(e) {
    e.stopPropagation();
    window.scrollTo(0, 0);
    this.props.setFormState(this.resetFormData);    
    let panel = document.querySelector('.js-booking-panel');
    if (panel.classList.contains('is-open')) {
      panel.classList.remove('is-open');
    } else {
      panel.classList.add('is-open');
    }
  }

  dateFields() {
    if(this.props.widget == 'sidebar'){ // only init once
      this.dateContainers = document.querySelectorAll('.js-date-container');
      if (this.dateContainers) {
        NodeList.prototype.forEach = Array.prototype.forEach;
        this.dateContainers.forEach((container, i) => {
          let dateInput = container.querySelector('.js-date');
          $('#'+dateInput.id).data('pikaday', new pikaday({
            container, 
            field: dateInput, 
            numberOfMonths: 1, 
            defaultDate: moment(dateInput.classList.contains('Pickup')? this.props.state.form.formPickupDate : this.props.state.form.formDropoffDate,this.dateFormat).toDate(),
            setDefaultDate: true,
            keyboardInput: false,
            minDate: this.today.toDate(),
            format: "DD, MMM, YYYY",
            position: 'bottom left',
            reposition: false,
            i18n: {
              previousMonth: 'Previous Month',
              nextMonth: 'Next Month',
              months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December', ], 
              weekdays: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', ],
              weekdaysShort: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', ],
            },
            onSelect: (selecteddate) => { 
              let datepickers = $('.js-date');
              const props = this.props;
              const dateformat = this.dateFormat
              let dropoffdate = props.state.form.formDropoffDate;
              let pickupdate = props.state.form.formPickupDate;
              if(dateInput.classList.contains('Dropoff')){
                datepickers.each(function(){
                  let el = $('#'+this.id);
                  dropoffdate = moment(selecteddate).format(dateformat);
                  if(el.hasClass('Dropoff')){
                    el.data('pikaday').setMoment(moment(selecteddate),true);
                  }

                  // daterange
                  el.data('pikaday').setStartRange(moment(pickupdate,dateformat).toDate());
                  el.data('pikaday').setEndRange(moment(dropoffdate,dateformat).toDate());
                });
                props.setFormState({
                  formDropoffDate: moment(selecteddate).format(dateformat),
                });
              }else{ //Pickup
                // - return date can't be earlier than pickup date + minimum booking days
                if(props.state.form.formPickupLocation != ''){
                  let loc = props.state.form.formPickupLocation.split('-');
                  let ploc = props.state[loc[0]].Locations.find(l => l.id == loc[1] );
                  
                 
                  datepickers.each(function(){
                    let el = $('#'+this.id);
                    pickupdate = moment(selecteddate).format(dateformat);
                    if(moment(dropoffdate,dateformat).isBefore(moment(selecteddate).add(1,'days'))){
                      dropoffdate = moment(selecteddate).add(1,'days').format(dateformat);
                    }
                    if(el.hasClass('Pickup')){
                      el.data('pikaday').setMoment(moment(selecteddate),true);
                    }else{
                      el.data('pikaday').setMoment(moment(dropoffdate,dateformat));
                      el.data('pikaday').setMinDate(moment(selecteddate).add(1,'days').toDate());
                    }

                    // daterange
                    el.data('pikaday').setStartRange(moment(pickupdate,dateformat).toDate());
                    el.data('pikaday').setEndRange(moment(dropoffdate,dateformat).toDate());
                  });

                  props.setFormState({
                    formPickupDate: pickupdate,
                    formDropoffDate: dropoffdate,
                  });
                }
              }
            },
            onClose: () => {
              // if(!window.inline_Dropoff_Date.isVisible()){
              //   // open dropoff date calendar immediately after selecting pickup date
              // }
            },
          }));
        });
      }
    }
  }

  componentWillMount() {
    api.initBooking(this);
    this.detectPresets();
  }

  componentDidMount(){   
    this.dateFields();    
    this.props.setFormState({
      isprocessing: false
    });
  }

  onFormChange(event){
    const newFormState = {...this.props.state.form, };
    switch(event.target.name){
      case 'formPickupLocation':
        let loc = event.target.value.split('-');
        let ploc = this.props.state[loc[0]].Locations.find(l => l.id == loc[1] );


        if(loc[0] != newFormState.country){ // if pickup is in a different country always set dropoff same as pickup
          newFormState.formDropoffLocation = event.target.value;
        }else{
          if(!this.props.state.form.formDifferentDropOff){ // dropoff is a configured to be always same as pickup
            newFormState.formDropoffLocation = event.target.value;
          }
        }

        newFormState.country = loc[0]; // change country based on pickup location.
        newFormState[event.target.name] = event.target.value;
        break;
      case 'formDifferentDropOff':
        newFormState[event.target.name] = event.target.checked; 
        newFormState['showDropoff'] = event.target.checked;
        break;
      default:
        newFormState[event.target.name] = event.target.value;
        break;
    }

    this.props.setFormState(newFormState);
  }

  checkAvailability() {
    if(this.props.state[this.country].Rates.length == 0){
      let heading="Just to let you know...";
      let msg="There are no cars available for the selected dates. Please try again.";
      this.openModal(heading,msg,0);
    }else{
      if(location.pathname.startsWith('/deals/relocations')){
        // pickup location
        let locp = this.props.state.form.formPickupLocation.split('-');
        let ploc = this.props.state[locp[0]].Locations.filter(l => l.id == locp[1] )[0];

        if(this.props.state[locp[0]].rcmAvailableCars.filter(c => c.carsizeid == this.props.state.form.selectedCarCategoryId)[0].available == 1){
          window.location.href = '/bookings/addextras';
        }else{
          let heading="Just to let you know...";
          let msg="The selected car is not available for the selected dates. Please try again.";
          this.openModal(heading,msg,0);
        }
      }else{
       window.location.href = window.locale + '/bookings/selectvehicle';
      }
    }
  }

  validate(){
    let msg="";
    if(this.props.state.form.formMinimumAge == 0){
      msg="Please Select Driver's Age";
    }
    return msg;
  }

  onSubmitSearch() {
    let msg = this.validate();
    if (msg){
      let heading="Oops! Looks like something is missing!";
      this.openModal(heading,msg,0);
    }
    else {
      // pickup location
      let locp = this.props.state.form.formPickupLocation.split('-');
      let ploc = this.props.state[locp[0]].Locations.find(l => l.id == locp[1]);

      // dropoff location
      let locd = this.props.state.form.formDropoffLocation.split('-');
      let dloc = this.props.state[locd[0]].Locations.find(l => l.id == locd[1]);

      let data = {
        pl: locp[1],
        rl: locd[1],
        pd: this.props.state.form.formPickupDate + 'T' + this.props.state.form.formPickupTime,
        rd: this.props.state.form.formDropoffDate + 'T' + this.props.state.form.formDropoffTime,
        dob: moment().subtract(this.props.state.form.formMinimumAge, 'years').format('YYYY-MM-DD'),
        cid: this.props.state.form.formPromoCode == '' ? 'ACEWEB': this.props.state.form.formPromoCode,
        et: 3,
      };

      this.props.setFormState({
        isprocessing: true
      });

      api.getRates(data, this, this.checkAvailability.bind(this));
    }
  }

  render() {
    let widget = null;
    let pickupTimeArr=[];
    let dropoffTimeArr=[];
    let form = this.props.state.form;
    let curentCountry = this.props.state[form.country];
    let ploc = form.formPickupLocation.split('-');
    let dloc = form.formDropoffLocation.split('-');
    if (curentCountry.Locations.length > 0){
      let pHours = curentCountry.Locations.find(l => l.id == ploc[1]).hours[moment(form.formPickupDate, 'YYYY-MM-DD').day()].split('-');
      let dHours = curentCountry.Locations.find(l => l.id == dloc[1]).hours[moment(form.formDropoffDate, 'YYYY-MM-DD').day()].split('-');
     
      for (let t = moment(pHours[0], 'HH:mm'); t <= moment(pHours[1], 'HH:mm'); t.add(15, 'minutes')) {
            pickupTimeArr.push(t.format('HH:mm:ss') + ','+ t.format('hh:mm A') );
      }
     
      for (let t =  moment(dHours[0], 'HH:mm'); t <=  moment(dHours[1], 'HH:mm'); t.add(15, 'minutes')) {
            dropoffTimeArr.push(t.format('HH:mm:ss') + ','+ t.format('hh:mm A') );
      }
    }
    switch(this.props.widget){
      case 'sidebar':
        widget = <SidebarWidget pickupTimeArr={pickupTimeArr} dropoffTimeArr={dropoffTimeArr} state={this.props.state} onClick={e => this.toggleOptions()} onFormChange={e => this.onFormChange(e)} onSubmitSearch={e => this.onSubmitSearch(e)} onTogglePanel={e => this.togglePanel(e)}/>;
        break;
      case 'inline':
        widget = <InlineWidget  pickupTimeArr={pickupTimeArr} dropoffTimeArr={dropoffTimeArr} state={this.props.state}  onClick={e => this.toggleOptions()}  onFormChange={e => this.onFormChange(e)} onSubmitSearch={e => this.onSubmitSearch(e)}/>;
        break;
      default:
        widget = <SidebarWidget pickupTimeArr={pickupTimeArr} dropoffTimeArr={dropoffTimeArr} state={this.props.state}  onClick={e => this.toggleOptions()}  onFormChange={e => this.onFormChange(e)} onSubmitSearch={e => this.onSubmitSearch(e)} onTogglePanel={e => this.togglePanel(e)}/>;
        break;
    }
    return widget;
  }
}

const mapStateToProps = (state) => {
  return {
    state: {
      nz: state.data.nz,
      au: state.data.au,
      form: state.form,
    }
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setFormState: (state) =>{
      dispatch({
        type: "SET_FORMSTATE",
        payload: state, 
      });
    },
    setDataState: (state) =>{
      dispatch({
        type: "SET_DATASTATE",
        payload: state, 
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Itinerary);
