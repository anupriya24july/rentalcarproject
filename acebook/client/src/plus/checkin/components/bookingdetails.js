import React from 'react';
import ReactSVG from 'react-svg';
import moment from 'moment';
import numeral from 'numeral';

export const BookingDetails = (props) => {
  let form = props.state.form;
  let locp = form.formPickupLocation.split('-');
  
  const activeCountry =  props.state[locp[0]];

  const p_location = activeCountry.Locations.find(loc => loc.id == form.formPickupLocation.split('-')[1]);
  const d_location = activeCountry.Locations.find(loc => loc.id == form.formDropoffLocation.split('-')[1]);
  const vehicle = cars? cars:[];
  const selectedCar = vehicle[form.selectedCarClass];
  let carCost = props.state.checkin.Charge.find(loc => loc['@attributes'].desc == 'DAYS');
  let days = carCost['@attributes'].quantity;
  const selectedInsurance = form.selectedCover;
  const selectedOptionalExtras = form.selectedAncillaries;
  const datetimeformat = 'YYYY-MM-DDTHH:mm:ss';
  const p_datetime = moment(form.formPickupDate + 'T' + form.formPickupTime, datetimeformat);
  const d_datetime = moment(form.formDropoffDate + 'T' + form.formDropoffTime, datetimeformat);


  // const estimate = activeCountry.Estimate.RenterEstimate;

  // const total = activeCountry.Estimate.RenterEstimate['@attributes'].total;
  // const paymentamount = numeral((form.selectedPaymentAmount == 'full')? total : (total * props.deposit)).format('0.00')
  return (
    <div className="l-summary">
      <div className="container">
          <h1 className="l-summary__heading">{props.title}</h1>
          <div className="l-summary__content" style={{margin: '0 auto 20px', width:'100%'}}>
              {props.content}
              <div id="booking_number" data-booking-number={form.bookingNo} style={{fontSize:'30px', border:'1px solid', width:'200px', margin:'20px auto 0px'}}>{form.bookingNo}</div>
              <div style={{fontSize:'14px'}}>BOOKING NUMBER</div>
          </div>


      </div>
      <div className="l-summary__container">
          <div className="l-summary__itinerary">        
            <h2 className="l-summary__heading">Itinerary Details</h2>
            <div className="l-summary__itinerary_rows">
              <div className="l-summary__itinerary-item">
                <div className="l-summary__itinerary-label">
                    Pick Up Location
                </div>
                  {p_location.location}
              </div>
              <div className="l-summary__itinerary-item">
                  <div className="l-summary__itinerary-label">
                    Pick Up Date
                  </div>
                  {p_datetime.format('DD, MMM, YYYY, hh:mma')}
              </div>
              <div className="l-summary__itinerary-arrow">
                  <ReactSVG path="/themes/base/production/svg/itinerary-arrow.svg" className='svg-itinerary-arrow'/>
              </div>
              <div className="l-summary__itinerary-item">
                  <div className="l-summary__itinerary-label">
                      Drop Off Location
                  </div>
                  {d_location.location}
              </div>
              <div className="l-summary__itinerary-item">
                  <div className="l-summary__itinerary-label">
                    Drop Off Date
                  </div>
                  {d_datetime.format('DD, MMM, YYYY, hh:mma')}
              </div>
            </div>
              <a href={"/checkin/bookinginfo"} className="btn btn--secondary l-summary__checkin-action">Change Itinerary</a>
          </div> 

          <div className="l-summary__vehicle">   
              <h2 className="l-summary__heading">Vehicle Details</h2>
              <div className="l-summary__vehicle-main">
                  <div className="l-summary__vehicle-row">
                      <div className="l-summary__vehicle-image">
                          <img src={selectedCar? selectedCar.image:''} alt={selectedCar.title} />
                      </div> 
                      <div className="l-summary__vehicle-summary">
                      <h2 className="l-summary__sub-heading"></h2>
                      <div className="l-summary__vehicle-title">{selectedCar.title}</div>
                      <div className="l-summary__vehicle-subtitle">{selectedCar.age}</div>
                      <div className="l-summary__summary-price">
                          <span className="l-summary__summary-price-math">For {days} days </span> {numeral(carCost['@attributes'].total).format('$0,0.00')}
                      </div>

                      {/*selectedCar.totaldiscount != 0
                      ?
                      <div className="l-summary__summary-discount">
                          <div className="l-summary-label">{numeral(selectedCar.totaldiscount).format('$0,0.00')} discount included.</div>
                          <div className="l-summary-price">
                              <span className="l-summary-price-math">({form.formPromoCode})</span>
                          </div>
                      </div>
                      : ''
                      */} 
                      </div>
                  </div>
              </div>
              <a href={"/checkin/changevehicle"} className="btn btn--secondary l-summary__checkin-action">Change Vehicle</a>
          </div>
          <div className="l-summary__extras">
              <h2 className="l-summary__heading">Extras Details</h2>

              <div className="l-summary__main">
                  <div className="l-summary__summary-row">
                      <div className="l-summary__summary-label">{selectedInsurance['@attributes'].desc}</div>
                      <div className="l-summary__summary-price">
                          <span className="l-summary__summary-price-math">{days} x {numeral(selectedInsurance['@attributes'].rate).format('$0,0.00')} per day =</span> {numeral(selectedInsurance['@attributes'].rate * days).format('$0,0.00')}
                      </div>
                  </div>

                  {selectedOptionalExtras.map((selex, i) => (
                  <div className="l-summary__summary-row" key={i}>
                      <div className="l-summary__summary-label">{selex.qty} {selex.opt['@attributes'].desc}</div>
                      <div className="l-summary__summary-price">
                          <span className="l-summary__summary-price-math">{(selex.opt['@attributes'].rate_type == 'rental days'? selex.qty+' x ' : "")} {(selex.opt['@attributes'].rate_type == 'rental days'? days : selex.qty)} x {numeral(selex.opt['@attributes'].rate).format('$0,0.00')} {selex.opt['@attributes'].rate_type == 'rental days'? ' per day ' : ' '} =</span> 
                          {selex.opt['@attributes'].rate_type == 'rental days'? numeral(selex.qty * selex.opt['@attributes'].rate * (selex.opt['@attributes'].rate_type == 'rental days'? days : selex.qty) ).format('$0,0.00') : numeral(selex.qty * selex.opt['@attributes'].rate).format('$0,0.00') }
                      </div>
                  </div>
                      ))}
                  {/*form.country == 'au'
                      ? <div className="l-summary__summary-row">
                      <div className="l-summary__summary-label">* GST on Total Price</div>
                      <div className="l-summary__summary-price">
                          ({numeral(total * props.gst).format('$0,0.00')})
                      </div>
                  </div>
                      : ''
                 */ }
              </div>
              <a href={"/checkin/changeextras"} className="btn btn--secondary l-summary__checkin-action">Change Extras</a>

          </div>

          <div className="l-summary__personal">
              <h2 className="l-summary__heading">Personal Details</h2>
              <div className="summary__main">

                  <div className="l-summary__summary-row">
                      <div className="l-summary__summary-label">First Name</div>
                      <div id="first_name" className="l-summary__summary-first-name">
                          {form.formCustomerData.fnm}
                      </div>
                  </div>
                  <div className="l-summary__summary-row">
                      <div className="l-summary__summary-label">Last Name</div>
                      <div className="l-summary__summary-last-name">
                          {form.formCustomerData.lnm}
                      </div>
                  </div>
                  <div className="l-summary__summary-row">
                      <div className="l-summary__summary-label">Email</div>
                      <div className="l-summary__summary-email">
                          {form.formCustomerData.eml}
                      </div>
                  </div>
                  <div className="l-summary__summary-row">
                      <div className="l-summary__summary-label">Mobile</div>
                      <div id="first_name" className="l-summary__summary-mobile">
                          {form.formCustomerData.mob}
                      </div>
                  </div>
                  <div className="l-summary__summary-row">
                      <div className="l-summary__summary-label">Country</div>
                      <div id="first_name" className="l-summary__summary-contact">
                          {form.formCustomerData.cnt}
                      </div>
                  </div>
                  <div className="l-summary__summary-row">
                      <div className="l-summary__summary-label">Notes</div>
                      <div id="first_name" className="l-summary__summary-notes">
                          {form.formCustomerData.rmk}
                      </div>
                  </div>
              </div>
              <a href={"/checkin/changepersonaldetails"} className="btn btn--secondary l-summary__checkin-action">Change Personal Details</a>
          </div>

          <div className="l-summary__payment">
            <div className="summary__main">
              <h2 className="l-summary__heading">Payment Details</h2>
                <div className="l-summary__summary-total">
                  <div className="l-summary__summary-total-label">Total Cost</div>
                  <div id="booking_total" data-booking-total={numeral(form.total).format('00.00')} className="l-summary__summary-total-price">
                          {numeral(form.total).format('$0,0.00')}
                  </div>
                </div>
                <div className="l-summary__summary-total" style={{margin:'0px', paddingTop:'0px'}}>
                  <div className="l-summary__summary-total-label">Amount Paid</div>
                    <div className="l-summary__summary-total-price">
                     {/* {numeral(paymentamount).format('$0,0.00')} */}
                    </div>
                </div>
                <div className="l-summary__summary-total" style={{margin:'0px', paddingTop:'0px'}}>
                  <div className="l-summary__summary-total-label">Balance Owing</div>
                    <div className="l-summary__summary-total-price">
                     {/* {numeral(total - paymentamount).format('$0,0.00')} */}
                    </div>
                </div>
              </div>

             {/*  {amountCheck?
                  <div className="l-summary__summary-total" style={{margin:'0px', paddingTop:'0px'}}>
              <button onClick={e => props.onPayBalance(e, numeral(total - paymentamount).format('0.00'))} className="btn btn--secondary l-summary__checkin-payaction">Pay Balance Amount Now</button>
              </div>
                      : <span>Please contact support@acerentalcars.{(location.hostname.split('.').pop() == 'nz' ? 'co.nz' : 'com.au')} for Refund.</span>
              } */}
          </div>
          <div>
          <div className="l-summary__footer">
              <button type="submit" className="btn btn--secondary l-summary__checkin-action-confirm" onClick={
              e => props.onSubmit(e)}>Confirm Changes</button>
              <button type="submit" className="btn btn--secondary l-summary__checkin-action-cancel"  onClick={e => props.onCancel(e)}>Cancel Changes</button>
         </div>
        </div>
      </div>
    </div>
  )
}
