import { REHYDRATE } from 'redux-persist/constants';
import moment from 'moment';
const defaultcheckin = {
  Pickup: [],
  Return: [],
  Option: [],
  RenterAddress: [],
  RenterName: [],
  RenterID: '',
  Source: [],
  Vehicle: '',
  Notes:'',
  bookingNo:'',
  Total: '',
  Charge: {}
}

const defaultdata = {
  Locations:[],
  DriverAges:[],
  Estimate:[],
  Options:[],
  Countries:[],
  ReservationRef:'',
  ReservationNo:'',
  PaymentSaved:false,
}

const tld = location.hostname.split('.').pop();

const defaultform = {
  step: 0,
  country: tld,
  formDifferentDropOff: false,
  formPickupLocation: tld + '-' + (tld=='au'? 'AME60':'16'),
  formDropoffLocation: tld + '-' + (tld=='au'? 'AME60':'16'),
  formPickupDate: moment(new Date()).format('YYYY-MM-DD'),
  formDropoffDate:  moment(new Date()).add(1,'days').format('YYYY-MM-DD'),
  formPickupTime: '12:00:00',
  formDropoffTime: '12:00:00',
  formCategoryType: '0',
  formMinimumAge:(tld=='au'? '0':'1'),
  formPromoCode: '',
  formEmailOptin:true,
  formAgreedToTerms:'',
  formCustomerData:{
    fnm:'',
    lnm:'',
    eml:'',
    mob:'',
    cnt: (tld=='au'? 'AU':'NZ'),
    rmk:'',
  },
  selectedCarClass: '',
  selectedRateID: '',
  selectedCover: {},
  selectedAncillaries: [],
  selectedPaymentAmount:'deposit',
  showDropoff: (tld=='au'? true:false),
}

export const initialState = {
  data:{
    nz: {
      ...defaultdata,
    },
    au: {
      ...defaultdata,
    },
  },
	checkin: {...defaultcheckin},
  form:{
    ...defaultform,
  }
}


export const formReducer = (previousState = initialState, action) => {
  let newstate = { ...previousState }
  switch (action.type) {
    case 'SET_FORMSTATE':
      $.extend(true, newstate, action.payload);
      break;
    case 'REPLACE_FORMSTATE':
      $.extend(false, newstate, action.payload);
      break;
    default:
      newstate = previousState;
      break;
  }
  return newstate;
};

export const checkinReducer = (previousState = initialState, action) => {
  let newstate = { ...previousState };
  switch (action.type){
    case 'SET_STATE':
      $.extend(true, newstate, action.payload);
      break;
    default:
      newstate = previousState;
      break;
  }
  return newstate;
}


export const dataReducer = (previousState = initialState, action) => {
  let newstate = { ...previousState, nz: { ...previousState.nz }, au: { ...previousState.au } };
  switch (action.type){
    case 'SET_DATASTATE':
      if(action.payload.hasOwnProperty('nz')){
        newstate.nz = action.payload.nz;
      }
      if(action.payload.hasOwnProperty('au')){
        newstate.au = action.payload.au;
      }
      break;
    default:
      newstate = previousState;
      break;
  }
  return newstate;
}

export const rehydrateReducer = (previousState = initialState, action) => {
  switch (action.type) {
    case REHYDRATE:
      return action.payload;
      break;
    default:
      return previousState;
      break;
  }
};
