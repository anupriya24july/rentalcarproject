import React from 'react';
import superagent from 'superagent';
import jsonp from 'superagent-jsonp';
import moment from 'moment';
import pikaday from 'pikaday';

import { AceBookAPI as api } from '../../common/api/acebookapi';
import { BrowserRouter as Router } from 'react-router-dom';
import { connect } from 'react-redux';

import { RenterInformation } from '../../common/components/partials/renterinformation';
import { VehiclePanel } from '../../common/components/partials/vehiclepanel';
import { StickyBar } from '../../common/components/partials/stickybar';
import { ProgressBar } from '../../common/components/partials/progressbar';

class ChangePersonalDetails extends React.Component {

  constructor(props) {
    super(props);
    this.openModal = props.openModal;    
    this.props.setFormState({
      isprocessing: false
    });
  }

  onContinue(){  
    this.props.setFormState({
      isprocessing: true
    });
    let autocompletefix = Array.from(document.querySelectorAll('.autocompletefix'));
    const newFormState = {...this.props.state.form};
    autocompletefix.forEach((ac) => {
      newFormState.formCustomerData[ac.name] = ac.value;
    });
    this.props.setFormState(newFormState);

    if (this.props.state.form.formAgreedToTerms){
      let msg = this.validate();
      if(msg == ''){
        this.props.history.push( window.locale + '/checkin/summary');
      }else{        
      let heading="Oops! Looks like something is missing!";
      this.openModal(heading,msg,0);
      }
    }else{  
      let heading="Oops! Looks like something is missing!";
      this.openModal(heading,"Please agree to our terms and conditions to proceed.",0);
    }

  }

  onFormChange(event){
    const newFormState = {...this.props.state.form};
    switch(event.target.name){
      case 'formEmailOptin':
      case 'formAgreedToTerms':
        newFormState[event.target.name] = event.target.checked;
        break;
      default:
        newFormState.formCustomerData[event.target.name] = event.target.value;
        break;
    }

    this.props.setFormState(newFormState);
  }

  validate(){
    let error_message="";
    let reg_alphanumeric = /^[a-z0-9 ]+$/i;
    if(this.props.state.form.formCustomerData.fnm==""){
      error_message=error_message+"<br>Please Enter First Name.";
    }else{
        if(!reg_alphanumeric.test(this.props.state.form.formCustomerData.fnm)){
          error_message=error_message+"<br>Please use letters and number only for First Name.";
        }
    }
    if(this.props.state.form.formCustomerData.lnm==""){
      error_message=error_message+"<br>Please Enter Last Name.";
    }else{
        if(!reg_alphanumeric.test(this.props.state.form.formCustomerData.lnm)){
          error_message=error_message+"<br>Please use letters and number only for Last Name.";
        }
    }
    if(this.props.state.form.formCustomerData.mob==""){
      error_message=error_message+"<br>Please Enter Your Contact Number.";
    }else{
      let reg_cnumber = /[a-zA-Z]/ ;
      if(reg_cnumber.test(this.props.state.form.formCustomerData.mob)){
        error_message=error_message+"<br>Please Enter a Valid Contact Number.";
      }
    }
    if(this.props.state.form.formCustomerData.eml==""){
      error_message=error_message+"<br>Please Enter Your Email.";
    }else{
      let reg_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!reg_email.test(this.props.state.form.formCustomerData.eml)){
        error_message=error_message+"<br>Please Enter a Valid Email.";
        }
    }
    return error_message;
  }

  render() {
    return (
      <div className="l-booking">
        <VehiclePanel gst={api.gst} state={this.props.state} showtotal={true}/>
        <div className="l-booking__main">
          <div className="l-booking__container">
            <div className="l-booking__header">
              <h1 className="l-booking__heading">Make a Booking</h1>
            </div>
            <ProgressBar progress={50}/>
            <RenterInformation state={this.props.state} onFormChange={this.onFormChange.bind(this)}/>
            {this.props.state.form.isprocessing
            ? 
            <button className="l-booking__next btn" onClick={this.onContinue.bind(this)} disabled>
              {window.labels.ProcessingLabel}...
            </button>
            :
            <button className="l-booking__next btn btn--primary" onClick={this.onContinue.bind(this)}>
              {window.labels.NextStepLabel}
            </button>
          }
          </div>
        </div>
        <StickyBar gst={api.gst} showtotal={true} state={this.props.state} />
      </div>
    );
  }

  componentDidMount () {
    window.scrollTo(0, 0);
    // this.pushAnalytics();
  }
}
const mapStateToProps = (state) => {
    return {
        state: {
            nz: state.data.nz,
            au: state.data.au,
            checkin: state.checkin,
            form: state.form,
        }
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setState: (state) => {
            dispatch({
                type: "SET_STATE",
                payload: state,
            });
        },
        setFormState: (state) => {
            dispatch({
                type: "SET_FORMSTATE",
                payload: state,
            });
        },
        setDataState: (state) => {
            dispatch({
                type: "SET_DATASTATE",
                payload: state,
            });
        }
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(ChangePersonalDetails);
