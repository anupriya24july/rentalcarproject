import React from 'react';
import moment from 'moment';
import pikaday from 'pikaday';
import numeral from 'numeral';

import { AceBookAPI as api } from '../../common/api/acebookapi';
import { BrowserRouter as Router } from 'react-router-dom';
import { connect } from 'react-redux';

import { VehicleList } from '../../common/components/vehiclelist';
import { VehiclePanel } from '../../common/components/partials/vehiclepanel';
import { StickyBar } from '../../common/components/partials/stickybar';
import { ProgressBar } from '../../common/components/partials/progressbar';

class ChangeVehicle extends React.Component {
  constructor(props) {
    super(props);
    this.openModal = props.openModal;
    this.props.setFormState({
      isprocessing: false
    });
    this.country=api.defaultcountry;
  }

  componentWillMount() {
    let locp = this.props.state.form.formPickupLocation.split('-');
  }

  togglePanel(e) {
    e.preventDefault();
    window.scrollTo(0, 0);
    const panel = document.querySelector('.js-booking-panel')
    if (panel.classList.contains('is-open')) {
      // panel.classList.remove('is-open');
    } else {
      panel.classList.add('is-open');
    }
  }

  hideMoreSpecs(e) {
    let specs = e.target.parentElement.querySelectorAll('.details.active');
    if(specs.length){
      specs.forEach((spec) => {
        spec.classList.remove('active');
        spec.classList.add('hidden');
      });
    }
  }

  toggleMoreSpecs(e) {
    let specs = e.target.parentElement.querySelectorAll('.details.hidden');
    if(specs.length){
      specs.forEach((spec) => {
        spec.classList.remove('hidden');
        spec.classList.add('active');
      });
    }else{
      specs = e.target.parentElement.querySelectorAll('.details.active');
      specs.forEach((spec) => {
        spec.classList.add('hidden');
        spec.classList.remove('active');
      });
    }
  }
  refreshEstimate(){
    let form = this.props.state.form;
     // pickup location
    let locp = form.formPickupLocation.split('-');
    let ploc = this.props.state[locp[0]].Locations.find(l => l.id == locp[1] );

    // dropoff location
    let locd = form.formDropoffLocation.split('-');
    let dloc = this.props.state[locd[0]].Locations.find(l => l.id == locd[1] );

    let options = [];
    if(form.selectedCover.hasOwnProperty('@attributes')){
      options.push(form.selectedCover['@attributes'].code + ':' + 1);
    }
    form.selectedAncillaries.map(item => {
      options.push(item.opt['@attributes'].code + ':' + item.qty);
    });

    let data = {
      pl: locp[1],
      rl: locd[1],
      pd: form.formPickupDate + 'T' + form.formPickupTime,
      rd: form.formDropoffDate + 'T' + form.formDropoffTime,
      dob: moment().subtract(form.formMinimumAge, 'years').format('YYYY-MM-DD'),
      cid: 'ACEWEB',
      rid: form.selectedRateID,
      car: form.selectedCarClass,
      opt: options.join('|'),
    };
    api.getEstimate(data, this, this.redirect.bind(this));
  }

  redirect(){
    this.props.history.push('/checkin/summary');
  }

  onSelectVehicle(car, index, e) {
    e.preventDefault();
    this.props.setFormState({
      isprocessing: true
    });
    if(this.props.match.path == window.locale + '/checkin/changevehicle'){
     let selectedRate=this.props.state[this.country].Rates.filter(cars =>cars.Class == this.props.state.checkin.Vehicle)[0]['Estimate'];
     let changedRate=this.props.state[this.country].Rates[index]['Estimate'];
     if(parseInt(changedRate)>parseInt(selectedRate))
      {
        this.props.setFormState({
          selectedCarClass: car.Class,
          selectedRateID: car.RateID.trim(),
        });
        this.refreshEstimate();
      }else{        
        let msg="";
        let heading="Oops! Looks like something went wrong!";
        msg='Sorry, To degrade car please contact support@acerentalcars.' + (location.hostname.split('.').pop() == 'nz'? 'co.nz':'com.au');
        this.openModal(heading,msg,0);
        this.props.setFormState({
          isprocessing: false
        });
      }
    }
  }

  render() {
    let locp = this.props.state.form.formPickupLocation.split('-');
    if (this.props.state[locp[0]].Rates.length == 0){
      return null;
    }

    return (
      <div className="l-booking">
        <div className="l-booking__main">
          <div className="l-booking__container">
            <div className="l-booking__header">
              <h1 className="l-booking__heading">{window.labels.BookingLabel}</h1>
            </div>
            <ProgressBar progress={0}/>
            <VehicleList gst={api.gst} cta="Book" state={this.props.state} onSelectVehicle={this.onSelectVehicle.bind(this)} toggleMoreSpecs={this.toggleMoreSpecs.bind(this)} hideMoreSpecs={this.hideMoreSpecs.bind(this)}/>
          </div>
        </div>
        <StickyBar showtotal={false} state={this.props.state} togglePanel={this.togglePanel.bind(this)}/>
      </div>
    );
  }
  
  componentDidMount () {
    window.scrollTo(0, 0);
  }
}

const mapStateToProps = (state) => {
  return {
    state: {
      nz: state.data.nz,
      au: state.data.au,
      checkin: state.checkin,
      form: state.form,
    }
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setState: (state) => {
      dispatch({
        type: "SET_STATE",
        payload: state,
      });
    },
    setFormState: (state) => {
      dispatch({
        type: "SET_FORMSTATE",
        payload: state,
      });
    },
    setDataState: (state) => {
      dispatch({
        type: "SET_DATASTATE",
        payload: state,
      });
    }
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(ChangeVehicle);
