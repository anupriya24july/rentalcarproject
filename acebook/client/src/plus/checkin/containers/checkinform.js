import React from 'react';

import superagent from 'superagent';
import moment from 'moment';
import pikaday from 'pikaday';

import { AceBookAPI as api } from '../../common/api/acebookapi';
import { BrowserRouter as Router } from 'react-router-dom';
import { connect } from 'react-redux';

class CheckinForm extends React.Component {

  constructor(props) {
    super(props);

    this.country = api.defaultcountry;
    this.openModal = props.openModal;
    this.today = moment(new Date(), 'DD/MMM/YYYY').format('DD/MM/YYYY');
  }

  componentWillMount() {
    api.initBooking(this);
  }

  checkinStep() {
    if (this.props.state.checkin.length == 0) {
      alert("Error");
      window.location = "/checkin";
    } else {
      let bookingDetails = this.props.state.checkin;
      let selectedInsurance=bookingDetails.Option.filter(opt => {
          if(allOptions[opt.Code] && allOptions[opt.Code]['Group']=='cover')
          {
            opt['@attributes']=allOptions[opt.Code]
            return opt;
          }
      });
      let ancillaries=bookingDetails.Option.filter(opt => {
          if(allOptions[opt.Code] && allOptions[opt.Code]['Group']=='option')
          {
            opt['opt']={'@attributes':allOptions[opt.Code]};
            opt['qty']=opt.qty;
            return opt;
          }
      });
      let selectedAncillaries=[];
      ancillaries.map((an, i) => (
        selectedAncillaries.push({
          qty: parseInt(an['Qty']),
          opt: {
            '@attributes': {
              code: an['opt']['@attributes']['code'],
              desc: an['opt']['@attributes']['desc'],
              rate: an['opt']['@attributes']['rate'],
              rate_type: an['opt']['@attributes']['rate_type'],
              available: 'true'
              }
            }
          })
        ));
        const tld = location.hostname.split('.').pop();
        this.props.setFormState({
          step: 0,
          country: this.country,
          formDifferentDropOff: bookingDetails.Pickup.locationCode == bookingDetails.Return.locationCode ? true : false,
          formPickupLocation: tld + '-' + bookingDetails.Pickup.locationCode,
          formDropoffLocation: tld + '-' + bookingDetails.Return.locationCode,
          formPickupDate: moment(bookingDetails.Pickup.dateTime, 'YYYY-MM-DDTHH:mm:ss').format('YYYY-MM-DD'),
          formDropoffDate: moment(bookingDetails.Return.dateTime, 'YYYY-MM-DDTHH:mm:ss').format('YYYY-MM-DD'),
          formPickupTime: moment(bookingDetails.Pickup.dateTime, 'YYYY-MM-DDTHH:mm:ss').format('HH:mm:ss'),
          formDropoffTime: moment(bookingDetails.Return.dateTime, 'YYYY-MM-DDTHH:mm:ss').format('HH:mm:ss'),
          formCategoryType: '0',
          formMinimumAge:'21',
          formPromoCode: '',
          formEmailOptin:true,
          formAgreedToTerms:'',
          formCustomerData:{
            fnm: bookingDetails.RenterName.firstName,
            lnm: bookingDetails.RenterName.lastName,
            eml: bookingDetails.RenterAddress.Email,
            mob: bookingDetails.RenterAddress.CellTelephoneNumber,
            cnt: (tld=='au'? 'AU':'NZ'),
            rmk: bookingDetails.Notes,
          },
          selectedCarClass: bookingDetails.Vehicle,
          selectedRateID: '',
          selectedCover: {
            '@attributes': {
              code: selectedInsurance[0]['@attributes']['code'],
              desc: selectedInsurance[0]['@attributes']['desc'],
              rate: selectedInsurance[0]['@attributes']['rate'],
              rate_type: selectedInsurance[0]['@attributes']['rate_type'],
              liability: selectedInsurance[0]['@attributes']['liability'],
              available: selectedInsurance[0]['@attributes']['available'],
              available: 'true',
              maximum_days: '15'
            }
          },
          selectedAncillaries: selectedAncillaries,
          selectedPaymentAmount:'branch',
          showDropoff: (tld=='au'? true:false),
          bookingNo:this.props.state.checkin.bookingNo,
          customerNo:bookingDetails.RenterID,
          total: bookingDetails.Total,
          isAmendment: true,
        });

      this.props.setFormState({
        isprocessing: false
      });
      this.props.history.push('/checkin/summary');
    }
  }
  getEstimate() {
    if(this.props.state.checkin.Vehicle){
      let data = {
        resno: this.props.state.checkin.bookingNo
      };
      api.retrieveEstimate(data, this, this.checkinStep.bind(this));
    }else{         
      this.props.setFormState({
        isprocessing: false
      });
    }
  }
  onSubmit() {   
    this.props.setFormState({
      isprocessing: true
    });
    let data = {
      resno: this.props.state.checkin.bookingNo
    };
    api.retrieveReservation(data, this, this.getEstimate.bind(this)); 
  }

  onFormChange(event) {
    this.props.setState({
      bookingNo: event.target.value
    });
  }

  render() {
    return (
      <div className="l-arrow-container">
        <div className="l-page">
          <div className="l-page__container">
            <h1 className="l-page__heading">  Booking Amendment</h1>
            <div className="l-page__content">
              <div className="l-form js-checkin-form" >      
                <div className="l-form__fields-checkin-form">
                  <div className="l-form__field-checkin-form  ">
                    <label htmlFor="bookingNo" className="l-form__label-checkin-form">
                      Booking Number
                    </label>
                    <input className="l-summary-checkin-form" type="text" name="bookingNo" id="bookingNo" onChange={this.onFormChange.bind(this)}/>
                  </div>
                </div>
              
              <div className="l-form__action-checkin-form">
              {this.props.state.form.isprocessing
                ? <button type="submit" className="l-booking-form__submit btn" onClick={e => this.onSubmit(e)} disabled>
                  {window.labels.ProcessingLabel}...
                  </button>
                :
                  <button type="submit" className="l-booking-form__submit " onClick={e => this.onSubmit(e)} className="l-booking-form__submit btn btn--secondary" >
                  GO
                  </button>
              }
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      );
  }
}


const mapStateToProps = (state) => {
  return {
    state: {
      nz: state.data.nz,
      au: state.data.au,
      checkin: state.checkin,
      form: state.form,
    }
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setState: (state) => {
      dispatch({
        type: "SET_STATE",
        payload: state,
      });
    },
    setFormState: (state) => {
      dispatch({
        type: "SET_FORMSTATE",
        payload: state,
      });
    },
    setDataState: (state) => {
      dispatch({
        type: "SET_DATASTATE",
        payload: state,
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CheckinForm);
