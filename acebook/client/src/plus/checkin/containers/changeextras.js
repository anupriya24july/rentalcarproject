import React from 'react';
import superagent from 'superagent';
import jsonp from 'superagent-jsonp';
import moment from 'moment';
import pikaday from 'pikaday';

import { AceBookAPI as api } from '../../common/api/acebookapi';
import { BrowserRouter as Router } from 'react-router-dom'; 
import { connect } from 'react-redux';

import { Extras } from '../../common/components/extras';
import { VehiclePanel } from '../../common/components/partials/vehiclepanel';
import { StickyBar } from '../../common/components/partials/stickybar';
import { ProgressBar } from '../../common/components/partials/progressbar';

class AddExtras extends React.Component {

  constructor(props) {
    super(props);
    this.total = 0;
    this.handleScroll = this.handleScroll.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.openModal = props.openModal;
    this.props.setFormState({
      isprocessing: false
    });
   }

  componentWillMount() {
    if(this.props.match.path == window.locale + '/checkin/changeextras'){
     
      this.refreshEstimate();
      this.refreshOptions();
    }
  }

  refreshEstimate(){
    let form = this.props.state.form;
     // pickup location
    let locp = form.formPickupLocation.split('-');
    let ploc = this.props.state[locp[0]].Locations.find(l => l.id == locp[1] );

    // dropoff location
    let locd = form.formDropoffLocation.split('-');
    let dloc = this.props.state[locd[0]].Locations.find(l => l.id == locd[1] );

    let options = [];
    if(form.selectedCover.hasOwnProperty('@attributes')){
      options.push(form.selectedCover['@attributes'].code + ':' + 1);
    }
    form.selectedAncillaries.map(item => {
      options.push(item.opt['@attributes'].code + ':' + item.qty);
    });

    let data = {
      pl: locp[1],
      rl: locd[1],
      pd: form.formPickupDate + 'T' + form.formPickupTime,
      rd: form.formDropoffDate + 'T' + form.formDropoffTime,
      dob: moment().subtract(form.formMinimumAge, 'years').format('YYYY-MM-DD'),
      cid: 'ACEWEB',
      rid: form.selectedRateID,
      car: form.selectedCarClass,
      opt: options.join('|'),
    };
    api.getEstimate(data, this, this.displayEstimate.bind(this));
  }

  refreshOptions(){
    let form = this.props.state.form;
     // pickup location
    let locp = form.formPickupLocation.split('-');
    let ploc = this.props.state[locp[0]].Locations.find(l => l.id == locp[1] );

    // dropoff location
    let locd = form.formDropoffLocation.split('-');
    let dloc = this.props.state[locd[0]].Locations.find(l => l.id == locd[1] );

    let data = {
      pl: locp[1],
      rl: locd[1],
      pd: form.formPickupDate + 'T' + form.formPickupTime,
      rd: form.formDropoffDate + 'T' + form.formDropoffTime,
      dob: moment().subtract(form.formMinimumAge, 'years').format('YYYY-MM-DD'),
      cid: 'ACEWEB',
      rid: form.selectedRateID,
      car: form.selectedCarClass,
    };
    api.getOptions(data, this, this.displayOptions.bind(this));
  }

  displayEstimate(){

  }

  displayOptions(){

  }

  toggleMoreSpecs(e) {
    let specs = e.target.parentElement.parentElement.querySelectorAll('.toggle-active');
    if(specs.length){
      specs.forEach((spec) => {
        spec.classList.remove('toggle-active');
        spec.classList.add('toggle-hidden');
      });
    }else{
      specs = e.target.parentElement.parentElement.querySelectorAll('.toggle-hidden');
      specs.forEach((spec) => {
        spec.classList.add('toggle-active');
        spec.classList.remove('toggle-hidden');
      });
    }
  }

  initStep(){
    let locp = this.props.state.form.formPickupLocation.split('-');
    let age = this.props.state[locp[0]].rcmDriverAgesInfo.find(age => age.id == this.props.state.form.formMinimumAge);
    const activeCountry =  this.props.state[locp[0]];
    
    // load default insurance if none is selected.
    if(api.defaultcountry == 'au'){
      if(this.props.state.form.selectedInsuranceId == '' || activeCountry.rcmInsuranceOptions.find(ins => ins.id == this.props.state.form.selectedInsuranceId) == undefined){
        // let selectedInsurance = activeCountry.rcmInsuranceOptions.find(ins => ins.fees == 0 && ins.fromage >= age.driverage);
        let selectedInsurance = activeCountry.rcmInsuranceOptions.sort((a,b)=>{return +a.fees - +b.fees});
        this.props.setFormState({
          selectedInsuranceId: selectedInsurance[0].id,
        });
      }
    }

    // remove optional extras that are not compatible with the current vehicle.
    let newSelEx = this.props.state.form.selectedOptionalExtras;
    this.props.state.form.selectedOptionalExtras.map((selex,i) => {
      let thisex = activeCountry.rcmOptionalFees.find(ex => selex.ex.id == ex.id);
      if(!thisex){
        newSelEx.splice(newSelEx.indexOf(selex),1);
      }

      // refresh aceplus discounts if there is any.
      if(selex.ex.sgroupname.match(/ace plus discount/ig)){
        newSelEx.splice(newSelEx.indexOf(selex),1);
      }
    });

    this.onSelectInsurance(this.props.state.form.selectedInsuranceId);

    this.props.setFormState({
      selectedOptionalExtras: newSelEx,
    });

    this.pushAnalytics();
  }

  onContinue(){
    this.props.setFormState({
      isprocessing: true
    });
    if(this.props.state.form.selectedInsuranceId == '' && api.defaultcountry == 'nz'){      
      let heading="Oops! Looks like something is missing!";
      let msg="Please choose an insurance option.";
      this.openModal(heading,msg,0);
    }else{
      this.props.setFormState({
        total: this.total,
      });
      window.location= window.locale + '/checkin/summary';
    }
  }

  onSelectCover(cover, e){
    let locp = this.props.state.form.formPickupLocation.split('-');
    const activeCountry =  this.props.state[locp[0]];

    let ancillaries = this.props.state.form.selectedAncillaries;
    const coveroptions = activeCountry.Options.Option.filter(opt => opt['@attributes'].hasOwnProperty('liability') && opt['@attributes'].available == 'true');

    this.props.setFormState({
      selectedCover: cover,
    });

    this.refreshEstimate();
  }

  onSelectAncillaries(opt, e){
    e.stopPropagation();
    let locp = this.props.state.form.formPickupLocation.split('-');
    let selectedAncillaries = this.props.state.form.selectedAncillaries;

    let thisOption = selectedAncillaries.find( item => item.opt['@attributes'].code == opt['@attributes'].code);
    if(thisOption){
      selectedAncillaries.splice(selectedAncillaries.indexOf(thisOption),1);
    }else{
      selectedAncillaries.push({
        qty: parseInt(e.target.querySelector('select')? e.target.querySelector('select').value: 1),
        opt: opt,
      })
    }
    this.props.setFormState({
      selectedAncillaries: selectedAncillaries,
    });

    this.refreshEstimate();
  }

  togglePanel(e) {
    e.preventDefault();
    window.scrollTo(0, 0);
    const panel = document.querySelector('.js-booking-panel')
    if (panel.classList.contains('is-open')) {
      //panel.classList.remove('is-open');
    } else {
      panel.classList.add('is-open');
    }
  }

  onQtyChange(e, opt){
    let selectedAncillaries = this.props.state.form.selectedAncillaries;
    selectedAncillaries.map( item =>{
      if(item.opt['@attributes'].code == opt['@attributes'].code){
        item.qty = +e.target.value;
      }
    });
    this.props.setFormState({
      selectedAncillaries: selectedAncillaries,
    });

    this.refreshEstimate();
  }

  render() {
    let locp = this.props.state.form.formPickupLocation.split('-');
    return (
      <div className="l-booking">
        <VehiclePanel gst={api.gst} state={this.props.state} showtotal={true}/>
        <div className="l-booking__main">
          <div className="l-booking__container">
            <div className="l-booking__header">
              <h1 className="l-booking__heading">{window.labels.BookingLabel}</h1>
            </div>
            <ProgressBar progress={25}/>
            <Extras state={this.props.state} onSelectCover={this.onSelectCover.bind(this)} onSelectAncillaries={this.onSelectAncillaries.bind(this)} onContinue={this.onContinue.bind(this)} toggleMoreSpecs={this.toggleMoreSpecs.bind(this)} onQtyChange={this.onQtyChange.bind(this)}/>
            { this.props.state.form.isprocessing
                ? <button className="l-booking__next btn" onClick={this.onContinue.bind(this)} disabled>
                    {window.labels.ProcessingLabel}...
                  </button>
                : <button className="l-booking__next btn btn--primary" onClick={this.onContinue.bind(this)}>
                    {window.labels.NextStepLabel}
                  </button>
            }
          </div>
        </div>
        <StickyBar gst={api.gst} showtotal={true} state={this.props.state} togglePanel={this.togglePanel.bind(this)} updateTotal={this.updateTotal.bind(this)}/>
      </div>
    );
  }

  updateTotal(total){
    // -
    this.total = total;
  }
  
  handleClick(event){
    if(event.target.classList.contains("leadin-button")){
        let acePlus= this.props.state[api.defaultcountry].rcmInsuranceOptions.find(ins => ins.default  == 'True');
            this.props.setFormState({
            selectedInsuranceId: acePlus.id
            });                
        document.getElementsByClassName('l-booking__options')[0].scrollIntoView();
    }
    document.getElementsByClassName("leadinModal-form")[0].style.display = "none";    
  }

  handleScroll() {
    var isPopUp= document.getElementsByClassName("leadinModal-preview")[0] ? true :false;
    var isAcePlus=false;
    if(isPopUp){  
      document.getElementsByClassName("leadin-preview-wrapper")[0].addEventListener('click',this.handleClick);
      if(this.props.state.form.selectedInsuranceId == '' && api.defaultcountry == 'nz'){
        document.getElementsByClassName("leadinModal-preview")[0].style.display = "none";
        document.getElementsByClassName("leadin-preview-wrapper")[0].removeEventListener('click', this.handleClick);
      }else{
        var isMin=  document.getElementsByClassName("pos2")[0].classList.contains("is-selected") ? true :false;
        if (isMin) {
          document.getElementsByClassName("leadinModal-preview")[0].style.display = "block";    
        } else {
          document.getElementsByClassName("leadinModal-preview")[0].style.display = "none";
          document.getElementsByClassName("leadin-preview-wrapper")[0].removeEventListener('click', this.handleClick);
        }
      }    
    }

    document.getElementsByClassName("leadinModal-form")[0] ?document.getElementsByClassName("leadinModal-form")[0].style.display = "none":"";   
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    window.addEventListener('scroll', this.handleScroll,this.props.state);
   
  }
} // class end


const mapStateToProps = (state) => {
  return {
    state: {
      nz: state.data.nz,
      au: state.data.au,
      form: state.form,
    }
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    replaceFormState: (state) =>{
      dispatch({
        type: "REPLACE_FORMSTATE",
        payload: state, 
      });
    },
    setFormState: (state) =>{
      dispatch({
        type: "SET_FORMSTATE",
        payload: state, 
      });
    },
    setDataState: (state) =>{
      dispatch({
        type: "SET_DATASTATE",
        payload: state, 
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddExtras);
