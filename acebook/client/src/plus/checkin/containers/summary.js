import React from 'react';
import superagent from 'superagent';
import jsonp from 'superagent-jsonp';
import moment from 'moment';
import pikaday from 'pikaday';

import { AceBookAPI as api } from '../../common/api/acebookapi';
import { BrowserRouter as Router } from 'react-router-dom';
import { connect } from 'react-redux';
import base64 from 'base-64';

import { BookingDetails } from '../components/bookingdetails';
class Summary extends React.Component {
  constructor(props) {
    super(props);
    this.country = api.defaultcountry;
    this.openModal = props.openModal;
    if(!props.state.form.bookingNo){
    window.location = "/checkin"
    }
  }

  togglePanel(e) {
    e.preventDefault();
    window.scrollTo(0, 0);
    const panel = document.querySelector('.js-booking-panel')
    if (panel.classList.contains('is-open')) {
      //panel.classList.remove('is-open');
    } else {
      panel.classList.add('is-open');
    }
  }

  componentWillMount() {
    api.initBooking(this);
  }

  onSubmit() {
    let mandatoryFees = this.props.state[api.defaultcountry].rcmMandatoryFees;      
    this.props.setFormState({
      mandatoryFees: mandatoryFees,
    });
    amendmentapi.amendBooking(amendmentapi.getEndPoint(this.props.state.form.country),JSON.stringify(this.props.state.form));
  } 

  onPayBalance(e, amount) { 
    this.props.setFormState({
      balance: amount,
    });
    this.props.history.push('/checkin/paymentoption');
  } 

  onCancel() {
    let msg="";
    let heading="Just to let you know...";
    msg='Please confirm that you want to cancel the changes as the changes made will be lost.';
    this.openModal(heading,msg,2);
  } 

  render() {
    return (
      <BookingDetails state={this.props.state} title="Booking Summary" content="" onSubmit={e => this.onSubmit(e)} onPayBalance={this.onPayBalance.bind(this)}  onCancel={e => this.onCancel(e)} />
    );
  }

  displayOptions(){

  }

  refreshEstimate(){
    let form = this.props.state.form;
     // pickup location
    let locp = form.formPickupLocation.split('-');
    let ploc = this.props.state[locp[0]].Locations.find(l => l.id == locp[1] );

    // dropoff location
    let locd = form.formDropoffLocation.split('-');
    let dloc = this.props.state[locd[0]].Locations.find(l => l.id == locd[1] );

    let selectedCarRate=this.props.state[locp[0]].Rates.filter(rate => rate.Class==form.selectedCarClass)[0];
    this.props.setFormState({
      selectedRateID: selectedCarRate.RateID.trim(),
    });

    let options = [];
    if(form.selectedCover.hasOwnProperty('@attributes')){
      options.push(form.selectedCover['@attributes'].code + ':' + 1);
    }
    form.selectedAncillaries.map(item => {
      options.push(item.opt['@attributes'].code + ':' + item.qty);
    });

    let data = {
      pl: locp[1],
      rl: locd[1],
      pd: form.formPickupDate + 'T' + form.formPickupTime,
      rd: form.formDropoffDate + 'T' + form.formDropoffTime,
      dob: moment().subtract(form.formMinimumAge, 'years').format('YYYY-MM-DD'),
      cid: 'ACEWEB',
      rid: selectedCarRate.RateID.trim(),
      car: form.selectedCarClass,
      opt: options.join('|'),
    };

    api.getEstimate(data, this, this.refreshOptions.bind(this));
  }

  refreshOptions(){
    let form = this.props.state.form;
     // pickup location
    let locp = form.formPickupLocation.split('-');
    let ploc = this.props.state[locp[0]].Locations.find(l => l.id == locp[1] );

    // dropoff location
    let locd = form.formDropoffLocation.split('-');
    let dloc = this.props.state[locd[0]].Locations.find(l => l.id == locd[1] );
    let data = {
      pl: locp[1],
      rl: locd[1],
      pd: form.formPickupDate + 'T' + form.formPickupTime,
      rd: form.formDropoffDate + 'T' + form.formDropoffTime,
      dob: moment().subtract(form.formMinimumAge, 'years').format('YYYY-MM-DD'),
      cid: 'ACEWEB',
      rid: form.selectedRateID,
      car: form.selectedCarClass,
    };
    api.getOptions(data, this, this.displayOptions.bind(this));
  }

  componentDidMount () {    
      // pickup location
      let locp = this.props.state.form.formPickupLocation.split('-');
      let ploc = this.props.state[locp[0]].Locations.find(l => l.id == locp[1]);

      // dropoff location
      let locd = this.props.state.form.formDropoffLocation.split('-');
      let dloc = this.props.state[locd[0]].Locations.find(l => l.id == locd[1]);

      let data = {
        pl: locp[1],
        rl: locd[1],
        pd: this.props.state.form.formPickupDate + 'T' + this.props.state.form.formPickupTime,
        rd: this.props.state.form.formDropoffDate + 'T' + this.props.state.form.formDropoffTime,
        dob: moment().subtract(this.props.state.form.formMinimumAge, 'years').format('YYYY-MM-DD'),
        cid: 'ACEWEB',
        et: 3,
        pc: this.props.state.form.formPromoCode == '' ? '-': this.props.state.form.formPromoCode
      };
      api.getRates(data, this, this.refreshEstimate.bind(this));
  }
}


const mapStateToProps = (state) => {
  return {
    state: {
      nz: state.data.nz,
      au: state.data.au,
      checkin: state.checkin,
      form: state.form,
    }
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setState: (state) => {
      dispatch({
        type: "SET_STATE",
        payload: state,
      });
    },
    setFormState: (state) => {
      dispatch({
        type: "SET_FORMSTATE",
        payload: state,
      });
    },
    setDataState: (state) => {
      dispatch({
        type: "SET_DATASTATE",
        payload: state,
      });
    }
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(Summary);
