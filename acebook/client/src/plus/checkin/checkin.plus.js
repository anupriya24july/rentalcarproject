import Author from "./components/author";
import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore, combineReducers, compose } from "redux";
import { persistStore, autoRehydrate } from 'redux-persist';
import { checkinReducer as checkin, formReducer as form, dataReducer as data, rehydrateReducer as rehydrate, initialState } from "./reducer/checkinReducer";
import CheckinForm from './containers/checkinform';
import BookingInfo from './containers/bookinginfo';
import ChangeVehicle from './containers/changevehicle';
import ChangeExtras from './containers/changeextras';
import ChangePersonalDetails from './containers/changepersonaldetails';
import PaymentOption from './containers/paymentoption';
import Summary from './containers/summary';

const routes = [
    {path: window.locale + '/checkin',
        exact: true,
        step: CheckinForm,
    },
    {path: window.locale + '/checkin/bookinginfo',
        exact: true,
        step: BookingInfo,
    },
    {path: window.locale + '/checkin/changevehicle',
        exact: true,
        step: ChangeVehicle,
    },
    {path: window.locale + '/checkin/changeextras',
        exact: true,
        step: ChangeExtras,
    },
    {path: window.locale + '/checkin/changepersonaldetails',
        exact: true,
        step: ChangePersonalDetails,
    },
    {path: window.locale + '/checkin/paymentoption',
        exact: true,
        step: PaymentOption,
    },
    {path: window.locale + '/checkin/summary',
        exact: true,
        step: Summary,
    }
]
if (!String.prototype.endsWith) {
    String.prototype.endsWith = function (suffix) {
        return this.indexOf(suffix, this.length - suffix.length) !== -1;
    };
}
let enhancer = {};
if (location.hostname.split('.').reverse().pop() == 'dev' && (/Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor))) {
    enhancer = compose(
            autoRehydrate({
                log: true
            }),
            window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
            );
} else {
    enhancer = compose(
            autoRehydrate({
                log: false
            }),
            );
}

const config = {
    whitelist: ['checkin', 'data', 'form'],
};

const onPersist = () => {
  if(document.getElementById('acebook-app') !== null){
    render( 
      <Provider store={store}>
        <Router>
          <div>
            {routes.map((route, index) => (<Route key={index} path={route.path} exact={route.exact}  render={ props  => <route.step openModal={openModal} {...props}   />} openModal={openModal}  /> ))}
          </div>
        </Router>
      </Provider>, document.getElementById('acebook-app')
    );
  }
}

const store = createStore(
        combineReducers({
            checkin,
            form,
            data,
            rehydrate
        }),
        initialState,
        enhancer
        );

if (location.pathname == window.locale + '/checkin') {
    persistStore(store, config, onPersist).purge();
} else {
    persistStore(store, config, onPersist);
}

function continueRedirect(redirectUrl) {
    let confirm = document.querySelector('.js-modal-confirm');
    confirm.addEventListener('click', () => {
      window.location = redirectUrl;
    });
}

function openModal(heading,msg,mode) {
    if(mode == 1){
        document.querySelector('.modal-message-title').innerHTML = 'Our Cover Options';
        document.querySelector('.modal-message-content').classList.add('modal-iframe-wrapper');
        document.querySelector('.modal-message-content').innerHTML = '<iframe class="modal-iframe" src="https://www.youtube.com/embed/HB1XlOBIT6c?autoplay=1" frameborder="0" allowfullscreen></iframe>';
    } else if (mode == 2) {
        document.querySelector('.modal-message-title').innerHTML=heading;
        document.querySelector('.modal-message-content').innerHTML=msg;  
        document.querySelector('.modal-footer').classList.remove('is-hidden');
    } else {
        document.querySelector('.modal-message-title').innerHTML=heading;
        document.querySelector('.modal-message-content').innerHTML=msg;  
    }
  document.querySelector('.js-modal').classList.remove('is-hidden');
  continueRedirect(window.locale + '/checkin');
}
