<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$hubspotKey = "1dc0066c-d4ea-4f15-9722-3a99ce3aef33";
$con = mysqli_connect("ace-sql01.c6pcciajngkr.ap-southeast-2.rds.amazonaws.com:3306", "acesql01master", "nIhGc7jm4vvsWabcjySujNaV_bLkdbD8", "ace_booking_management");
if (!$con) {
    die("Connection failed: " . mysqli_connect_error());
}
$select = "SELECT * from hubspot_deals WHERE HubspotID IS NULL ";
$start = 0;
$range = 2000;
$select .= " LIMIT $start, $range";
$query = mysqli_query($con, $select);
$contactUrl = 'https://api.hubapi.com/contacts/v1/contact/?hapikey=' . $hubspotKey;
$dealUrl = 'https://api.hubapi.com/deals/v1/deal?hapikey=' . $hubspotKey;
$importData = [];
$i = 0;
if (!empty($query)) {
    while ($result = mysqli_fetch_array($query)) {
        $importData[] = $result;
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    foreach ($importData as $row) {
        echo "Importing : " . $row['ID'];
        $contact = array(
            array("property" => "email", "value" => $row['CustEmail']),
            array("property" => "firstname", "value" => $row['CustFname']),
            array("property" => "lastname", "value" => $row['CustLname']),
            array("property" => "company", "value" => $row['CustCompany']),
            array("property" => "address", "value" => $row['CustAddress']),
            array("property" => "city", "value" => $row['CustCity']),
            array("property" => "state", "value" => $row['CustState']),
            array("property" => "zip", "value" => $row['CustPostcode']),
            array("property" => "phone", "value" => $row['CustPhone']),
            array("property" => "mobilephone", "value" => $row['CustMobile']),
            array("property" => "date_of_birth", "value" => $row['CustDOB']),
            array("property" => "country", "value" => $row['Country']),
        );
        $contactData = '{"properties": ' . json_encode($contact) . '}';
        curl_setopt($ch, CURLOPT_URL, $contactUrl);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $contactData);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($contactData))
        );
        $response = curl_exec($ch);
        if (!curl_errno($ch)) {
            $vid = NULL;
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $contactResponse = json_decode($response);
            if ($httpcode == 200) {
                $vid = $contactResponse->vid;
            } else if ($httpcode == 409) {
                $vid = $contactResponse->identityProfile->vid;
            }
            if ($vid) {
                $selectedIns = $selectedExtras = '';
                $selectedExtrasArray = [];
                if ($row['Extras'] != 'NULL') {
                    $extras = explode(';', $row['Extras']);
                    foreach ($extras as $extra) {
                        $extraName = explode('_', $extra);
                        if ($extraName[1] == '1') {
                            $selectedIns = $extraName[0];
                        } else {
                            $selectedExtrasArray[] = $extraName[0];
                        }
                    }
                    if (!empty($selectedExtrasArray)) {
                        $selectedExtras = implode(';', $selectedExtrasArray);
                    }
                }
                $dropoffdate = (strtotime($row['DropoffDate'])) * 1000;
                $pickupdate = (strtotime($row['PickupDate'])) * 1000;
                $deal = array(
                    array("name" => "dealname", "value" => $row['BookingNo'] . '-' . $row['PickupLoc']),
                    array("name" => "bookingnumber", "value" => $row['BookingNo']),
                    array("name" => "bookingreferencenumber", "value" => $row['BookingRef']),
                    array("name" => "country", "value" => $row['Country']),
                    array("name" => "dropoffdate", "value" => $dropoffdate),
                    array("name" => "dropofftime", "value" => $row['DropoffTime']),
                    array("name" => "dropofflocation", "value" => $row['DropoffLoc']),
                    array("name" => "pickupdate", "value" => $pickupdate),
                    array("name" => "pickuptime", "value" => $row['PickupTime']),
                    array("name" => "pickuplocation", "value" => $row['PickupLoc']),
                    array("name" => "paymentsaved", "value" => $row['Revenue']),
                    array("name" => "promocode", "value" => $row['Promocode']),
                    array("name" => "remarks", "value" => $row['Remarks']),
                    array("name" => "selectedcar", "value" => $row['SelectedCar']),
                    array("name" => "reservation_status", "value" => $row['ReservationStatus']),
                    array("name" => "hire_days", "value" => $row['HireDays']),
                    array("name" => "rental_source", "value" => $row['RentalSource']),
                    array("name" => "selectedinsurance", "value" => $selectedIns),
                    array("name" => "selectedextras", "value" => $selectedExtras),
                );
                $dealData = '{"associations":{ 
                                    "associatedVids": [' . $vid . ']},
                                     "properties": ' . json_encode($deal) . '}';
                curl_setopt($ch, CURLOPT_URL, $dealUrl);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $dealData);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($dealData))
                );
                $response = curl_exec($ch);
                if (!curl_errno($ch)) {
                    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    $dealResponse = json_decode($response);
                    if ($httpcode == 200) {
                        $dealId = $dealResponse->dealId;
                        $query = mysqli_query($con, "UPDATE hubspot_deals SET HubspotID=" . $dealId . " WHERE  ID=" . $row['ID']);
                        echo 'Imported ID: ' . $row['ID'] . '<br>';
                    } else {
                        print_r($dealResponse);
                    }
                } else {
                    echo 'Curl error: ' . curl_error($ch) . '->' . $row['ID'] . '<br>';
                }
            }
        } else {
            echo 'Curl error: ' . curl_error($ch) . '->' . $row['ID'] . '<br>';
        }
    }
    curl_close($ch);
}
mysqli_close($con);
?>